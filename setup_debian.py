#!/usr/bin/env python
# To use:
#       python setup.py install
#
import os, sys, string, re
from glob import glob
    
import distutils

#try:
import distutils
from distutils.command.install import install
from distutils.core import setup, Extension
# except:
#    raise SystemExit, "Distutils problem, see Numeric README."

headers = glob (os.path.join ("Include","*.h") )
header = headers + glob (os.path.join ("Include/Numeric","*.h") ) 

# The version is set in Lib/numeric_version.py
libraries_spf=[  "Hxx_Sparsa",
                                             "gsl",
                                             "gslcblas",
                                             "stdc++",
                                             "gcc",
                                             "m"]



liblapackg77=[  "lapack", "blasg77", "g2c",]
liblapackg77= ["lapack","goto_opteron64p-r1.00"]
liblapackg77= ["lapack","blas" ,"atlas"]

librariesmkl=["mkl_lapack","mkl_ia32","guide" ]


libraries_mpi=[   "mpich" ,"rt" ]
libraries_mpi=[ "mpi_cxx",  "mpi"  ,"mca_common_sm","open-pal" ]

libraries_sparsa=[  "Hxx_Sparsa",]+libraries_mpi+liblapackg77+["stdc++",
                                             "gcc",
                                             "m"]
libraries_sparsa3AP=[  "Hxx_Sparsa",]+libraries_mpi+liblapackg77+["stdc++",
                                             "gcc",
                                             "m"]

libraries_mpilight=libraries_mpi+["stdc++", "gcc",
                                             "m"]

setup (name = "extension",
       description = " Extension to Python",
       ext_modules = [Extension('_Hilbertxxmodule',
                                ['Hilbertxx_wrap.cxx' ],
                                libraries=["Hilbertxx" , 
                                            "gsl",
                                             "gslcblas",
                                           "stdc++",
                                           "gcc",]+
                                ["m"],
                                
                                include_dirs  = ['./','/usr/include/mpi/'],
                                library_dirs  = ['.libs/']),
                      
	              Extension('_Sparsa',	
                                ['Sparsa_wrap.cxx' ],
                                libraries=libraries_sparsa,
                                include_dirs  = ['./','/usr/include/mpi/'],
                                library_dirs  = ['.libs/']),
                      
	              Extension('_Sparsa3AP',	
                                ['Sparsa3AP_wrap.cxx' ],
                                libraries=libraries_sparsa3AP, 
                                include_dirs  = ['./','/usr/include/mpi/'],
                                library_dirs  = ['.libs/']),		
                      
	              Extension('_mpilight',	
                                ['mpilight_wrap.cxx', "mpilight.cc" ],
                                libraries=libraries_mpilight, 
                                include_dirs  = ['./','/usr/include/mpi/'],
                               ),		

	              Extension('_preload',	
                                ['preload_wrap.cxx', "preload.cc" ],
                                libraries=["stdc++", "gcc"], 
                                include_dirs  = ['./','/usr/include/mpi/'],
                               ),		

                     ]	
       )	
	







