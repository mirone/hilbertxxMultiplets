#!/usr/bin env python
# -*- coding: utf-8 -*-

#/*##########################################################################
# Copyright (C) 2001-2013 European Synchrotron Radiation Facility
#
#              PyHST2  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# PyHST2 is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for PyHST2: Alessandro Mirone.
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PyHST2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PyHST2; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PyHST2 follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/

"""
Installation script: 

python setup.py build

"""
__author__ = "Alessandro Mirone"
__contact__ = "mirone@ESRF.fr"
__license__ = "2002"
__copyright__ = "European Synchrotron Radiation Facility, Grenoble, France"
__date__ = ""
__satus__ = "development"

import sys, os
import glob
from distutils.core import Extension, setup
# from distutils.command.build_ext import build_ext
from numpy.distutils.misc_util import get_numpy_include_dirs
from os.path import join as pjoin
import string
import stat 

global version
global aumento_versione
aumento_versione=""



global version
version = [eval(l.split("=")[1]) for l in open(os.path.join(os.path.dirname(
          os.path.abspath(__file__)), "hilbertxx", "__init__.py"))
           if l.strip().startswith("version")][0]


def do_Hilbertxx():
    global version
    try:
        import numpy
    except ImportError:
        text = "You must have numpy installed.\n"
        text += "See http://sourceforge.net/project/showfiles.php?group_id=1369&package_id=175103\n"
        raise( ImportError, text)

    jn = os.sep.join

    # mpi_dirs = ["/usr/include/openmpi/"]
    # hdf5_dirs = ["/usr/include/hdf5/serial/"]
    # hdf5_lib  = "hdf5_serial"
    # # hdf5_dirs = []
    # # hdf5_lib  = "hdf5"
    
    cmdclass = {}
    ext_modules = []
    internal_clibraries = []

    packages = ['Hilbertxx'+"_v"+version,'Hilbertxx'+"_v"+version+'.exa_2p3d']

    package_dir = {'Hilbertxx'+"_v"+version: 'hilbertxx', 'Hilbertxx'+"_v"+version+'.exa_2p3d':'examples/exa_2p3d'    }
    # HILBERTXX_DATA_DIR = pjoin( 'Hilbertxx'+"_v"+version ,      'DATA'  )
    HILBERTXX_DIR =  'Hilbertxx'+"_v"+version 
    
    data_files = [( HILBERTXX_DIR+'/exa_2p3d/DATA/Mn3+/',  glob.glob('examples/exa_2p3d/DATA/Mn3+/*'))
              ]
    
    if sys.platform == "win32":
        raise RuntimeError("win32 to be tested ")
    else:
        define_macros = []
        script_files = glob.glob('scripts/*')

    def find_in_path(name, path):
        "Find a file in a search path"
        #adapted fom http://code.activestate.com/recipes/52224-find-a-file-given-a-search-path/
        for dirn in path.split(os.pathsep):
            binpath = pjoin(dirn, name)
            if os.path.exists(binpath):
                return os.path.abspath(binpath)
        return None

    # def locate_cuda():
    #     """Locate the CUDA environment on the system

    #     Returns a dict with keys 'home', 'nvcc', 'include', and 'lib'
    #     and values giving the absolute path to each directory.

    #     Starts by looking for the CUDAHOME env variable. If not found, everything
    #     is based on finding 'nvcc' in the PATH.
    #     """

    #     # first check if the CUDAHOME env variable is in use
    #     if 'CUDAHOME' in os.environ:
    #         home = os.environ['CUDAHOME']
    #         nvcc = pjoin(home, 'bin', 'nvcc')
    #     else:
    #         # otherwise, search the PATH for NVCC
    #         nvcc = find_in_path('nvcc', os.environ['PATH'])
    #         if nvcc is None:
    #             raise EnvironmentError('The nvcc binary could not be '
    #                 'located in your $PATH. Either add it to your path, or set $CUDAHOME')
    #         home = os.path.dirname(os.path.dirname(nvcc))

    #     cudaconfig = {'home':home, 'nvcc':nvcc,
    #                   'include': pjoin(home, 'include')}
    #     lib =  pjoin(home, 'lib')
    #     if os.path.exists(lib+"64") and sys.maxint == 2**63-1:
    #         cudaconfig["lib"] = lib+"64"
    #     elif os.path.exists(lib+"32") and sys.maxint == 2**31-1:
    #         cudaconfig["lib"] = lib+"32"
    #     elif os.path.exists(lib):
    #         cudaconfig["lib"] = lib
    #     else:
    #         print("No cuda library found !!!!")
    #     for key, val in cudaconfig.iteritems():
    #         if not os.path.exists(val):
    #             raise EnvironmentError('The CUDA %s path could not be located in %s' % (key, val))
    #     return cudaconfig


    def customize_compiler_for_nvcc(self):
        """inject deep into distutils to customize how the dispatch
        to gcc/nvcc works.

        If you subclass UnixCCompiler, it's not trivial to get your subclass
        injected in, and still have the right customizations (i.e.
        distutils.sysconfig.customize_compiler) run on it. So instead of going
        the OO route, I have this. Note, it's kindof like a wierd functional
        subclassing going on."""

        # tell the compiler it can processes .cu
        self.src_extensions.append('.cu')

        # save references to the default compiler_so and _comple methods
        default_compiler_so = self.compiler_so
        super_comp = self._compile

        # now redefine the _compile method. This gets executed for each
        # object but distutils doesn't have the ability to change compilers
        # based on source extension: we add it.
        def _compile(obj, src, ext, cc_args, extra_postargs, pp_opts):
            if os.path.splitext(src)[1] == '.cu':
                # use the cuda for .cu files
                self.set_executable('compiler_so', CUDA['nvcc'])
                # use only a subset of the extra_postargs, which are 1-1 translated
                # from the extra_compile_args in the Extension class
                postargs = extra_postargs['nvcc']
            else:
                postargs = extra_postargs['gcc']

            super_comp(obj, src, ext, cc_args, postargs, pp_opts)
            # reset the default compiler_so, which we might have changed for cuda
            self.compiler_so = default_compiler_so

        # inject our redefined _compile method into the class
        self._compile = _compile


    #######################################
    ## cython is not used  but just in case
    try:
        from Cython.Distutils import build_ext
        CYTHON = True
        print " CYTHON"
    except ImportError:
        CYTHON = False

    if CYTHON:
        cython_c_ext = ".pyx"
    else:
        cython_c_ext = ".cpp"
        from distutils.command.build_ext import build_ext
    #######################################

    # We subclass the build_ext class in order to handle compiler flags
    # for openmp and opencl etc in a cross platform way
    translator = {
            # Compiler
                # name, compileflag, linkflag
            'msvc' : {
                'openmp' : ('/openmp', ' '),
                'debug'  : ('/Zi', ' '),
                'OpenCL' : 'OpenCL',
                },
            'mingw32':{
                'openmp' : ('-fopenmp', '-fopenmp'),
                'debug'  : ('-g', '-g'),
                'stdc++' : 'stdc++',
                'OpenCL' : 'OpenCL'
                },
            'default':{
                'openmp' : ('-fopenmp', '-fopenmp'),
                'debug'  : ('-g', '-g'),
                'stdc++' : 'stdc++',
                'OpenCL' : 'OpenCL'
                }
            }

    cmdclass = {}


    def build_Hilbertxx():
        """
        Define how to build the Hilbertxx C extension

        @return: module 
        """
        global version
        if sys.platform == "win32" :
            raise RuntimeError("to be adapted yet ")
    #    else:
    #        sdk_libs = ['gcc', 'm']

        c_sorgenti_tmp = ["Hilbertxx.cc" ,"Hilbertxx_wrap.cxx"    ]
        c_sorgenti = [jn(['hilbertxx',  tok]) for tok in c_sorgenti_tmp]

        depends_tmp = [ "Hilbertxx.hh"]
        depends = [jn(['hilbertxx',  tok]) for tok in depends_tmp]
        libraries=["boost_filesystem","boost_system","gsl", "gslcblas","stdc++","gcc",]

        # libraries=["mkl_intel_lp64","mkl_intel_thread","mkl_core","iomp5", "boost_filesystem","boost_system","gsl", "gslcblas","stdc++","gcc",]
        # library_dirs = ["/sware/com/intel_compiler/composerxe-2011/mkl/lib/intel64","/sware/com/intel_compiler/lib/intel64"]


        print " DEPENDS " , depends
        module = Extension(name='Hilbertxx'+"_v"+version+'._Hilbertxx',
                           sources=c_sorgenti ,
                           depends=depends,
                           libraries= libraries, 

                           # library_dirs = library_dirs,

                           #extra_compile_args=['-fopenmp'] ,
                           #extra_link_args=['-fopenmp'] ,
                           extra_compile_args={'gcc': ["-fPIC"]},
                           define_macros=define_macros,
                           include_dirs=[ numpy.get_include()]+["hilbertxx/"] ) ##### + mpi_dirs + hdf5_dirs )
        return module


    def build_Sparsa():
        """
        Define how to build the Sparsa C extension

        @return: module 
        """
        global version
        if sys.platform == "win32" :
            raise RuntimeError("to be adapted yet ")
    #    else:
    #        sdk_libs = ['gcc', 'm']

        c_sorgenti_tmp = ["Sparsa.cc", "Sparsa_wrap.cxx" ]

        c_sorgenti = [jn(['hilbertxx',  tok]) for tok in c_sorgenti_tmp]

        depends_tmp = [ "Sparsa.hh"]
        depends = [jn(['hilbertxx',  tok]) for tok in depends_tmp]


        libraries=["lapack","blas","stdc++","gcc",]
        
        # libraries=["mkl_intel_lp64","mkl_intel_thread","mkl_core","iomp5","stdc++","gcc",]
        # library_dirs = ["/sware/com/intel_compiler/composerxe-2011/mkl/lib/intel64","/sware/com/intel_compiler/lib/intel64"]

        print " DEPENDS " , depends

        module = Extension(name='Hilbertxx'+"_v"+version+'._Sparsa',
                           sources=c_sorgenti ,
                           depends=depends,
                           libraries= libraries,

                           # library_dirs = library_dirs, 
                           #extra_compile_args=['-fopenmp'] ,
                           #extra_link_args=['-fopenmp'] ,
                           extra_compile_args={'gcc': ["-fPIC"]},
                           define_macros=define_macros,
                           include_dirs=[ numpy.get_include()]+["hilbertxx/"] ) ##### + mpi_dirs + hdf5_dirs )
        return module


    # run the customize_compiler
    class custom_build_ext(build_ext):
        """
        Cuda aware builder with nvcc compiler support
        """
        def build_extensions(self):
            customize_compiler_for_nvcc(self.compiler)
            build_ext.build_extensions(self)

    cmdclass['build_ext'] = custom_build_ext


    # if DOCUDA:
    #     CUDA = locate_cuda()
    #     CUDA["arch"] = ["-gencode" , "arch=compute_13,code=compute_13", 
    #                     "-gencode", "arch=compute_20,code=compute_20",
    #                     "-gencode", "arch=compute_20,code=compute_20",
    #                     "-gencode", "arch=compute_30,code=compute_30",
    #                     "-gencode", "arch=compute_20,code=compute_20" ]

    #     # CUDA["arch"] = ["-gencode" , "arch=compute_13", 
    #     #                 "-gencode", "arch=compute_20",
    #     #                 "-gencode", "arch=compute_20",
    #     #                 "-gencode", "arch=compute_30",
    #     #                 "-gencode", "arch=compute_20" ]

    #     # CUDA["arch"] =["-gencode", "arch=compute_20,code=compute_20" ]
    #     # CUDA["arch"] =[]

    #     CUDA["Xptxas"] = [" ", " -Xptxas=-v"]





    # def build_gputomo():
    #     """
    #     Build the GPU tomo C module

    #     @return: module

    #     """
    #     global version
    #     module = Extension('PyHST'+version+'/libgputomo',
    #                 sources=["PyHST/Cspace/gputomo.cu", "PyHST/Cspace/cudamedianfilter.cu","PyHST/Cspace/chambollepock.cu" ],
    #                 depends=["PyHST/Cspace/gputomo_test.h"], #does this work ?
    #                 library_dirs=[CUDA['lib']],
    #                 libraries=['cudart', "cublas", "cuda", "cufft"],
    #                 runtime_library_dirs=[CUDA['lib']],
    #                 # this syntax is specific to this build system
    #                 # we're only going to use certain compiler args with nvcc and not with gcc
    #                 # the implementation of this trick is in customize_compiler() below
    #                 extra_compile_args={'gcc': ["-g","-fPIC", "-O3"],
    #                                     'nvcc': CUDA["arch"] + [ "--compiler-options",  "-fPIC", "-O3", "-g" ]},
    #                 include_dirs=[numpy.get_include(), CUDA['include'], 'PyHST/Cspace'])
    #     return module


    #############################################################################

    from distutils.command.install_data import install_data
    class smart_install_data(install_data):
        def run(self):
            global PPM_INSTALL_DIR
        #need to change self.install_dir to the library dir
            install_cmd = self.get_finalized_command('install')
            self.install_dir = getattr(install_cmd, 'install_lib')
            PPM_INSTALL_DIR = self.install_dir
            return install_data.run(self)



    ###########################################################################################################
    from distutils.command.install_scripts import install_scripts
    class smart_install_scripts(install_scripts):
        def run (self):
            global  version

            install_cmd = self.get_finalized_command('install')
            self.install_dir = getattr(install_cmd, 'install_scripts')
            self.install_lib_dir = getattr(install_cmd, 'install_lib')

            self.outfiles = []
            for filein in glob.glob('scripts/*'):
                if filein in glob.glob('scripts/*~'): continue

                print "INSTALLO ", filein

                if not os.path.exists(self.install_dir):
                    os.makedirs(self.install_dir)

                filedest = os.path.join(self.install_dir, os.path.basename(filein+"_v"+version))
                print os.path.basename(filein+"_v"+version)
                print filedest

                if os.path.exists(filedest):
                    os.remove(filedest)

                text = open(filein, 'r').read()
                # text=string.replace(text,"import Hilbertxx", "import Hilbertxx"+"_v"+version)
                text=string.replace(text,"Hilbertxx", "Hilbertxx"+"_v"+version)
                # text=string.replace(text,"python", sys.executable)

                f=open(filedest, 'w')
                f.write(text)
                f.close()
                self.outfiles.append(filedest)

            if os.name == 'posix':
                # Set the executable bits (owner, group, and world) on
                # all the scripts we just installed.
                for file in self.get_outputs():
                    if self.dry_run:
                        pass
                    else:
                        mode = ((os.stat(file)[stat.ST_MODE]) | 0555) & 07777
                        os.chmod(file, mode)


    #############################################################################################################

    cmdclass['install_scripts'] = smart_install_scripts
    cmdclass['install_data'] = smart_install_data
    
    ext_modules.append(build_Hilbertxx())
    ext_modules.append(build_Sparsa())


    #######################
    # build_doc commandes #
    #######################

    try:
        import sphinx
        import sphinx.util.console
        sphinx.util.console.color_terminal = lambda: False
        from sphinx.setup_command import BuildDoc
    except ImportError:
        sphinx = None

    if sphinx:
        class build_doc(BuildDoc):

            def run(self):
                # make sure the python path is pointing to the newly built
                # code so that the documentation is built on this and not a
                # previously installed version

                build = self.get_finalized_command('build')
                sys.path.insert(0, os.path.abspath(build.build_lib))
                # we need to reload PyMca from the build directory and not
                # the one from the source directory which does not contain
                # the extensions
                BuildDoc.run(self)
                sys.path.pop(0)
        cmdclass['build_doc'] = build_doc


    description = "Hilbertxx program for many body  exact solution of very small correlated systems"
    long_description = """
             Hilbertxx program for many body  exact solution of very small correlated systems
    """


    post_fix=""
    # if DOCUDA==0 and ONLYCUDA==0:
    #     post_fix="_base"
    # elif  DOCUDA==1 and ONLYCUDA==1:
    #     post_fix="_cudaextensions"

    distrib = setup(name="Hilbertxx"+"_v"+version+post_fix,
                    license="",
                    version=version+aumento_versione, ## aumenta qua versione
                    description=description,
                    author="Alessandro Mirone",
                    author_email="mirone@esrf.fr",
                    url="http://forge.epn-campus.fr/projects/PyHST",
                    long_description=long_description,
                    packages=packages,
                    package_dir=package_dir,
                    data_files = data_files,
                    platforms='any',
                    ext_modules=ext_modules,
                    cmdclass=cmdclass ,
                    scripts=script_files,
                    )

























do_Hilbertxx()
