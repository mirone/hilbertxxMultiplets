/************************************************************************

  Copyright
  Alessandro MIRONE
  mirone@esrf.fr

  Copyright 2002  by European Synchrotron Radiation Facility, Grenoble, 
                  France

                               ----------
 
                           All Rights Reserved
 
                               ----------

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the names of European Synchrotron
Radiation Facility or ESRF or BLISS not be used in advertising or 
publicity pertaining to distribution of the software without specific, 
written prior permission.

EUROPEAN SYNCHROTRON RADIATION FACILITY DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL EUROPEAN SYNCHROTRON
RADIATION FACILITY OR ESRF BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

**************************************************************************/
%module mpilight
%{


#include<stdexcept>

#include<string.h>
#include<stdio.h>
#include <stdlib.h>
#include<math.h>


#define MPICH_SKIP_MPICXX
#define OMPICH_SKIP_MPICXX
#include"mpi.h"


MPI_Comm get_MPI_COMM_WORLD();


int prova(int  N, double * x) ;

void settaMPI(int argc, char ** argv);

int get_MPI_myid();

int get_MPI_numprocs();

int array_MPI_bcast(int N, double *data,int  root) ;

int MPI_barrier();

extern "C" {
	int MPI_Finalize();
}







int array_MPI_sendint(int N, int *data,int  dest,int tag);
int array_MPI_receiveint(int N, int *data,int  source,int tag) ;


int reduce(int N, double *sendbuf,int N1, double *recvbuf, int root );






int array_MPI_senddouble(int N, double *data,int  dest,int tag);

int array_MPI_sendfloat(int N, float *data,int  dest,int tag);

int array_MPI_receiveadddouble(int N, double *data,int  source,int tag, int Nbuffer, double *buffer);

int array_MPI_receiveaddfloat(int N, float *data,int  source,int tag, int Nbuffer, float *buffer) ;

int array_MPI_receivedouble(int N, double *data,int  source,int tag) ;

int array_MPI_receivefloat(int N, float *data,int  source,int tag, int Nbuffer, float *buffer);











%}

%{
#include <numpy/oldnumeric.h>
#include<numpy/arrayobject.h>
%}



%init %{	
  import_array();
%}	



%include typemaps.i

// THE FOLLOWING TYPEMAPS HAVE BEEN STOLEN FROM
// THE SWIG *.I FILES OF THE PLPLOT PROJECT

%{
   // global variables for consistency check on argument patterns
  static int Alen = 0;
//  static int Xlen = 0, Ylen = 0;
%}


%typemap(in) (int  N, double * ArrayFLOAT) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 1, 1);
  if(tmp == NULL) return NULL;
  $1 = Alen = tmp->dimensions[0];
  $2 = (double  *)tmp->data;
}
%typemap(freearg) (int  N, double * ArrayFLOAT) {Py_DECREF(tmp$argnum);}

%typemap(in) (int  N, float * ArrayFLOAT4) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_FLOAT, 1, 1);
  if(tmp == NULL) return NULL;
  $1 = Alen = tmp->dimensions[0];
  $2 = (float  *)tmp->data;
}
%typemap(freearg) (int  N, float * ArrayFLOAT4) {Py_DECREF(tmp$argnum);}


%typemap(in) (int  N, int * Array) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_INT, 1, 1);
  if(tmp == NULL) return NULL;
  $1 = Alen = tmp->dimensions[0];
  $2 = (int  *)tmp->data;
}
%typemap(freearg) (int  N, int  * Array) {Py_DECREF(tmp$argnum);}



%typemap(in) (double * ArrayFLOATnoN) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 1, 1);
  if(tmp == NULL) return NULL;
  Alen = tmp->dimensions[0];
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * ArrayFLOATnoN) {Py_DECREF(tmp$argnum);}

%typemap(in) (double * VectFLOATnoN) (PyArrayObject* tmp=NULL) {
  PyObject* array;
  array = PyObject_GetAttrString($input,"data");
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject(array, PyArray_DOUBLE, 1, 1);
  Py_DECREF(array);
  if(tmp == NULL) return NULL;
  Alen = tmp->dimensions[0];
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * VectFLOATnoN) {Py_DECREF(tmp$argnum);}



%typemap(in) (double * ArrayFLOAT2dNOCHECK) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 2, 2);
  if(tmp == NULL) return NULL;
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * ArrayFLOAT2dNOCHECK) {Py_DECREF(tmp$argnum);}





%typemap(in) ( double * ArrayFLOATCHECK) (PyArrayObject* tmp=NULL ) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 1, 1);
  if(tmp == NULL) return NULL;
  if(tmp->dimensions[0] != Alen) {
    PyErr_SetString(PyExc_ValueError, "Vectors must be same length.");
    return NULL;
  }
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * ArrayFLOATCHECK) {Py_DECREF(tmp$argnum);}





%typemap(in) ( int * ArrayCHECK) (PyArrayObject* tmp=NULL ) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_INT, 1, 1);
  if(tmp == NULL) return NULL;
  if(tmp->dimensions[0] != Alen) {
    PyErr_SetString(PyExc_ValueError, "Vectors must be same length.");
    return NULL;
  }
  $1 = (int  *)tmp->data;
}
%typemap(freearg) (int * ArrayCHECK) {Py_DECREF(tmp$argnum);}




%typemap(in) ( int * ArrayNOCHECK) (PyArrayObject* tmp=NULL ) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_INT, 1, 1);
  if(tmp == NULL) return NULL;

  $1 = (int  *)tmp->data;
}
%typemap(freearg) (int * ArrayNOCHECK) {Py_DECREF(tmp$argnum);}







%typemap(in, numinputs=1) ( int start, int  N,  double *&ArrayFLOAT) (double * tmp_vista) {




    if (SWIG_arg_fail(1)) SWIG_fail;
    {
        arg2 = (int)(PyLong_AsLong(      PyTuple_GetItem(	obj1,0)             )); 
        if (SWIG_arg_fail(2)) SWIG_fail;
    }
    {
        arg3 = (int)(PyLong_AsLong(        PyTuple_GetItem(	obj1,1)             )); 
        if (SWIG_arg_fail(3)) SWIG_fail;
    }


  $3 = &tmp_vista ;
} 
%typemap(argout, fragment="t_output_helper") ( int start, int  N,  double *&ArrayFLOAT  )  {
  int nd=1;
  int dims[1];
  PyObject *arr_a;
  dims[0]=$2;

  // arr_a = PyArray_FromDims(nd,dims,PyArray_DOUBLE) ;
  arr_a = PyArray_FromDimsAndData(nd,dims,PyArray_DOUBLE, (char * )  *$3 ) ;
	
  // memcpy( ( (PyArrayObject *) arr_a )->data  ,  *$3 , dims[0]*sizeof(double) );



  resultobj = t_output_helper(resultobj,arr_a );
} 


// ------------------------------------------------------------------------------------------------------------
// A common problem in many C programs is the processing of command line arguments, which are usually passed in an array of NULL terminated strings. The following SWIG interface file allows a Python list object to be used as a char ** object.



// This tells SWIG to treat char ** as a special case
%typemap(in) char ** {
  /* Check if is a list */
  if (PyList_Check($input)) {
    int size = PyList_Size($input);
    int i = 0;
    $1 = (char **) malloc((size+1)*sizeof(char *));
    for (i = 0; i < size; i++) {
      PyObject *o = PyList_GetItem($input,i);
      if (PyString_Check(o))
	$1[i] = PyString_AsString(PyList_GetItem($input,i));
      else {
	PyErr_SetString(PyExc_TypeError,"list must contain strings");
	free($1);
	return NULL;
      }
    }
    $1[i] = 0;
  } else {
    PyErr_SetString(PyExc_TypeError,"not a list");
    return NULL;
  }
}

// This cleans up the char ** array we malloc'd before the function call
%typemap(freearg) char ** {
  free((char *) $1);
}

// Now a test function
%inline %{
int print_args(char **argv) {
    int i = 0;
    while (argv[i]) {
         printf("argv[%d] = %s\n", i,argv[i]);
         i++;
    }
    return i;
}
%}


// This tells SWIG to treat char *** as a special case
%typemap(in) char *** {
  /* Check if is a list */
  if (PyList_Check($input)) {
    int size = PyList_Size($input);
    int i = 0;
    $1 = (char ***) malloc((size+1)*sizeof(char **));
    for (i = 0; i < size; i++) {
      PyObject *o = PyList_GetItem($input,i);
      if (PyList_Check(o)) {
	int size = PyList_Size(o);
	$1[i] = (char **) malloc((size+1)*sizeof(char *)); 
	
	int j = 0;
	for (j = 0; j < size; j++) {
	  PyObject *oo = PyList_GetItem(o,j);
	  if (PyString_Check(oo)) {
	    $1[i][j] = PyString_AsString(PyList_GetItem(o,j));
	  } else {
	    PyErr_SetString(PyExc_TypeError,"list must contain strings");
	    for(; i>=0; i--) {
	      free($1[i]); 
	    }
	    free($1);
	    return NULL;
	  }
	}
	$1[i][j] = 0;
      }else {
	PyErr_SetString(PyExc_TypeError,"list must contain lists");
	free($1);
	return NULL;
      }
    }
    $1[i] = 0;
  }else {
    PyErr_SetString(PyExc_TypeError,"not a list");
    return NULL;
  }
}

// This cleans up the char *** array we malloc'd before the function call
%typemap(freearg) char *** {
  char ***ptr;
  ptr=$1;
  while(*ptr) {
    free(*ptr);
    ptr++;
  }
  free( $1);
}
// Now a test function
%inline %{
int print_argvs(char ***argvs) {
    int j = 0;
    while(argvs[j]) {
      int i = 0;
      while (argvs[j][i]) {
	printf("argvs[%d][%d] = %s\n", j,i,argvs[j][i]);
	i++;
      }
      j++;
    }
    return j;
}
%}




// ---------------------------------------------------------

int prova(int  N, double * ArrayFLOAT) ;


%exception {
        try {
        $action
        }
        catch ( std::out_of_range &e ) {
           PyErr_SetString(PyExc_IndexError, e.what() );
           return NULL;
        }

}


int MPI_barrier();
void settaMPI(int argc, char ** argv);
int get_MPI_myid();
int get_MPI_numprocs();
int array_MPI_bcast(int  N, double * ArrayFLOAT, int  root) ;
int array_MPI_senddouble(int N, double *ArrayFLOAT,int  dest,int tag);
int array_MPI_sendfloat(int N, float *ArrayFLOAT4,int  dest,int tag);
int array_MPI_receiveadddouble(int N, double *ArrayFLOAT,int  source,int tag, int N, double *ArrayFLOAT) ;
int array_MPI_receiveaddfloat(int N, float *ArrayFLOAT4,int  source,int tag, int N, float *ArrayFLOAT4) ;

int array_MPI_receivedouble(int N, double *ArrayFLOAT,int  source,int tag) ;

int array_MPI_receivefloat(int N, float *ArrayFLOAT4,int  source,int tag, int Nbuffer, float *buffer);


int array_MPI_sendint(int N, int *Array,int  dest,int tag);
int array_MPI_receiveint(int N, int *Array,int  source,int tag) ;



int reduce(int N, double *ArrayFLOAT,int N, double *ArrayFLOAT, int root ); 


int MPI_Finalize();

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


typedef struct ompi_info_t *MPI_Info;



%typemap(out)   MPI_Comm   {
  resultobj = SWIG_NewPointerObj((new $1_ltype(static_cast< const $1_ltype & >(result))), SWIGTYPE_p_MPI_Comm, SWIG_POINTER_OWN &  0 );
}


%typemap(in, numinputs=0) ( MPI_Comm *comm2create  ) {
  $1 = new  MPI_Comm;
} 
%typemap(argout,fragment="t_output_helper") (  MPI_Comm *comm2create  )  {
  PyObject *obj;
  obj = SWIG_NewPointerObj(SWIG_as_voidptr(($1) ), SWIGTYPE_p_MPI_Comm, 0 |  0 ); // l'ultimo flag
  resultobj = t_output_helper(resultobj, obj );
} 


%typemap(in, numinputs=0) ( MPI_Info *info  )(MPI_Info info ) {
  $1 = &info;
} 
%typemap(argout,fragment="t_output_helper") ( MPI_Info *info   )  {
  PyObject *infoobj;
  infoobj = SWIG_NewPointerObj(SWIG_as_voidptr(*($1) ), SWIGTYPE_p_ompi_info_t, 0 |  0 ); // l'ultimo flag
  // se positivo in SWIG_POINTER_OWN portera ad un tentativo di trovare la funzione destroy
  // nella fase di garbage_collecting.  MPI_Info lo creiamo e distruiamo con MPI

  resultobj = t_output_helper(resultobj, infoobj );
} 


%typemap(in) ( MPI_Info *array_of_info  ) {
  
  if (PyList_Check($input)) {
    int size = PyList_Size($input);
    int i = 0;
    $1 = (MPI_Info *) malloc((size+1)*sizeof(MPI_Info ));
    for (i = 0; i < size; i++) {
      PyObject *o = PyList_GetItem($input,i);

      
      MPI_Info argi;
      void * argip;
      int res1 = SWIG_ConvertPtr(o, &argip ,SWIGTYPE_p_ompi_info_t, 0 |  0 );
      if (!SWIG_IsOK(res1)) {
	SWIG_exception_fail(SWIG_ArgError(res1), "in method '" "MPI_Info_set" "', argument " "1"" of type list of '" "MPI_Info""'"); 
      } else {
	argi = reinterpret_cast< MPI_Info >(argip);
      }

      $1[i] =  argi ;
      
    }
    $1[i] = 0;
  } else {
    PyErr_SetString(PyExc_TypeError,"not a list");
    return NULL;
  }
} 

%typemap(argout ) (  MPI_Info *array_of_info  )  {
  free($1);
} 




int MPI_Info_create(MPI_Info *info);



// MPI_Info   MPI_Info_create_get();




int MPI_Info_set(MPI_Info info, char *key, char *value);



%typemap(in, numinputs=0) (  int  *flag )(int flag) {
  $1 = &flag;
} 
%typemap(argout,fragment="t_output_helper") (  int  *flag  )  {
  PyObject *pyflag;
  pyflag=PyInt_FromLong(*($1));
  resultobj = t_output_helper(resultobj, pyflag );
} 


int MPI_Info_get(MPI_Info info, char *key, int valuelen, char *value, int *flag);

// int MPI_Comm_spawn_multiple(int count,
// 			    char **array_of_commands,
// 			    char *** array_of_argv , 
// 			    int * array_of_maxprocs, 
// 			    MPI_Info * array_of_info,
// 			    int root, 
// 			    MPI_Comm comm,
// 			    MPI_Comm *intercomm ,  < ===
// 			    int * array_of_errcodes);


// int MPI_Comm_spawn(char *command, char *argv[], int maxprocs,
// 		   MPI_Info info, int root, MPI_Comm comm,
// 		   MPI_Comm *intercomm, int array_of_errcodes[])
  

int MPI_Comm_spawn_multiple(int count,
			    char **array_of_commands,
			    char *** array_of_argv , 
			    int * ArrayNOCHECK , 
			    MPI_Info * array_of_info,
			    int root, 
			    MPI_Comm comm,
			    MPI_Comm *comm2create , 
			    int * ArrayNOCHECK );

int MPI_Comm_spawn(char *command,
		   char **argv,
		   int maxprocs,
		   MPI_Info info, 
		   int root,
		   MPI_Comm comm,
		   MPI_Comm *comm2create,
		   int * ArrayNOCHECK );
  
  


%inline %{
  MPI_Comm get_MPI_COMM_WORLD_(){return MPI_COMM_WORLD;}; // questo porta ad un segnal di perdita di memoria
 %}

%typemap(out)   MPI_Comm   {
  resultobj = SWIG_NewPointerObj((new $1_ltype(static_cast< const $1_ltype & >(result))), SWIGTYPE_p_MPI_Comm, SWIG_POINTER_OWN &  0 );

}


int MPI_Comm_get_parent(MPI_Comm *comm2create);

%inline %{
  MPI_Comm get_MPI_COMM_WORLD(){return MPI_COMM_WORLD;}; // dopo il giusto typemap non prova a distruggere
                                                         // quello a cui punta di memoria e'eliminata
%}




%typemap(in, numinputs=0) ( int fromRecvS, int tagRecvS, MPI_Comm comm ) {
    int source ;
    int tag ;
    PyObject *pyComm, *resStri;
    char *s;
    MPI_Comm comm;
    
    
    if (!  PyArg_ParseTuple(args,"iiO",&source,&tag, &pyComm)  ) SWIG_fail;
    

    // -------------------------------------------
    {
      void *argp;
      int res = SWIG_ConvertPtr(pyComm, &argp, SWIGTYPE_p_MPI_Comm,  0  | 0);
      if (!SWIG_IsOK(res)) {
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "MPI_recvString" "', argument " "3"" of type '" "MPI_Comm""'"); 
      }  
      if (!argp) {
	SWIG_exception_fail(SWIG_ValueError, "invalid null reference " "in method '" "MPI_recvString" "', argument " "3"" of type '" "MPI_Comm""'");
      } else {
	MPI_Comm * temp = reinterpret_cast< MPI_Comm * >(argp);
	comm = *temp;
	if (SWIG_IsNewObj(res)) delete temp;
      }
    }    
    // -------------------------------------------------------
    
    MPI_Status *status;
    status= new MPI_Status;
    
    int len;
    MPI_Recv( &len, 1, MPI_INT, source, tag, comm, &(*status) );
    
    
    resStri = PyString_FromStringAndSize( 0 , len); 
    s = PyString_AsString(resStri) ;
    MPI_Recv( s, len , MPI_CHAR, source, tag, comm, &(*status) );
    
    PyObject* pyTuple = 0;
    PyObject* pyStat = 0;

    pyStat = SWIG_NewPointerObj( status , SWIGTYPE_p_MPI_Comm, SWIG_POINTER_OWN &  0 );
    if (!  (pyTuple = Py_BuildValue("(OO)",pyStat,resStri)) ) SWIG_fail;
    
    Py_XDECREF(pyStat);
    Py_XDECREF(resStri);
    
    return pyTuple;
} 

%inline %{
  void MPI_recvString(int fromRecvS, int tagRecvS, MPI_Comm comm) {};
  %}






%typemap(in, numinputs=0) ( char *,int toSendS, int tagSendS, MPI_Comm comm ) {
    int receiver ;
    int tag ;
    PyObject *pyComm, *pyString;
    char *s;
    MPI_Comm comm;
    
    
    if (!  PyArg_ParseTuple(args,"OiiO",&pyString, &receiver,&tag, &pyComm)  ) SWIG_fail;
    

    // -------------------------------------------
    {
      void *argp;
      int res = SWIG_ConvertPtr(pyComm, &argp, SWIGTYPE_p_MPI_Comm,  0  | 0);
      if (!SWIG_IsOK(res)) {
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "MPI_sendString" "', argument " "3"" of type '" "MPI_Comm""'"); 
      }  
      if (!argp) {
	SWIG_exception_fail(SWIG_ValueError, "invalid null reference " "in method '" "MPI_sendString" "', argument " "3"" of type '" "MPI_Comm""'");
      } else {
	MPI_Comm * temp = reinterpret_cast< MPI_Comm * >(argp);
	comm = *temp;
	if (SWIG_IsNewObj(res)) delete temp;
      }
    }    
    // -------------------------------------------------------
    

    // -------------------------------------------
    {
      if(!PyString_Check(pyString)) {
	PyErr_SetString(PyExc_ValueError, "In MPI_sendString  argument 1 must be of type string.");
	return NULL;
	s = PyString_AsString(pyString) ;
      }
    }




    
    int len;
    len = PyString_Size(pyString);
    int s1=MPI_Send( &len, 1, MPI_INT, receiver, tag, comm);
    
   
    int s2=MPI_Send( s, len , MPI_CHAR, receiver, tag, comm );

 
    PyObject* pyTuple = 0;

    if (!  (pyTuple = Py_BuildValue("(ii)",s1,s2)) ) SWIG_fail;
    
   
    return pyTuple;

} 



%inline %{
  void MPI_sendString(char *,int toSendS, int tagSendS, MPI_Comm comm) {};
  %}











%exception;
