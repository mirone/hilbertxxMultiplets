/************************************************************************

  Copyright
  Alessandro MIRONE
  mirone@esrf.fr

  Copyright 2002  by European Synchrotron Radiation Facility, Grenoble, 
                  France

                               ----------
 
                           All Rights Reserved
 
                               ----------

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the names of European Synchrotron
Radiation Facility or ESRF or BLISS not be used in advertising or 
publicity pertaining to distribution of the software without specific, 
written prior permission.

EUROPEAN SYNCHROTRON RADIATION FACILITY DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL EUROPEAN SYNCHROTRON
RADIATION FACILITY OR ESRF BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

**************************************************************************/
%module Sparsa3AP
%{
#include<string.h>
#include<string.h>
#include<stdio.h>
#include <stdlib.h>
#include<iostream>
#include<math.h>
#include<complex.h>
#include<Sparsa.h>

/* per un bug in mpich2 */
#define MPICH_IGNORE_CXX_SEEK    

#include"mpi.h"

int get_MPI_numprocs() {
  int  numprocs;

  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

  return numprocs;
}


int get_MPI_myid() {
  int myid;

  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  return myid;
}



%}

%{
#include <numpy/oldnumeric.h>
#include<numpy/arrayobject.h>
%}


%init %{	
  import_array();
%}	



%include typemaps.i

// THE FOLLOWING TYPEMAPS HAVE BEEN STOLEN FROM
// THE SWIG *.I FILES OF THE PLPLOT PROJECT

%{
   // global variables for consistency check on argument patterns
  static int Alen = 0;
//  static int Xlen = 0, Ylen = 0;
%}


%typemap(in) (int  N, double * ArrayFLOAT) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 1, 1);
  if(tmp == NULL) return NULL;
  $1 = Alen = tmp->dimensions[0];
  $2 = (double  *)tmp->data;
}
%typemap(freearg) (int  N, double * ArrayFLOAT) {Py_DECREF(tmp$argnum);}


%typemap(in) (int  N, int * Array) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_INT, 1, 1);
  if(tmp == NULL) return NULL;
  $1 = Alen = tmp->dimensions[0];
  $2 = (int  *)tmp->data;
}
%typemap(freearg) (int  N, int  * Array) {Py_DECREF(tmp$argnum);}



%typemap(in) (double * ArrayFLOATnoN) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 1, 1);
  if(tmp == NULL) return NULL;
  Alen = tmp->dimensions[0];
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * ArrayFLOATnoN) {Py_DECREF(tmp$argnum);}

%typemap(in) (double * VectFLOATnoN) (PyArrayObject* tmp=NULL) {
  PyObject* array;
  array = PyObject_GetAttrString($input,"data");
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject(array, PyArray_DOUBLE, 1, 1);
  Py_DECREF(array);
  if(tmp == NULL) return NULL;
  Alen = tmp->dimensions[0];
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * VectFLOATnoN) {Py_DECREF(tmp$argnum);}



%typemap(in) (double * ArrayFLOAT2dNOCHECK) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 2, 2);
  if(tmp == NULL) return NULL;
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * ArrayFLOAT2dNOCHECK) {Py_DECREF(tmp$argnum);}





%typemap(in) ( double * ArrayFLOATCHECK) (PyArrayObject* tmp=NULL ) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 1, 1);
  if(tmp == NULL) return NULL;
  if(tmp->dimensions[0] != Alen) {
    PyErr_SetString(PyExc_ValueError, "Vectors must be same length.");
    return NULL;
  }
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * ArrayFLOATCHECK) {Py_DECREF(tmp$argnum);}





%typemap(in) ( int * ArrayCHECK) (PyArrayObject* tmp=NULL ) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_INT, 1, 1);
  if(tmp == NULL) return NULL;
  if(tmp->dimensions[0] != Alen) {
    PyErr_SetString(PyExc_ValueError, "Vectors must be same length.");
    return NULL;
  }
  $1 = (int  *)tmp->data;
}
%typemap(freearg) (int * ArrayCHECK) {Py_DECREF(tmp$argnum);}







%typemap(in, numinputs=1) ( int start, int  N,  double *&ArrayFLOAT) (double * tmp_vista) {




    if (SWIG_arg_fail(1)) SWIG_fail;
    {
        arg2 = (int)(PyInt_AsLong(      PyTuple_GetItem(	obj1,0)             )); 
        if (SWIG_arg_fail(2)) SWIG_fail;
    }
    {
        arg3 = (int)(PyInt_AsLong(        PyTuple_GetItem(	obj1,1)             )); 
        if (SWIG_arg_fail(3)) SWIG_fail;
    }


  $3 = &tmp_vista ;
} 
%typemap(argout, fragment="t_output_helper") ( int start, int  N,  double *&ArrayFLOAT  )  {
  int nd=1;
  int dims[1];
  PyObject *arr_a;
  dims[0]=$2;

  // arr_a = PyArray_FromDims(nd,dims,PyArray_DOUBLE) ;
  arr_a = PyArray_FromDimsAndData(nd,dims,PyArray_DOUBLE, (char * )  *$3 ) ;
	
  // memcpy( ( (PyArrayObject *) arr_a )->data  ,  *$3 , dims[0]*sizeof(double) );



  resultobj = t_output_helper(resultobj,arr_a );
} 



%typemap(in, numinputs=1) ( int  n,  double *&ArrayFLOATritorno) (double * tmp) {

    $1  =  (int)(PyInt_AsLong(   $input    )); 
    $2 = &tmp ;	
} 
%typemap(argout, fragment="t_output_helper") (  int  n,  double *&ArrayFLOATritorno )  {
  int nd=1;
  int dims[1];
  PyObject *arr_a;
  dims[0]=$1;

  arr_a = PyArray_FromDimsAndData(nd,dims,PyArray_DOUBLE, (char * )  *$2 ) ;
	
  resultobj = t_output_helper(resultobj,arr_a );
} 


// ------------------------------------------------------------------------------------------------------------
// A common problem in many C programs is the processing of command line arguments, which are usually passed in an array of NULL terminated strings. The following SWIG interface file allows a Python list object to be used as a char ** object.



// This tells SWIG to treat char ** as a special case
%typemap(in) char ** {
  /* Check if is a list */
  if (PyList_Check($input)) {
    int size = PyList_Size($input);
    int i = 0;
    $1 = (char **) malloc((size+1)*sizeof(char *));
    for (i = 0; i < size; i++) {
      PyObject *o = PyList_GetItem($input,i);
      if (PyString_Check(o))
	$1[i] = PyString_AsString(PyList_GetItem($input,i));
      else {
	PyErr_SetString(PyExc_TypeError,"list must contain strings");
	free($1);
	return NULL;
      }
    }
    $1[i] = 0;
  } else {
    PyErr_SetString(PyExc_TypeError,"not a list");
    return NULL;
  }
}

// This cleans up the char ** array we malloc'd before the function call
%typemap(freearg) char ** {
  free((char *) $1);
}

// Now a test function
%inline %{
int print_args(char **argv) {
    int i = 0;
    while (argv[i]) {
         printf("argv[%d] = %s\n", i,argv[i]);
         i++;
    }
    return i;
}
%}


// ---------------------------------------------------------

void settaMPI(int argc, char ** argv);


class inizializzaMPI {
 public:
  inizializzaMPI();
};


class Sparsa3AP {
 public:
  Sparsa3AP();
  ~Sparsa3AP();
  void caricaArrays(int N , int * Array, int * ArrayCHECK, double * ArrayFLOATCHECK /* ,int dim_from, int dim_to*/);
  void Moltiplica(ArrayP *ris, ArrayP *vect  );
  void trasforma(double fattore, double addendo) ;	
  // double goherschMin();
  // double goherschMax();

  int dim;	
	
};


%rename (__getitem__) get_array(int index);

struct ArrayP {
  ArrayP();
  ArrayP( int size );

  ArrayP( int fd, int sd );


  ~ArrayP() ;


  ArrayP * get_array(int index) ;


  void set_all_random(  double value );


  void set_to_zero(  );

  void set_to_one(  );
  
  static void copy_to_a_from_b(ArrayP &a, ArrayP&b) ;
  
  
  void copy_from_b( ArrayP&b);
  
  void add_from_vect( ArrayP &vect) ;

  
  void add_from_vect_with_fact( ArrayP & vect, double fact) ;


  void mult_by_fact(  double fact);
  
  
  void caricaArray(int N , double * ArrayFLOAT);

  ArrayP* copy() const;
  
  int len() const ;
  
  void  dividebyarray( ArrayP &b);
  static double scalare(ArrayP &a, ArrayP &b) ;


  static double sqrtscalare(ArrayP &a, ArrayP &b) ;


  void normalizza(double norm);
  void normalizzaauto();


  void scriviArray();

  static void mat_mult(ArrayP & b, double * ArrayFLOAT2dNOCHECK ,ArrayP & a);

};








struct ArrayP2 {
  // ArrayP2( int  N, int * Array  )   ; 
  // ArrayP2( int fd, int  N, int * Array );

  ArrayP2();
  ArrayP2( int dim_from);
  void setta( int  N, int * Array  );
  ArrayP2( int fd, int sd   ) ;


  ~ArrayP2() ;
  ArrayP2 * get_array(int index) ;
  

  void set_all_random(  double value );


  void set_to_zero(  );

  void set_to_one(  );
  static void copy_to_a_from_b(ArrayP2 &a, ArrayP2&b);
  
  
  void copy_from_b( ArrayP2 &b) ;
  
  
  void add_from_vect( ArrayP2  &vect) ;
  
  void add_from_vect_with_fact( ArrayP2 & vect, double fact) ;

  void mult_by_fact(  double fact);
  
  

  void caricaArray(int N , double * ArrayFLOAT) ;
  
  ArrayP2* copy( int  N, int * Array ) const;
 
  int len() ;
  
  void  dividebyarray( ArrayP2 &b);
  void  multbyarray  ( ArrayP2 &b);

  static double scalare(ArrayP2 &a, ArrayP2 &b);

  static double sqrtscalare(ArrayP2 &a, ArrayP2 &b) ;
  static void scalaremulti( ArrayP2 &a , ArrayP2 &b , int n , double *&ArrayFLOATritorno );
	
   void add_from_vect_with_fact_multi( ArrayP2 & vect, int N, double *ArrayFLOAT) ;


  void normalizza(double norm);

  void normalizzaauto();

  void scriviArray(char * nomefile, int start, int dim) ;

  static void  mat_mult( ArrayP2 & b, double * ArrayFLOAT2dNOCHECK ,ArrayP2 & a    ) ;


};





class Sparsa3AP2 {
 public:

  Sparsa3AP2();
  ~Sparsa3AP2();

  void caricaArrays(  int N , int * Array, int * ArrayCHECK, double * ArrayFLOATCHECK ,int N , int * Array,int N , int * Array );

  void Moltiplica(ArrayP2 *ris, ArrayP2 *vect  ) ;

  void MoltiplicaExp(ArrayP2 *ris, ArrayP2 *vect  ) ;
  void MoltiplicaExpL(ArrayP2 *ris, ArrayP2 *vect  ) ;

  void MoltiplicaMinus(ArrayP2 *ris, ArrayP2 *vect  ) ;


  void	 LLtSolve(ArrayP2 *Ares, ArrayP2  *Avect  );
  void preparaLLt() ;


  void trasforma(double fattore, double addendo) ;	
 
  void preparaPreco( ) ; 
  void preparaPrecoExp( ) ; 
	
  void  Preco(ArrayP2 *ris,ArrayP2  *vect , double omega ){


    Preco(ris->data, vect->data , omega );
  };

  void expandi();
  void fissaexp();
	

  int dim;


};


int get_MPI_numprocs() ;
int get_MPI_myid() ;


