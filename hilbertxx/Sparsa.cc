/************************************************************************

  Copyright
  Alessandro MIRONE
  mirone@esrf.fr

  Copyright 2002  by European Synchrotron Radiation Facility, Grenoble, 
                  France

                               ----------
 
                           All Rights Reserved
 
                               ----------

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the names of European Synchrotron
Radiation Facility or ESRF or SCISOFT not be used in advertising or 
publicity pertaining to distribution of the software without specific, 
written prior permission.

EUROPEAN SYNCHROTRON RADIATION FACILITY DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL EUROPEAN SYNCHROTRON
RADIATION FACILITY OR ESRF BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

**************************************************************************/
/***************************************************************************
                          Sparsa.cc  -  description
                             -------------------
    begin                : Tue Feb 1 2000
    copyright            : (C) 2000 by Alessandro MIRONE
    email                : mirone@lure.u-psud.fr
 ***************************************************************************/

#define  _FILE_OFFSET_BITS 64
#define  _LARGEFILE_SOURCE
#define BLOCKING
#define MAPPA
#define NOPIRAMIDE
#include<string.h>
#include<string.h>
#include<stdio.h>
#include <stdlib.h>
#include<iostream>
#include<math.h>
#include<complex.h>

#include"Sparsa.h"

#include <sys/types.h>
#include <unistd.h>
#include<time.h>
#include<map>
#include <time.h>



#define Max(a,b) ( ((a)>(b))?  (a):(b) )

#ifdef SPARSAMPI
/* per un bug in mpich2 */
#define MPICH_IGNORE_CXX_SEEK    
#include<mpi.h>
#endif

#define DEBUG(a)  
#define DEBUGA(a) a

#define Min(a,b) (((a)<(b))? (a):(b)  )

#define _UNROLL_


// #define MPI_Barrier(MPI_COMM_WORLD )




Sparsa3A::Sparsa3A() {
  n=0;
  dim=0;
  nsize=1000;
  coeff=new double[nsize];  
  col=new int[nsize]   ;
  row=new int[nsize]   ;
  nG=0;
  Gmin=0;
  Gmax=0;

};


void Sparsa3A::extractM( double *&mat, int &ncF , int *&corrF,  int &ncT , int *&corrT,       int sF,int dF ,int sT , int dT ) {

  int maxF=0;
  for(int i=0; i<n; i++)  {
    if( col[i]>maxF) maxF=col[i];
  }

  int maxT=0;
  for(int i=0; i<n; i++)  {
    if( row[i]>maxT) maxT=row[i];
  }
  int *inv_corrF = new int [maxF+1];
  int *inv_corrT = new int [maxT+1];
  

  memset(inv_corrF, 0, (maxF+1)*sizeof(int));
  memset(inv_corrT, 0, (maxT+1)*sizeof(int));



  for(int i=0; i<n; i++)  {
    if( col[i]>=sF && col[i]<sF+dF &&    row[i]>=sT && row[i]<sT+dT       ) {
      inv_corrF[col[i]]=1;
    }
  }
  for(int i=0; i<n; i++)  {
    if( row[i]>=sT && row[i]<sT+dT       &&        col[i]>=sF && col[i]<sF+dF    ) {
      inv_corrT[row[i]]=1;
    }
  }


  ncF=0;
  for(int i=0; i<=maxF; i++) {
    ncF +=  inv_corrF[ i ]; 
//     if(inv_corrF[ i ]) {
//       printf(" extractM inv_corrF[ i ]=1 i= %d \n", i  ); 
//     }
  }

  printf(" ncF %d dF %d \n", ncF, dF);

  ncT=0;
  for(int i=0; i<=maxT; i++) {
    ncT +=  inv_corrT[ i ]; 
  }

  printf(" ncT %d dT %d \n", ncT, dT);

  corrF = new int [ncF];
 

  ncF=0;
  for(int i=0; i<=maxF; i++) {
    if( inv_corrF[i]) {
      inv_corrF[i]=ncF;
      corrF[ncF] = i;
      ncF +=  1; 
    } else {
      inv_corrF[i]=-1;
    }
  }


  corrT = new int [ncT];

  ncT=0;
  for(int i=0; i<=maxT; i++) {
    if( inv_corrT[i]) {
      inv_corrT[i]=ncT;
      corrT[ncT] = i;
      ncT +=  1; 
    } else {
      inv_corrT[i]=-1;
    }
  }



  /**********************************************************/

  mat=new double [ ncT*ncF  ];
  memset(mat, 0,  ncT*ncF *sizeof(double));

  int from, to;

  for(int i=0; i<n; i++)  {
    if( row[i]>=sT && row[i]<sT+dT         &&   col[i]>=sF && col[i]<sF+dF  ) {
      from= inv_corrF[  col[i]];
      to= inv_corrT[ row[i]];
      mat [to*ncF+from  ]=coeff[i];
    }
  }

  delete inv_corrF;
  delete inv_corrT;

  printf(" uscita da extractM \n");
}



void  Sparsa3A::get_3arrays(int &Nels , int *&from, int *&to, double *&coeffs){
    Nels = n;
    from = col;
    to   = row;
    coeffs = coeff;
  };


void Sparsa3A::debug_sufile(char * nome) {
  FILE *f=fopen(nome,"w");

  fwrite(&n,sizeof(int),1,f);
  fwrite(&dim  , sizeof(int), 1, f);
  fwrite(&dim2 , sizeof(int), 1, f);

  for (int i=0; i<n; i++) {
    fwrite(&(row[i])   , sizeof(int)      , 1    , f) ;
    fwrite(&(col[i]) , sizeof(int)      , 1    , f) ;
    fwrite(&(coeff[i]), sizeof(double), 1    , f) ;
  }

  
//   for (int i=0; i<n; i++) {
//     fprintf(f, "%d %d %e\n", row[i], col[i], coeff[i]);
//   }
  fclose(f);
}



Sparsa3A::Sparsa3A(int N, double *c, int *i, int *j) {
   this->inizializza(  N, c, i, j, 0  , 0 );
};
// Sparsa3A::Sparsa3A(int N, double *c, int *i, int *j, double *pot_for_from  , double *pot_for_to ){
//  cout << "in inizializza  con pot " << endl;
//   this->inizializza(  N, c, i, j, pot_for_from  , pot_for_to);
// }




void Sparsa3A::add2template_forhops(int N,  double *c , int *goes2,
				    int *cc , double * hopfacts) {
  int posint, i_to, i_from, cc1,cc2;
  for (int i=0 ; i<N; i++) {
    posint = goes2[i];
    i_to   = row[posint];
    i_from = col[posint];
    cc1=cc[i_to];
    cc2=cc[i_from];
    if( cc2>cc1) cc1=cc2;
    coeff[posint]+=c[i]*hopfacts[cc1]  ;
  }
}




 
void  Sparsa3A::add_( int Nto , int Nfrom,double *c, int *corrto, int *corrfrom, int symm){

  int datasize;
  if (Nto!=-1) {
    datasize = Nto*Nfrom;
  } else {
    datasize=Nfrom;
  }

  int ntoadd=0;
  for( int k=0; k< datasize ; k++)  {
    if( c[k]!=0.0 ) {
      ntoadd++;
      if(symm) ntoadd++;
    }
  }

  int newN = n + ntoadd  ; 
  int * new_i_to = new int[newN];
  int * new_i_from = new int [ newN];
  double * new_coeff = new  double  [ newN];

  memcpy( new_i_to, row, n*sizeof(int));
  memcpy( new_i_from, col, n*sizeof(int));
  memcpy( new_coeff , coeff, n*sizeof(double));
    
  newN=n;
  {
    int f,t;
    for( int k=0; k< datasize ; k++)  {
      if( c[k]!=0.0 ) {

	if( Nto!=-1){
	  f = corrfrom[k%Nfrom] ;
	  t = corrto  [k/Nfrom] ;
	} else {
	  f =  corrfrom  [k] ;
	  t =  corrto    [k] ; 
	}

	new_i_from[newN]=f;
	new_i_to  [newN]=t;
	new_coeff [newN]=c[k];
	newN++;
	if(symm) {
	  new_i_from[newN]=t;
	  new_i_to  [newN]=f;
	  new_coeff [newN]=symm*c[k];
	  newN++;
	}
      }
    }
  }
  delete col;
  delete row;
  delete coeff;
  this->inizializza(newN, new_coeff, new_i_from, new_i_to, NULL, NULL);

  delete new_i_from;
  delete new_i_to;
  delete new_coeff;


};





void Sparsa3A::add2template(int N, double *c, int * goes2) {
  for (int i=0 ; i<N; i++) {
    coeff[goes2[i]]+=c[i];
  }
}

void Sparsa3A::readfile(char * nome) {
  FILE * f = fopen(nome, "r");
  fread(&n,sizeof(int),1,f);
  fread(&dim  , sizeof(int), 1, f);
  fread(&dim2 , sizeof(int), 1, f);
  delete row;
  delete col;
  delete coeff;
  nsize=n+1000;
  coeff=new double[nsize];  
  col=new int[nsize]   ;
  row=new int[nsize]   ;
  


  for (int i=0; i<n; i++) {
    fread(&(row[i])   , sizeof(int)      , 1    , f) ;
    fread(&(col[i]) , sizeof(int)      , 1    , f) ;
    fread(&(coeff[i]), sizeof(double), 1    , f) ;
  }
  fclose(f);
}

void Sparsa3A::inizializza(int N, double *c, int *i, int *j, double *pot_for_from  , double *pot_for_to ) {
  n=N;
  std::cout << "in inizializza " << pot_for_to << std::endl;
  dim=0;
  dim2=0;
  for(int k=0; k<N; k++) {
    if(dim<i[k]+1) dim=i[k]+1 ;
    if(dim2<j[k]+1) dim2=j[k]+1 ;
  }
  nsize=N+1000;
  coeff=new double[nsize];  
  col=new int[nsize]   ;
  row=new int[nsize]   ;
  memcpy(coeff,c,N*sizeof(double));
  memcpy(col, i, N*sizeof(int ));
  memcpy(row, j, N*sizeof(int ));


//   {
//     int cemeno=0;
//     for(int i=0; i<n;i++ ) {
//       if( col[i]==row[i] && coeff[i]<0) {
// 	cemeno=1;
//       }
//     }
//     if( cemeno) {
//       printf(" ce del meno sulla diagonale \n");
//     } else {
//       printf(" non ce del meno sulla diagonale \n");
//     }
//   }


  if(pot_for_from && pot_for_to ){
    std::cout << pot_for_from << std::endl;
    std::cout << pot_for_to   << std::endl;
    
    for(int k=0; k<N; k++) {
      std::cout << k << "  " << pot_for_from[ col[k]  ] << std::endl;
      coeff[k]=coeff[k]*( pot_for_from[col[k] ]+pot_for_to[row[k]] )*0.5;
    }
  }
  nG=0;
  Gmin=0;
  Gmax=0;  
};

void Sparsa3A::setpot( double *pot_for_from  , double *pot_for_to ) {
  if(pot_for_from && pot_for_to ){
    
    for(int k=0; k<n; k++) {
      coeff[k]=coeff[k]*( pot_for_from[col[k] ]+pot_for_to[row[k]] )*0.5;
    }
  }
};

void Sparsa3A::pulisci() {
  delete 	coeff ;
  delete 	col   ;
  delete 	row   ;

  n=0;
  dim=0;
  nsize=1000;

  coeff=new double[nsize];
  col=new int[nsize]   ;
  row=new int[nsize]   ;
};


Sparsa3A::~Sparsa3A() {
  std::cout << " DELETE di una sparsa\n";
	if(nsize) {
	  delete coeff  ;
	  delete col   ;
	  delete row  ;
	}

	if(nG) {
	  delete Gmin;
	  delete Gmax;
	}
	std::cout << " DELETE OK \n";
};


void Sparsa3A::inizializza( Sparsa3A &a ) {
  delete 	coeff ;
  delete 	col   ;
  delete 	row   ;

  n=a.n;

  dim =a.dim ;
  dim2=a.dim2;

  nsize=a.nsize;

  coeff=new double[nsize];
  col=new int[nsize]   ;
  row=new int[nsize]   ;

  memcpy(coeff, a.coeff, nsize*sizeof(double) );
  memcpy(col, a.col, nsize*sizeof(int) );
  memcpy(row, a.row, nsize*sizeof(int) );
}



void allocallt(int *&lltcol, int *&lltrow, double *&lltcoeff, int oldsize, int newsize){  
  int * newlltcol;
  newlltcol = new int[newsize];
  if(oldsize) {
    memcpy(newlltcol, lltcol, oldsize*sizeof(int));
    delete lltcol;
  }
  lltcol = newlltcol;
  
  int * newlltrow;
  newlltrow = new int[newsize];
  if(oldsize) {
    memcpy(newlltrow, lltrow, oldsize*sizeof(int));
    delete lltrow;
  }
  lltrow = newlltrow;
  
  double * newlltcoeff;
  newlltcoeff = new double[newsize];
  if(oldsize) {
    memcpy(newlltcoeff, lltcoeff, oldsize*sizeof(double));
    delete lltcoeff;
  }
  lltcoeff = newlltcoeff;
};


void allocallt2(int *&lltrow, double *&lltcoeff, int oldsize, int newsize){  

  int * newlltrow;
  newlltrow = new int[newsize];
  if(oldsize) {
    memcpy(newlltrow, lltrow, oldsize*sizeof(int));
    delete lltrow;
  }
  lltrow = newlltrow;
  
  double * newlltcoeff;
  newlltcoeff = new double[newsize];
  if(oldsize) {
    memcpy(newlltcoeff, lltcoeff, oldsize*sizeof(double));
    delete lltcoeff;
  }
  lltcoeff = newlltcoeff;
};



void     addtollt(int  idiag, int jdiag  , double newelcoeff ,  
		  int &lltn,int *&lltcol, int *&lltrow, double *&lltcoeff, int & lttsize, double tol ) {

  if ( fabs(newelcoeff)<=tol) return ;

  static int oi=-1, oj=-1;
  if( idiag<=oi && jdiag <=oj) {
    printf(" ripetizione %d %d %d %d  \n", idiag, jdiag, oi, oj);
    exit(0);
  }
  if( idiag<oi) {
    printf(" indietro %d %d %d %d  \n", idiag, jdiag, oi, oj);
    exit(0);
  }
  if( idiag==oi && jdiag<=oj) {
    printf(" indietro  2%d %d %d %d  \n", idiag, jdiag, oi, oj);
    exit(0);
  }
  oi=idiag;
  oj=jdiag;

		    if( lltn+1<lttsize) {
		    } else {
		      allocallt(lltcol, lltrow, lltcoeff, lttsize, lttsize+100000);
		      lttsize+=100000;
		    }
		    lltcol[lltn]= idiag; 
		    lltrow[lltn]= jdiag; 
		    lltcoeff[lltn]= newelcoeff; 
		    lltn+=1;
}



void Sparsa3A::LLtSolve(Array *Ares, Array *Avect  ) {

  double *res, *vect;
  res=Ares->dataAddress();
  vect=Avect->dataAddress();

	 

  for(int  idiag=0; idiag<dim; idiag++) {

    if(diagpos[idiag]==-1) continue;

    double sum=0.0;
    for(int i=lltstart[idiag] ; i<lltstart[idiag+1]-1; i++ ) {
      
      sum+= res[lltrow[  i  ]  ]  *   lltcoeff[   i  ]   ; 
    }
    res[idiag ] =(vect[idiag]-sum)/lltcoeff[ lltstart[idiag+1]-1  ];

  }


  for(int  idiag=dim-1; idiag>=0; idiag--) {
    if(diagpos[idiag]==-1) continue;
    double sum=0.0;
    for(int i=patternrowstart[idiag]+1; i<patternrowstart[idiag+1]; i++ ) {
      sum+= res[lltcol[ patternbyrow[ i ] ]  ]  *   lltcoeff[   patternbyrow[ i ] ]   ; 
    }
   
    res[idiag ] =(res[idiag]-sum)/lltcoeff[ patternbyrow[ patternrowstart[idiag]] ];

  }





}


class ndx4reo {
public:
  int indice;
  int subindice;
  static int * from;
  static int * to ;

  int operator= (const ndx4reo& ndx)  {
    indice = ndx.indice;
    subindice=ndx.subindice;
    return 1;
  };
  int operator== (const ndx4reo& ndx) const {
    if( from[indice]==ndx.from[ndx.indice] && to[indice]==ndx.to[ndx.indice] ) {
      return  subindice == ndx.subindice;;
    } else {
      return 0;
    }
    
  };
  
  int operator< (const ndx4reo & ndx) const{
    if( from[indice]<ndx.from[ndx.indice]  ) {
      return 1;
    } else if (  from[indice]==ndx.from[ndx.indice] ){
      if( to[indice]<ndx.to[ndx.indice] ) {
	return 1;
      } else if ( to[indice]==ndx.to[ndx.indice] ) {
	return subindice<ndx.subindice;
      } else {
	return 0;
      }
    } else {
      return 0;
    } 
  }
} ;
  
int * ndx4reo::from=0;
int * ndx4reo::to=0;



void Sparsa3A::riordina() {
  printf(" riordino \n");
  if(1)
  {

  ndx4reo::from = col;
  ndx4reo::to = row;
  



  int *scramble=new int[n];
  {
    for (int i=0; i<n; i++) {
      scramble[i]=i;
    }
    int j,tmp;
    for (int i=0; i<n; i++) {
      j=(int) ( n*random()*1.0/RAND_MAX ) ;
      tmp=scramble[i];
      scramble[i]=scramble[j];
      scramble[j]=tmp;
    }
  }

  std::map<ndx4reo,int> *ordinatore;
  ordinatore = new std::map<ndx4reo,int>[dim];
  ndx4reo  ndxo;
  for (int i=0; i<n; i++ ) {
    ndxo    .indice = scramble[i];
    ndxo.subindice = i;
    ordinatore[ndx4reo::from[   ndxo    .indice     ]][ ndxo ] =ndxo.indice;
  }
  
  delete scramble;

  std::map<ndx4reo,int>::iterator pos;

  int * newcol = new int [nsize];
  memset(newcol, 0, nsize*sizeof(int));
  int totn=0;
  for (int ic=0; ic<dim; ic++) {
    int n = ordinatore[ic].size();
    pos= ordinatore[ic].begin();
    for(int i=0; i<n; i++) {
      newcol[totn] = col[ pos->second ] ;
      pos++;
      totn++;
    }
  }
//   pos= ordinatore.begin();
//   for(int i=0; i<n; i++) {
//     newcol[i] = col[ pos->second ] ;
//     pos++;
//   }
  delete col;
  col=newcol;
  

  totn=0;
  int * newrow = new int [nsize];
  memset(newrow, 0, nsize*sizeof(int));
  for (int ic=0; ic<dim; ic++) {
    int n = ordinatore[ic].size();
    pos= ordinatore[ic].begin();
    for(int i=0; i<n; i++) {
      newrow[totn] = row[ pos->second ] ;
      pos++;
      totn++;
    }
  }
  delete row;
  row=newrow;
  
  
  
  double * newcoeff = new double [nsize];
  memset(newcoeff, 0, nsize*sizeof(double));

  totn=0;

  for (int ic=0; ic<dim; ic++) {
    int n = ordinatore[ic].size();
    pos= ordinatore[ic].begin();
    for(int i=0; i<n; i++) {
      newcoeff[totn] = coeff[ pos->second ] ;
      pos++;
      totn++;
    }
  }
  for(int i=0; i<dim; i++)  ordinatore[i].clear();
  delete [] ordinatore;

  delete coeff;
  coeff=newcoeff;
  
  // printf(" fatto riordina \n");
   
  int i, inew;
  i=0;
  inew=0;
  
  while(i<n) {
    col[inew]=col[i];
    row[inew]=row[i];
    coeff[inew]=coeff[i];
    i++;
    while(i<n && col[i]==col[inew]   && row[i]==row[inew]    ) {
	coeff[inew]+=coeff[i];
	i++;
    }
    inew++;
  }
  n=inew;
  }
  
}


void Sparsa3A::preparaLLt(double tol){
  fromstart = new int [ dim];
  patternrowstart = new int [dim+1];     
  diagpos = new int [dim];

  fromstart[0]=0;
  int actfrom=0;
  for(int i = 0 ; i<dim; i++ ) {
    fromstart[i]=0;
    diagpos[i]=-1;
  }
  for(int i = 0 ; i<n; i++ ) {
    // printf(" col[i] %d row[i] %d \n", col[i], row[i]);
    if( col[i]>actfrom) {
      actfrom=col[i] ;
      fromstart[actfrom]=i;
      diagpos[actfrom]=-1;
      
    } else if ( col[i]<actfrom ) {
      printf("problema col[i]<actfrom in LLt\n");
      exit(0);
    } 
    if(diagpos[actfrom]==-1) {
      if( row[i]> col[i]) {
	printf("problema  row[i]> col[i] con diagpos==-1 in LLt\n");
	exit(0);
      }
      if( row[i]==col[i]) {
	diagpos[actfrom]=i;
      };
    }
  }
  
  lltsize=100000;          
  lltcol=0, lltrow=0;    
  lltcoeff=0;  
  lltn=0;
  allocallt(lltcol, lltrow, lltcoeff, 0, lltsize);
  
  lltstart= new int [dim+1];
  
  
  double totsum=0;
  
  for ( int idiag=0; idiag<dim; idiag++) {
    printf("--------------------- idiag %d %d \n", idiag, dim );

    lltstart[idiag]=lltn;
      double dumA, dumB;
    
      // printf(" idiag %d \n", idiag);
    if( diagpos[idiag]==-1) continue;
    
    int lltn0=lltn;
    int oldjdiag=0;
    for(int is=fromstart[idiag]; is<=diagpos[idiag]; is++) {
      int newjdiag ;
      double target ;
      if(is==diagpos[idiag] ) {
	newjdiag = idiag-1;
	target=0;
      } else {
	newjdiag = row[is];
	target = coeff[is];
      }
      
      if (target>1000) target=0;
      

      for(int jdiag=oldjdiag ;  jdiag<=newjdiag; jdiag++) {
	int li, lj;
	li= lltstart[idiag];
	lj=lltstart[jdiag];
	double pregressum=0;
	if(jdiag==newjdiag) {
	  pregressum=target;
	}

	int dafare=1;
	if( jdiag==newjdiag) dafare=1;

	while(lj<lltstart[jdiag+1]-1  && li<lltn) {
	  if( lltrow[lj]==lltrow[li]) {

	    dumA=lltcoeff[lj];
	    dumB=lltcoeff[li];
	    if(dumA<1000 && dumB<1000) {
	      dafare=1;
	    }
	    if(dumA>1000) dumA-=2000;
	    if(dumB>1000) dumB-=2000;
	      


	    pregressum-= dumA*dumB;
	    lj++;
	    li++;
	  } else if(lltrow[lj]>lltrow[li] ) {
	    li++;
	  } else  {
	    lj++;
	  }
	}
	if( jdiag==newjdiag || pregressum!=0.0) {
	  double nuovo = (pregressum)/lltcoeff[ lltstart[jdiag+1]-1];

	  if( jdiag!=newjdiag) nuovo+=2000*0;

	  if(dafare) 
	    if( nuovo!=0.0) {

// 	      printf("idiag %d jdiag %d  nuovo %e \n", idiag, jdiag, fmod(nuovo+10*0 , 1000.0)-10*0 );
	      addtollt( idiag, jdiag  , nuovo,  lltn, lltcol, lltrow, lltcoeff, lltsize, tol);
	    }
	}
      }
      oldjdiag = newjdiag+1;
    }

    double pregressum=0;
    {
      for(int i=lltn0; i<lltn; i++ ) {
	    dumA=lltcoeff[i];
	    if(dumA>1000) dumA-=2000;
	pregressum+=  dumA*dumA;
      }
    }
    totsum+=pregressum;
    double newelcoeff;
    newelcoeff = sqrt( coeff[diagpos[idiag]]-pregressum );

    if( coeff[diagpos[idiag]]-pregressum<0) {
      printf("rotto \n");
      exit(0);
    }
    if( newelcoeff!=0.0) {
      printf("2 idiag %d jdiag %d  newelcoeff %e \n", idiag, idiag, newelcoeff );
      addtollt( idiag, idiag  , newelcoeff ,  lltn, lltcol, lltrow, lltcoeff, lltsize, 0.0);
      // printf("ltn %d -----------------------  %e \n", lltn,newelcoeff );
    }
  }
  lltstart[dim]=lltn;

  for(int i=0; i<lltn; i++) {
    if(lltcoeff[i]>1000) lltcoeff[i]-=2000;
  }


  // printf(" passo a pattern \n");
  patternbyrow = new int [lltn ];
  for(int i=0; i<lltn; i++ ) {
    patternbyrow [i]=i;
  }
  {
    int *scramble=new int[lltn];
    {
      for (int i=0; i<lltn; i++) {
	scramble[i]=i;
      }
      int j,tmp;
      for (int i=0; i<lltn; i++) {
	j=(int) ( lltn*random()*1.0/RAND_MAX ) ;
	tmp=scramble[i];
	scramble[i]=scramble[j];
	scramble[j]=tmp;
      }
    }
    
    std::map<double,int> byrow;
    std::map<double,int>::iterator rpos;
    int j;
    double tmp;					
    for(int i=0; i<lltn; i++) {
      j=scramble[i];
      j=i;

     
      tmp=lltrow[j]+lltcol[j]*1.0/dim;
      rpos = byrow.find(tmp);
      if( rpos!=byrow.end() ){
	printf(" c' e' %d %d  \n",lltrow[j],lltcol[j] );
	exit(0);
      }
      byrow[tmp]=j;
    }
    std::map<double,int>::iterator pos;
    pos=byrow.begin();

    for(int k=0; k<lltn; k++) {
      patternbyrow[k]=pos->second;
      pos++;
    }
    delete scramble;


//     printf(" ---------------------------------------------------\n");

//     for(int i=0; i<lltn; i++) {
//       printf("%d %d %e \n", lltrow[i], lltcol[i], lltcoeff[i]);
//     }

//     printf(" ------------------------------------\n");
//     for(int i=0; i<lltn; i++) {
//       int j=patternbyrow[i];
//       printf("%d %d %e \n", lltrow[j], lltcol[j], lltcoeff[j]);
//     }
//     // exit(0);

  }
  
  
  
//   int itmp;
//   printf(" bolla \n");
//   if(1) 
//   for(int i=0; i<lltn-1; i++) {
//     for(int j=0; j<lltn-1-i; j++ ) {
//       if( lltrow[ patternbyrow[j]]>lltrow[patternbyrow[j+1]]) {
// 	itmp = patternbyrow[j];
// 	patternbyrow[j]=patternbyrow[j+1];
// 	patternbyrow[j+1]=itmp;
//       }
//     }
//   }

  printf(" OK \n" );
  for(int i=0; i<dim; i++ ) {
    patternrowstart[i] =-1;
  }
  int irow;
  int oldrow = -1;
  for(int i=0; i<lltn; i++ ) {

    // printf("%d %d \n", i, lltn);
    //printf("row  %d col %d \n", lltrow[ patternbyrow[i]], lltcol[ patternbyrow[i]]);
    // printf("row  %d col %d  pattern %d     lttn %d \n", lltrow[ i], lltcol[i],patternbyrow[i],lltn);
    // continue;
    irow = lltrow[ patternbyrow[i]];
    if (patternrowstart[irow] ==-1) {
      for(int k=oldrow+1; k<=irow; k++ ) {
	patternrowstart[k]=i;
      }
      oldrow=irow;
      if(irow != lltcol[ patternbyrow[i]]) {
	printf("by row comincia male \n");
	exit(0);
      }
    };
    // printf("--\n");
  }
  // printf(" cuci \n");
  patternrowstart[dim]=lltn; // per finire 
  printf( " finito llt \n");

}



#ifdef Pippo

void Sparsa3A::preparaLLtComplex( Sparsa3A * mI ){

  Sparsa3A *mR = this, *mPtr;


  for(int volta=0; volta<2; volta++) {

    if( volta==0) mPtr = mR;
    else          mPtr = mI;

    mPtr->fromstart = new int [ dim];
    mPtr->patternrowstart = new int [dim];     
    mPtr->diagpos = new int [dim];

    mPtr->fromstart[0]=0;
    int actfrom=0;
    for(int i = 0 ; i<dim; i++ ) {
      mPtr->fromstart[i]=0;
      mPtr->diagpos[i]=-1;
    }
    for(int i = 0 ; i<n; i++ ) {
      printf(" col[i] %d row[i] %d \n", col[i], row[i]);
      if( mPtr->col[i]>actfrom) {
	actfrom=mPtr->col[i] ;
	mPtr->fromstart[actfrom]=i;
	mPtr->diagpos[actfrom]=-1;
	
      } else if ( mPtr->col[i]<actfrom ) {
	printf("problema col[i]<actfrom in LLt\n");
	exit(0);
      } 
      if(mPtr->diagpos[actfrom]==-1) {
	if( mPtr->row[i]> mPtr->col[i]) {
	  printf("problema  row[i]> col[i] con diagpos==-1 in LLt\n");
	  exit(0);
	}
	if( mPtr->row[i]==mPtr->col[i]) {
	  mPtr->diagpos[actfrom]=i;
	};
      }
    }
    
    mPtr->lltsize=100000;          
    mPtr->lltcol=0, lltrow=0;    
    mPtr->lltcoeff=0;  
    
    allocallt(mPtr->lltcol, mPtr->lltrow, mPtr->lltcoeff, 0, mPtr->lltsize);
    
    mPtr->lltstart= new int [dim+1];
    mPtr->lltn=0;
  }
    
  
  
  for ( int idiag=0; idiag<dim; idiag++) {
    
    mR->lltstart[idiag]=mR->lltn;
    mI->lltstart[idiag]=mI->lltn;
    
    if( mR->diagpos[idiag]==-1) continue;
    
    int lltn0=lltn;
    int Istart, Iend, Ipos;
    if( mI->diagpos[idiag]!=-1) {
      Istart = mI->fromstart[idiag];
      Iend   = mI->diagpos[idiag];
    } else {
      Istart=0;
      Iend =-1;
    }

    Ipos=Istart;
    for(int is= mR->fromstart[idiag]; is<mR->diagpos[idiag]; is++) {


      int jdiag = mR->row[is];
      double targetR = mR->coeff[is];
      double targetI;
      while(Ipos< mI->n &&  mI->row[Ipos]<jdiag) {
	Ipos++
      }
      if ( mI->row[Ipos]==jdiag ) {
	targetI = mI->coeff[Ipos]; 
      } else {
	targetI=0.0;
      }
      
      double pregressum[4];
      Sparsa3A * Aptr, *Bptr;
      
      for(int volta=0; volta<4; volta++) {
	
	pregressum[volta]=0.0;
	if( volta % 2 ) {
	  Bptr = mI;
	} else {
	  Bptr = mR;
	}
	if( volta / 2 ) {
	  Aptr = mI;
	} else {
	  Aptr = mR;
	}
	
	

	
	int li, lj;
	li= Aptr->lltstart[idiag];
	lj= Bptr->lltstart[jdiag];
	
	while(lj<Bptr->lltstart[jdiag+1]-1  && li<Aptr->lltn) {
	  
	  if( Bptr->lltrow[lj]==Aptr->lltrow[li]) {
	    pregressum[volta]+= Bptr->lltcoeff[lj]*Aptr->lltcoeff[li];
	    lj++;
	    li++;
	  } else if(Bptr->lltrow[lj]>Aptr->lltrow[li] ) {
	    li++;
	  } else  {
	    lj++;
	  }
	}
      }
      double nuovo;
      nuovo = (targetR- (pregressum[0]+pregressum[3])        )/mR->lltcoeff[ lltstart[jdiag+1]-1];

      addtollt( idiag, jdiag  , nuovo,  mR->lltn, mR->lltcol, mR->lltrow, mR->lltcoeff, mR->lltsize);

      nuovo = (targetI- (pregressum[2]-pregressum[1])        )/mR->lltcoeff[ lltstart[jdiag+1]-1];

      addtollt( idiag, jdiag  , nuovo,  mI->lltn, mI->lltcol, mI->lltrow, mI->lltcoeff, mI->lltsize);

    }
  
    double pregressum=0;
    {
      for(int i=mR->lltstart[idiag] ;  i<mR->lltn ; i++ ) {
	pregressum+=  mR->lltcoeff[i]*mR->lltcoeff[i];
      }

      for(int i=mI->lltstart[idiag]; i<mI->lltn; i++ ) {
	pregressum+=  mI->lltcoeff[i]*mI->lltcoeff[i];
      }
    }

    double newelcoeff;
    newelcoeff = sqrt(  mR->coeff[ mR->diagpos[idiag]]-pregressum );
    addtollt( idiag, idiag  , newelcoeff ,   mR->lltn,  mR->lltcol,  mR->lltrow,  mR->lltcoeff,  mR->lltsize);
  }

  Sparsa3A *mR = this, *mPtr;
  
  
  for(int volta=0; volta<2; volta++) {
    
    if( volta==0) mPtr = mR;
    else          mPtr = mI;
    

    mPtr->patternbyrow = new int [mPtr->lltn ];
    for(int i=0; i<mPtr->lltn; i++ ) {
      mPtr->patternbyrow [i]=i;
    }
    int itmp;
    for(int i=0; i<mPtr->lltn-1; i++) {
      for(int j=0; j<mPtr->lltn-1-i; j++ ) {
	if( mPtr->lltrow[ mPtr->patternbyrow[j]]>mPtr->lltrow[mPtr->patternbyrow[j+1]]) {
	  itmp = mPtr->patternbyrow[j];
	  mPtr->patternbyrow[j]=mPtr->patternbyrow[j+1];
	  mPtr->patternbyrow[j+1]=itmp;
	}
      }
    }
    for(int i=0; i<dim; i++ ) {
      mPtr->patternrowstart[i] =-1;
    }
    int irow;
    int oldrow = -1;
    for(int i=0; i<mPtr->lltn; i++ ) {
      irow = mPtr->lltrow[ mPtr->patternbyrow[i]];
      if (mPtr->patternrowstart[irow] ==-1) {
	for(int k=oldrow+1; k<=irow; k++ ) {
	  mPtr->patternrowstart[k]=i;
	}
	oldrow=irow;
	if(irow != mPtr->lltcol[ mPtr->patternbyrow[i]]) {
	  printf("by row comincia male \n");
	  exit(0);
	}
      };
    }
    mPtr->patternrowstart[dim]=mPtr->lltn; // per finire
  }
}


#endif





void Sparsa3A::aggiungimemoria() {
  double *newcoeff;
  int    *newcol;
  int    *newrow;

  newcoeff =  new double [ nsize+1000]      ;
  newcol   =  new int    [ nsize+1000]      ;
  newrow   =  new int    [ nsize+1000]      ;
  
  memcpy(newcoeff, coeff, nsize*sizeof(double));
  memcpy(newcol,   col  , nsize*sizeof(int));
  memcpy(newrow,   row  , nsize*sizeof(int));

  delete col;
  delete row;
  delete coeff;

  col = newcol;
  row = newrow;
  coeff = newcoeff;

  nsize = nsize + 1000;
}


double scalare( int n, double * a, double *b) {
  return scalare(a, b,  n);
}

double sqrtscalare(double * a, double *b, int n) {
  double d;
  d=scalare(a,b,n);
  return sqrt(d);
}

double scalare(double * a, double *b, int n) {
	double ris=0.0;
#ifdef _UNROLL_
  // int end;
  // end=n/4;
  double r1,r2,r3,r4;
  double b1,b2,b3,b4;
  double a1,a2,a3,a4;
  int ci=0;

  for(int i=0; i<n/4; i++)
    {
	  a1=a[ci];
	  a2=a[ci+1];
	  a3=a[ci+2];
	  a4=a[ci+3];
	
	  b1=b[ci];
	  b2=b[ci+1];
	  b3=b[ci+2];
	  b4=b[ci+3];
	
	  r1=a1*b1;
	  r2=a2*b2;
	  r3=a3*b3;
	  r4=a4*b4;
	
	  ris=ris+r1+r2+r3+r4;	
      ci+=4;
    }

  for(int i=4*(n/4); i<n; i++)
    {
      ris  += a[i]*b[i];
    }

#else
  for(int i=0; i<n; i++)
    {
       ris  += a[i]*b[i];
    }
#endif
	return ris;
}




void normalizzaauto(double * a,  int n) {
  double b=sqrt(scalare(a,a,n));
  normalizza(a,b,n);
}

void normalizza(double * a, double b, int n) {
#ifdef _UNROLL_
  // int end;
  // end=n/4;
  int ci=0;

  for(int i=0; i<n/4; i++)
    {
	  a[ci]=a[ci]/b;
	
 	  a[ci+1]=a[ci+1]/b;
 	  a[ci+2]=a[ci+2]/b;
 	  a[ci+3]=a[ci+3]/b;
      ci+=4;
    }

  for(int i=4*(n/4); i<n; i++)
    {
	  a[i]=a[i]/b;
    }

#else
  for(int i=0; i<n; i++)
    {
	  a[i]=a[i]/b;
    }
#endif
}


void Sparsa3A::Moltiplica(Array *ris, Array *vect  ) {
  this->Moltiplica(ris->dataAddress(), vect->dataAddress() );
}
void Sparsa3A::MoltiplicaMinus(Array *ris, Array *vect  ) {
  this->MoltiplicaMinus(ris->dataAddress(), vect->dataAddress() );
}
void Sparsa3A::MoltiplicaDiag(Array *ris, Array *vect  ) {
   this->MoltiplicaDiag(ris->dataAddress(), vect->dataAddress() );
}
void  Sparsa3A::MoltiplicaDiagMinus(Array * ris, Array*vect  ){
  this->MoltiplicaDiagMinus(ris->dataAddress(), vect->dataAddress() );
}

void Sparsa3A::Moltiplica(double *ris, double *vect  )
{

#ifdef _UNROLL_
  // int end;
  // end=n/4;
  double v1,v2,v3,v4;
  double c1,c2,c3,c4;
  double a1,a2,a3,a4;
  int ci=0;

  for(int i=0; i<n/4; i++)
    {

      v1=vect[col[ci  ]];
      v2=vect[col[ci+1]];
      v3=vect[col[ci+2]];
      v4=vect[col[ci+3]];

      c1 = coeff[ci  ];
      c2 = coeff[ci+1];
      c3 = coeff[ci+2];
      c4 = coeff[ci+3];

      a1 = c1*v1;
      a2 = c2*v2;
      a3 = c3*v3;
      a4 = c4*v4;

      ris[row[ci  ]]+=a1;
      ris[row[ci+1]]+=a2;
      ris[row[ci+2]]+=a3;
      ris[row[ci+3]]+=a4;

      ci+=4;
    }

  for(int i=4*(n/4); i<n; i++)
    {
      ris[row[i]] += coeff[i]*vect[col[i]];
    }

#else
  // printf(" dim dim2 %d %d  \n", dim, dim2);
  if( 0 && dim==dim2) {
    DEBUG(printf(" simmetrizzato \n");)
    for(int i=0; i<n; i++)
      {
	ris[row[i]] += 0.5*coeff[i]*vect[col[i]];
      }
    for(int i=0; i<n; i++)
      {
	ris[col[i]] += 0.5*coeff[i]*vect[row[i]];
      }
  } else {
    for(int i=0; i<n; i++)
      {
	ris[row[i]] += coeff[i]*vect[col[i]];
      }
  }
#endif
};



void Moltiplica(double *ris, double *vect , int n , int *row, int *col, double *coeff )
{

#ifdef _UNROLL_
  // int end;
  // end=n/4;
  double v1,v2,v3,v4;
  double c1,c2,c3,c4;
  double a1,a2,a3,a4;
  int ci=0;

  for(int i=0; i<n/4; i++)
    {

      v1=vect[col[ci  ]];
      v2=vect[col[ci+1]];
      v3=vect[col[ci+2]];
      v4=vect[col[ci+3]];

      c1 = coeff[ci  ];
      c2 = coeff[ci+1];
      c3 = coeff[ci+2];
      c4 = coeff[ci+3];

      a1 = c1*v1;
      a2 = c2*v2;
      a3 = c3*v3;
      a4 = c4*v4;

      ris[row[ci  ]]+=a1;
      ris[row[ci+1]]+=a2;
      ris[row[ci+2]]+=a3;
      ris[row[ci+3]]+=a4;

      ci+=4;
    }

  for(int i=4*(n/4); i<n; i++)
    {
      ris[row[i]] += coeff[i]*vect[col[i]];
    }

#else
  {
    for(int i=0; i<n; i++)
      {
	ris[row[i]] += coeff[i]*vect[col[i]];
      }
  }
#endif
};


void Moltiplica(double *ris, double *vect , int n , int *row, int *col, float *coeff )
{

#ifdef _UNROLL_
  // int end;
  // end=n/4;
  double v1,v2,v3,v4;
  double c1,c2,c3,c4;
  double a1,a2,a3,a4;
  int ci=0;

  for(int i=0; i<n/4; i++)
    {

      v1=vect[col[ci  ]];
      v2=vect[col[ci+1]];
      v3=vect[col[ci+2]];
      v4=vect[col[ci+3]];

      c1 = coeff[ci  ];
      c2 = coeff[ci+1];
      c3 = coeff[ci+2];
      c4 = coeff[ci+3];

      a1 = c1*v1;
      a2 = c2*v2;
      a3 = c3*v3;
      a4 = c4*v4;

      ris[row[ci  ]]+=a1;
      ris[row[ci+1]]+=a2;
      ris[row[ci+2]]+=a3;
      ris[row[ci+3]]+=a4;

      ci+=4;
    }

  for(int i=4*(n/4); i<n; i++)
    {
      ris[row[i]] += coeff[i]*vect[col[i]];
    }

#else
  {
    for(int i=0; i<n; i++)
      {
	ris[row[i]] += coeff[i]*vect[col[i]];
      }
  }
#endif
};



void MoltiplicaMinus(double *ris, double *vect , int n , int *row, int *col, double *coeff )
{

#ifdef _UNROLL_
  // int end;
  // end=n/4;
  double v1,v2,v3,v4;
  double c1,c2,c3,c4;
  double a1,a2,a3,a4;
  int ci=0;

  for(int i=0; i<n/4; i++)
    {

      v1=vect[col[ci  ]];
      v2=vect[col[ci+1]];
      v3=vect[col[ci+2]];
      v4=vect[col[ci+3]];

      c1 = coeff[ci  ];
      c2 = coeff[ci+1];
      c3 = coeff[ci+2];
      c4 = coeff[ci+3];

      a1 = c1*v1;
      a2 = c2*v2;
      a3 = c3*v3;
      a4 = c4*v4;

      ris[row[ci  ]]-=a1;
      ris[row[ci+1]]-=a2;
      ris[row[ci+2]]-=a3;
      ris[row[ci+3]]-=a4;

      ci+=4;
    }

  for(int i=4*(n/4); i<n; i++)
    {
      ris[row[i]] -= coeff[i]*vect[col[i]];
    }

#else
  {
    for(int i=0; i<n; i++)
      {
	ris[row[i]] -= coeff[i]*vect[col[i]];
      }
  }
#endif
};



void Sparsa3A::MoltiplicaMinus(double *ris, double *vect  )
{
#ifdef _UNROLL_
  // int end;
  // end=n/4;
  double v1,v2,v3,v4;
  double c1,c2,c3,c4;
  double a1,a2,a3,a4;
  int ci=0;

//   int *col, *row;
//   col=this->row;
//   row=this->col;

  for(int i=0; i<n/4; i++)
    {

      v1=vect[col[ci  ]];
      v2=vect[col[ci+1]];
      v3=vect[col[ci+2]];
      v4=vect[col[ci+3]];

      c1 = coeff[ci  ];
      c2 = coeff[ci+1];
      c3 = coeff[ci+2];
      c4 = coeff[ci+3];

      a1 = c1*v1;
      a2 = c2*v2;
      a3 = c3*v3;
      a4 = c4*v4;

      ris[row[ci  ]]-=a1;
      ris[row[ci+1]]-=a2;
      ris[row[ci+2]]-=a3;
      ris[row[ci+3]]-=a4;

      ci+=4;
    }

  for(int i=4*(n/4); i<n; i++)
    {
      ris[row[i]] -= coeff[i]*vect[col[i]];
    }

#else
  // printf(" dim dim2 %d %d  \n", dim, dim2);
  if( 0 && dim==dim2) {
    DEBUG(printf(" simmetrizzato \n");)
    for(int i=0; i<n; i++)
      {
	ris[row[i]] -= 0.5*coeff[i]*vect[col[i]];
      }
    for(int i=0; i<n; i++)
      {
	ris[col[i]] -= 0.5*coeff[i]*vect[row[i]];
      }
  } else {
    for(int i=0; i<n; i++)
      {
	ris[row[i]] -= coeff[i]*vect[col[i]];
      }
  }
#endif
};




void Sparsa3A::MoltiplicaDiag(double *ris, double *vect  )
{
  
 
    for(int i=0; i<n; i++)
      {
	if( row[i]==col[i])	ris[row[i]] += coeff[i]*vect[col[i]];
      }
 

};


void Sparsa3A::MoltiplicaDiagMinus(double *ris, double *vect  )
{
  
 
    for(int i=0; i<n; i++)
      {
	if( row[i]==col[i])	ris[row[i]] -= coeff[i]*vect[col[i]];
      }
 

};








void Sparsa3A::Moltiplica(double *ris, double *vect , int start, int end )
{
#ifdef _UNROLL_
  int passages;
  passages=(end-start)/4;
  double v1,v2,v3,v4;
  double c1,c2,c3,c4;
  double a1,a2,a3,a4;
  int ci=start;

  for(int i=0; i<passages; i++)
    {

      v1=vect[col[ci  ]];
      v2=vect[col[ci+1]];
      v3=vect[col[ci+2]];
      v4=vect[col[ci+3]];

      c1 = coeff[ci  ];
      c2 = coeff[ci+1];
      c3 = coeff[ci+2];
      c4 = coeff[ci+3];

      a1 = c1*v1;
      a2 = c2*v2;
      a3 = c3*v3;
      a4 = c4*v4;

      ris[row[ci  ]]+=a1;
      ris[row[ci+1]]+=a2;
      ris[row[ci+2]]+=a3;
      ris[row[ci+3]]+=a4;

      ci+=4;
    }

  for(int i=ci; i<end; i++)
    {
      ris[row[i]] += coeff[i]*vect[col[i]];
    }
  
#else
  for(int i=start; i<end; i++)
    {
      ris[row[i]] += coeff[i]*vect[col[i]];
    }
#endif
};



void Sparsa3A::gohersch()
{
  if(nG) {
		delete Gmin;
		delete Gmax;
  }
	nG=2*dim;
	Gmin= new double[nG];		
	Gmax= new double[nG];	
	
	for(int i=0; i<nG;i++) {
		Gmin[i]=Gmax[i]=0.0;
	}
	
	for(int i=0; i<n;i++) {
		if(row[i]==col[i]) {
			Gmin[col[i]]=Gmax[col[i]]=coeff[i];
		}
	}
	for(int i=0; i<n;i++) {
		if(row[i]!=col[i]) {
			Gmin[col[i]]-=fabs( coeff[i] );
			Gmax[col[i]]+=fabs( coeff[i] );
		}
	}
}

double Sparsa3A::goherschMin()
{
	double res=0;
	for(int i=0; i<nG;i++) {
		if(i==0) res=Gmin[i];
		if(res>Gmin[i]) res=Gmin[i];
	}
	return res;
}

double Sparsa3A::goherschMax()
{
	double res=0;
	for(int i=0; i<nG;i++) {
		if(i==0) res=Gmax[i];
		if(res<Gmax[i]) res=Gmax[i];
	}
	return res;

}

void Sparsa3A::trasforma(double fattore, double addendo) {
  int *fatto;
  fatto = new int [ dim];
  for(int i=0; i<dim; i++) {
    fatto[i]=0;
  }
  for(int i=0; i<n; i++) {
    coeff[i]=fattore*coeff[i];
    if(col[i]==row[i] && fatto[col[i]]==0 )  {
      coeff[i]+=addendo;
      fatto[col[i]]=1;
    }
  }
  for(int i=0; i<dim; i++) {
    if(!fatto[i]) {
      {
	double a;
	int r;
	int c;
	a=addendo;
	r=i;
	c=i;
	
	if(n==nsize) aggiungimemoria();
	row[n]=r;
	col[n]=c;
	
	if(r>dim-1)  dim=r+1;
	if(c>dim-1) dim=c+1;
	
	coeff[n]=a ;
	n++;
      }
    }
  }
  delete fatto;
}

  
extern "C" {
  void dsyev_( char *jobz, char *uplo, int *N, double *a, int *lda, double *w,
	       double *work, int *lwork, int *info );

  void dgemm_( char *transa, char *transb, int *m, int *n, int *k, 
	       double *alpha, double *a, int *Ida, double *b, int *Idb, 
	       double *beta, double* c, int *Idc );
  

}

void diagonalizza4py(int k, int m, double *alpha,  double *beta, double *pevect, double *peval)
{


  double * A = new double [ m*m];
	

  memset( pevect, 0, m*m*sizeof(double) );
  memset( peval , 0, m*m*sizeof(double) );
  memset( A , 0, m*m*sizeof(double) );



#define A(i,j)    A[ (i)*m+(j)]
#define peval(i,j)    peval[ (i)*m+(j)]
#define pevect(i,j)    pevect[ (i)*m+(j)]

  
  
  for(int i=0; i<m;i++)
    {
      A(i,i)=alpha[i];
    }
  for(int i=0;  i<k ; i++)
    {
      A(k,i)=A(i,k)=beta[i];
    }	
  for(int i=k; i<m-1; i++)
    {
      A(i,i+1)=A(i+1,i)=beta[i];
    }	
  
   char jobz = 'V';
   char uplo = 'U';
   int N = m;
   int lda = m;
   int lwork = 3*N-1;
   double *work = new double[lwork];
   int info;

   double w[N];

   dsyev_( &jobz, &uplo, &N, A, &lda, w,
           work, &lwork, &info );


   for (int i = 0; i < N; i++ )
   {
      peval(i,i) = w[i];
   }
//    for(int i=0; i<N; i++) {
//      for (int j=0; j<N; j++) {
//        pevect(i,j)=A(j,i);
//      }
//    }

   memcpy( pevect , A , m*m*sizeof(double)); 

   delete[] work;

#undef A
#undef peva
#undef pevect
}






double scalareA( double *a , double *b, int n ) {
  return scalare(a,b,n);
}



double sqrtscalareA( double *a , double *b, int n ) {
  return sqrtscalare(a,b,n);
}

void normalizzaA(double *a, double b, int c) {
  normalizza(a,b,c);
}
void normalizzaautoA(double *a,  int c) {
  normalizzaauto(a,c);
}



void Array::normalizza( double norm ){
  normalizzaA(data, norm, size   );
}
void Array::normalizzaauto(  ){
  normalizzaautoA(data,  size   );
}

double Array::scalare(Array &a, Array &b){
  if( a.size != b.size) {
    throw std::out_of_range("frames are not aligned");   
  }
  return scalareA( a.dataAddress(), b.dataAddress() , a.len());
}

void  Array::dividebyarray( Array &b){
  if( this->size != b.size) {
    throw std::out_of_range("frames are not aligned");   
  }
  for(int i=0; i<this->size; i++) {
    this->data[i]/=b.data[i];
  }
}
void  Array::multbyarray( Array &b){
//   if( this->size != b.size) {
//     throw std::out_of_range("frames are not aligned");   
//   }
  int loopend;
  loopend =Min(this->size, b.size) ; 
  for(int i=0; i<loopend; i++) {
    this->data[i]*=b.data[i];
  }
}


void  Array::set_indices(int N, int *indices, double *values){
  if(  N != len()) {
    throw std::out_of_range("   int  void  Array::set_indices(int N, int indices, double *values)");   
  }
  double *d= dataAddress();
  for (int i=0; i<N; i++) {
    d[i]=values[indices[i]];
  }
}



void  Array::set_indices_inv(int N, int *indices, double *values){
  if(  N != len()) {
    throw std::out_of_range("   int  void  Array::set_indices(int N, int indices, double *values)");   
  }
  double *d=  dataAddress();
  for (int i=0; i<N; i++) {
    values[indices[i]]=d[i];
  }
}



double Array::sqrtscalare(Array &a, Array &b){
  if( a.size != b.size) {
    throw std::out_of_range("frames are not aligned");   
  }
  return sqrtscalareA( a.dataAddress(), b.dataAddress() , a.len());
}


void  Array::mat_mult(Array & b, double * mat,Array & a) {
  int m=a.firstdimension-1;
  int k=b.firstdimension;
  int dim = a.size/a.firstdimension;

  // uMatrix<double> E(m, k,  mat);
//   uMatrix<double> A(dim, m, a.data );
//   uMatrix<double> B(dim, k, b.data );
  
//   ::dumptofile(mat, m*k, "bloccovectpy");
   
  char transa='N', transb='N';
  double alpha = double(1.0);
  double beta = double(0.0);
  
  dgemm_( &transa, &transb, &dim, &k, &m, &alpha, a.data, &dim, 
           mat, &m, &beta, b.data, &dim );
   
}

void  Array::get_numarrayview(int start, int  N,  double *&ArrayFLOAT) {
  ArrayFLOAT =  this->data+start ;
}


#ifdef SPARSAMPI
#define MSGLEN 200
 inizializzaMPI::inizializzaMPI() {
  for (int i=0; i< MAXNSPARSA3AP; i++) {
    Sparsa3AP::items[i]=0;
  }
  for (int i=0; i< MAXNSPARSA3AP; i++) {
    Sparsa3AP2::items[i]=0;
  }
  for (int i=0; i<MAXNVECTORP ; i++) {
    VectorP::items[i]=0;
  }
  for (int i=0; i<MAXNVECTORP ; i++) {
    VectorP2::items[i]=0;
  }
  int myid, numprocs;
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  if(myid!=0) {
    while(1) {
      char buffer[MSGLEN];
      MPI_Status status;
      int id2dest;
      DEBUG(printf("aspetto il comando \n");)
      MPI_Recv(buffer,MSGLEN , MPI_CHAR, 0 , 99, MPI_COMM_WORLD,&status);
      DEBUG(printf("nel processo %d il comando est %s \n",myid ,  buffer);)

      if( strcmp( buffer,"creaS")==0 ) {

	new Sparsa3AP;


      } else      if( strcmp( buffer,"creaS2")==0 ) {

	new Sparsa3AP2;


      } else if( strcmp( buffer,"creaV")==0 ) {

	new VectorP;


      }else if( strcmp( buffer,"creaV2")==0 ) {

	new VectorP2(0, NULL );


      } else if( strcmp( buffer,"preparapreco")==0 ) {

	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->preparaPreco_();

      }else if( strcmp( buffer,"preparaprecoexp")==0 ) {

	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->preparaPrecoExp_();

      }else if( strcmp( buffer,"distruggiS")==0 ) {

	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	delete Sparsa3AP::items[id2dest];
	Sparsa3AP::items[id2dest] = 0 ;
	DEBUG(printf("nel processo %d ho distrutto l' item  %d il comando era %s \n",myid , id2dest, buffer );)

  
      } else if( strcmp( buffer,"distruggiS2")==0 ) {

	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	delete Sparsa3AP2::items[id2dest];
	Sparsa3AP2::items[id2dest] = 0 ;
	DEBUG(printf("nel processo %d ho distrutto l' item  %d il comando era %s \n",myid , id2dest, buffer );)

  
      }else if( strcmp( buffer,"expandi")==0 ) {
	DEBUG(printf( " ho ricevuto buffer %s  proc = %d\n ", buffer, myid );)

	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->expandimpi();

      } else if( strcmp( buffer,"expandiMult")==0 ) {
	DEBUG(printf( " ho ricevuto buffer %s  proc = %d\n ", buffer, myid );)
	int a, b;
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&a,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	MPI_Recv(&b,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->expandimpi(a,b);
      } else if( strcmp( buffer,"fissaexp")==0 ) {
	DEBUG(printf( " ho ricevuto buffer %s  proc = %d\n ", buffer, myid );)
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->fissaexp();
      }else if( strcmp( buffer,"scalareexp")==0 ) {
	int a, b;
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&a,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	MPI_Recv(&b,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->scalareexp( a,b);
      }else if( strcmp( buffer,"expPreco")==0 ) {
	int a, b;
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&a,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	MPI_Recv(&b,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->expPreco( a,b);
      } else if( strcmp( buffer,"expcopy")==0 ) {
	int a, b;
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&a,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	MPI_Recv(&b,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->expcopy( a,b);
      } else if( strcmp( buffer,"expsettozero")==0 ) {
	int a;
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&a,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->expsettozero( a);
      } else if( strcmp( buffer,"expsettodiag")==0 ) {
	int a;
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&a,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->expsettodiag( a);
      }else if( strcmp( buffer,"expsum")==0 ) {
	int a,b,c;
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&a,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	MPI_Recv(&b,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&c,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->expsum( a,b,c);
      }else if( strcmp( buffer,"expsub")==0 ) {
	int a,b,c;
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&a,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	MPI_Recv(&b,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&c,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->expsub( a,b,c);
      }else if( strcmp( buffer,"expmultbyfact")==0 ) {
	int a,b;
	float c;
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&a,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	MPI_Recv(&b,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&c,1 , MPI_FLOAT, 0 , 102, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->expmultbyfact( a,b,c);
      }else if( strcmp( buffer,"expgensum")==0 ) {
	int a,b,c;
	float B,C;
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&a,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	MPI_Recv(&b,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&c,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	MPI_Recv(&B,1 , MPI_FLOAT, 0 , 103, MPI_COMM_WORLD,&status);
	MPI_Recv(&C,1 , MPI_FLOAT, 0 , 104, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[id2dest]->expgensum( a,b,c,B,C);
      }





      else if( strcmp( buffer,"distruggiV")==0 ) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	delete VectorP::items[id2dest];
	VectorP::items[id2dest] = 0 ;
	DEBUG(printf("nel processo %d ho distrutto l' item VectorP  %d il comando era %s \n",myid , id2dest, buffer );  )
      } else if( strcmp( buffer,"distruggiV2")==0 ) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	delete VectorP2::items[id2dest];
	VectorP2::items[id2dest] = 0 ;
	DEBUG(printf("nel processo %d ho distrutto l' item VectorP  %d il comando era %s \n",myid , id2dest, buffer );  )
      } else if( strcmp( buffer,"set_all_random")==0 ) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	double value;
	MPI_Recv(&value, 1, MPI_DOUBLE, 0, 100, MPI_COMM_WORLD, &status);
	VectorP::items[id2dest]->set_all_random(value);
	DEBUG(printf("nel processo %d, id era %d   il comando era %s \n",myid , id2dest, buffer );  )
      } else if( strcmp( buffer,"set_all_random2")==0 ) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	double value;
	MPI_Recv(&value, 1, MPI_DOUBLE, 0, 100, MPI_COMM_WORLD, &status);
	VectorP2::items[id2dest]->set_all_random(value);
	DEBUG(printf("nel processo %d, id era %d   il comando era %s \n",myid , id2dest, buffer );  )
      } else if( strcmp( buffer,"set_to_zero")==0 ) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	VectorP::items[id2dest]->set_to_zero();
	DEBUG(printf("nel processo %d, id era %d   il comando era %s \n",myid , id2dest, buffer );  )
      }else if( strcmp( buffer,"set_to_zero2")==0 ) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	VectorP2::items[id2dest]->set_to_zero();
	DEBUG(printf("nel processo %d, id era %d   il comando era %s \n",myid , id2dest, buffer );  )
      }else if( strcmp( buffer,"set_to_one")==0 ) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	VectorP::items[id2dest]->set_to_one();
	DEBUG(printf("nel processo %d, id era %d   il comando era %s \n",myid , id2dest, buffer );  )
      }else if( strcmp( buffer,"set_to_one2")==0 ) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	VectorP2::items[id2dest]->set_to_one();
	DEBUG(printf("nel processo %d, id era %d   il comando era %s \n",myid , id2dest, buffer );  )
      }else if( strcmp( buffer,"scriviArray")==0 ) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	VectorP::items[id2dest]->scriviArray();
	DEBUG(printf("nel processo %d, id era %d   il comando era %s \n",myid , id2dest, buffer );  )
      }else if( strcmp( buffer,"scriviArray2")==0 ) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	
	char nomefile[MSGLEN];
	int start;
	int dim;
	MPI_Recv(nomefile,MSGLEN , MPI_CHAR, 0 , 103, MPI_COMM_WORLD,&status);
	MPI_Recv(&start,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	MPI_Recv(&dim  ,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);

	VectorP2::items[id2dest]->scriviArray(nomefile, start, dim);
	DEBUG(printf("nel processo %d, id era %d   il comando era %s \n",myid , id2dest, buffer );  )
      } else if( strcmp( buffer,"copy_to_a_from_b")==0 ) {
	int ida, idb;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	VectorP::copy_to_a_from_b(VectorP::items[ida],VectorP::items[idb] ) ;
	DEBUG(printf("nel processo %d, id era %d , %d  il comando era %s \n",myid , ida, idb, buffer );  )
      } else if( strcmp( buffer,"copy_to_a_from_b2")==0 ) {
	int ida, idb;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	VectorP2::copy_to_a_from_b(VectorP2::items[ida],VectorP2::items[idb] ) ;
	DEBUG(printf("nel processo %d, id era %d , %d  il comando era %s \n",myid , ida, idb, buffer );  )
      } else if( strcmp( buffer,"add_from_vect")==0 ) {
	int ida, idb;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	VectorP::items[ida]->add_from_vect(VectorP::items[idb] ) ;
	DEBUG(printf("nel processo %d, id era %d , %d  il comando era %s \n",myid , ida, idb, buffer );  )
      } else if( strcmp( buffer,"add_from_vect2")==0 ) {
	int ida, idb;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	VectorP2::items[ida]->add_from_vect(VectorP2::items[idb] ) ;
	DEBUG(printf("nel processo %d, id era %d , %d  il comando era %s \n",myid , ida, idb, buffer );  )
      } else if( strcmp( buffer,"dividebyarray")==0 ) {
	int ida, idb;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	VectorP::items[ida]->dividebyarray(VectorP::items[idb] ) ;
	DEBUG(printf("nel processo %d, id era %d , %d  il comando era %s \n",myid , ida, idb, buffer );  )
      }else if( strcmp( buffer,"dividebyarray2")==0 ) {
	int ida, idb;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	VectorP2::items[ida]->dividebyarray(VectorP2::items[idb] ) ;
	DEBUG(printf("nel processo %d, id era %d , %d  il comando era %s \n",myid , ida, idb, buffer );  )
      }else if( strcmp( buffer,"multbyarray2")==0 ) {
	int ida, idb;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	VectorP2::items[ida]->multbyarray(VectorP2::items[idb] ) ;
	DEBUG(printf("nel processo %d, id era %d , %d  il comando era %s \n",myid , ida, idb, buffer );  )
      }else if( strcmp( buffer,"scalare")==0 ) {
	int ida, idb;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	DEBUG(printf("nel processo %d, id est  %d , %d  il comando est %s \n",myid , ida, idb, buffer );  )
	scalarP( VectorP::items[ida],VectorP::items[idb] ) ;
	DEBUG(printf("nel processo %d, id era %d , %d  il comando era %s \n",myid , ida, idb, buffer );  )

      } else if( strcmp( buffer,"scalare2")==0 ) {
	int ida, idb;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);
	DEBUG(printf("nel processo %d, id est  %d , %d  il comando est %s \n",myid , ida, idb, buffer );  )
	scalarP2( VectorP2::items[ida],VectorP2::items[idb] ) ;
	DEBUG(printf("nel processo %d, id era %d , %d  il comando era %s \n",myid , ida, idb, buffer );  )

      } else if( strcmp( buffer,"scalare2multi")==0 ) {
	int  idb;
	double *resdum;
	int n;
	MPI_Recv(&n, 1, MPI_INT, 0, 97, MPI_COMM_WORLD,&status);
	int ids[n];
	
	MPI_Recv(ids,n , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);

	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);

	scalarP2multi(ids, VectorP2::items[idb] ,  n, resdum); 
	
      } else if( strcmp( buffer,"add_from_vect_with_fact")==0 ) {

	int ida, idb;
	double fact;

	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);

	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);

	MPI_Recv(&fact,1 , MPI_DOUBLE, 0 , 101, MPI_COMM_WORLD,&status);

	VectorP::items[ida]->add_from_vect_with_fact(VectorP::items[idb], fact ) ;



      }else if( strcmp( buffer,"add_from_vect_with_fact2")==0 ) {

	int ida, idb;
	double fact;

	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);

	MPI_Recv(&idb,1 , MPI_INT, 0 , 100, MPI_COMM_WORLD,&status);

	MPI_Recv(&fact,1 , MPI_DOUBLE, 0 , 101, MPI_COMM_WORLD,&status);

	VectorP2::items[ida]->add_from_vect_with_fact(VectorP2::items[idb], fact ) ;



      }else if( strcmp( buffer,"add_from_vect_with_fact2_multi")==0 ) {

	int ida, idb;

	int n;

	MPI_Recv(&n, 1, MPI_INT, 0 , 97, MPI_COMM_WORLD, &status);
	int ids[n];

	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);

	MPI_Recv(ids,n , MPI_INT, 0 , 99, MPI_COMM_WORLD,&status);

	double fact[n];

	MPI_Recv(fact,n , MPI_DOUBLE, 0 , 101, MPI_COMM_WORLD,&status);

	VectorP2::items[ida]->add_from_vect_with_fact_multi(ids, n, fact ) ;

      } else if( strcmp( buffer,"mult_by_fact")==0 ) {
	int ida;
	double fact;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&fact,1 , MPI_DOUBLE, 0 , 101, MPI_COMM_WORLD,&status);
	VectorP::items[ida]->mult_by_fact( fact ) ;
	DEBUG(printf("nel processo %d, id era %d ,  %e il comando era %s \n",myid , ida,  fact, buffer );  )
      } else if( strcmp( buffer,"mult_by_fact2")==0 ) {
	int ida;
	double fact;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&fact,1 , MPI_DOUBLE, 0 , 101, MPI_COMM_WORLD,&status);
	VectorP2::items[ida]->mult_by_fact( fact ) ;
	DEBUG(printf("nel processo %d, id era %d ,  %e il comando era %s \n",myid , ida,  fact, buffer );  )
      } else if( strcmp( buffer,"Moltiplica")==0 ) {
	int ida, idb, idM;
	double fact;
	MPI_Recv(&idM,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&ida,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	
	Sparsa3AP::items[idM]->Moltiplica(    VectorP::items[ida] ,    VectorP::items[idb]        );
	
	DEBUG(printf("nel processo %d, id era %d , %d  %d il comando era %s \n",myid , idM,  ida, idb,  buffer );  )
      }else if( strcmp( buffer,"Moltiplica2")==0 ) {
	int ida, idb, idM;
	double fact;
	MPI_Recv(&idM,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&ida,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	
	Sparsa3AP2::items[idM]->Moltiplica(    VectorP2::items[ida] ,    VectorP2::items[idb]        );
	
	DEBUG(printf("nel processo %d, id era %d , %d  %d il comando era %s \n",myid , idM,  ida, idb,  buffer );  )
      }else if( strcmp( buffer,"Moltiplica2Exp")==0 ) {
	int ida, idb, idM;
	double fact;
	MPI_Recv(&idM,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&ida,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	
	Sparsa3AP2::items[idM]->MoltiplicaExp(    VectorP2::items[ida] ,    VectorP2::items[idb]        );
	
	DEBUG(printf("nel processo %d, id era %d , %d  %d il comando era %s \n",myid , idM,  ida, idb,  buffer );  )
      }else if( strcmp( buffer,"Moltiplica2ExpL")==0 ) {
	int ida, idb, idM;
	double fact;
	MPI_Recv(&idM,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&ida,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[idM]->MoltiplicaExpL(    VectorP2::items[ida] ,    VectorP2::items[idb]        );
	DEBUG(printf("nel processo %d, id era %d , %d  %d il comando era %s \n",myid , idM,  ida, idb,  buffer );  )
      }else if( strcmp( buffer,"LLtSolve2")==0 ) {
	int ida, idb, idM;
	MPI_Recv(&idM,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&ida,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	
	Sparsa3AP2::items[idM]->LLtSolve(    VectorP2::items[ida] ,    VectorP2::items[idb]        );
	
	DEBUG(printf("nel processo %d, id era %d , %d  %d il comando era %s \n",myid , idM,  ida, idb,  buffer );  )
      }else if( strcmp( buffer,"Preco2")==0 ) {
	int ida, idb, idM;
	double omega;
	MPI_Recv(&idM,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&ida,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	MPI_Recv(&omega,1 , MPI_DOUBLE, 0 , 103, MPI_COMM_WORLD,&status);
	
	Sparsa3AP2::items[idM]->Preco(    VectorP2::items[ida] ,    VectorP2::items[idb] , omega       );
	
	DEBUG(printf("nel processo %d, id era %d , %d  %d il comando era %s \n",myid , idM,  ida, idb,  buffer );  )
      }
      else if( strcmp( buffer,"preparaLLt2")==0 ) {
	int  idM;
	printf(" ricevuto preparaLLt2\n");
	MPI_Recv(&idM,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[idM]->preparaLLt();
	DEBUG(printf("nel processo %d, id era %d   il comando era %s \n",myid , idM, buffer );  )
      }
      else if( strcmp( buffer,"MoltiplicaMinus2")==0 ) {
	int ida, idb, idM;
	double fact;
	MPI_Recv(&idM,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&ida,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	
	Sparsa3AP2::items[idM]->MoltiplicaMinus(    VectorP2::items[ida] ,    VectorP2::items[idb]        );
	
	DEBUG(printf("nel processo %d, id era %d , %d  %d il comando era %s \n",myid , idM,  ida, idb,  buffer );  )
      }else if( strcmp( buffer,"Moltiplica2")==0 ) {
	int ida, idb, idM;
	double fact;
	MPI_Recv(&idM,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&ida,1 , MPI_INT, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&idb,1 , MPI_INT, 0 , 102, MPI_COMM_WORLD,&status);
	
	Sparsa3AP2::items[idM]->Moltiplica(    VectorP2::items[ida] ,    VectorP2::items[idb]        );
	DEBUG(printf("nel processo %d, id era %d , %d  %d il comando era %s \n",myid , idM,  ida, idb,  buffer );  )

      } else if( strcmp( buffer,"trasforma")==0 ) {
	int ida;
	double fattore, addendo;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&fattore ,1 , MPI_DOUBLE, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&addendo ,1 , MPI_DOUBLE, 0 , 102, MPI_COMM_WORLD,&status);
	Sparsa3AP::items[ida]->trasforma(fattore, addendo) ;
	DEBUG(printf("nel processo %d, id era %d ,  %e  ,  %e il comando era %s \n",myid , ida,  fattore, addendo,  buffer );  )
      }else if( strcmp( buffer,"trasforma2")==0 ) {
	int ida;
	double fattore, addendo;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&fattore ,1 , MPI_DOUBLE, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&addendo ,1 , MPI_DOUBLE, 0 , 102, MPI_COMM_WORLD,&status);
	Sparsa3AP2::items[ida]->trasforma(fattore, addendo) ;
	DEBUG(printf("nel processo %d, id era %d ,  %e  ,  %e il comando era %s \n",myid , ida,  fattore, addendo,  buffer );  )
      } else if( strcmp( buffer,"trasforma")==0 ) {
	int ida;
	double fattore, addendo;
	MPI_Recv(&ida,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	MPI_Recv(&fattore ,1 , MPI_DOUBLE, 0 , 101, MPI_COMM_WORLD,&status);
	MPI_Recv(&addendo ,1 , MPI_DOUBLE, 0 , 102, MPI_COMM_WORLD,&status);
	Sparsa3AP::items[ida]->trasforma(fattore, addendo) ;
	DEBUG(printf("nel processo %d, id era %d ,  %e  ,  %e il comando era %s \n",myid , ida,  fattore, addendo,  buffer );  )
      } else if( strcmp( buffer,"FINE")==0) {
	std::cout << "sclavo esce \n";
	MPI_Finalize();
	exit(0) ;
      } else if( strcmp( buffer,"caricaArraysS")==0) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	Sparsa3AP::items[id2dest]->riceviArrays();
	DEBUG(printf(" carica arrays terminato con id = %d nel processo %d  \n", id2dest , myid);)

      } else if( strcmp( buffer,"caricaArraysS2")==0) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);

	Sparsa3AP2::items[id2dest]->riceviArrays();

	DEBUG(printf(" carica arrays terminato con id = %d nel processo %d  \n", id2dest , myid);)

      }  else if( strcmp( buffer,"goherschMin")==0) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	Sparsa3AP::items[id2dest]->goherschMin();
	DEBUG(printf("nel processo %d, id era %d  il comando era %s \n",myid , id2dest ,  buffer );  )
      }  else if( strcmp( buffer,"caricaArray")==0) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	VectorP::items[id2dest]->riceviArray();
	DEBUG(printf(" carica arrayV terminato con id = %d nel processo %d  \n", id2dest , myid);)
      }else if( strcmp( buffer,"caricaArray2")==0) {
	MPI_Recv(&id2dest,1 , MPI_INT, 0 , 98, MPI_COMM_WORLD,&status);
	VectorP2::items[id2dest]->caricaArray(0,NULL);
	DEBUG(printf(" carica arrayV terminato con id = %d nel processo %d  \n", id2dest , myid);)
      } else {
	DEBUG(printf("comando %s sconosciuto in  inizializzaMPI \n", buffer );)
	exit(0);
      }
      DEBUG(printf(" slave alla barriera \n");)
      MPI_Barrier(MPI_COMM_WORLD );
      DEBUG(printf(" slave alla barriera OK \n");)
    }
  }
}
inizializzaMPI::~inizializzaMPI() { 
  int myid, numprocs;
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  if(myid==0) {
    char buffer[MSGLEN];
    int status;
    sprintf(buffer,"FINE");
    std:: cout << "mando la parola fine \n";
    for( int proc =1; proc< numprocs ; proc++) {
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
    }
  
    MPI_Finalize();
    std::cout << "master esce \n";
    exit(0);
  }
    std::cout << "forse master esce \n";
}

void settaMPI(int argc, char ** argv) {

  // std::cout << "faccio init  \n";
  MPI_Init(&argc,&argv);
  // std::cout << " init  OK  \n";

  int myid, numprocs,n;
    for(n=0; n<argc;  n++ ) {
      DEBUG(printf("--%s-- %d \n", argv[n],MPI_COMM_WORLD );)
    }
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

//   if(myid) {
//     inizializzaMPI  MPIobject;
//   }
}

Sparsa3AP:: Sparsa3AP() {
  this->crea();
}

VectorP:: VectorP() {
  this->crea();
}


void Sparsa3AP::trasforma(double fattore, double addendo) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"trasforma");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&fattore, 1, MPI_DOUBLE, proc, 101, MPI_COMM_WORLD);
      MPI_Send(&addendo, 1, MPI_DOUBLE, proc, 102, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {

    int *fatto;
    int dim;
    dim = dims2[proc_id];
    fatto = new int [ dim ];
    for(int i=0; i<dim; i++) {
      fatto[i]=0;
    }
    int n = Nelspp[proc_id];
    double *coeff=coeffspp[proc_id];
    int *col=frompp[proc_id] , *row=topp[proc_id];
    for(int i=0; i<n; i++) {
      coeff[i]=fattore*coeff[i];
      if(col[i]==row[i] && fatto[col[i]]==0 )  {
	coeff[i]+=addendo;
	 fatto[col[i]]=1;
      }
    }
    for(int i=0; i<dim; i++) {
      if(!fatto[i]) {
	{
	  for(int k=0; k<100; k++)
	  printf( " ci sono dei buchi in  Sparsa3AP::trasforma  %d \n", proc_id);
	  exit(1);
	}
      }
    }
    delete fatto;
  }
}



void Sparsa3AP::crea() {
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"creaS");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  }
  for( item_id=0; item_id< MAXNSPARSA3AP; item_id++) {
    if( Sparsa3AP::items[item_id] ==0 ) {
      Sparsa3AP::items[item_id]=this;
      break;
    }
  }
  if( item_id==MAXNSPARSA3AP ) {
    printf(" raggiunto massino MAXNSPARSA3AP \n");
    MPI_Finalize();
    exit(0);
  } else {
    DEBUG(printf(" creata una matrice con id = %d nel processo %d  \n", item_id, proc_id);)
  }
  
  Nelspp=0;
};


void VectorP::crea() {
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"creaV");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  }
  for( item_id=0; item_id< MAXNVECTORP; item_id++) {
    if( VectorP::items[item_id] ==0 ) {
      VectorP::items[item_id]=this;
      break;
    }
  }
  if( item_id==MAXNVECTORP ) {
    printf(" raggiunto massino MAXNVECTORP \n");
    MPI_Finalize();
    exit(0);
  } else {
    DEBUG(printf(" creata un vettore con id = %d nel processo %d  \n", item_id, proc_id);)
  }
  dim=0;
};


#define DBP -1

int arrayDimEstimate(int N , int * a) {
  int res=0;
  for(int  i=0; i<N; i++ ) {
    if(a[i]>res) {
      res=a[i];
    }
  }
  return res+1;
}

void Sparsa3AP::caricaArrays(int Nels, int * from, int * to, double * coeffs// ,int dim_from, int dim_to
			     ) {

  // if( DBP  == proc_id) std::cout << " sono in caricaArrays \n";
  int dim_from = arrayDimEstimate( Nels , from) ;
  int dim_to   = arrayDimEstimate( Nels , to  ) ;

  if ( dim_from == dim_to) {
    dim=dim_from;
  } else {
    dim=-100000;
  }


  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"caricaArraysS");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    int pos=0;
    
    for(int k=0; k< Nels-1 ; k++) {
      if( from[k]>from[k+1]) {
	for(int j=0; j<100; j++) 
	printf("inversione  \n");
	exit(1);
      }
    } 


    int oldlimite=0;
    int DLIM =  (dim_from)/(n_procs-1);
    for( int  proc =1; proc< n_procs ; proc++) { 
      int len=0;
      int limite =  DLIM*proc;
      if( proc == n_procs-1) limite = dim_from;



      while(pos+len< Nels && from[pos+len]< limite) {
	len++;
      }



      oldlimite=limite;
      MPI_Send(&dim_from, 1, MPI_INT, proc, 97, MPI_COMM_WORLD);
      MPI_Send(&dim_to, 1, MPI_INT, proc, 96, MPI_COMM_WORLD);
      MPI_Send(&len, 1, MPI_INT, proc, 95, MPI_COMM_WORLD);
      
      MPI_Send( from+pos , len, MPI_INT, proc, 94, MPI_COMM_WORLD);
      MPI_Send( to  +pos , len, MPI_INT, proc, 93, MPI_COMM_WORLD);
      MPI_Send( coeffs  +pos , len, MPI_DOUBLE, proc, 92, MPI_COMM_WORLD);

      pos+=len;
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } 
}


void VectorP::caricaArray(int Nels,  double * coeffs) {
  dim=Nels;
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"caricaArray");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    int pos=0;
    

    int DLIM =  (dim)/(n_procs-1);
    for( int  proc =1; proc< n_procs ; proc++) { 
      DEBUG(printf(" mando al processo %d \n", proc);)
      int len=0;
      int limite =  proc*DLIM;
      if(proc==n_procs-1) limite=dim;
      len=limite-pos;
      
      MPI_Send(&len, 1, MPI_INT, proc, 95, MPI_COMM_WORLD);
      
      MPI_Send( coeffs  +pos , len, MPI_DOUBLE, proc, 92, MPI_COMM_WORLD);

      pos=limite;
      DEBUG(printf(" mando OK \n");)
    }
    DEBUG(printf( " alla barriera \n");)
    MPI_Barrier(MPI_COMM_WORLD );  
    DEBUG(printf( " dopo alla barriera \n");)
  
  } 
}


void VectorP::set_all_random(double value) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"set_all_random");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&value, 1, MPI_DOUBLE, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    srand(proc_id*1000+(unsigned) time(NULL));
    for(index= 0; index<dim; index++) {
      coeffs[index]=value*random()*1.0/RAND_MAX;
    }
  }
}




void VectorP::set_to_zero() {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"set_to_zero");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    memset(coeffs,0, dim*sizeof(double));
  }
}




void VectorP::set_to_one() {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"set_to_one");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    for(index= 0; index<dim; index++) {
      coeffs[index]=1.0;
    }
  }
}



void VectorP::copy_to_a_from_b(VectorP* adata ,VectorP * bdata) {
  int n_procs, proc_id;
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);

  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"copy_to_a_from_b");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&adata->item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&bdata->item_id, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    int dim;
    if ( adata->dim !=bdata->dim) {
      VectorP::error( (char * )  "problema in     VectorP::copy_to_a_from_b, dimensioni incompatibili \n " );
    }
    dim= adata->dim;
    memcpy(adata->coeffs , bdata->coeffs, dim*sizeof(double) );
    
  }
}

void VectorP::add_from_vect(VectorP* bdata ) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"add_from_vect");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&(this->item_id), 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&(bdata->item_id), 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    int dim;
    if ( this->dim !=bdata->dim) {
      VectorP::error(  "problema in     VectorP::copy_to_a_from_b, dimensioni incompatibili \n " );
    }
    dim=this->dim;
    for (index=0; index<dim; index++) {
      this->coeffs[index]+=bdata->coeffs[index];
    }    
  }
}

void VectorP::dividebyarray(VectorP* bdata ) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"dividebyarray");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&(this->item_id), 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&(bdata->item_id), 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    int dim;
    if ( this->dim !=bdata->dim) {
      VectorP::error(  "problema in     VectorP::copy_to_a_from_b, dimensioni incompatibili \n " );
    }
    dim=this->dim;
    for (index=0; index<dim; index++) {
      this->coeffs[index]/=bdata->coeffs[index];
    }    
  }
}


void VectorP::add_from_vect_with_fact(VectorP* bdata, double fact ) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    FILE * debug;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"add_from_vect_with_fact");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&this->item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&bdata->item_id, 1, MPI_INT   , proc, 100, MPI_COMM_WORLD);
      MPI_Send(&fact,           1, MPI_DOUBLE, proc, 101, MPI_COMM_WORLD);

    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    int dim;
    if ( this->dim !=bdata->dim) {
      VectorP::error(  "problema in     VectorP::add_from_vect_with_fact, dimensioni incompatibili \n " );
    }
    dim=this->dim;
    for (index=0; index<dim; index++) {
      this->coeffs[index]+=fact*bdata->coeffs[index];
    }    
  }
}

void VectorP2::add_from_vect_with_fact(VectorP2* bdata, double fact ) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    FILE * debug;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"add_from_vect_with_fact2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&this->item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&bdata->item_id, 1, MPI_INT   , proc, 100, MPI_COMM_WORLD);
      MPI_Send(&fact,           1, MPI_DOUBLE, proc, 101, MPI_COMM_WORLD);

    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    if ( this->dims2[proc_id-1] !=bdata->dims2[proc_id-1]) {
      VectorP2::error(  "problema in     VectorP2::add_from_vect_with_fact, dimensioni incompatibili \n " );
    }
    int dim=this->dims2[proc_id-1];
    for (index=0; index<dim; index++) {
      this->coeffs[index]+=fact*bdata->coeffs[index];
    }    
  }
}

double scalarP(VectorP* adata, VectorP* bdata ) {
  double sum=0.0, res=0.0;
  DEBUG(printf("sono in scalarP proc_id = %d \n", adata->proc_id);)

  if( adata->proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< adata->n_procs ; proc++) {    
      sprintf(buffer,"scalare");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&(adata->item_id), 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&(bdata->item_id), 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    int dim;
    if ( adata->dim !=bdata->dim) {
      VectorP::error(  "problema in     VectorP::copy_to_a_from_b, dimensioni incompatibili \n " );
    }
    dim=adata->dim;
    sum=0.0;
    DEBUG(printf("dim = %d   proc_id = %d \n", dim, adata->proc_id);)
    for (index=0; index<dim; index++) {
      sum+= adata->coeffs[index]*bdata->coeffs[index];
    }    
  }
  DEBUG(printf("passo a reduce  proc_id = %d \n", adata->proc_id);)
  MPI_Reduce(&sum,&res, 1, MPI_DOUBLE, MPI_SUM,  0 , MPI_COMM_WORLD);
  return res;
}

void VectorP::mult_by_fact( double fact ) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"mult_by_fact");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&this->item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&fact, 1, MPI_DOUBLE, proc, 101, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    for (index=0; index<dim; index++) {
      coeffs[index]*=fact;
    }    
  }
}


double  Sparsa3AP::goherschMin()
{
  double res=0;
  double resmin=1.0e10;

  if(proc_id==0){
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"goherschMin");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&this->item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );        
  } else {
    int nG=dims2[proc_id];
    double *Gmin= new double[nG];		
    
    for(int i=0; i<nG;i++) {
      Gmin[i]=0.0;
    }
    
    for(int i=0; i<Nelspp[proc_id];i++) {
      if(frompp[proc_id][i]==loc2glob[proc_id][ topp[proc_id][i]]) {
	Gmin[  frompp[proc_id] [i]]=coeffspp[proc_id][i];
      }
    }
    for(int i=0; i< Nelspp[proc_id];i++) {
      if(frompp[proc_id][i]!=loc2glob[proc_id][ topp[proc_id][i]]) {
	Gmin[frompp[proc_id][i]]-=fabs( coeffspp[proc_id][i]  );
      }
    }
    
    for(int proc=1; proc<n_procs; proc++) {
      if( proc==proc_id) continue;
      
      for(int i=0; i< Nelspp[proc];i++) {
	Gmin[frompp[proc_id][i]]-=fabs( coeffspp[proc][i]  );
      }
    }
    for(int i=0; i<nG;i++) {
      if(i==0) res=Gmin[i];
      if(res>Gmin[i]) res=Gmin[i];
    }
    delete Gmin;
  }
  MPI_Reduce(&res,&resmin, 1, MPI_DOUBLE, MPI_MIN,  0 , MPI_COMM_WORLD);
  return resmin;

}





void Sparsa3AP::Moltiplica(VectorP *ris, VectorP *vect  ) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"Moltiplica");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(& (ris->item_id)  , 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(& (vect->item_id), 1, MPI_INT, proc, 102, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    // std::cout<< "####  processo " << proc_id  << endl;


    double ** resbuff;
    resbuff = new double * [ n_procs];
    int maxdimfrom=0;
    for ( int proc = 1; proc< n_procs; proc++) {
      if( others_dims2[proc] > maxdimfrom) maxdimfrom = others_dims2[proc];
      resbuff[proc] = new double [ dims2[proc] ];
      memset(resbuff[proc], 0, dims2[proc]*sizeof(double)  );

      ::Moltiplica( resbuff[proc] , vect->coeffs , Nelspp[proc] , topp[proc], frompp[proc], coeffspp[proc] );

    }
    
    

    double *coeffsfrom = new double [maxdimfrom];


    int proc_to;
    int proc_from;
    for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {
      proc_to=proc_id+Dproc;
      if(proc_to>=n_procs) {
	proc_to=proc_to-n_procs +1;
      }
      proc_from=proc_id - Dproc;
      if(proc_from<1) {
	proc_from=proc_from + n_procs-1;
      }
      // if( DBP  == proc_id) std::cout << " multiple send" << proc_id << " "<<proc_from << " " << proc_to <<"  \n";

      MPI_Request statusa, statusb, statusc ;
      MPI_Status status;

      // std::cout<< "####  processo " << proc_id << " riceve " << others_dims2[proc_from] << " e riceve " << dims2[proc_to]  << endl;
      MPI_Sendrecv( resbuff[proc_to]  , dims2[proc_to] , MPI_DOUBLE , proc_to, 89,
		    coeffsfrom , others_dims2[proc_from] , MPI_DOUBLE , proc_from, 89,
      		    MPI_COMM_WORLD, &status);
      // std::cout<< "####  processo " << proc_id << " riceve OK  "  << endl;
      delete resbuff[proc_to]   ; 
      for(int i=0; i<others_dims2[proc_from]; i++) {
	ris->coeffs[ others_loc2glob[proc_from][i] ]+=coeffsfrom[i];
	// std::cout<< "----  processo " << proc_id << " ADDIZIONO extra  " << coeffsfrom[i]  << endl;
      }
    }


    for(int i=0; i<dims2[proc_id]; i++) {
      ris->coeffs[ loc2glob[proc_id][i] ]+=  resbuff[proc_id][i];
      DEBUG(cout <<proc_id <<  " ++++ " <<  loc2glob[proc_id][i] << std::endl;)
	DEBUG(std::cout<< "----  processo " << proc_id << " ADDIZIONO AUTO   " <<  resbuff[proc_id][i] << std::endl;)
    }
    delete resbuff[proc_id];
    delete coeffsfrom;
    delete resbuff;
    // std::cout<< "####  processo " << proc_id  << " schiavo finito moltiplicazione " <<  endl;
   
  }
}

void Sparsa3AP::riceviArrays() {
  /*
    int dim_from;
    int dim_to;
    */
  int Nels;

  MPI_Status status;
  
  // if( DBP  == proc_id) std::cout << " sono in riceviarrays \n";

  MPI_Recv(&(this->dim_from) ,1 , MPI_INT, 0 , 97, MPI_COMM_WORLD,&status);
  MPI_Recv(&(this->dim_to)   ,1 , MPI_INT, 0 , 96, MPI_COMM_WORLD,&status);

  // if( DBP  == proc_id) std::cout << " sono in riceviarrays " << dim_from << " " << dim_to << "\n";


  MPI_Recv(&(Nels)     ,1 , MPI_INT, 0 , 95, MPI_COMM_WORLD,&status);
  // if( DBP  == proc_id) std::cout << " sono in riceviarrays Nels " <<Nels <<   "\n";


  int *from = new int [ Nels ] ;
  int *to   = new int [ Nels ] ;
  double *coeffs = new double [ Nels];
  
  MPI_Recv(from    ,Nels , MPI_INT    , 0 , 94, MPI_COMM_WORLD,&status);
  MPI_Recv(to      ,Nels , MPI_INT    , 0 , 93, MPI_COMM_WORLD,&status);
  MPI_Recv(coeffs  ,Nels , MPI_DOUBLE , 0 , 92, MPI_COMM_WORLD,&status);

  if( DBP  == proc_id) std::cout << "  ricevuti " <<Nels <<   "\n";
  std::cout << " dim_from = " << dim_from << std::endl;
  std::cout << " dim_to = " << dim_to << std::endl;

  // 1001/10 = 100  
  int DLIM =  (dim_from)/(n_procs-1);

  int posA =  (proc_id-1)*DLIM;
  int count=0;
  for(int i=0; i<Nels; i++) {
    if( from[i]==to[i]) count++;
    from[i]-=posA;
    if( from[i]<0) {
      printf("  sono arrivati degli arrai sbagliati \n");
      exit(0);
    }
  }

  // (101*10)/1001 =1
  // (201*10)/1001=2

  int * goes2 = new int [Nels];

  for(int i=0; i<Nels; i++) {
    goes2[i] = ((to[i]))/DLIM +1 ;
    if(goes2[i]==n_procs)     goes2[i] -=1;
  }



  this->Nelspp= new int [ n_procs];
  this->topp  = new int *[ n_procs];
  this->frompp  = new int *[ n_procs];
  this->coeffspp  = new double *[ n_procs];


  memset( this->Nelspp, 0, n_procs*sizeof(int));
  for(int i=0; i<Nels; i++) {
      this->Nelspp[goes2[i]]++;
  }
  this->dims2 = new int [n_procs];

  memset( this->dims2, 0, n_procs*sizeof(int));


  this->loc2glob = new int * [n_procs] ;

  int * used   = new int [ dim_to] ;

  // if( DBP  == proc_id)std::cout << "  sparpaglio  " <<Nels <<   "\n";

  for(int proc=1; proc< n_procs; proc++) {
    int nels = Nelspp[proc] ;

    this->topp[proc]=new int [nels ];
    this->frompp[proc]=new int [nels ];
    this->coeffspp[proc]=new double [nels ];


    // if( DBP  == proc_id) std::cout << "  sparpaglio bis " <<Nels <<   "\n";
    memset(used, 0, dim_to*sizeof(int));
    for(int i=0; i< Nels; i++) {
      if( goes2[i]==proc ) {
	used[ to[i]]=1;
      }
    }
    this->dims2[proc]=0;
    for(int i=0; i< this->dim_to; i++) {
      if( used[i] ) {
	this->dims2[proc]++;
      }
    }


    
    
    // if( DBP  == proc_id) std::cout << " per proc " <<  proc << " dims est " << dims2[proc]<<   "\n";
    this->loc2glob[proc]= new int [ this->dims2[proc] ];
    int pos=0;
    for(int i=0; i< dim_to; i++) {
      if( used[i] ) {
	this->loc2glob[proc][pos]=i;
	used[i]=pos;
	pos++;
      } 

      
    }  
    int ne=0;


    for(int i=0; i<Nels; i++) {
      if(proc == goes2[i]){
	this->frompp[proc][ne]=from[i];
	this->topp  [proc][ne]=used[to[i]];
	if( used[to[i]] >= this->dims2[proc] || used[to[i]]<0 ) {
	  printf(" grosso grosso problema \n");
	  exit(0);
	}

	this->coeffspp[proc][ne] = coeffs[i];

	ne++;	
      }
    }


    if(this->Nelspp[proc]!=ne) {
      printf(" this->Nelspp[proc]!=ne\n");
      exit(0);
    }


  }

  // if( DBP  == proc_id) std::cout << "  sparpaglio 4  " <<Nels <<   "\n";

  this->others_loc2glob = new int * [n_procs] ;
  this->others_dims2 = new int [n_procs];
  memset(this->others_dims2 , 0, n_procs*sizeof(int));



  int proc_to;
  int proc_from;

  for(int i=0; i<dims2[proc_id]; i++) {
    loc2glob[proc_id][i]-=posA;
  }



  for(int Dproc=1; Dproc< (n_procs-1); Dproc++) {
    proc_to=proc_id+Dproc;
    if(proc_to>=n_procs) {
      proc_to=proc_to-n_procs +1;
    }
    proc_from=proc_id - Dproc;
    if(proc_from<1) {
      proc_from=proc_from + n_procs-1;
    }
    
    // if( DBP  == proc_id) std::cout << " multiple send" << proc_id << " "<<proc_from << " " << proc_to <<"  \n";
    MPI_Request statusa, statusb, statusc ;
    MPI_Status status;
    MPI_Sendrecv(dims2+proc_to, 1, MPI_INT , proc_to, 1,
		 others_dims2+proc_from ,  1,  MPI_INT ,  proc_from,  1,  MPI_COMM_WORLD, &status );
    // if( DBP  == proc_id) std::cout << " multiple send OK \n";

    others_loc2glob[proc_from] = new int [others_dims2[proc_from]];

    MPI_Sendrecv( loc2glob[proc_to], dims2[proc_to] , MPI_INT , proc_to, 89,
		  others_loc2glob[proc_from], others_dims2[proc_from] , MPI_INT , proc_from, 89,
		  MPI_COMM_WORLD, &status);

    for(int i=0; i<others_dims2[proc_from]; i++) {
      others_loc2glob[proc_from][i]-=posA;
    }

    if(proc_id==1) {
      DEBUG(printf( "proc_to est %d \n", proc_to );)
      for(int i=0; i<dims2[proc_to]; i++) {
	DEBUG(printf("<<<<<<<<<<<< %d %d\n", i, loc2glob[proc_to][i]);)
      }
    }

    if(proc_id==2) {
      DEBUG(printf( "proc_from est %d \n", proc_from );)
      for(int i=0; i<others_dims2[proc_from]; i++) {
	DEBUG(printf(">>>>>>>>>>> %d %d\n", i, others_loc2glob[proc_from][i]);)
      }


    }


  }

  
  // if( DBP  == proc_id) std::cout << "  sparpaglio 5 " <<Nels <<   "\n";
  if(0) {
    char nfpp[80];
    sprintf(nfpp,"dopo_ricevi_%d", proc_id);
    FILE *fpp=fopen(nfpp,"w");
    for(int i=0; i<Nels; i++ ) {
      fprintf(fpp," to[%d] = %d  goes2[%d]= %d\n", i, to[i],i,goes2[i] );
    }
    fclose(fpp);
    fpp=fopen(nfpp,"a");
    for(int proc=1; proc<n_procs; proc++) {
      fprintf(fpp," per processo %d\n", proc);
      int nel=Nelspp[proc];
      fprintf(fpp," numero elementi  %d\n", nel);
      for(int i=0; i<nel; i++) {
	fprintf(fpp," %d %d %e\n", topp[proc][i], frompp[proc][i], coeffspp[proc][i]);
      }
      
      fclose(fpp);
      fpp=fopen(nfpp,"a");
      
      fprintf(fpp,"la dimensione utile est  %d\n", dims2[proc]);
      for(int i=0; i<dims2[proc]; i++) {
	fprintf(fpp,"%d %d\n", i, loc2glob[proc][i]);
      }
      
      fclose(fpp);
      fpp=fopen(nfpp,"a");

      fprintf(fpp,"invece dal processo %d la  dimensione utile est  %d\n", proc ,others_dims2[proc]);
      for(int i=0; i<others_dims2[proc]; i++) {
	fprintf(fpp,"%d %d\n", i, others_loc2glob[proc][i]);
      }
      
      fclose(fpp);
      fpp=fopen(nfpp,"a");
      
      DEBUG(std::cout << " ############# il processo " << proc_id << " a finito un giro  \n";)
       
    }
    fclose(fpp);
  }
  DEBUG(std::cout << " ############# il processo " << proc_id << " est uscito \n";)
  
  delete used;
  delete coeffs;
  delete to;
  delete from;
  delete goes2;  
}


void VectorP::riceviArray() {
  int Nels;
  MPI_Status status;
  MPI_Recv(&(Nels)     ,1 , MPI_INT, 0 , 95, MPI_COMM_WORLD,&status);
  dim=Nels;
  coeffs = new double [ Nels];
  MPI_Recv(coeffs  ,Nels , MPI_DOUBLE , 0 , 92, MPI_COMM_WORLD,&status);
  if(0) {
    char nfpp[80];
    sprintf(nfpp,"vecttor_dopo_ricevi_%d", proc_id);
    FILE *fpp=fopen(nfpp,"w");
    for(int i=0; i<dim; i++ ) {
      fprintf(fpp," coeff[%d] = %e  \n", i, coeffs[i] );
    }
    fclose(fpp);
  }  
}

void VectorP::scriviArray() {
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"scriviArray");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  }  else {
    if(1) {
      char nfpp[80];
      sprintf(nfpp,"vector_%d_%d", item_id, proc_id);
      FILE *fpp=fopen(nfpp,"w");
      for(int i=0; i<dim; i++ ) {
	fprintf(fpp," coeff[%d] = %e  \n", i, coeffs[i] );
      }
      fclose(fpp);
    }  
  }
}


Sparsa3AP::~Sparsa3AP() {
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    items[item_id]=0;
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {
      sprintf(buffer,"distruggiS");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );


  } else {

    if(Nelspp) {
      for(int proc=1; proc< n_procs; proc++) {
	delete topp[proc];
	delete frompp[proc];
	delete coeffspp[proc];
	delete loc2glob[proc];
	
	delete others_loc2glob[proc];
      }
      delete Nelspp;
      delete dims2;
      delete others_dims2;
    }
  }
};

VectorP::~VectorP() {
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    items[item_id]=0;
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {
      sprintf(buffer,"distruggiV");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {

    if(dim) {
      delete coeffs;
    }
  }
};



Sparsa3AP2 ** Sparsa3AP2::items = new Sparsa3AP2 * [MAXNSPARSA3AP];
Sparsa3AP ** Sparsa3AP::items = new Sparsa3AP * [MAXNSPARSA3AP];
VectorP ** VectorP::items = new VectorP * [MAXNVECTORP];
VectorP2 ** VectorP2::items = new VectorP2 * [MAXNVECTORP];






Trasmissione::Trasmissione(int taglia, int proca, int procb) {
  this->taglia=taglia;
  this->proca=proca;
  this->procb=procb;
}

Trasmissione& Trasmissione::operator= (const Trasmissione & tet){
  proca=tet.proca;
  procb=tet.procb;
  taglia=tet.taglia;
  return *this;
};

int Trasmissione::operator== (const Trasmissione& tet) const{
  if( proca==tet.proca &&
      procb==tet.procb &&
      taglia==tet.taglia ) {
    return 1;
  }  else {
    return 0;
  }
};

int Trasmissione::operator< (const Trasmissione& tet) const {
  if(  taglia<tet.taglia ) {
    return 0; 
  } else {
    if(  taglia>tet.taglia ) {
      return 1; 
    } else {
      if(  proca<tet.proca ) {
	return 0; 
      } else {
	if(  proca>tet.proca ) {
	  return 1; 
	} else {
	  return  procb>tet.procb;
	}	
      }      
    } 
  }
};









Sparsa3AP2:: Sparsa3AP2() {
  
  acc_Nels=0;
  acc_from=0;
  acc_to=0;
  acc_coeffs=0;

  exp_Nelspp=0;
  exp_frompp=0;
  exp_topp=0;
  exp_coeffspp=0;

  this->crea();
}





void Sparsa3AP2::fissaexp() {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"fissaexp");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {
    exp_coeffspp=new float * [NEXPVECT*(n_procs-1)];
    for(int proc=0; proc< NEXPVECT*(n_procs-1) ; proc++) {
      exp_coeffspp[proc] = new float [ exp_Nelspp[ proc%(n_procs-1)]+1 ];
    }
    for(int proc=0; proc<n_procs-1; proc++) {
      if(exp_others_dims2[proc]>exp_maxdim2) exp_maxdim2= exp_others_dims2[proc]; // per il trasposto 
    }
    
    delete bufferinvio;
    bufferinvio = new double[exp_maxdim2];
    for(int proc=0; proc<n_procs-1; proc++) {
      delete others_buffer[proc];
      if( proc!=proc_id-1) {
	others_buffer[proc] = new double [ exp_maxdim2  ];
      } else {
	others_buffer[proc] = new double [ dims[proc_id-1] ];
      }
    }


    if(1) {
      
      for( int proc=0; proc<n_procs-1; proc++) {
	if ( proc==proc_id-1) continue;
	
	if( exp_dims2[proc]) {
	  trasmissioniExp[ Trasmissione(  exp_dims2[proc] ,proc_id-1, proc   )   ]=1;
	}
	if( exp_others_dims2[proc]) {
	  trasmissioniExp[ Trasmissione(  exp_others_dims2[proc], proc  ,proc_id-1  )   ]=1;
	}
      }
    }
    


  }
  if( proc_id) return;
  
#define Xvect 0
#define Rvect 1
#define Pvect 2
#define APvect 3
#define Zvect  4  


  expsettozero(Xvect);

  expsettozero(Rvect);

  expandiMult(Rvect, Xvect);

  expmultbyfact(Rvect,Rvect,-1.0 );
 
  expsettodiag(Rvect);


  expPreco(Zvect, Rvect);

  expcopy(Pvect,Zvect);

  float beta=0.0;
  float rho=scalareexp( Zvect, Rvect );
  printf(" CGexp erroro %e \n", rho);
  float err=rho;
  int k = 0;
  int tolcount=0;
  int nmax=2000;
  float alpha=0.0;
  float rhoold=0.0;
  float tol=1.0e-1;

  while ( (  (fabs(err) > tol) &&  k < nmax) ) {
    

    expsettozero(APvect);
    expandiMult(APvect, Pvect );
    alpha = rho/ scalareexp( APvect,  Pvect);
    expgensum(Xvect, Xvect, Pvect , 1.0,  alpha  );
    expgensum(Rvect, Rvect, APvect, 1.0, -alpha  );


    expPreco(Zvect, Rvect);

    rhoold=rho;
    rho=scalareexp( Zvect, Rvect );



    printf(" CGexp erroro %e \n", rho);

    beta=rho/rhoold;
    expgensum(Pvect, Zvect, Pvect, 1.0, beta);
    err=rho;
    k = k+1;
    
    if ( fabs(err)< tol ) 
      tolcount+=1;


  }

  //   expsettozero(Rvect);
  //   expandiMult(Rvect, Xvect);
  //   expmultbyfact(Rvect,Rvect,-1.0 );
  //   rho=scalareexp( Rvect, Rvect );
  

}




void Sparsa3AP2::expsettozero(int a) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"expsettozero");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&a, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {
    for(int proc=0; proc< (n_procs-1) ; proc++) {
      memset(exp_coeffspp[proc+a*(n_procs-1)],0,  exp_Nelspp[ proc] *sizeof(float));
    }
  }
}


void Sparsa3AP2::expsettodiag(int a) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"expsettodiag");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&a, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {
    //     for(int proc=0; proc< (n_procs-1) ; proc++) {
    //       memset(exp_coeffspp[proc+a*(n_procs-1)],0,  exp_Nelspp[ proc] *sizeof(float));
    //     }
    int proc=proc_id-1;
    for(int i=0; i< exp_Nelspp[proc]; i++) {
      if( exp_topp[proc][i]==exp_frompp[proc][i]) {
	if( precodiag[ exp_frompp[proc][i]  ]!=0.0) {
	  exp_coeffspp[proc+a*(n_procs-1)][i] += 1.0;
	} else {
	  printf(" trovato un pattern penzolante \n");
	}
      }
    }
  }
}

void Sparsa3AP2::expsum(int a,int b,int c) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"expsum");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&a, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
      MPI_Send(&b, 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(&c, 1, MPI_INT, proc, 102, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {
    for(int proc=0; proc< (n_procs-1) ; proc++) {
      for(int i=0; i< exp_Nelspp[proc]; i++) {
	exp_coeffspp[proc+a*(n_procs-1)][i]=exp_coeffspp[proc+b*(n_procs-1)][i]+
	  exp_coeffspp[proc+c*(n_procs-1)][i];
      }
    }
  }
}
void Sparsa3AP2::expsub(int a,int b,int c) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"expsub");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&a, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
      MPI_Send(&b, 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(&c, 1, MPI_INT, proc, 102, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {
    for(int proc=0; proc< (n_procs-1) ; proc++) {
      for(int i=0; i< exp_Nelspp[proc]; i++) {
	exp_coeffspp[proc+a*(n_procs-1)][i]=exp_coeffspp[proc+b*(n_procs-1)][i]-
	  exp_coeffspp[proc+c*(n_procs-1)][i];
      }
    }
  }
}


void Sparsa3AP2::expmultbyfact(int a,int b,float c) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"expmultbyfact");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&a, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
      MPI_Send(&b, 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(&c, 1, MPI_FLOAT, proc, 102, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {
    for(int proc=0; proc< (n_procs-1) ; proc++) {
      for(int i=0; i< exp_Nelspp[proc]; i++) {
	exp_coeffspp[proc+a*(n_procs-1)][i]=exp_coeffspp[proc+b*(n_procs-1)][i]*c;
      }
    }
  }
}


void Sparsa3AP2::expgensum(int a,int b,int c, float B, float C) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"expgensum");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&a, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
      MPI_Send(&b, 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(&c, 1, MPI_INT, proc, 102, MPI_COMM_WORLD);
      MPI_Send(&B, 1, MPI_FLOAT, proc, 103, MPI_COMM_WORLD);
      MPI_Send(&C, 1, MPI_FLOAT, proc, 104, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {
    for(int proc=0; proc< (n_procs-1) ; proc++) {
      for(int i=0; i< exp_Nelspp[proc]; i++) {
	exp_coeffspp[proc+a*(n_procs-1)][i]=exp_coeffspp[proc+b*(n_procs-1)][i]*B+
	  exp_coeffspp[proc+c*(n_procs-1)][i]*C;
      }
    }
  }
}

float  Sparsa3AP2::scalareexp(int a, int b) {
  float sum=0, res=0;
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"scalareexp");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&a, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
      MPI_Send(&b, 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {
    
    for(int proc=0; proc< (n_procs-1) ; proc++) {
      for(int i=0; i< exp_Nelspp[proc]; i++) {
	sum=sum+ exp_coeffspp[proc+a*(n_procs-1)][i]* exp_coeffspp[proc+b*(n_procs-1)][i];
      }
    }
  }
  
  MPI_Reduce(&sum,&res, 1, MPI_FLOAT, MPI_SUM,  0 , MPI_COMM_WORLD);
  return res;
}


void   Sparsa3AP2::expPreco(int a, int b) {
  float sum=0, res=0;
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"expPreco");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&a, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
      MPI_Send(&b, 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {
    
    for(int proc=0; proc< (n_procs-1) ; proc++) {
      for(int i=0; i< exp_Nelspp[proc]; i++) {
	if( precodiag[   exp_topp[proc] [    i  ]         ]  !=0.0)
	  exp_coeffspp[proc+a*(n_procs-1)][i]  =  exp_coeffspp[proc+b*(n_procs-1)][i]/precodiag[   exp_topp[proc] [    i  ]         ] ;
      }
    }
  }
  
}

void  Sparsa3AP2::expcopy(int a, int b) {
  float sum=0, res=0;
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"expcopy");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&a, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
      MPI_Send(&b, 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {
    for(int proc=0; proc< (n_procs-1) ; proc++) {
      memcpy(exp_coeffspp[proc+a*(n_procs-1)], exp_coeffspp[proc+b*(n_procs-1)], exp_Nelspp[proc]*sizeof(float) );
    }
  }
}


void Sparsa3AP2::expandi() {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"expandi");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    for(int i=0; i<n_procs-1; i++) {
    //       for(int j=0; j<n_procs-1; j++) {
    // 	printf("  BARRIERA interna %d \n", j);
    // 	MPI_Barrier(MPI_COMM_WORLD); // @%^&
    // 	printf("  BARRIERA interna %d???????????????? \n ", j);
    
    //       }
    //       printf("  BARRIERA %d???????????????? \n ", i);
    //       MPI_Barrier(MPI_COMM_WORLD); // @%^&
    //       printf("  BARRIERA %d \n", i);
    
    

    
      
    }
 
    MPI_Barrier(MPI_COMM_WORLD );


    MPI_Barrier(MPI_COMM_WORLD );
  }
}


void Sparsa3AP2::expandiMult(int a, int b) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"expandiMult");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&a, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
      MPI_Send(&b, 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
    }


    MPI_Barrier(MPI_COMM_WORLD );


    MPI_Barrier(MPI_COMM_WORLD );
  }
}


#define dbp 2

void Sparsa3AP2::expandimpi(int target, int source) {
  
  MPI_Status status;
  int *  ppnels;
  int ** ppfrom;
  int ** ppto;
  float ** ppcoeffs;
  int **pploc2glob;
  int **ppothers_loc2glob;
  std::map<int,float> ** rcvmap ;
  int primavolta=0;

  if( exp_Nelspp) {
   
    ppnels = exp_Nelspp;
    ppfrom = exp_frompp;
    ppto   = exp_topp    ;
    pploc2glob=exp_loc2glob;
    ppothers_loc2glob=exp_others_loc2glob;
    if( exp_coeffspp ) {
      if( target==-1) {
	printf("target non fissato con pattern fissato \n");
	exit(1);
      }
      ppcoeffs = exp_coeffspp+(n_procs-1)*source;
    } else {
      ppcoeffs=0;
    }
    
  } else {
    primavolta=1;
    if(0) {         ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      exp_Nelspp= new int [n_procs-1];
      exp_frompp= new int* [ n_procs-1];
      exp_topp  = new int* [ n_procs-1];
      exp_loc2glob = new int *[ n_procs-1];
      exp_others_loc2glob = new int *[ n_procs-1];
      exp_dims2 = new int [ n_procs-1];
      exp_others_dims2 = new int [ n_procs-1];
      
      for(int proc=0; proc<n_procs-1; proc++) {
	exp_Nelspp[proc]  =  Nelspp[proc]  ; 
	exp_frompp[proc] = new int [exp_Nelspp[proc]+1];
	exp_topp  [proc] = new int [exp_Nelspp[proc]+1];

	for(int i=0 ; i< Nelspp[proc] ; i++) {
	  exp_frompp[proc][i] = frompp[proc][i];
	  exp_topp  [proc][i] =  topp [proc][i];
	}
	
	exp_dims2 [proc] = dims2[proc];
	exp_others_dims2[proc]= others_dims2[proc]  ; 
	    
	exp_loc2glob[proc]        =   new int [ dims2[proc]  ] ; 
	memcpy(    exp_loc2glob[proc]  ,  loc2glob[proc] ,  dims2[proc]  * sizeof(int)  ) ; 
	    
	if( proc!=(proc_id-1) ){
	  exp_others_loc2glob[proc] =   new int [ others_dims2[proc]  ] ; 
	  memcpy(    exp_others_loc2glob[proc]  ,  others_loc2glob[proc] ,  others_dims2[proc]  * sizeof(int)  ) ; 
	} else {
	  exp_others_loc2glob[proc]  = exp_loc2glob[proc] ; 
	}	
      }
        MPI_Barrier(MPI_COMM_WORLD );

      return ;
    }
    ppnels = Nelspp;
    ppfrom = frompp;
    ppto   = topp  ;
    pploc2glob=loc2glob;
    ppothers_loc2glob=others_loc2glob;
    
    if( target!=-1) {
      printf("target  fissato con pattern  non fissato \n");
      exit(1);
    }
    ppcoeffs=0;
    
  }
  
  int thisproc=proc_id-1;
  
  int N1 ;
  int N2 ;
  int n1,n2;
  
  int *B1;
  int *A1;
  double *C1;
  
  int *A2;
  int *B2;
  float  *C2;

  int *messaggiostore[n_procs];
  int  messaggiostorelen[n_procs];
  int *messaggioreceive[n_procs];
  int  messaggioreceivelen[n_procs];

  DEBUG(if( proc_id==dbp) printf ( "AAA expandi mpi2 \n"););
    
  double maxC2=0;

  for( int proc1=0; proc1<n_procs-1; proc1++) {
    
    N1=  Nelspp[proc1];

    // metter qui N1=0 se primavolta e se proc1 != proc_id-1
    if(primavolta &&  proc1 != proc_id-1) {
      N1=0;
    }
    if( proc1!=proc_id-1) {
      A1=  topp [proc1];
      B1=  frompp [proc1];
    } else {
      
      B1=  topp [proc1];
      A1=  frompp [proc1];
      
    }
    C1=  coeffspp[proc1];
    
    int * messaggio=0;
    int      meslen=0;
    

    for( int proc2=proc1; proc2<n_procs-1; proc2++) {
      DEBUG(if( proc_id==dbp) printf( "AAA processo %d alloca pmappa %d di %d  \n", proc_id, proc1, dims[proc1]););
    
      std::map<int,float> * mappa = new std::map<int,float> [dims[proc1] ]  ,* mp   ;
      
      
      N2=  ppnels[proc2];
      if(   ppfrom !=frompp   ||  proc2!=proc_id-1) {
	A2= ppto   [proc2];
	B2= ppfrom [proc2];
      } else {
	B2= ppto   [proc2];
	A2= ppfrom [proc2];	
      }
      if( ppcoeffs) {
	C2 = ppcoeffs [proc2];
      }
      
      int i1,i2,j1,j2;
      i1=0;
      i2=0;
      
      if(N1>0 && N2>0) {
	while(1) {
	  
	  DEBUG(if( proc_id==dbp)  printf ( "AAA i1 i2 %d %d %d \n", i1,i2, proc_id););
	  while(  A1[i1]!=A2[i2] ) {
	    if(  A1[i1]<A2[i2] ) {
	      i1++;
	      if(!(i1<N1)) {
		break;
	      }
	    } else {
	      i2++;
	      if(!(i2<N2)) {
		break;
	      } 
	    }
	  }
	  if( (!(i1<N1)) || (!(i2<N2)) ) {
	    break;
	  }
	  
	  for(j1=i1; j1<N1 && A1[j1]==A1[i1]  ; j1++); 
	  for(j2=i2; j2<N2 && A2[j2]==A2[i2]  ; j2++); 
	  
	  int  b1[j1-i1];
	  
	  {
	    for(n1=i1; n1<j1; n1++) {
	      if ( proc1==proc_id-1) {
		b1[n1-i1]=B1[n1];
	      } else {
		b1[n1-i1]=others_loc2glob[proc1][B1[n1]];	    
	      }
	    }
	  }
	  int  b2[j2-i2];
	  {
	    for(n2=i2; n2<j2; n2++) {
	      if ( proc2==proc_id-1) {
		b2[n2-i2]=B2[n2];
	      } else {
		b2[n2-i2]=ppothers_loc2glob[proc2][B2[n2]];	    
	      }
	    }
	  }
	  
	  if( ppcoeffs==0) {
	    for(n1=i1; n1<j1; n1++) {
	      for(n2=i2; n2<j2; n2++) {

		// if(C1[n1]<=0.0) continue;

		if( proc1==proc2  && b1[n1-i1] > b2[n2-i2]) continue; 

		// METTERE QUI un controllo b1 == A1  se primavolta
		if(primavolta &&  b1[n1-i1] !=A1[i1] ) continue;


		mappa [ b1[n1-i1] ] [ b2[n2-i2]  ] =1;
	      }
	    }
	  } else {
	    std::map<int,float>::iterator pos;
	    for(n1=i1; n1<j1; n1++) {
	      mp =  mappa + b1[n1-i1] ; 
	      for(n2=i2; n2<j2; n2++) {

		if(fabs(C2[n2])>maxC2) maxC2=fabs(C2[n2]);
		
		if( proc1==proc2  && b1[n1-i1] > b2[n2-i2]) continue; 
		pos = mp->find(  b2[n2-i2]);
		
		if(pos==mp->end()) {
		  (*mp)[ b2[n2-i2]  ] =
		    C1[n1]*C2[n2];
		} else {
		  (*mp)[ b2[n2-i2]  ] = pos->second + C1[n1]*C2[n2];
		}
	      }
	    }
	  }
	  
	  i1=j1;
	  i2=j2;
	}
      }
      DEBUG(if( proc_id==dbp)       printf ( "AAA i1 i2 O OK OK OK OK OK proc_id %d  proc2 %d  \n\n\n\n\n\n\n\n", proc_id, proc2););
      
      int newmeslen=meslen;
      newmeslen++; // processo
      newmeslen++; // lunghezza per il processo
      int lungproc=0;
      int MPL;
      if(ppcoeffs==0) {
	MPL=2;
      } else {
	MPL=3;
      }
      for(int i=0; i<dims[proc1] ; i++) {
	newmeslen += mappa[i].size()*MPL;  // mettere qua un tre se molti
	lungproc  += mappa[i].size()  ; 
      }
      int * newmessaggio = new int [ newmeslen];
      if( meslen) {
	memcpy( newmessaggio, messaggio, meslen*sizeof(int));
	delete messaggio;
      }
      
      newmeslen = meslen  ; 
      
      newmessaggio[newmeslen] = proc2 ; 
      newmeslen++;
      newmessaggio[newmeslen] = lungproc ; 
      newmeslen++;
      
      
      for(int i=0; i<dims[proc1] ; i++) {
	std::map<int,float>::iterator pos;
	pos = mappa[i].begin();
	for(int k=0; k< mappa[i].size(); k++) {
	  newmessaggio[newmeslen] = i;       // to 
	  newmeslen++;
	  newmessaggio[newmeslen] = pos->first;  // from
	  newmeslen++;
	  if(ppcoeffs) {
	    ((float *)newmessaggio)[newmeslen] = pos->second;
	    newmeslen++;
	  }
	  pos++;
	  
	}
      }   
      meslen=newmeslen;
      messaggio=newmessaggio;
      
      delete []mappa;
    }
   
    messaggiostore[proc1]=messaggio;
    messaggiostorelen[proc1]= meslen; 
    // printf(" processo %d ha fatto messaggio per %d \n", proc_id-1, proc1);
  }

  printf(" maxC2 %e \n", maxC2);


  for(int proc1=0; proc1<n_procs-1; proc1++) {
    
    if(1)
    {
      if( proc1==0) {
	int proc=proc_id-1;
	messaggioreceivelen[proc]= messaggiostorelen[proc];
	messaggioreceive[proc]=messaggiostore[proc];
	
      } else {
	int proc_to = proc_id-1+ proc1;
	int proc_from = proc_id-1- proc1;
	int LP = n_procs-1;

	proc_to=( proc_to+LP  )%LP;
	proc_from=( proc_from+LP  )%LP;

	MPI_Sendrecv(&(messaggiostorelen[proc_to]) , 1 , MPI_INT , proc_to+1, 89,
		     &(messaggioreceivelen[proc_from]),1  , MPI_INT , proc_from+1, 89,
		      MPI_COMM_WORLD, &status);

	messaggioreceive[proc_from] = new int [messaggioreceivelen[proc_from]+1];

	MPI_Sendrecv(messaggiostore[proc_to] , messaggiostorelen[proc_to] , MPI_INT , proc_to+1, 89,
		     messaggioreceive[proc_from], messaggioreceivelen[proc_from] , MPI_INT , proc_from+1, 89,
		      MPI_COMM_WORLD, &status);

	delete messaggiostore[proc_to];

      }
    }
    else
      {
    if(proc1!=proc_id-1) {
      printf(" proc %d mand a a %d \n", proc_id-1, proc1);
      MPI_Send(&(messaggiostorelen[proc1]),1,MPI_INT, proc1+1, 321+proc1, MPI_COMM_WORLD);
      MPI_Send(messaggiostore[proc1],messaggiostorelen[proc1],MPI_INT, proc1+1, 322+proc1, MPI_COMM_WORLD);
      delete messaggiostore[proc1];

    }  else {
      
      for(int proc=0; proc<n_procs-1; proc++) {
	
	if( proc!=proc_id-1) {
	  MPI_Recv(&(messaggioreceivelen[proc]),1,MPI_INT, proc+1, 321+proc1, MPI_COMM_WORLD, &status);
	  messaggioreceive[proc] = new int [messaggioreceivelen[proc]+1];
	  MPI_Recv(messaggioreceive[proc],messaggioreceivelen[proc],MPI_INT, proc+1, 322+proc1, MPI_COMM_WORLD, &status);
	} else {
	  messaggioreceivelen[proc]= messaggiostorelen[proc];
	  messaggioreceive[proc]=messaggiostore[proc];
	}
      }
    }
      }
  }
  
  

  
  {
    
    // togliere le trasmissioni qua dentro 
    
    {
      rcvmap = new std::map<int,float>* [n_procs-1];
      for(int proc=0; proc<n_procs-1; proc++) {
	rcvmap[proc] = new std::map<int,float> [dims[  thisproc ] ];
      }
      
      
      if(ppcoeffs) {
	for(int proc=0; proc<n_procs-1; proc++) {
	  if( proc!=proc_id-1) {
	    for(int i=0; i< exp_Nelspp[proc]; i++) {
	      rcvmap[proc][ exp_topp[proc][i]] [             exp_others_loc2glob[proc][ exp_frompp[proc][i] ]  ]    =0;
	    }
	  } else {
	    for(int i=0; i< exp_Nelspp[proc]; i++) {
	      rcvmap[proc] [ exp_topp[proc][i] ]  [  exp_frompp[proc][i]  ]   =0;
	    }
	  }
	}
      }
      
      
      for(int proc=0; proc<n_procs-1; proc++) {
	int meslen;
	int *messaggio;
	meslen    =  messaggioreceivelen[proc];
	messaggio =  messaggioreceive   [proc];
	

	int pos = 0 ;
	for(int k=proc_id-1; k<n_procs-1; k++) {
	  
	  int proc2 = messaggio[pos];
	  DEBUG( if (proc_id==dbp)   printf("proc_id %d proc2 %d\n",  proc_id ,proc2 ) ; );
	  pos++;
	  int nel = messaggio[pos];
	  pos++;
	  if(ppcoeffs==0) {
	    for(int i=0; i<nel; i++) {
	      // printf("BBBBBBB %d %d %d \n", proc2, messaggio[pos],messaggio[pos+1]  );
	      rcvmap[proc2][messaggio[pos]][messaggio[pos+1]]=1;
	      pos+=2;
	    }
	  } else {
	    
	    for(int i=0; i<nel; i++) {
	      std::map<int,float>::iterator  mspos;
	      mspos = rcvmap[proc2][messaggio[pos]].find(messaggio[pos+1]);
	      if(mspos!= rcvmap[proc2][messaggio[pos]].end()) {
		rcvmap[proc2][messaggio[pos]][messaggio[pos+1]]+=((float*) messaggio)[pos+2];
	      }
	      pos+=3;
	    }
	  }
	}
	// si cancella messaggio ; 
	delete messaggio;      }
    }
  }
  MPI_Barrier(MPI_COMM_WORLD );
  
  if( ppcoeffs==0) {
    
    
    if(!exp_Nelspp) {
      exp_Nelspp= new int [n_procs-1];
      exp_frompp= new int* [ n_procs-1];
      exp_topp  = new int* [ n_procs-1];
      exp_loc2glob = new int *[ n_procs-1];
      exp_others_loc2glob = new int *[ n_procs-1];
      exp_dims2 = new int [ n_procs-1];
      exp_others_dims2 = new int [ n_procs-1];
      
    } else {
      for(int proc=0; proc<n_procs-1; proc++) {
	delete exp_frompp[proc];
	delete exp_topp  [proc];
	delete exp_loc2glob  [proc];
	if( proc!=proc_id-1)  {   // *)(
	  delete exp_others_loc2glob [proc];
	}
      }
    }
     
    // gli exp_coeffspp sono fissati da fissaexp
    
    for(int proc=0; proc<n_procs-1; proc++) {
      
      exp_Nelspp[proc]=0;
      for(int i=0; i< dims[thisproc]; i++) {
	exp_Nelspp[proc]+=rcvmap[proc][i].size();
      }
      DEBUG(printf(" FFFF processo %d size per %d %d  \n", proc_id,proc, exp_Nelspp[proc] );)
      exp_frompp[proc] = new int [exp_Nelspp[proc]+1];
      exp_topp  [proc] = new int [exp_Nelspp[proc]+1];
      int count=0;
      for(int i=0; i< dims[thisproc]; i++) { 
	int n= rcvmap[proc][i].size();
	std::map<int,float>::iterator pos;
	pos=rcvmap[proc][i].begin();
	for(int k=0; k<n;k++) {
	  exp_frompp[proc][count] =  pos->first        ;
	  exp_topp  [proc][count] =  i     ; 
	  pos++;
	  count++;
	}
      }
      delete [] rcvmap[proc];
    }
    
  } else {
    
    DEBUG(printf("   ZZZZZZZ processo %d \n", proc_id);)
      
      for(int proc=0; proc<n_procs-1; proc++) {
	DEBUG(printf(" FFFFGGG  processo %d size per %d %d  \n", proc_id,proc, exp_Nelspp[proc] );)
	
	int count=0;
	for(int i=0; i< dims[thisproc]; i++) { 
	  int n= rcvmap[proc][i].size();
	  std::map<int,float>::iterator pos;
	  pos=rcvmap[proc][i].begin();
	  for(int k=0; k<n;k++) {
	    exp_coeffspp[proc+target*(n_procs-1)][count] = pos->second;
	    pos++;
	    count++;
	  }
	}
	delete [] rcvmap[proc];
      }    
    return;
  }
  
  
  delete [] rcvmap;
  
  DEBUG(printf(" FFFF processo %d giunto \n", proc_id););
  
  // MPI_Finalize();
  // exit(0);
  
  {
    exp_maxdim2=0;
    memset(exp_others_dims2 , 0, (n_procs-1)*sizeof(int));
    
    MPI_Request request;
    int * used;
    
    for(int proc=0; proc< n_procs-1; proc++) {
      used = new int [ dims[proc]];
      memset(used, 0, dims[proc]*sizeof(int));
      for(int i=0; i<exp_Nelspp[proc]; i++) {
	used[exp_frompp[proc][i]]=1;
      }
      
      
      exp_others_dims2[proc]=0;
      for(int i=0; i<dims[proc]; i++) {
	if( used[i]) exp_others_dims2[proc]++;
      }
      
      if(proc!=proc_id-1) {
	MPI_Isend(&(exp_others_dims2[proc]), 1, MPI_INT, proc+1, 123, MPI_COMM_WORLD, &request);
      }
      delete used;
    }
    exp_dims2[proc_id-1]= dims[proc_id-1]; 
    
    
    MPI_Status status;
    for(int proc=0; proc< n_procs-1; proc++) {
      if(proc!=proc_id-1) {
	
	MPI_Recv(&(exp_dims2[proc]), 1, MPI_INT, proc+1, 123, MPI_COMM_WORLD, &status);
	
	if( exp_dims2[proc]>exp_maxdim2) exp_maxdim2=exp_dims2[proc];
      }
    }
    
    
    
    
    for(int ishift=0; ishift<n_procs-1; ishift++) {
      int procto   = proc_id-1+ishift;
      int procfrom = proc_id-1-ishift;
      procto=procto%(n_procs-1 );
      procfrom=(procfrom+(n_procs-1 ))%(n_procs-1 );
      
      int proc=procto;
      int proctk=procfrom;
      
      used = new int[ dims[proc]]; 
      memset(used, 0, dims[proc]*sizeof(int));
      
      for(int i=0; i<exp_Nelspp[proc]; i++) {
	used[exp_frompp[proc][i]]=1;
      }
      

      exp_others_loc2glob[proc]  = new int [  exp_others_dims2[proc]  ];
      if(ishift) {
	exp_loc2glob[proctk] = new int [  exp_dims2[proctk]  ];    
      } else {  // *)(
	exp_loc2glob[proctk] = exp_others_loc2glob[proc] ; 
      }
      
      int count=0;
      
      for(int i=0; i<dims[proc]; i++) {
	if( used[i]) {
	  exp_others_loc2glob[proc][count]=i;
	  used[i]=count;
	  count++;
	}
      }
      
      
      if(ishift) {
	// usare used come inverso per aggiustare i from ; 
	for(int i=0; i<exp_Nelspp[proc]; i++) {
	  exp_frompp[proc][i] = used[  exp_frompp[proc][i]     ];
	}
      }
      
      delete used;
      if(ishift) {
	MPI_Sendrecv(   exp_others_loc2glob[proc] , exp_others_dims2[proc] , MPI_INT , proc+1, 89,
			exp_loc2glob[proctk]  , exp_dims2[proctk] , MPI_INT , proctk+1, 89,
			MPI_COMM_WORLD, &status);
      }  
    } 
  }  
}








Sparsa3AP2::~Sparsa3AP2() {

  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    items[item_id]=0;
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {
      sprintf(buffer,"distruggiS2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );


  } else {
   
    printf( "distruggi schiavo \n") ;
    if(Nelspp) {
      for(int proc=1; proc< n_procs; proc++) {
	if( !Nelspp[proc-1]) continue;

	delete topp[proc-1];

 	delete frompp[proc-1];
 	delete coeffspp[proc-1];
 	delete loc2glob[proc-1];

 	if( proc !=proc_id) {
	  delete others_loc2glob[proc-1];
	}
	delete others_buffer[proc-1];
      }

      delete Nelspp;
      delete dims2;
      delete others_dims2;
      delete bufferinvio;
      delete others_buffer;
      delete dims;
    }
  }
  
  if(exp_Nelspp) {
    for(int proc=1; proc< n_procs; proc++) {
      if( !exp_Nelspp[proc-1]) continue;
      
      delete exp_topp[proc-1];
      
      delete exp_frompp[proc-1];
      if(exp_coeffspp) {
	for(int nexp=0;nexp<NEXPVECT; nexp++) {
	  delete exp_coeffspp[proc-1 +nexp*(n_procs-1)];
	}
      }
      delete exp_loc2glob[proc-1];
      // printf( "distruggi schiavo %d %d %ld\n", proc_id, proc, exp_others_loc2glob[proc-1]) ;
	if( proc !=proc_id) {
	delete exp_others_loc2glob[proc-1];
      }
      // delete exp_others_buffer[proc-1];
    }
    
      delete exp_Nelspp;
      delete exp_dims2;
      delete exp_others_dims2;
      // delete bufferinvio;
      // delete others_buffer;
  }
  



};


void Sparsa3AP2::LLtSolve(VectorP2 *Ares,  VectorP2 *Avect  ) {

  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"LLtSolve2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(& (Ares->item_id)  , 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(& (Avect->item_id), 1, MPI_INT, proc, 102, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
  
    MPI_Status status ; 
    int thisproc=proc_id-1;
    int thisdim = this->dims[proc_id-1];

    double pregressi[thisdim];

    for(int i=0; i<thisdim; i++) {
      pregressi[i]=0.0;
    }
  
    for(int p=0; p<thisproc+1; p++) {
      double *sol;
      if(p!=thisproc) {
	sol= new double [ this->dims[  p   ]   ] ; 
	MPI_Recv( sol   ,   this->dims[p]  ,  MPI_DOUBLE ,  p+1    ,  39 ,  MPI_COMM_WORLD, &status);
      } else {
	sol = Ares->coeffs;
      }
      for(int d=0; d<thisdim; d++) {
      
	int iend = lltn[p][d]  - (p==thisproc)  ;
      
	for(int iel=0; iel < iend ; iel++) {
	  pregressi[ d ]  +=   lltcoeff[p][d][iel]  *  sol[  lltrow[p][d][iel]  ]           ;
	}
      }
      if( p==thisproc) {
	for(int d=0; d<thisdim; d++) { 
	  if(lltn[p][d]) {
	    sol[d]  =(Avect->coeffs[d]-pregressi[d])/lltcoeff[p][d] [ lltn[p][d]-1  ];
	  }
	}
      }
      if(p!=thisproc) {
	delete sol;
      }
    }
 
    // ********************************************************************************************

    for(int i=0; i<thisdim; i++) {
      pregressi[i]=0.0;
    }
  
    for(int p=this->n_procs-1; p>=thisproc; p--) {
      double *sol;
      if(p!=thisproc) {
	sol= new double [ this->dims[  p   ]   ] ; 
	MPI_Recv( sol   ,   this->dims[p]  ,  MPI_DOUBLE ,  p+1    ,  39 ,  MPI_COMM_WORLD, &status);
      } else {
	sol = Ares->coeffs;
      }
      for(int d=thisdim-1; d>=0; d--) {
      
	int iend = Tlltn[p][d]     ;
      
	for(int iel=(p==thisproc); iel < iend ; iel++) {
	  pregressi[ d ]  +=   Tlltcoeff[p][d][iel]  *  sol[  Tlltcol[p][d][iel]  ]           ;
	}
	if( p==thisproc) {
	  if(lltn[p][d]) {
	    sol[d]  =(sol[d]-pregressi[d])/Tlltcoeff[p][d] [ Tlltn[p][d]-1  ];
	  }
	}
      }
      if(p!=thisproc) {
	delete sol;
      }
    }
 
    // for(int p= thisproc+1; p< this->n_procs; p++) {
    for(int p=thisproc-1 ; p>=0 ; p--) {
      MPI_Send(   Ares->coeffs  ,   this->dims[thisproc]  ,  MPI_DOUBLE ,  p+1    ,  39 ,  MPI_COMM_WORLD);
    }
  }
}



void Sparsa3AP2::Moltiplica(VectorP2 *ris, VectorP2 *vect  ) {
  if( proc_id==0) {
    double t1 = MPI_Wtime();
    char buffer[MSGLEN];
    MPI_Status status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"Moltiplica2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(& (ris->item_id)  , 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(& (vect->item_id), 1, MPI_INT, proc, 102, MPI_COMM_WORLD);
    }
#ifdef PIRAMIDE
    double T1 = MPI_Wtime();
    
    if(!multdone) {
      this->Ntorecv = new int[this->n_procs-1];
      this->buffertorecv = new double * [this->n_procs-1];
      this->Nmax2trasm=0;
      for(int i=0; i<this->n_procs-1; i++) {
	int dum;
	MPI_Recv(this->Ntorecv+i,1,MPI_INT,i+1,200+i+1,MPI_COMM_WORLD,&status);
	printf(" dal processo %d ricevero u buffer lungo %d \n",  i+1,  this->Ntorecv[i ] );
	MPI_Recv(&dum,1,MPI_INT,i+1,500+i+1,MPI_COMM_WORLD,&status);
	if( Nmax2trasm<dum) Nmax2trasm=dum;
	this->buffertorecv[i]=new double[   this->Ntorecv[i] ];
      }
      this->bufferinvio = new double[Nmax2trasm];
      multdone=1;
    }
    for(int i=0; i<this->n_procs-1; i++) {
      MPI_Recv(   this->buffertorecv[i]     , this->Ntorecv[i], MPI_DOUBLE , i+1 ,300+i+1,MPI_COMM_WORLD,&status);
    }
    for(int proc=1 ; proc<this->n_procs; proc++) {
      int proc_from;
      int count=0;
      for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {
	proc_from=proc - Dproc;
	if(proc_from<1) {
	  proc_from=proc_from + n_procs-1;
	}
	double *bufptr =   this->buffertorecv[proc_from-1] ;
	for(int j=1; j<Dproc; j++ ) {
	  bufptr +=   (int) ( *bufptr    ) +1;
	}
	int dim=*bufptr;
	bufptr++;
	memcpy(   bufferinvio+count  , bufptr, dim* sizeof(double)      );
	count+= dim ;
      }
      MPI_Send(   bufferinvio    , count  , MPI_DOUBLE , proc ,400+proc,MPI_COMM_WORLD );
    }

    double T2 = MPI_Wtime();
    // printf(" Moltiplica per TRASMISSIONE  PIRAMIDE %e \n",  T2-T1);


#endif    
    
    MPI_Barrier(MPI_COMM_WORLD );    
    double t2 = MPI_Wtime();
    // printf(" Moltiplica per il calcolo TOTALE %e \n",  t2-t1);
    
  } else {
    
    {
      if(proc_id==-2) {
	for(int proc=0; proc<n_procs-1; proc++) {
	  printf(" Nelspp[%d, %d] = %d \n",proc_id, proc, Nelspp[proc]);
	
	  for(int i=0; i< Nelspp[proc]; i++) {
	    printf("%d %d %e \n", frompp[proc][i], topp[proc][i], coeffspp[proc][i]);
	  }
	}
	for(int proc=0; proc<n_procs-1; proc++) {
	  printf(" oters_dims2[%d, %d] = %d    dims2[%d,%d]= %d \n",proc_id, proc, others_dims2[proc], proc_id, proc, dims2[proc]   );
	}
	for(int proc=0; proc<n_procs-1; proc++) {
	  printf(" from proc %d \n", proc);
	  for(int i=0; i<Nelspp[proc]; i++) {
	    printf(" %d %d %e \n", frompp[proc][i], topp[proc][i], coeffspp[proc][i]); 
	  }
	  for(int i=0; i<dims2[proc]; i++) {
	    printf("loc2glob %d \n",  loc2glob[proc][i]);
	  }
	}
      }
    }
    
    int proc_to;
    int proc_from;


    struct timespec start, end;
    double tempo;
    



#ifdef BLOCKING


#ifdef MAPPA
    
    std::map<Trasmissione,int>::iterator pos;
    
    pos = trasmissioni.begin();
    
    int N=trasmissioni.size();
    
    int senso;
    int proc;
    
    for(int i=0; i<N; i++ ) {
      if ( pos->first.proca == proc_id-1) {
	senso=1;
	proc=pos->first.procb ; 
      }  else {
	senso=-1;
	proc=pos->first.proca ; 
      }
      


      if( senso==1) {
	// printf(" processo %d verso %d \n", proc_id-1, proc);
	
	for(int i=0; i<dims2[proc]; i++) {
	  bufferinvio[i]= vect->coeffs[   loc2glob[proc][i]]; 
	}
	
	MPI_Send(  bufferinvio  , dims2[proc] , MPI_DOUBLE , proc+1, 89, MPI_COMM_WORLD);	
	// tot_t+= dims2[proc]; 
	
      } else {
	// printf(" processo %d da %d \n", proc_id-1, proc);
	MPI_Status status;
	MPI_Recv(  others_buffer[proc] , others_dims2[proc] , MPI_DOUBLE , proc+1, 89,
		       MPI_COMM_WORLD, &status);
      }
      pos++;
    }
#else

    double t1 = MPI_Wtime();
        
    int tot_t=0;
    int tot_r=0;

    for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {
      proc_to=proc_id+Dproc;
      if(proc_to>=n_procs) {
	proc_to=proc_to-n_procs +1;
      }
      proc_from=proc_id - Dproc;
      if(proc_from<1) {
	proc_from=proc_from + n_procs-1;
      }
      
 
      

      MPI_Request statusa, statusb, statusc ;
      MPI_Status status;
      
      for(int i=0; i<dims2[proc_to-1]; i++) {
	bufferinvio[i]= vect->coeffs[   loc2glob[proc_to-1][i]]; 
      }
      
      MPI_Sendrecv(  bufferinvio  , dims2[proc_to-1] , MPI_DOUBLE , proc_to, 89,
		     others_buffer[proc_from-1] , others_dims2[proc_from-1] , MPI_DOUBLE , proc_from, 89,
		     MPI_COMM_WORLD, &status);

      tot_t+= dims2[proc_to-1]; 
      tot_r+=  others_dims2[proc_from-1]; 
      
    }


    double t2 = MPI_Wtime();

#endif

//     if(proc_id==1)
//       printf(" proc_id %d tempo per trasmissione %e tot-T %d  tot_R %d  \n", proc_id, t2-t1,tot_t, tot_r );
    



#else

#ifdef PIRAMIDE

    if(!multdone) {

      printf(" in piramide processo %d, multdone=0 \n", proc_id);
      multdone=1;

      this->len2trasm=0;
      this->len2recv =0;

      for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {

	proc_to=proc_id+Dproc;
	if(proc_to>=n_procs) {
	  proc_to=proc_to-n_procs +1;
	}


	proc_from=proc_id - Dproc;
	if(proc_from<1) {
	  proc_from=proc_from + n_procs-1;
	}
	this->len2trasm+=dims2[proc_to-1] + 1 ; 
	this->len2recv +=others_dims2[proc_from-1]; 

      }

      printf(" il processo %d mandera a 0 un buffer lungo %d \n", proc_id, len2trasm ) ;
      MPI_Send(&len2trasm,1,MPI_INT,0,200+proc_id,MPI_COMM_WORLD);
      MPI_Send(&len2recv,1, MPI_INT,0,500+proc_id,MPI_COMM_WORLD);
      this->bufferinvio = new double [ Max(this->len2trasm, this->len2recv) ];
    } 

    {
      int count=0; 
      for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {
	proc_to=proc_id+Dproc;
	if(proc_to>=n_procs) {
	  proc_to=proc_to-n_procs +1;
	}
	this->bufferinvio[count]=dims2[proc_to-1];
	for(int i=0; i<dims2[proc_to-1]; i++) {
	  this->bufferinvio[count+i+1]= vect->coeffs[   loc2glob[proc_to-1][i]]; 
	}
	count+= dims2[proc_to-1]+1;
      }
    }
    MPI_Status status;
    double t1 = MPI_Wtime();
    MPI_Send(   this->bufferinvio     , this->len2trasm, MPI_DOUBLE , 0 ,300+proc_id,MPI_COMM_WORLD);
    MPI_Recv(   this->bufferinvio     , this->len2recv, MPI_DOUBLE , 0 ,400+proc_id,MPI_COMM_WORLD, &status);
    double t2 = MPI_Wtime();
    
    //if(proc_id==1)
    //  printf(" tempo per trasmissione %e \n", t2-t1);


    int count=0;
    for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {
      proc_from=proc_id - Dproc;
      if(proc_from<1) {
	proc_from=proc_from + n_procs-1;
      }
      memcpy(   others_buffer[proc_from-1]  , this->bufferinvio+count, this->others_dims2[proc_from-1] * sizeof(double)      );
      count+=this->others_dims2[proc_from-1] ;
    }

#else

    if(!multdone) {
      multdone=1;
      this->Nreqs = 2*(n_procs-2);
      this->reqs = new MPI_Request[2*(n_procs-2)] ;
      int coutnreq=0;
      int posinbuff=0;
      for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {

	proc_to=proc_id+Dproc;
	if(proc_to>=n_procs) {
	  proc_to=proc_to-n_procs +1;
	}


	proc_from=proc_id - Dproc;
	if(proc_from<1) {
	  proc_from=proc_from + n_procs-1;
	}
	
	if(dims2[proc_to-1]) {
	  MPI_Send_init (  this->bufferinvio +  posinbuff   ,  dims2[proc_to-1], MPI_DOUBLE,  proc_to     , 89, MPI_COMM_WORLD, &(this->reqs[coutnreq]) );
	  coutnreq++;
	  posinbuff+= dims2[proc_to-1];
	}

	if(others_dims2[proc_from-1]  ) {
	  MPI_Recv_init ( others_buffer[proc_from-1]  , others_dims2[proc_from-1]  , MPI_DOUBLE,  proc_from     , 89, MPI_COMM_WORLD, &(this->reqs[coutnreq]) );
	  coutnreq++;
	}	
      }
      this->Nreqs =  coutnreq;
    } 




    {
      int count=0;
      
      for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {
	proc_to=proc_id+Dproc;
	if(proc_to>=n_procs) {
	  proc_to=proc_to-n_procs +1;
	}
	for(int i=0; i<dims2[proc_to-1]; i++) {
	  this->bufferinvio[count+i]= vect->coeffs[   loc2glob[proc_to-1][i]]; 
	}
	count+= dims2[proc_to-1];
      }
    }



    MPI_Status stats[this->Nreqs ];

    // printf(" ci sono %d richieste \n", this->Nreqs);
    double t1 = MPI_Wtime();

    MPI_Startall ( this->Nreqs  ,this->reqs );
    MPI_Waitall (this->Nreqs  ,this->reqs ,  stats);

    double t2 = MPI_Wtime();
    
    //if(proc_id==1)
    //  printf(" tempo per trasmissione %e \n", t2-t1);

#endif
#endif


    

    for ( int proc = 1; proc< n_procs; proc++) {
      if( proc!=proc_id) {
	if(proc_id==-2) {
	  printf(" da %d \n", proc-1);
	  for(int i=0 ; i< others_dims2[proc-1]; i++) {
	    printf(" %e \n", others_buffer[proc-1][i] );
	  }
	
	printf(" prima " );
	for(int k=0 ; k< dims[proc_id-1]; k++) printf(" %e ", ris->coeffs[k]);
	printf("\n");
	}
	  ::Moltiplica( ris->coeffs , others_buffer[proc-1]   , Nelspp[proc-1] , topp[proc-1], frompp[proc-1], coeffspp[proc-1] );
	if(proc_id==-2) {
	printf(" dopo " );
	for(int k=0 ; k< dims[proc_id-1]; k++) printf(" %e ", ris->coeffs[k]);
	printf("\n");
	}
	

	  
      } else {
	::Moltiplica( ris->coeffs , vect->coeffs   , Nelspp[proc-1] , topp[proc-1], frompp[proc-1], coeffspp[proc-1] );
      }
    }

  

  }
}

void Sparsa3AP2::MoltiplicaExp(VectorP2 *ris, VectorP2 *vect  ) {
  if( proc_id==0) {
    double t1 = MPI_Wtime();
    char buffer[MSGLEN];
    MPI_Status status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"Moltiplica2Exp");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(& (ris->item_id)  , 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(& (vect->item_id), 1, MPI_INT, proc, 102, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
    double t2 = MPI_Wtime();
    // printf(" Moltiplica per il calcolo TOTALE %e \n",  t2-t1);
    
  } else {
    
    
    int proc_to;
    int proc_from;


    struct timespec start, end;
    double tempo;
    
#ifdef BLOCKING
#ifdef MAPPA
    
    std::map<Trasmissione,int>::iterator pos;
    
    pos = trasmissioniExp.begin();
    
    int N=trasmissioniExp.size();
    
    int senso;
    int proc;
    
    for(int i=0; i<N; i++ ) {
      if ( pos->first.proca == proc_id-1) {
	senso=1;
	proc=pos->first.procb ; 
      }  else {
	senso=-1;
	proc=pos->first.proca ; 
      }
      
      if( senso==1) {
	// printf(" processo %d verso %d \n", proc_id-1, proc);
	
	for(int i=0; i<exp_dims2[proc]; i++) {
	  bufferinvio[i]= vect->coeffs[   exp_loc2glob[proc][i]]/precodiag[ exp_loc2glob[proc][i]  ]; 
	}
	
	MPI_Send(  bufferinvio  , exp_dims2[proc] , MPI_DOUBLE , proc+1, 89, MPI_COMM_WORLD);	
	// tot_t+= dims2[proc]; 
	
      } else {
	// printf(" processo %d da %d \n", proc_id-1, proc);
	MPI_Status status;
	MPI_Recv(  others_buffer[proc] , exp_others_dims2[proc] , MPI_DOUBLE , proc+1, 89,
		       MPI_COMM_WORLD, &status);
      }
      pos++;
    }

#endif
#endif
    for ( int proc = 1; proc< n_procs; proc++) {
      if( proc!=proc_id) {
	::Moltiplica( ris->coeffs , others_buffer[proc-1]   , exp_Nelspp[proc-1] , exp_topp[proc-1], exp_frompp[proc-1], exp_coeffspp[proc-1] );
      } else {
	memset(others_buffer[proc-1], 0, dims[proc-1]*sizeof(double));
	for(int i=0; i<dims[proc-1]; i++) {
	  if(precodiag[i]) 
	    others_buffer[proc-1][i]=vect->coeffs [i]/precodiag[i];
	}
	::Moltiplica( ris->coeffs , others_buffer[proc-1]   , exp_Nelspp[proc-1] , exp_topp[proc-1], exp_frompp[proc-1], exp_coeffspp[proc-1] );
      }
    }
  }
}










void Sparsa3AP2::MoltiplicaExpL(VectorP2 *ris, VectorP2 *vect  ) {
  if( proc_id==0) {
    double t1 = MPI_Wtime();
    char buffer[MSGLEN];
    MPI_Status status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"Moltiplica2ExpL");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(& (ris->item_id)  , 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(& (vect->item_id), 1, MPI_INT, proc, 102, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
    double t2 = MPI_Wtime();
    // printf(" Moltiplica per il calcolo TOTALE %e \n",  t2-t1);
    
  } else {
    
    
    int proc_to;
    int proc_from;
    
    
    struct timespec start, end;
    double tempo;
    
    for ( int proc = 1; proc< n_procs; proc++) {
      if( proc!=proc_id) {
	memset(others_buffer[proc-1]  , 0 , exp_others_dims2[proc-1]*sizeof(double));
      } else {
	memset(others_buffer[proc-1]  , 0 , dims[proc-1]*sizeof(double));
      }
      
      ::Moltiplica(others_buffer[proc-1] ,  vect->coeffs , exp_Nelspp[proc-1] , exp_frompp[proc-1], exp_topp[proc-1],  exp_coeffspp[proc-1] );
      //       } else {
      // 	::Moltiplica( ris->coeffs , vect->coeffs   ,  exp_Nelspp[proc-1] , exp_frompp[proc-1], exp_topp[proc-1],  exp_coeffspp[proc-1] );
      //       }
    }
    
    
#ifdef BLOCKING
#ifdef MAPPA
    
    std::map<Trasmissione,int>::iterator pos;
    
    pos = trasmissioniExp.begin();
    
    int N=trasmissioniExp.size();
    
    int senso;
    int proc;
    
    for(int i=0; i<N; i++ ) {
      if ( pos->first.proca == proc_id-1) {
	senso=-1;
	proc=pos->first.procb ; 
      }  else {
	senso= 1;
	proc=pos->first.proca ; 
      }
      
      if( senso==1) {
	MPI_Send(  others_buffer[proc]   , exp_others_dims2[proc] , MPI_DOUBLE , proc+1, 89, MPI_COMM_WORLD);	
 	
      } else {
	MPI_Status status;
	MPI_Recv(  bufferinvio , exp_dims2[proc] , MPI_DOUBLE , proc+1, 89,
		   MPI_COMM_WORLD, &status);
	
	
	
	for(int i=0; i<exp_dims2[proc]; i++) { 
	  ris->coeffs[   exp_loc2glob[proc][i]] +=      bufferinvio[i]/precodiag[exp_loc2glob[proc][i]] ; 
	}
	
      }
      pos++;
    }
    for(int i=0; i<dims[proc_id-1]; i++) { 
      if(precodiag[i]!=0.0)
	ris->coeffs[   i ] +=      others_buffer[proc_id-1] [i] /precodiag[i] ; 
    }
    
#endif
#endif
    
  }
}




















void Sparsa3AP2::preparaPrecoExp( ) {
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"preparaprecoexp");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  }
}
void Sparsa3AP2::preparaPrecoExp_( ) {

    int thisdim = this->dims[this->proc_id-1] ;
    precodiag = new double [   thisdim ] ;
    for(int i=0; i< thisdim  ; i++  ) {
      precodiag[i]=0;
    }

    int thisproc =this->proc_id-1  ; 
    int maxi=0;

    for(int i=0; i< exp_Nelspp[ thisproc];i++ ) {
      if (  exp_frompp[thisproc][i]  == exp_topp[thisproc][i]    ) {
	precodiag[ exp_frompp[thisproc][i]   ] =   sqrt(exp_coeffspp[thisproc][i])   ; 
      } 
    }

//     for(int i=0; i< thisdim  ; i++  ) {
//       if( precodiag[i]!=0.0)
// 	precodiag[i]= 1.0/(precodiag[i]) ; 
//     }


//     char nome[80];
//     sprintf(nome, "matrice_%d", proc_id-1);
//     FILE * f = fopen(nome,"w");
//     char  pesca[]="AB";
//     for(int proc=0; proc<n_procs-1; proc++) {
//       // fprintf(f,"proc %d \n", proc);
//       if(proc==thisproc) {
// 	for(int i=0; i< exp_Nelspp[ thisproc];i++ ) {
// 	  // fprintf(f, "%d %d  %e\n", exp_frompp[thisproc][i],exp_topp[thisproc][i], exp_coeffspp[thisproc][i]    ) ;
// 	  fprintf(f, " %e, # %c\n",  exp_coeffspp[thisproc][i] , pesca[exp_topp[proc][i] ]   ) ;
// 	}
//       } else {
// 	for(int i=0; i< exp_Nelspp[ proc];i++ ) {
// 	  fprintf(f, "  %e, # %c \n",      exp_coeffspp[proc][i] , pesca[exp_topp[proc][i] ]   ) ;
// 	  // fprintf(f, "%d %d %e\n", exp_others_loc2glob[proc][exp_frompp[proc][i]],exp_topp[proc][i], exp_coeffspp[proc][i]    ) ;
// 	}
//       }
//     }
//     fclose(f);

//     sprintf(nome, "matriceO_%d", proc_id-1);
//     f = fopen(nome,"w");
//     for(int proc=0; proc<n_procs-1; proc++) {
//       //      fprintf(f,"proc %d \n", proc);
//       if(proc==thisproc) {
// 	for(int i=0; i< Nelspp[ thisproc];i++ ) {
// 	  //fprintf(f, "%d %d  %e\n", frompp[thisproc][i],topp[thisproc][i], coeffspp[thisproc][i]    ) ;
// 	  fprintf(f, " %e\n",  coeffspp[thisproc][i]    ) ;
// 	}
//       } else {
// 	for(int i=0; i< Nelspp[ proc];i++ ) {
// 	  fprintf(f, " %e\n",  coeffspp[proc][i]    ) ;
// 	  // fprintf(f, "%d %d %e\n", others_loc2glob[proc][frompp[proc][i]],topp[proc][i], coeffspp[proc][i]    ) ;
// 	}
//       }
//     }
//     fclose(f);




}






void Sparsa3AP2::preparaPreco( ) {
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"preparapreco");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  }
}
void Sparsa3AP2::preparaPreco_( ) {

//     printf(" inizio preparapreco %d \n", proc_id);

    int thisdim = this->dims[this->proc_id-1] ;

    

    precoN = new int[   thisdim ] ;
    precoN_t = new int[  thisdim  ] ;
    precodiag = new double [   thisdim ] ;
    for(int i=0; i< thisdim  ; i++  ) {
      precoN[i]=0;
      precoN_t[i]=0;
      precodiag[i]=0;
    }
    int thisproc =this->proc_id-1  ; 
    int maxi=0;
    for(int i=0; i< Nelspp[ thisproc];i++ ) {
      if (  frompp[thisproc][i]  == topp[thisproc][i]    ) {
	precodiag[ frompp[thisproc][i]   ] =   coeffspp[thisproc][i]   ; 
      } else if (  frompp[thisproc][i]   > topp[thisproc][i]   ) {
	precoN[frompp[thisproc][i] ]++; 
	precoN_t[topp[thisproc][i] ]++; 
	if( frompp[thisproc][i] > maxi) maxi =frompp[thisproc][i] ;
	if( topp[thisproc][i] > maxi) maxi =topp[thisproc][i] ;

      } 
    }
    printf(" preparapreco  limitato  alla diagonale \n");
    // printf(" inizio preparapreco %d +1  thisdim %d  maxi %d \n", proc_id, thisdim, maxi);
   
    precoFrom = new int *[thisdim];
    precoCoeff = new double * [ thisdim];

    precoFrom_t = new int *[thisdim];
    precoCoeff_t = new double * [ thisdim];

    // printf(" inizio preparapreco %d +1bis  thisdim %d\n", proc_id, thisdim);

    for(int i=0; i< thisdim  ; i++  ) {
//       if(proc_id==1) {
// 	printf(" %d %d \n", precoN[i], precoN_t[i] );
//       }
      if(precoN[i]) {
	precoFrom[i]  = new int[precoN[i] ];
	precoCoeff[i]   = new double[precoN[i]];
      }
      precoN[i]=0;


      if(precoN_t[i]) {
	precoFrom_t[i]   = new int[precoN_t[i] ];
	precoCoeff_t[i]   = new double[precoN_t[i]];
      }
      precoN_t[i]=0;
    }
//      printf(" inizio preparapreco %d +2\n", proc_id);
   
    for(int i=0; i< Nelspp[ thisproc];i++ ) {
      if (  frompp[thisproc][i]   > topp[thisproc][i]   ) {
	precoFrom[frompp[thisproc][i] ][precoN[frompp[thisproc][i] ]  ] = topp[    thisproc     ][i]; 
	precoCoeff[frompp[thisproc][i] ][precoN[frompp[thisproc][i] ]  ] = coeffspp[    thisproc     ][i]; 
	precoN[frompp[thisproc][i] ]++; 

	precoFrom_t[topp[thisproc][i] ][precoN_t[topp[thisproc][i] ]  ] = frompp[    thisproc     ][i]; 
	precoCoeff_t[topp[thisproc][i] ][precoN_t[topp[thisproc][i] ]  ] = coeffspp[    thisproc     ][i]; 
	precoN_t    [topp[thisproc][i] ]++; 
      } 
    }   
//     Printf(" finito preparapreco %d \n", proc_id);
}

void Sparsa3AP2::Preco(VectorP2 *ris, VectorP2 *vect , double omega ) {


  if( proc_id==0) {
    double t1 = MPI_Wtime();
    char buffer[MSGLEN];
    MPI_Status status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"Preco2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(& (ris->item_id)  , 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(& (vect->item_id), 1, MPI_INT, proc, 102, MPI_COMM_WORLD);
      MPI_Send(& omega, 1, MPI_DOUBLE, proc, 103, MPI_COMM_WORLD);
    }

    
    MPI_Barrier(MPI_COMM_WORLD );    
    double t2 = MPI_Wtime();
    // printf(" Preco  per il calcolo TOTALE %e \n",  t2-t1);
    
  } else {
    int thisdim = this->dims[this->proc_id-1] ;





    // ###########################################
    {
      int dim= thisdim;
      // for(int i=0;  i<dim;  i++) {
      for(int i=dim-1; i>=0; i--) {
	if( precodiag[i]!=0.0) {
	  ris->coeffs[i] = vect->coeffs[i]/precodiag[i];
	} else {
	  ris->coeffs[i] = 0.0;
	}  
      }
    }

    return;
    // ####################################################






    // printf(" sono in preco %d \n", proc_id);
    int proc_to;
    int proc_from;


    struct timespec start, end;
    double tempo;
    
    double tmpres[thisdim ] ;

    memset(tmpres , 0 , thisdim *sizeof( double ) );


    if( 0 && proc_id==1) {
      clock_gettime(CLOCK_REALTIME, &start );
    }




    double t1 = MPI_Wtime();
        
    int tot_t=0;
    int tot_r=0;

    MPI_Status status ; 
//     // riceve dai superiori 
//     printf(" AAA  sono in preco %d   prima di riceve \n", proc_id);
    
//     for( int procsup = this->n_procs-1-1   ; procsup > this->proc_id-1 ; procsup-- ) {
//       MPI_Recv(  others_buffer[ procsup]  , others_dims2[procsup] , MPI_DOUBLE ,procsup+1 , 89,  MPI_COMM_WORLD, &status );
//       ::Moltiplica( tmpres  , others_buffer[procsup]   , Nelspp[procsup] , topp[procsup], frompp[procsup], coeffspp[procsup] );
//     }
//      printf(" AAA sono in preco %d   dopo di riceve \n", proc_id);
   
    // riceve dai inferiori
    // printf(" AAA  sono in preco %d   prima di riceve \n", proc_id);
    

    if(0) { // si fa una risoluzione locale

      for( int procsup = 0   ; procsup <this->proc_id-1 ; procsup++ ) {
	MPI_Recv(  others_buffer[ procsup]  , others_dims2[procsup] , MPI_DOUBLE ,procsup+1 , 89,  MPI_COMM_WORLD, &status );
	::Moltiplica( tmpres  , others_buffer[procsup]   , Nelspp[procsup] , topp[procsup], frompp[procsup], coeffspp[procsup] );
      }
    }
    //  printf(" AAA sono in preco %d   dopo di riceve \n", proc_id);
   
    // risolve

    {
      int dim= thisdim;
//       for(int i=dim-1; i>=0; i--) {
      for(int i=0; i<dim; i++) {
	int N = precoN[i];
	for(int j=0; j <N; j++) {
	  tmpres[i] += precoCoeff[i][j]*tmpres[precoFrom[i][j]];
	}
	if( precodiag[i] != 0.0)

	  // printf(" in processo %d  per elemento %d  precodiag = %e  vect->coeffs %e tmpres %e \n", proc_id, i, precodiag[i], vect->coeffs[i], tmpres[i]); 
	  tmpres[i] = omega*( vect->coeffs[i] - tmpres[i] )/precodiag[i];
	
      }
    }

//     // trasmette agli inferiori 
//     // ******************************************
//     for( int procsup=0;   procsup<this->proc_id-1   ; procsup++ ) {
//       for(int i=0; i<dims2[procsup]; i++) {
// 	bufferinvio[i]= tmpres[   loc2glob[procsup][i]]; 
//       }
//       printf(" BBBB sono in preco %d   trasmetto a %d \n", proc_id, procsup+1);
//       MPI_Send(  bufferinvio  , dims2[procsup] , MPI_DOUBLE ,procsup+1 , 89,  MPI_COMM_WORLD);
//     }


    // trasmette agli superiori 
    // ******************************************
    if(0) { // si fa una risoluzione locale
      
      for( int procsup= this->proc_id ;   procsup<this->n_procs-1   ; procsup++ ) {
	for(int i=0; i<dims2[procsup]; i++) {
	  bufferinvio[i]= tmpres[   loc2glob[procsup][i]]; 
	}
	// printf(" BBBB sono in preco %d   trasmetto a %d \n", proc_id, procsup+1);
	MPI_Send(  bufferinvio  , dims2[procsup] , MPI_DOUBLE ,procsup+1 , 89,  MPI_COMM_WORLD);
      }
    }



   
    {
      int dim= thisdim;
      for(int i=dim-1; i>=0; i--) {
	tmpres[i] = (2-omega)* tmpres[i] * precodiag[i]/omega;
      }
    }

    memset(ris->coeffs , 0 , thisdim *sizeof( double ) );
    

    if(0) { // si fa una risoluzione locale
      
      
      // riceve dagi superiori
      for( int procsup = this->n_procs-1-1   ; procsup > this->proc_id-1 ; procsup-- ) {
	// for( int procsup =0;  procsup<this->proc_id-1   ;  procsup++ ) {
	// printf(" CCC  sono in preco %d   ricevo  da %d \n", proc_id, procsup+1);
	MPI_Recv(  others_buffer[ procsup]  , others_dims2[procsup] , MPI_DOUBLE ,procsup+1 , 89,  MPI_COMM_WORLD, &status );
	::Moltiplica( ris->coeffs  , others_buffer[procsup]   , Nelspp[procsup] , topp[procsup], frompp[procsup], coeffspp[procsup] );
      }
    }

    // risolve

    {
      int dim= thisdim;
      // for(int i=0;  i<dim;  i++) {
      for(int i=dim-1; i>=0; i--) {
	int N = precoN_t[i];
	for(int j=0; j<N; j++) {
	  ris->coeffs[i] += precoCoeff_t[i][j]*ris->coeffs[precoFrom_t[i][j]];
	}
	if( precodiag[i]!=0.0)
	  ris->coeffs[i] = omega*( tmpres[i] - ris->coeffs[i] )/precodiag[i];
      }
    }
    if(0) { // si fa una risoluzione locale
      
      // trasmetti ai inferiori
      // *************************
      // for( int procsup=this->proc_id;   procsup<this->n_procs-1   ; procsup++ ) {
      for( int procsup=0 ;   procsup<this->proc_id  -1   ; procsup++ ) {
	for(int i=0; i<dims2[procsup]; i++) {
	  bufferinvio[i]= ris->coeffs[   loc2glob[procsup][i]]; 
	}
	MPI_Send(  bufferinvio  , dims2[procsup] , MPI_DOUBLE ,procsup+1 , 89,  MPI_COMM_WORLD);
      }
    }
  }
}





void Sparsa3AP2::MoltiplicaMinus(VectorP2 *ris, VectorP2 *vect  ) {
  if( proc_id==0) {

    double t1 = MPI_Wtime();

    char buffer[MSGLEN];
    MPI_Status status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"MoltiplicaMinus2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(& (ris->item_id)  , 1, MPI_INT, proc, 101, MPI_COMM_WORLD);
      MPI_Send(& (vect->item_id), 1, MPI_INT, proc, 102, MPI_COMM_WORLD);
    }

#ifdef PIRAMIDE    
    if(!multdone) {
      this->Ntorecv = new int[this->n_procs-1];
      this->buffertorecv = new double * [this->n_procs-1];
      this->Nmax2trasm=0;
      for(int i=0; i<this->n_procs-1; i++) {
	int dum;
	MPI_Recv(this->Ntorecv+i,1,MPI_INT,i+1,200+i+1,MPI_COMM_WORLD,&status);
	MPI_Recv(&dum,1,MPI_INT,i+1,500+i+1,MPI_COMM_WORLD,&status);
	if( Nmax2trasm<dum) Nmax2trasm=dum;
	this->buffertorecv[i]=new double[   this->Ntorecv[i] ];
      }
      this->bufferinvio = new double[Nmax2trasm];
      multdone=1;
    }

    for(int i=0; i<this->n_procs-1; i++) {

      MPI_Recv(   this->buffertorecv[i]     , this->Ntorecv[i], MPI_DOUBLE , i+1 ,300+i+1,MPI_COMM_WORLD,&status);

    }

    for(int proc=1 ; proc<this->n_procs; proc++) {
      int proc_from;
      int count=0;
      for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {
	proc_from=proc - Dproc;
	if(proc_from<1) {
	  proc_from=proc_from + n_procs-1;
	}
	double *bufptr =   this->buffertorecv[proc_from-1] ;
	for(int j=1; j<Dproc; j++ ) {
	  bufptr +=   (int) ( *bufptr    ) +1;
	}
	int dim=*bufptr;
	bufptr++;
	memcpy(   bufferinvio+count  , bufptr, dim* sizeof(double)      );
	count+= dim ;
      }
      MPI_Send(   bufferinvio    , count  , MPI_DOUBLE , proc ,400+proc,MPI_COMM_WORLD );
    }
#endif    



    MPI_Barrier(MPI_COMM_WORLD );    

    double t2 = MPI_Wtime();
    // printf(" MoltiplicaMinus per il calcolo TOTALE %e \n",  t2-t1);


  } else {
    int proc_to;
    int proc_from;

#ifdef PIRAMIDE

    if(!multdone) {
      multdone=1;

      this->len2trasm=0;
      this->len2recv =0;

      for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {

	proc_to=proc_id+Dproc;
	if(proc_to>=n_procs) {
	  proc_to=proc_to-n_procs +1;
	}


	proc_from=proc_id - Dproc;
	if(proc_from<1) {
	  proc_from=proc_from + n_procs-1;
	}
	this->len2trasm+=dims2[proc_to-1] + 1 ; 
	this->len2recv +=others_dims2[proc_from-1]; 

      }
      MPI_Send(&len2trasm,1,MPI_INT,0,200+proc_id,MPI_COMM_WORLD);
      MPI_Send(&len2recv,1, MPI_INT,0,500+proc_id,MPI_COMM_WORLD);
      this->bufferinvio = new double [ Max(this->len2trasm, this->len2recv) ];
    } 

    {


      int count=0;
      
      for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {
	proc_to=proc_id+Dproc;
	if(proc_to>=n_procs) {
	  proc_to=proc_to-n_procs +1;
	}
	this->bufferinvio[count]=dims2[proc_to-1];
	for(int i=0; i<dims2[proc_to-1]; i++) {
	  this->bufferinvio[count+i+1]= vect->coeffs[   loc2glob[proc_to-1][i]]; 
	}
	count+= dims2[proc_to-1]+1;
      }
    }



    MPI_Status status;
    double t1 = MPI_Wtime();
    MPI_Send(   this->bufferinvio     , this->len2trasm, MPI_DOUBLE , 0 ,300+proc_id,MPI_COMM_WORLD);
    MPI_Recv(   this->bufferinvio     , this->len2recv, MPI_DOUBLE , 0 ,400+proc_id,MPI_COMM_WORLD, &status);
    double t2 = MPI_Wtime();
    
    //if(proc_id==1)
    //  printf(" tempo per trasmissione %e \n", t2-t1);


    int count=0;
    for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {
      proc_from=proc_id - Dproc;
      if(proc_from<1) {
	proc_from=proc_from + n_procs-1;
      }
      memcpy(   others_buffer[proc_from-1]  , this->bufferinvio+count, this->others_dims2[proc_from-1] * sizeof(double)      );
      count+=this->others_dims2[proc_from-1] ;
    }

#else



    for(int Dproc=1; Dproc< n_procs-1 ; Dproc++) {
      proc_to=proc_id+Dproc;
      if(proc_to>=n_procs) {
	proc_to=proc_to-n_procs +1;
      }
      proc_from=proc_id - Dproc;
      if(proc_from<1) {
	proc_from=proc_from + n_procs-1;
      }
      
      MPI_Request statusa, statusb, statusc ;
      MPI_Status status;
      


      for(int i=0; i<dims2[proc_to-1]; i++) {
	bufferinvio[i]= vect->coeffs[   loc2glob[proc_to-1][i]]; 
      }

      
      
      MPI_Sendrecv(  bufferinvio  , dims2[proc_to-1] , MPI_DOUBLE , proc_to, 89,
		     others_buffer[proc_from-1] , others_dims2[proc_from-1] , MPI_DOUBLE , proc_from, 89,
		     MPI_COMM_WORLD, &status);
      
    }

#endif

    for ( int proc = 1; proc< n_procs; proc++) {
      if( proc!=proc_id) {


	  ::MoltiplicaMinus( ris->coeffs , others_buffer[proc-1]   , Nelspp[proc-1] , topp[proc-1], frompp[proc-1], coeffspp[proc-1] );
	  
      } else {
	::MoltiplicaMinus( ris->coeffs , vect->coeffs   , Nelspp[proc-1] , topp[proc-1], frompp[proc-1], coeffspp[proc-1] );
      }
    }
  }
}






void Sparsa3AP2::caricaArrays(int Nels, int * from, int * to, double * coeffs,int Nf,  int * dominiofrom, int Nt, int * dominioto) {



  multdone=0;

  // if( DBP  == proc_id) std::cout << " sono in caricaArrays \n";
//   int dim_from = arrayDimEstimate( Nels , from) ;
//   int dim_to   = arrayDimEstimate( Nels , to  ) ;

  int dim_from = Nf ;
  int dim_to   = Nt ;



  if ( dim_from == dim_to) {
    dim=dim_from;
  } else {
    dim=-100000;
  }

  
  if( proc_id==0) {

      
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"caricaArraysS2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    int pos=0;
    
    for(int k=0; k< Nels-1 ; k++) {
      if( from[k]>from[k+1]) {
	for(int j=0; j<100; j++) 
	printf("inversione  \n");
	exit(1);
      }
    } 



    if(0 &&   dim_from && acc_Nels==0) {

      for( int  proc =1; proc< n_procs ; proc++) { 

	// oldlimite=limite;
	MPI_Send(&dim_from, 1, MPI_INT, proc, 97, MPI_COMM_WORLD);
	MPI_Send(&dim_to, 1, MPI_INT, proc, 96, MPI_COMM_WORLD);
	MPI_Send(&Nels, 1, MPI_INT, proc, 95, MPI_COMM_WORLD);
	
	
	MPI_Send( from , Nels, MPI_INT, proc, 94, MPI_COMM_WORLD);
	
	MPI_Send( to   , Nels, MPI_INT, proc, 93, MPI_COMM_WORLD);
	MPI_Send( coeffs  , Nels, MPI_DOUBLE, proc, 92, MPI_COMM_WORLD);
	
	MPI_Send( dominiofrom ,dim_from , MPI_INT, proc, 95, MPI_COMM_WORLD);
	MPI_Send( dominioto   ,dim_to   , MPI_INT, proc, 96, MPI_COMM_WORLD);       
      }

    } else {


      acc_Nels=1;
      for( int  proc =1; proc< n_procs ; proc++) { 
	int nts=0;
	int *shto;
	int *shfrom;
	double *shcoeffs;
	for(int volta=0; volta<2; volta++) {

	  for(int i=0; i<Nels; i++) {
	    if( dominioto[to[i]]==proc-1) {
	      if(volta) {
		shto[nts]=to[i];
		shfrom[nts]=from[i];
		shcoeffs[nts]=coeffs[i];
	      }
	      nts++;
	    }
	  }

	  if(volta) {


	    MPI_Send(&dim_from, 1, MPI_INT, proc, 97, MPI_COMM_WORLD);
	    MPI_Send(&dim_to, 1, MPI_INT, proc, 96, MPI_COMM_WORLD);

	    MPI_Send(&nts, 1, MPI_INT, proc, 95, MPI_COMM_WORLD);
	    
	    
	    MPI_Send( shfrom , nts, MPI_INT, proc, 94, MPI_COMM_WORLD);
	    
	    MPI_Send( shto   , nts, MPI_INT, proc, 93, MPI_COMM_WORLD);
	    MPI_Send( shcoeffs  , nts, MPI_DOUBLE, proc, 92, MPI_COMM_WORLD);
	    
	    if(dim_from) {
	      MPI_Send( dominiofrom ,dim_from , MPI_INT, proc, 95, MPI_COMM_WORLD);
	      MPI_Send( dominioto   ,dim_to   , MPI_INT, proc, 96, MPI_COMM_WORLD);
	    }


	    delete shto;
	    delete shfrom;
	    delete shcoeffs;
	  } else {
	    shto=new int [nts];
	    shfrom=new int [nts];
	    shcoeffs=new double [nts];
	  }
	  nts=0;
	}
      }

    }
    MPI_Barrier(MPI_COMM_WORLD );    

  }
}


void Riordina(int &n, int *&col, int *&to, double *&coeff) ;

void Sparsa3AP2::riceviArrays() {

  
  int Nels;
  this->multdone=0;

  MPI_Status status;
  
  if( DBP  == proc_id) std::cout << "DBP  sono in riceviarrays \n";

  MPI_Recv(&(this->dim_from) ,1 , MPI_INT, 0 , 97, MPI_COMM_WORLD,&status);
  MPI_Recv(&(this->dim_to)   ,1 , MPI_INT, 0 , 96, MPI_COMM_WORLD,&status);

  if( DBP  == proc_id) std::cout << "DBP  sono in riceviarrays " << dim_from << " " << dim_to << "\n";


  MPI_Recv(&(Nels)     ,1 , MPI_INT, 0 , 95, MPI_COMM_WORLD,&status);
  if( DBP  == proc_id) std::cout << "DBP  sono in riceviarrays Nels " <<Nels <<   "\n";


  int *from = new int [ Nels ] ;
  int *to   = new int [ Nels ] ;
  double *coeffs = new double [ Nels];

  int *dominiofrom ;
  int *dominioto  ;


  if(dim_from) {
    dominiofrom = new int  [ dim_from];
    dominioto   = new int  [ dim_to  ];
  }

  

  MPI_Recv(from    ,Nels , MPI_INT    , 0 , 94, MPI_COMM_WORLD,&status);

 
  MPI_Recv(to      ,Nels , MPI_INT    , 0 , 93, MPI_COMM_WORLD,&status);

  MPI_Recv(coeffs  ,Nels , MPI_DOUBLE , 0 , 92, MPI_COMM_WORLD,&status);


  if( dim_from) {
    
    MPI_Recv(dominiofrom  ,dim_from , MPI_INT , 0 , 95, MPI_COMM_WORLD,&status);
    
    MPI_Recv(dominioto  ,dim_to , MPI_INT , 0 , 96, MPI_COMM_WORLD,&status);
  }

  if( DBP  == proc_id) std::cout << " DBP ricevuto dati bruti  " <<Nels <<   "\n";

  if( dim_from==0 ||      acc_Nels    ) {
    int * new_acc_from= new int [  acc_Nels+Nels      ];
    memcpy( new_acc_from , acc_from,     acc_Nels * sizeof(int )      );
    memcpy( new_acc_from+acc_Nels , from,     Nels * sizeof(int )      );
    if( acc_Nels) delete acc_from;
    
    int * new_acc_to= new int [  acc_Nels+Nels      ];
    memcpy( new_acc_to , acc_to,     acc_Nels * sizeof(int )      );
    memcpy( new_acc_to+acc_Nels , to,     Nels * sizeof(int )      );
    if( acc_Nels)  delete acc_to;
    
    
    double * new_acc_coeffs= new double  [  acc_Nels+Nels      ];
    memcpy( new_acc_coeffs , acc_coeffs,     acc_Nels * sizeof(double )      );
    memcpy( new_acc_coeffs+acc_Nels , coeffs,     Nels * sizeof(double )      );
    if( acc_Nels)  delete acc_coeffs;
    
    acc_Nels=acc_Nels+Nels  ;
    acc_from=new_acc_from;
    acc_to=new_acc_to;
    acc_coeffs=new_acc_coeffs;
  }
  
  if(dim_from) {
    if(acc_Nels) {
      
      
      Riordina(acc_Nels, acc_from, acc_to,  acc_coeffs);
      
      riceviArraysLocal( acc_Nels, acc_from, acc_to,  acc_coeffs,dim_from,
			 dominiofrom, dim_to,  dominioto );
    } else {
      riceviArraysLocal( Nels, from, to,  coeffs,dim_from,
			 dominiofrom, dim_to,  dominioto );
    }
  } 
  delete to;
  delete from;
  delete coeffs;
  if(dim_from ) {
    delete dominiofrom;
    delete dominioto;
    
    if( acc_Nels) {
      acc_Nels=0;
      delete acc_from; acc_from=0;
      delete  acc_to; acc_to=0;
      delete  acc_coeffs; acc_coeffs=0;
    }
  }

}

void Sparsa3AP2::riceviArraysLocal(int Nels, int * from, int * to, double * coeffs,int dim_from,
				   int * dominiofrom, int dim_to, int * dominioto ) {
 

  int ndominifrom=0, ndominito=0;
  for(int i=0; i<dim_from; i++) {
    if ( ndominifrom < dominiofrom[i]) ndominifrom = dominiofrom[i] ;
  }
  for(int i=0; i<dim_to; i++) {
    if ( ndominito < dominioto[i]) ndominito = dominioto[i] ;
  }
  ndominito+=1;
  ndominifrom+=1;

  


  
  if( DBP  == proc_id) std::cout << " DBP ndominito  " <<ndominito <<   "\n";
  if( DBP  == proc_id) std::cout << " DBP ndominifrom  " <<ndominifrom <<   "\n";
  

  if( ndominifrom!=n_procs-1 || ndominito!=n_procs-1) {
    printf(" ndominifrom!=n_procs-1 || ndominito!=n_procs-1 %d %d %d\n",ndominifrom,ndominito, n_procs  );
    exit(1);
  }
  
  int * corrispondenzafrom = new int [dim_from];
  int * corrispondenzato   = new int [dim_to];
  
  int count;

  count=0;
  int endfrom [ ndominifrom];
  int endto [ ndominito];



  for(int nd=0; nd< ndominifrom; nd++ ) {
    for(int i=0; i<dim_from; i++ ) {
      if( dominiofrom[i]==nd) {
	corrispondenzafrom[i]=count;
	count++;
      }
    }
    endfrom[nd]=count;
  }

  count=0;
  for(int nd=0; nd< ndominifrom; nd++ ) {
    for(int i=0; i<dim_to; i++ ) {
      if( dominioto[i]==nd) {
	corrispondenzato[i]=count;
	count++;
      }
    }
    endto[nd]=count;
  }


  if( DBP  == proc_id) std::cout << " DBP creo un po  " <<   "\n";

  this->Nelspp= new int [ n_procs-1];
  this->topp  = new int *[ n_procs-1];
  this->frompp  = new int *[ n_procs-1];
  this->coeffspp  = new double *[ n_procs-1];
  if( DBP  == proc_id) std::cout << " DBP creo un po OK  " <<   "\n";
  
  
  memset( this->Nelspp, 0, (n_procs-1)*sizeof(int));
  for(int i=0; i<Nels; i++) {
    if( dominioto[to[i]] == proc_id-1 ) {
      this->Nelspp[dominiofrom[from[i]]]++;
    }
  }



  if( DBP  == proc_id) std::cout << " DBP creo un po ancora " <<   "\n";

  
  this->dims2 = new int [n_procs-1];
  memset( this->dims2, 0, (n_procs-1)*sizeof(int));
  this->loc2glob = new int * [n_procs-1] ;
  this->others_loc2glob  = new int* [n_procs-1];

  int * used   = new int [ dim_from] ;

  int maxdim2=0;
  int totdim2=0;

  if( DBP  == proc_id) std::cout << " DBP creo un po ancora OK  " <<   "\n";

  this->dims = new int [n_procs-1];

  for(int proc=0; proc< n_procs-1; proc++) {
    this->dims[proc]=0;
  }
  for(int k=0; k< dim_from; k++) {
    this->dims[dominiofrom[k]]++;
  }


  if(1) {

    // inizio parte nuova


    this->others_dims2 = new int [n_procs-1];
    memset(this->others_dims2 , 0, (n_procs-1)*sizeof(int));

    MPI_Request request;
    
    for(int proc=0; proc< n_procs-1; proc++) {
      
      memset(used, 0, dim_from*sizeof(int));
      for(int i=0; i<Nels; i++) {
	if( dominiofrom[from[i]] == proc && dominioto[to[i]] == proc_id-1 ) {
	  used[from[i]]=1;
	}
      }
      
      this->others_dims2[proc]=0;
      for(int i=0; i<dim_from; i++) {
	if( used[i]) this->others_dims2[proc]++;
      }
      
      if(proc!=proc_id-1) {
	MPI_Isend(&(this->others_dims2[proc]), 1, MPI_INT, proc+1, 123, MPI_COMM_WORLD, &request);
      }
      this->dims2[proc_id-1]= this->dims[proc_id-1]; 
    }
    


    MPI_Status status;
    for(int proc=0; proc< n_procs-1; proc++) {
      if(proc!=proc_id-1) {
	
	MPI_Recv(&(this->dims2[proc]), 1, MPI_INT, proc+1, 123, MPI_COMM_WORLD, &status);
	
	if( dims2[proc]>maxdim2) maxdim2=dims2[proc];
      }
    }
    
    
    for(int ishift=0; ishift<n_procs-1; ishift++) {
      int procto   = proc_id-1+ishift;
      int procfrom = proc_id-1-ishift;
      procto=procto%(n_procs-1 );
      procfrom=(procfrom+(n_procs-1 ))%(n_procs-1 );
      
      int proc=procto;
      int proctk=procfrom;
      
      memset(used, 0, dim_from*sizeof(int));
      for(int i=0; i<Nels; i++) {
	if( dominiofrom[from[i]] == proc && dominioto[to[i]] == proc_id-1 ) {
	  used[from[i]]=1;
	}
      }
      


      others_loc2glob[proc]  = new int [  this->others_dims2[proc]  ];
      if(ishift) {
	this->loc2glob[proctk] = new int [  this->dims2[proctk]  ];    
      } else {
	this->loc2glob[proctk] = others_loc2glob[proc]  ; 
      }
      int *duml2g =  others_loc2glob[proc] ;
      
      int countcorr=0;
      int count=0;
      
      for(int i=0; i<dim_from; i++) {
	if( used[i]) {
	  duml2g[count]=countcorr;
	  used[i]=count;
	  count++;
	}
	if(dominiofrom[i] == proc) {
	  countcorr++;
	}
      }
      
      {
	int nels = Nelspp[proc] ;
      
	this->topp[proc]=new int [nels ];
	this->frompp[proc]=new int [nels ];
	this->coeffspp[proc]=new double [nels ];
      
	int ne=0;

	int tostart;
	int fromstart;
	
	if( proc==0) fromstart=0;
	else         fromstart=endfrom[proc-1];
      	
	if( proc_id-1==0) tostart=0;
	else         tostart=endto[proc_id-2];
	
	
	for(int i=0; i<Nels; i++) {
	  if(proc == dominiofrom[from[i]] && proc_id-1==dominioto[to[i]]){
    
	    if(ishift) {
	      this->frompp[proc][ne]=used[from[i]];
	    } else {
	      this->frompp[proc][ne]=corrispondenzafrom[from[i]]-fromstart; // mettere qui loc2glob
	    }
	    
	    this->topp  [proc][ne]= corrispondenzato[to[i]]-tostart ;      
	    
	    this->coeffspp[proc][ne] = coeffs[i];
	    
	    ne++;	
	  }
	}
	
	if(this->Nelspp[proc]!=ne) {
	  printf(" this->Nelspp[proc]!=ne\n");
	  printf("proc_id  %d  ne %d nels %d\n", proc_id, ne,this->Nelspp[proc]  );
	  exit(1);
	}
	
      }
      
      if(ishift) {
	MPI_Sendrecv(  duml2g  , others_dims2[proc] , MPI_INT , proc+1, 89,
		       this->loc2glob[proctk]  , dims2[proctk] , MPI_INT , proctk+1, 89,
		       MPI_COMM_WORLD, &status);
	
	// delete duml2g;
      }
      
    }
    
    
    // #######################################################################
    for(int proc=0; proc<n_procs-1; proc++) {
      if(proc==proc_id-1) continue;
      Riordina(Nelspp[proc], topp[proc], frompp[proc], coeffspp[proc]); // #%^ (%&
    }
    
    this->bufferinvio = new double [maxdim2] ;

    this->others_buffer = new double *[n_procs-1];
    for(int proc=0; proc< n_procs-1; proc++) {
      this->others_buffer[proc] = new double [this->others_dims2[proc]];
    }
    
    delete corrispondenzafrom;
    delete corrispondenzato;
    
    delete used;
    


    if(1) {
      
      for( int proc=0; proc<n_procs-1; proc++) {
	if ( proc==proc_id-1) continue;

	if( dims2[proc]) {
	  trasmissioni[ Trasmissione(  dims2[proc] ,proc_id-1, proc   )   ]=1;
	}
	if( others_dims2[proc]) {
	  trasmissioni[ Trasmissione(  others_dims2[proc], proc  ,proc_id-1  )   ]=1;
	}
      }
    }
    return;
    
  }
  
  
  for(int proc=0; proc< n_procs-1; proc++) {

    memset(used, 0, dim_from*sizeof(int));
    for(int i=0; i<Nels; i++) {
      if( dominiofrom[from[i]] == proc_id-1 && dominioto[to[i]] == proc ) {
	used[from[i]]=1;
      }
    }
    this->dims2[proc]=0;
    for(int i=0; i<dim_from; i++) {
      if( used[i]) this->dims2[proc]++;
      if(  this->dims2[proc] > maxdim2  )  maxdim2  =  this->dims2[proc];
    }


    
    if(proc_id-1!=proc) {
      totdim2+= this->dims2[proc];
    }

    this->loc2glob[proc] = new int [  this->dims2[proc]  ];
    count=0;
    int countcorr=0;

    for(int i=0; i<dim_from; i++) {

      if( used[i]) {
	this->loc2glob[proc][count]=countcorr;
	used[i]=count;
	count++;
      }

      if(dominiofrom[i] == proc_id-1) {
	countcorr++;
      }

    }

    memset(used, 0, dim_from*sizeof(int));
    for(int i=0; i<Nels; i++) {
      if( dominiofrom[from[i]] == proc && dominioto[to[i]] == proc_id-1 ) {
	used[from[i]]=1;
      }
    }

    count=0;
    for(int i=0; i<dim_from; i++) {
      if( used[i]) {
	used[i]=count;
	count++;
      }
    }

    int nels = Nelspp[proc] ;

    this->topp[proc]=new int [nels ];
    this->frompp[proc]=new int [nels ];
    this->coeffspp[proc]=new double [nels ];

    int ne=0;
    int tostart;
    int fromstart;

    if( proc_id-1==0) tostart=0;
    else         tostart=endto[proc_id-2];
    if( proc==0) fromstart=0;
    else         fromstart=endfrom[proc-1];


    for(int i=0; i<Nels; i++) {
      if(proc == dominiofrom[from[i]] && proc_id-1==dominioto[to[i]]){

	if( proc!=proc_id-1) {
	  this->frompp[proc][ne]=used[from[i]];
	} else {
	  this->frompp[proc][ne]=corrispondenzafrom[from[i]]-fromstart; // mettere qui loc2glob
	}
	this->topp  [proc][ne]= corrispondenzato[to[i]]-tostart ;      

	this->coeffspp[proc][ne] = coeffs[i];
	
	ne++;	
      }
    }
    
    if(this->Nelspp[proc]!=ne) {
      printf(" this->Nelspp[proc]!=ne\n");
      printf("proc_id  %d  ne %d nels %d\n", proc_id, ne,this->Nelspp[proc]  );
      exit(1);
    }
  }

  if( DBP  == proc_id) std::cout << " DBP QUi " <<   "\n";


  {
    perto = new int * [ n_procs-1];
    std::map<double,int> ordinatore;
    for(int proc=0; proc< n_procs-1; proc++) {
      std::map<double,int> ordinatore;
      int nels= Nelspp[proc];
      double dim=0.;
      for(int i=0; i<nels; i++) {
	if( dim<frompp[proc][i]) dim= frompp[proc][i];
      }
      dim=dim+1;
      for(int i=0; i<nels; i++) {
	ordinatore[ topp[proc][i]  +  frompp[proc][i]/dim  ]=i;
      }
      dim=dim+1;
      
      perto[proc] = new int [  ordinatore.size()   ];
      std::map<double,int>::iterator  pos;
      pos=ordinatore.begin();
      for(int i=0; i< ordinatore.size(); i++) {
	perto[proc][i]=pos->second;
	pos++;
      }
    }





  }



  if( DBP  == proc_id) std::cout << " DBP QUi 2" <<   "\n";






  this->others_dims2 = new int [n_procs-1];
  memset(this->others_dims2 , 0, (n_procs-1)*sizeof(int));

  this->others_loc2glob  = new int* [n_procs-1];


  delete used;
  used   = new int [ dim_to ] ;


  this->others_buffer = new double *[n_procs-1];

  for(int proc=0; proc< n_procs-1; proc++) {

    memset(used, 0, dim_to*sizeof(int));
    for(int i=0; i<Nels; i++) {
      if( dominiofrom[from[i]] == proc && dominioto[to[i]] == proc_id-1 ) {
	used[from[i]]=1;
      }
    }
    this->others_dims2[proc]=0;
    for(int i=0; i<dim_from; i++) {
      if( used[i]) this->others_dims2[proc]++;
    }

    if( DBP  == proc_id) std::cout << " DBP QUi 2bis  " <<this->others_dims2[proc] <<  " proc = " << proc  << "\n";
    std::cout <<  proc_id-1 << " DBP QUi 2bis  " <<this->others_dims2[proc] <<  " proc = " << proc  << "\n";

   
    this->others_buffer[proc] = new double [this->others_dims2[proc]];


    this->others_loc2glob[proc] = new int[this->others_dims2[proc]];

    if( DBP  == proc_id) std::cout << " DBP QUi 2bis OK   " << "\n";
    std::cout  <<  proc_id-1<< "  QUi 2bis OK   " << "\n";
    int count=0;
    int countcorr=0;

    for(int i=0; i<dim_from; i++) {
      if( used[i]) {
	this->others_loc2glob[proc][count]=countcorr;
	count++;
      };

      if(dominiofrom[i] == proc) {
	countcorr++;
      }
    }
    if( DBP  == proc_id) std::cout << " DBP QUi 2bis bis   " << "\n";
    
  }
 

  if( DBP  == proc_id) std::cout << " DBP QUi 3" <<   "\n";

#ifdef BLOCKING
  this->bufferinvio = new double [maxdim2] ;
#else
  this->bufferinvio = new double [totdim2] ;
#endif


  delete corrispondenzafrom;
  delete corrispondenzato;
  
  delete used;
  if( DBP  == proc_id) std::cout << " DBP QUi 4" <<   "\n";

 
}


void mandaTllt(int  p, int thisproc, int  dimfrom, std::map<int,double> *byrow, int **Tlltn, int *** Tlltcol, double ***Tlltcoeff   ) ;
void    riceviTllt( int p , int  thisdim ,   int **  Tlltn , int ***  Tlltcol , double *** Tlltcoeff ) ;

void Sparsa3AP2::preparaLLt() {
  if( proc_id==0) {

    printf(" sono in preparaLLt %d \n", proc_id);

    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"preparaLLt2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    printf(" MASTER alla BARRIERA \n");
    MPI_Barrier(MPI_COMM_WORLD );    
    printf(" MASTER dopo BARRIERA \n");
  } else {


    if( proc_id==0) {
      Sparsa3A prova( Nelspp[0],coeffspp[0] , frompp[0], topp[0]         );
      prova.preparaLLt(0.0);


      printf(" tutto bene per Sparsa3A\n");
      exit(0);
      
    }

    printf(" sono in preparaLLt %d \n", proc_id);

    int thisproc=proc_id-1;
    int thisdim = this->dims[proc_id-1];

    lltn =  new int * [thisproc+1];
    lltsize =  new int * [thisproc+1];
    lltrow =  new int ** [thisproc+1];
    lltcoeff =  new double ** [thisproc+1];

    printf(" sono in preparaLLt %d dopo primi new\n", proc_id);

    for(int p=0; p<thisproc+1; p++) {
      lltn   [p] = new int [thisdim];
      lltsize[p] = new int [thisdim];

      lltrow  [p] = new int    * [thisdim ];
      lltcoeff[p] = new double * [thisdim ];

      for(int d=0; d<thisdim; d++) {

	lltrow[p][d]=new int [1];
	lltcoeff[p][d] = new double [1]; 

	lltn[p][d]=0;
	lltsize[p][d]=1;
      }
    }
    printf(" sono in preparaLLt %d dopo secondi new\n", proc_id);
    for(int otherproc=0; otherproc<=thisproc; otherproc++) {
      int otherdim = this->dims[otherproc];
      int ** otherlltn;

      int *** otherlltrow;
      double *** otherlltcoeff;
      printf(" sono in preparaLLt %d prima ricevi otherproc= %d\n", proc_id, otherproc);

      this->riceviOthersLLt(otherproc, otherlltn,otherlltrow, otherlltcoeff ) ;


      printf(" sono in preparaLLt %d dopo ricevi otherproc= %d\n", proc_id, otherproc);


      int lastifrom[ thisdim ];
    
      for(int k=0; k<thisdim ; k++) {
	lastifrom[k]=-1;
      }


      int ito, newifrom, newito;
      int startf, endf;
      double coefftarget;



      int iel;
      for(int iel_=0; iel_<Nelspp[otherproc]; iel_++) {
	iel = perto[     otherproc     ][iel_ ];

	// printf(" sono in preparaLLt %d iel =  %d     Nelspp[otherproc]=%d  \n", proc_id, iel, Nelspp[otherproc]);


	ito = this  ->topp[otherproc][iel];
	newifrom = this->frompp[otherproc][iel];
	if(otherproc==thisproc  &&   ito<newifrom) {
	  continue;
	}


	//       ito = this->others_loc2glob[ito];

	if( thisproc!=otherproc) {
	  newifrom = this->others_loc2glob[otherproc][newifrom];
	}

	startf = lastifrom[ito]+1;
	endf = newifrom;
	coefftarget =this->coeffspp[otherproc][iel] ;
	lastifrom[ito]=newifrom;
	// if(proc_id==1) 
	// printf(" sono in preparaLLt %d iel =  %d  vado in  addnewelements \n", proc_id, iel);

	// startf=endf;

	this->addnewelements(startf, endf, newifrom,  ito,  coefftarget,
			     otherlltn, otherlltrow, otherlltcoeff,
			     lltn, lltrow, lltcoeff,lltsize,
			     otherproc, thisproc);

	// if(proc_id==1) 
	// printf(" sono in preparaLLt %d iel =  %d   dopo   addnewelements \n", proc_id, iel);

      }
      printf(" aggiungo ######################### \n");
      if( otherproc!=thisproc) {
	for(int ito=0; ito<this->dims[thisproc]; ito++) {
	  startf= lastifrom[ito]+1;
	  endf  =  this->dims[otherproc]-1;
	  coefftarget=0;
	  this->addnewelements(startf, endf, newifrom,  ito,  coefftarget,
			       otherlltn, otherlltrow, otherlltcoeff,
			       lltn, lltrow, lltcoeff,lltsize,
			       otherproc, thisproc);
	}
	printf(" sono in preparaLLt %d   libero  \n", proc_id);
	this->liberaOthersLLt(otherproc, otherlltn,otherlltrow, otherlltcoeff) ;
	printf(" sono in preparaLLt %d   libero  OK  \n", proc_id);
      }
    }

    printf(" TRASMETTO A TUTTI \n");

    // trasmetti a tutti ;
    this->trasmettiOthersLLt( ) ;

    printf(" TRASMETTO A TUTTI OK \n");
    return;
    Tlltn     =  (new int *     [this->n_procs  - this->proc_id])-thisproc;
    Tlltcol   =  (new int **    [this->n_procs  - this->proc_id])-thisproc;
    Tlltcoeff =  (new double ** [this->n_procs  - this->proc_id])-thisproc;


    for(int p=0; p<=thisproc; p++) {
      int dimfrom = this->dims[p];
      std::map<int,double> byrow[dimfrom];
      for(int d=0; d<thisdim; d++) {
	for(int i=0; i<lltn[p][d]; i++) {
	  byrow[ lltrow[p][d][i]  ][d] = lltcoeff[p][d][i];
	}      
      }
      mandaTllt( p, thisproc, dimfrom, byrow, Tlltn, Tlltcol, Tlltcoeff   );
    }
  
    for(int p=thisproc+1; p<this->n_procs-1; p++) {
      riceviTllt(p, thisdim,     Tlltn, Tlltcol, Tlltcoeff );
    }
        printf(" FINE preparaLLt %d \n", proc_id);

  }
}

void mandaTllt(int  p, int thisproc, int  dimfrom, std::map<int,double> *byrow, int **Tlltn, int *** Tlltcol, double ***Tlltcoeff   ) {
  int * lltnbuffer = new int [dimfrom];

  int dimlltn=0;
  for(int i=0; i<dimfrom; i++) {
    lltnbuffer[i] =   byrow[i].size() ; 
    dimlltn      +=   byrow[i].size() ; 
  }
  
  int *lltcolbuffer =  new int [dimlltn];
  double *lltcoeffbuffer = new double [dimlltn];

  int count=0;
  for(int i=0; i<dimfrom; i++) {

    std::map<int,double>::iterator pos;
    pos = byrow[i].begin();
    for(int c=0; c<byrow[i].size() ; c++) {
      lltcolbuffer[count]=pos->first;
      lltcoeffbuffer[count]=pos->second;
      pos++;
      count++;
    }
  }
  if( p==thisproc) {
    Tlltn[thisproc]=lltnbuffer;
    count=0;
    for(int i=0; i<dimfrom; i++) {
      Tlltcol[thisproc][i]=lltcolbuffer+count;
      Tlltcoeff[thisproc][i]=lltcoeffbuffer+count;
      count+=lltnbuffer[i] ; 
    }
  } else {
      MPI_Send( lltnbuffer, dimfrom, MPI_INT, p+1 , 4, MPI_COMM_WORLD);
      MPI_Send( &dimlltn , 1 , MPI_INT, p+1, 5, MPI_COMM_WORLD);
      MPI_Send( lltcolbuffer, dimlltn , MPI_INT, p+1, 6, MPI_COMM_WORLD);    
      MPI_Send( lltcoeffbuffer,dimlltn  , MPI_DOUBLE, p+1, 7, MPI_COMM_WORLD);    
      delete lltnbuffer;
      delete lltcolbuffer;
      delete lltcoeffbuffer;
  }
}

void    riceviTllt( int p , int  thisdim ,   int **  Tlltn , int ***  Tlltcol , double *** Tlltcoeff ) {
  MPI_Status status;
  int * lltnbuffer = new int [thisdim];
  MPI_Recv( lltnbuffer, thisdim , MPI_INT, p+1 , 4, MPI_COMM_WORLD, &status);

  int dimlltn;
  MPI_Recv( &dimlltn , 1 , MPI_INT, p+1, 5, MPI_COMM_WORLD, &status);

  int *lltrowbuffer    = new int    [dimlltn];
  double *lltcoeffbuffer  = new double [dimlltn];
  
  MPI_Recv( lltrowbuffer, dimlltn , MPI_INT, p+1 , 6, MPI_COMM_WORLD, &status);    
  MPI_Recv( lltcoeffbuffer,dimlltn  , MPI_DOUBLE, p+1 , 7, MPI_COMM_WORLD, &status);    
  


  Tlltn[p]=lltnbuffer;
  int count=0;
  for(int i=0; i<thisdim; i++) {
    Tlltcol[p][i]=lltrowbuffer+count;
    Tlltcoeff[p][i]=lltcoeffbuffer+count;
    count+=lltnbuffer[i] ; 
  }



};




void Sparsa3AP2::riceviOthersLLt(int &otherproc, int ** &otherlltn,int *** &otherlltrow,
				 double *** &otherlltcoeff )  {
  MPI_Status status;
  if( otherproc==this->proc_id-1) {
    otherlltn=this->lltn;
    otherlltrow=lltrow;
    otherlltcoeff=lltcoeff;
    return ;
  }
  printf(" in riceviOthersLLt processo %d riceve da %d \n", this->proc_id, otherproc);
  int lltndim= this->dims[otherproc]*(otherproc+1);
  int *lltnbuffer = new int [lltndim];
  printf(" in riceviOthersLLt processo %d sta per ricevere da %d \n", this->proc_id, otherproc);
  MPI_Recv( lltnbuffer, lltndim, MPI_INT, otherproc  +1, 4, MPI_COMM_WORLD, &status);
  printf(" 2 in riceviOthersLLt processo %d riceve da %d \n", this->proc_id, otherproc);

  int totdim;
  MPI_Recv( &totdim , 1 , MPI_INT, otherproc +1, 5, MPI_COMM_WORLD, &status);

  int *lltrowbuffer = new int [totdim];
  double *lltcoeffbuffer = new double [totdim];
  
  MPI_Recv( lltrowbuffer, totdim , MPI_INT, otherproc +1, 6, MPI_COMM_WORLD, &status);    
  MPI_Recv( lltcoeffbuffer, totdim , MPI_DOUBLE, otherproc +1, 7, MPI_COMM_WORLD, &status);    
  printf(" 3 in riceviOthersLLt processo %d ricevuti da %d \n", this->proc_id, otherproc);

  otherlltn = new int * [otherproc+1];
  otherlltrow = new int ** [otherproc+1];
  otherlltcoeff = new double ** [otherproc+1];
  int otherdim=this->dims[otherproc] ; 

  int counta=0;
  int countb=0;

  for(int k=0; k< otherproc+1; k++) {
    otherlltn[k] = new int [otherdim];
    otherlltrow[k] = new int * [otherdim];
    otherlltcoeff[k] = new double * [otherdim];
    printf(" riceviOthersLLt processo %d  otherproc %d \n", this->proc_id, otherproc);

    for(int l=0 ; l< otherdim; l++) {
      // printf(" riceviOthersLLt processo %d  l %d otherdim %d  \n", this->proc_id, l, otherdim );
      otherlltn[k][l] =lltnbuffer[counta] ;
      counta++;
      // printf(" riceviOthersLLt processo %d  otherproc %d countb %d  totdim %d \n", this->proc_id, otherproc, countb, totdim);
      otherlltrow[k][l]  = lltrowbuffer+countb;
      otherlltcoeff[k][l] =  lltcoeffbuffer+countb;
      countb+= otherlltn[k][l];
      // printf(" riceviOthersLLt  processo %d  otherproc %d  ?? otherdim %d \n",this->proc_id, otherproc,otherdim);
    }
  }
  printf(" riceviOthersLLt processo %d OK \n", this->proc_id);
  delete lltnbuffer;
}

void Sparsa3AP2::liberaOthersLLt(int &otherproc, int ** &otherlltn,int *** &otherlltrow,
				 double *** &otherlltcoeff )  {

  if( otherproc==this->proc_id-1) {
    return ;
  }

  delete otherlltrow[0][0] ;
  delete otherlltcoeff[0][0] ;

  for(int k=0; k< otherproc+1; k++) {
    
    
    
    delete otherlltn[k] ;
    delete otherlltrow[k] ;
    delete otherlltcoeff[k] ;
  }
  delete otherlltn ;
  delete otherlltrow ;
  delete otherlltcoeff ;
}

void Sparsa3AP2::trasmettiOthersLLt( ) {
  int thisproc=this->proc_id-1;
  int thisdim = this->dims[thisproc];

  int totdim=0;
  for(int k=0; k<this->proc_id; k++) {
    for(int l=0; l<thisdim; l++) {
      totdim+= this->lltn[k][l]; 
    }
  }
  
  int lltndim;
  lltndim = thisdim*this->proc_id;

  int *lltnbuffer = new int [lltndim];
  int *lltrowbuffer = new int [totdim];
  double *lltcoeffbuffer = new double [totdim];

  
  int counta, countb;
  counta=0;
  countb=0;
    
  for(int k=0; k<this->proc_id; k++) {
    for(int l=0; l<thisdim; l++) {
      lltnbuffer[counta] = lltn[k][l] ;
      counta++;
      memcpy(lltrowbuffer+countb, lltrow[k][l], lltn[k][l]*sizeof(int));
      memcpy(lltcoeffbuffer+countb, lltcoeff[k][l], lltn[k][l]*sizeof(int));
      countb+= this->lltn[k][l];
    }
  }
  for(int procto = thisproc+1; procto<this->n_procs-1; procto++) {
    printf( " mando a processo %d totdim %d \n", procto+1, totdim);
      MPI_Send( lltnbuffer, lltndim, MPI_INT, procto+1, 4, MPI_COMM_WORLD);
      MPI_Send( &totdim , 1 , MPI_INT, procto+1, 5, MPI_COMM_WORLD);
      MPI_Send( lltrowbuffer, totdim , MPI_INT, procto+1, 6, MPI_COMM_WORLD);    
      MPI_Send( lltcoeffbuffer, totdim , MPI_DOUBLE, procto+1, 7, MPI_COMM_WORLD);    
    printf( " mando a processo %d OK \n", procto+1);
  }
  
  printf(" delete \n");
  delete lltcoeffbuffer;
  delete lltrowbuffer;
  delete lltnbuffer;
  printf ( " delete OK \n" );
}




double scalare4llt(int olltn, int * otherlltrow, double *otherlltcoeff,
		   int  lltn , int * lltrow, double *lltcoeff) {

  double res=0;
  double dumA, dumB;
  int li, lj;
  li= 0 ;
  lj= 0;



  while(lj<olltn  && li<lltn) {
    if( otherlltrow[lj]==lltrow[li]) {
      dumA=otherlltcoeff[lj];
      dumB=lltcoeff[li];
      //       if(dumA<1000 && dumB<1000) {
      // 	dafare=1;
      //       }
      //       if(dumA>1000) dumA-=2000;
      //       if(dumB>1000) dumB-=2000;
      
      res += dumA*dumB;
      lj++;
      li++;
    } else if(otherlltrow[lj]>lltrow[li] ) {
      li++;
    } else  {
      lj++;
    }
  }
  
  return res;

}


void     addtollt2( int ifrom , double nuovo, int  &lltn,
		    int * &lltrow, double *  &lltcoeff,
		    int &lltsize) { 
  if( lltn+1<lltsize) {
  } else {
    allocallt2( lltrow, lltcoeff, lltsize, lltsize+10);
    lltsize+=10;
  }
  lltrow[lltn]= ifrom; 
  lltcoeff[lltn]= nuovo; 
  lltn+=1;
}



void Sparsa3AP2::addnewelements(int startf, int endf,int newifrom, int ito,  double coefftarget,
		    int **otherlltn, int***otherlltrow, double ***otherlltcoeff,
		    int **lltn, int ***lltrow, double ***lltcoeff, int **lltsize,
				 int otherproc, int thisproc)
{
  double scalres;

  for(int ifrom = startf ; ifrom<=endf; ifrom++ ) {
    double target=0.0;
    if(ifrom == newifrom ) target=coefftarget;
    double pregressum=target;
    scalres=0.0;
    for(int iproc=0; iproc<=otherproc; iproc++) {
      scalres += scalare4llt(otherlltn[iproc][ifrom], otherlltrow[iproc][ifrom], otherlltcoeff[iproc][ifrom],
				lltn[iproc][ito], lltrow[iproc][ito], lltcoeff[iproc][ito] ); 
    }
    pregressum -=scalres;
    if( pregressum!=0.0) {
      double nuovo;
//       printf(" aggiungo %d %d %e coefftarget = %e  scalare =%e \n", ifrom, ito, pregressum, coefftarget, scalres);
      if( ifrom!=ito || otherproc!=thisproc) {
	nuovo = (pregressum)/otherlltcoeff[otherproc][ifrom][ otherlltn[otherproc][ifrom]-1];
      } else {
	nuovo=sqrt(pregressum);
      }  

      if(ito==ifrom) {
// 	printf("2 idiag %d jdiag %d  newelcoeff %e \n", ito, ifrom, nuovo);
      } else {
	if( fabs(nuovo)<1.0e-6) continue;
// 	printf("idiag %d jdiag %d  nuovo %e \n", ito, ifrom, nuovo);
      }

      addtollt2( ifrom , nuovo,  lltn[otherproc][ito],
		 lltrow[otherproc][ito], lltcoeff[otherproc][ito],
		 lltsize[otherproc][ito]);

    }
  }
}


 
 
 


void Sparsa3AP2::trasforma(double fattore, double addendo) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"trasforma2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&fattore, 1, MPI_DOUBLE, proc, 101, MPI_COMM_WORLD);
      MPI_Send(&addendo, 1, MPI_DOUBLE, proc, 102, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {

    int *fatto;
    int dim;
    dim = dims2[proc_id-1];
    fatto = new int [ dim ];
    for(int i=0; i<dim; i++) {
      fatto[i]=0;
    }

    for(int proc=1; proc<n_procs; proc++) {
      int n = Nelspp[proc -1];
      double *coeff=coeffspp[proc-1];
      int *col=frompp[proc-1] , *row=topp[proc-1];
      for(int i=0; i<n; i++) {
	coeff[i]=fattore*coeff[i];
	if(proc==proc_id) {
	  if(col[i]==row[i] && fatto[col[i]]==0 )  {
	    coeff[i]+=addendo;
	    fatto[col[i]]=1;
	  }
	}
      }
    }

    for(int i=0; i<dim; i++) {
      if(!fatto[i]) {
	{
	  // for(int k=0; k<100; k++)
	  // printf( " ci sono dei buchi in  Sparsa3AP::trasforma  %d \n", proc_id);
	  // exit(1);
	}
      }
    }
    delete fatto;
  }
}


void Sparsa3AP2::crea() {

  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"creaS2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  }
  for( item_id=0; item_id< MAXNSPARSA3AP; item_id++) {
    if( Sparsa3AP2::items[item_id] ==0 ) {
      Sparsa3AP2::items[item_id]=this;
      break;
    }
  }
  if( item_id==MAXNSPARSA3AP ) {
    printf(" raggiunto massimo MAXNSPARSA3AP \n");
    MPI_Finalize();
    exit(0);
  } else {
    DEBUG(printf(" creata una matrice con id = %d nel processo %d  \n", item_id, proc_id);)
  }
  
  Nelspp=0;


};




VectorP2:: VectorP2(int dim_from, int * dominiofrom) {
  this->crea(dim_from,  dominiofrom);
}
// -------------------------------------------------------
int **VectorP2::dims2static=0;
int ** VectorP2::loc2globstatic=0;
int VectorP2::nshapes=0;
int * VectorP2::dimsstatic=0;
// -------------------------------------------------------------


void VectorP2::crea(int dim, int * dominiofrom) {
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"creaV2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
    }
  }  


  MPI_Bcast( &dim, 1, MPI_INT, 0 ,MPI_COMM_WORLD ); 


  // this->dim=dim;




  for( item_id=0; item_id< MAXNVECTORP; item_id++) {
    if( VectorP2::items[item_id] ==0 ) {
      VectorP2::items[item_id]=this;
      break;
    }
  }

  if( item_id==MAXNVECTORP ) {
    printf(" raggiunto massino MAXNVECTORP \n");
    MPI_Finalize();
    exit(0);
  } else {
    DEBUG(printf(" creata un vettore con id = %d nel processo %d  \n", item_id, proc_id);)
  }
  
 
  


  if(nshapes==0) {
    dims2static= new int * [10];
    loc2globstatic=new int * [10];
    dimsstatic=new int [10];
  }




  int found=-1;
  for(int k=0; k<nshapes; k++) {
    if( dim==dimsstatic[k])  { found=k; break; } 
  }
  if( found==-1) {
    dims2static[nshapes] =new int[n_procs-1 ];
    loc2globstatic[nshapes] = new int [dim];
    dimsstatic[nshapes]=dim;
    this->dims2 = dims2static[nshapes];
    this->loc2glob =loc2globstatic[nshapes];
    nshapes++;
  }  else {
    this->dims2 = dims2static[found];
    this->loc2glob =loc2globstatic[found];
  }
  

  if(found==-1) {


    if(proc_id!=0) {
      dominiofrom = new int [dim];
    }
    
    MPI_Bcast( dominiofrom, dim, MPI_INT, 0 ,MPI_COMM_WORLD ); 
    
 
    
    int ndominifrom=0;
    for(int i=0; i<dim; i++) {
      if ( ndominifrom < dominiofrom[i]) ndominifrom = dominiofrom[i] ;
    }
    ndominifrom+=1;

    if( ndominifrom!=n_procs-1 ) {
      printf(" ndominifrom!=n_procs-1  \n");
      exit(1);
    }
  


    int count;
    count=0;
    
    // this->dims2=new int[n_procs-1 ];
    // this->loc2glob = new int [dim];
    
    for(int nd=0; nd< ndominifrom; nd++ ) {
      this->dims2[nd]=0;
      for(int i=0; i<dim; i++ ) {
	if( dominiofrom[i]==nd) {
	  loc2glob[count]=i;
	  count++;
	  this->dims2[nd]++;
	}
      }
    }



  }
  if(proc_id!=0) {
    this->coeffs = new double[this->dims2[proc_id-1]];
    if(found==-1) delete dominiofrom ;
  } else {
        MPI_Barrier(MPI_COMM_WORLD );
  }
};


void VectorP2::caricaArray(int Nels,  double * acoeffs) {

  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"caricaArray2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
  }
    

      
  MPI_Bcast( &Nels  , 1, MPI_INT, 0,  MPI_COMM_WORLD);
  if( this->proc_id) {
    acoeffs = new double [Nels];
  }
  MPI_Bcast( acoeffs  , Nels, MPI_DOUBLE, 0,  MPI_COMM_WORLD);


  if( this->proc_id) {
    int offset=0;
    for(int nd=0; nd<this->proc_id-1; nd++) {
      offset += this->dims2[nd];
    }
    for(int i=0; i<this->dims2[this->proc_id-1]; i++) {
      this->coeffs[ i  ]=acoeffs[  this->loc2glob[i+offset]   ];
    }
  }

  if( proc_id==0) {
    MPI_Barrier(MPI_COMM_WORLD );  
  } else {

    delete acoeffs;

  }
}


void VectorP2::set_all_random(double value) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"set_all_random2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&value, 1, MPI_DOUBLE, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    srand(proc_id*1000+(unsigned) time(NULL));
    for(index= 0; index<this->dims2[proc_id-1] ; index++) {
      coeffs[index]=value*random()*1.0/RAND_MAX;
    }
  }
}


void VectorP2::set_to_zero() {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"set_to_zero2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    memset(coeffs,0, this->dims2[proc_id-1]*sizeof(double));
  }
}



void VectorP2::set_to_one() {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"set_to_one2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    for(index= 0; index<this->dims2[proc_id-1]; index++) {
      coeffs[index]=1.0;
    }
  }
}







void VectorP2::copy_to_a_from_b(VectorP2* adata ,VectorP2 * bdata) {
  int n_procs, proc_id;
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);

  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"copy_to_a_from_b2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&adata->item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&bdata->item_id, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;

    if ( adata->dims2[proc_id-1]  !=bdata->dims2[proc_id-1]) {
      VectorP2::error( (char * )  "problema in     VectorP2::copy_to_a_from_b, dimensioni incompatibili \n " );
    }
    int dim= adata->dims2[proc_id-1];
    memcpy(adata->coeffs , bdata->coeffs, dim*sizeof(double) );
    
  }
}







double scalarP2(VectorP2* adata, VectorP2* bdata ) {
  double sum=0.0, res=0.0;
  DEBUG(printf("sono in scalarP2 proc_id = %d \n", adata->proc_id);)
    double t1 = MPI_Wtime();

  if( adata->proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< adata->n_procs ; proc++) {    
      sprintf(buffer,"scalare2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&(adata->item_id), 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&(bdata->item_id), 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    int dim;
 
    if ( adata->dims2[adata->proc_id-1] !=bdata->dims2[adata->proc_id-1]) {
      VectorP2::error(  "problema in     VectorP2::scalarP2, dimensioni incompatibili \n " );
    }
    dim=adata->dims2[adata->proc_id-1];
    sum=0.0;
    DEBUG(printf("dim = %d   proc_id = %d \n", dim, adata->proc_id);)
    for (index=0; index<dim; index++) {
      sum+= adata->coeffs[index]*bdata->coeffs[index];
    }    
  }
  DEBUG(printf("passo a reduce  proc_id = %d \n", adata->proc_id);)


    double t2 = MPI_Wtime();
    
  MPI_Reduce(&sum,&res, 1, MPI_DOUBLE, MPI_SUM,  0 , MPI_COMM_WORLD);
  double t3 = MPI_Wtime();
     
  // if(adata->proc_id==1)
  //   printf(" Scalare per il calcolo %e per la trasmissione %e\n",  t2-t1,  t3-t2);
 
  return res;
}

void  VectorP2::add_from_vect_with_fact_multi( int *ids, int n, double *fact) {
  
  VectorP2 *bdata;
  DEBUG(printf("sono in VectorP2::add_from_vect_with_fact_multi proc_id = %d \n", this->proc_id);)
    
  if( this->proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< this->n_procs ; proc++) {    
      

      sprintf(buffer,"add_from_vect_with_fact2_multi");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&n, 1, MPI_INT, proc, 97, MPI_COMM_WORLD);
      MPI_Send(&this->item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(ids, n, MPI_INT, proc, 99, MPI_COMM_WORLD);
      MPI_Send(fact,           n, MPI_DOUBLE, proc, 101, MPI_COMM_WORLD);     
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {

    for(int ivect=0; ivect<n; ivect++) {
      
      bdata=VectorP2::items[ids[ivect]];

   
      int index;
      if ( this->dims2[proc_id-1] !=bdata->dims2[proc_id-1]) {
	VectorP2::error(  "problema in     VectorP2::add_from_vect_with_fact, dimensioni incompatibili \n " );
      }
      int dim=this->dims2[proc_id-1];
      for (index=0; index<dim; index++) {
	this->coeffs[index]+=fact[ivect]*bdata->coeffs[index];
      }    
    }
  }
}


void  scalarP2multi(int *ids, VectorP2* bdata, int n, double *&sum ) {
  sum= new double[n];
  double  res[n];
  VectorP2 *adata;
  

  DEBUG(printf("sono in scalarP2multi proc_id = %d \n", bdata->proc_id);)
    double t1 = MPI_Wtime();


  for(int i=0; i<n; i++) {
    sum[i]=0;
    res[i]=0;
  } 
 

  if( bdata->proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< bdata->n_procs ; proc++) {    
      sprintf(buffer,"scalare2multi");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);

      MPI_Send(&n, 1, MPI_INT, proc, 97, MPI_COMM_WORLD);

      MPI_Send(ids, n, MPI_INT, proc, 98, MPI_COMM_WORLD);

      MPI_Send(&(bdata->item_id), 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Reduce(res,sum, n, MPI_DOUBLE, MPI_SUM,  0 , MPI_COMM_WORLD);
    
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {

    // printf(" sono in scalarP2multi slave \n");

    int index;
    int dim;
 
    for(int ivect=0; ivect<n; ivect++) {
     
      adata=VectorP2::items[ids[ivect]];
      
      if ( adata->dims2[adata->proc_id-1] !=bdata->dims2[adata->proc_id-1]) {
	VectorP2::error(  "problema in     VectorP2::scalarP2, dimensioni incompatibili \n " );
      }
      dim=adata->dims2[adata->proc_id-1];
      res[ivect]=0.0;
      DEBUG(printf("dim = %d   proc_id = %d \n", dim, adata->proc_id);)
	for (index=0; index<dim; index++) {
	  res[ivect]+= adata->coeffs[index]*bdata->coeffs[index];
	}    
    }
    MPI_Reduce(res,sum, n, MPI_DOUBLE, MPI_SUM,  0 , MPI_COMM_WORLD);
    
  }


  if( bdata->proc_id) {
    delete sum; 
  }
}




void VectorP2::add_from_vect(VectorP2* bdata ) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"add_from_vect2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&(this->item_id), 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&(bdata->item_id), 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    int dim;
    if ( this->dims2[proc_id-1] !=bdata->dims2[proc_id-1]) {
      VectorP2::error(  "problema in     VectorP2::add_from_vect, dimensioni incompatibili \n " );
    }
    dim=this->dims2[proc_id-1];
    for (index=0; index<dim; index++) {
      this->coeffs[index]+=bdata->coeffs[index];
    }    
  }
}


void VectorP2::mult_by_fact( double fact ) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"mult_by_fact2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&this->item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&fact, 1, MPI_DOUBLE, proc, 101, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    for (index=0; index<this->dims2[proc_id-1]; index++) {
      coeffs[index]*=fact;
    }    
  }
}


void VectorP2::dividebyarray(VectorP2* bdata ) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"dividebyarray2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&(this->item_id), 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&(bdata->item_id), 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    int dim;
    if ( this->dims2[proc_id-1] !=bdata->dims2[proc_id-1]) {
      VectorP::error(  "problema in     VectorP::copy_to_a_from_b, dimensioni incompatibili \n " );
    }
    dim=this->dims2[proc_id-1];
    for (index=0; index<dim; index++) {
      this->coeffs[index]/=bdata->coeffs[index];
    }    
  }
}


void VectorP2::multbyarray(VectorP2* bdata ) {
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"multbyarray2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&(this->item_id), 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
      MPI_Send(&(bdata->item_id), 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );    
  } else {
    int index;
    int dim;
    if ( this->dims2[proc_id-1] !=bdata->dims2[proc_id-1]) {
      VectorP::error(  "problema in     VectorP::copy_to_a_from_b, dimensioni incompatibili \n " );
    }
    dim=this->dims2[proc_id-1];
    for (index=0; index<dim; index++) {
      this->coeffs[index]*=bdata->coeffs[index];
    }    
  }
}


void VectorP2::scriviArray(char * nomefile, int start, int dim) {
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {    
      sprintf(buffer,"scriviArray2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);

      int lenstring=strlen(nomefile);

      MPI_Send(nomefile, lenstring+1, MPI_CHAR, proc, 103, MPI_COMM_WORLD);
      MPI_Send(&start, 1, MPI_INT, proc, 100, MPI_COMM_WORLD);
      MPI_Send(&dim, 1, MPI_INT, proc, 101, MPI_COMM_WORLD);


    }
    MPI_Barrier(MPI_COMM_WORLD );    
  }  else {
    if(1) {
      int dafare=0;
      
      int offset=0;
      for(int nd=0; nd<this->proc_id-1; nd++) {
	offset += this->dims2[nd];
      }

      for(int i=0; i<dims2[proc_id-1]; i++ ) {
	if (  this->loc2glob[offset+i] >= start &&      this->loc2glob[offset+i] - start<dim            )
	  dafare=1;
	if( dafare) break;
      }
      
      if( dafare) {
	char nfpp[400];
	sprintf(nfpp,"%s_%d_%d",nomefile,  item_id, proc_id);
	FILE *fpp=fopen(nfpp,"w");
	
	for(int i=0; i<dims2[proc_id-1]; i++ ) {
	  if (  this->loc2glob[offset+i] >= start &&      this->loc2glob[offset+i] - start<dim            )
	    fprintf(fpp," coeff[%d] = %e  \n", this->loc2glob[offset+i], coeffs[i] );
	}
	fclose(fpp);
      }
    }  
  }
}


VectorP2::~VectorP2() {
  MPI_Comm_size(MPI_COMM_WORLD,&n_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&proc_id);
  if( proc_id==0) {
    items[item_id]=0;
    char buffer[MSGLEN];
    int status;
    for ( int proc =1; proc< n_procs ; proc++) {
      sprintf(buffer,"distruggiV2");
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, proc, 99, MPI_COMM_WORLD);
      MPI_Send(&item_id, 1, MPI_INT, proc, 98, MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD );
  } else {

    if(dims2[proc_id-1]) {
      delete coeffs;
      // delete dims2;
      // delete loc2glob;
    }
  }
  
}



void Riordina(int &n, int *&col, int *&row, double *&coeff) {
  // printf(" Riordino \n");
  if(1)
  {

    ndx4reo::from = col;
    ndx4reo::to = row;
    


    int nsize=n;
    int dim=0;
    for(int i=0; i<n; i++) {

      if(col[i]+1>dim) dim=col[i]+1;
      if(row[i]+1>dim) dim=row[i]+1;

    }



  int *scramble=new int[n];
  {
    for (int i=0; i<n; i++) {
      scramble[i]=i;
    }
    int j,tmp;
    for (int i=0; i<n; i++) {
      j=(int) ( n*random()*1.0/RAND_MAX ) ;
      tmp=scramble[i];
      scramble[i]=scramble[j];
      scramble[j]=tmp;
    }
  }

  std::map<ndx4reo,int> *ordinatore;
  ordinatore = new std::map<ndx4reo,int>[dim];
  ndx4reo  ndxo;
  for (int i=0; i<n; i++ ) {
    ndxo    .indice = scramble[i];
    ndxo.subindice = i;
    ordinatore[ndx4reo::from[   ndxo    .indice     ]][ ndxo ] =ndxo.indice;
  }
  
  delete scramble;

  std::map<ndx4reo,int>::iterator pos;

  int * newcol = new int [nsize];
  memset(newcol, 0, nsize*sizeof(int));
  int totn=0;
  for (int ic=0; ic<dim; ic++) {
    int n = ordinatore[ic].size();
    pos= ordinatore[ic].begin();
    for(int i=0; i<n; i++) {
      newcol[totn] = col[ pos->second ] ;
      pos++;
      totn++;
    }
  }
//   pos= ordinatore.begin();
//   for(int i=0; i<n; i++) {
//     newcol[i] = col[ pos->second ] ;
//     pos++;
//   }
  delete col;
  col=newcol;
  

  totn=0;
  int * newrow = new int [nsize];
  memset(newrow, 0, nsize*sizeof(int));
  for (int ic=0; ic<dim; ic++) {
    int n = ordinatore[ic].size();
    pos= ordinatore[ic].begin();
    for(int i=0; i<n; i++) {
      newrow[totn] = row[ pos->second ] ;
      pos++;
      totn++;
    }
  }
  delete row;
  row=newrow;
  
  
  
  double * newcoeff = new double [nsize];
  memset(newcoeff, 0, nsize*sizeof(double));

  totn=0;

  for (int ic=0; ic<dim; ic++) {
    int n = ordinatore[ic].size();
    pos= ordinatore[ic].begin();
    for(int i=0; i<n; i++) {
      newcoeff[totn] = coeff[ pos->second ] ;
      pos++;
      totn++;
    }
  }
  for(int i=0; i<dim; i++)  ordinatore[i].clear();
  delete [] ordinatore;

  delete coeff;
  coeff=newcoeff;
  
  // printf(" fatto riordina \n");
   
  int i, inew;
  i=0;
  inew=0;
  
  while(i<n) {
    col[inew]=col[i];
    row[inew]=row[i];
    coeff[inew]=coeff[i];
    i++;
    while(i<n && col[i]==col[inew]   && row[i]==row[inew]    ) {
	coeff[inew]+=coeff[i];
	i++;
    }
    inew++;
  }
  n=inew;
  }
  
}



#endif
