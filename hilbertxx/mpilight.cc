
#include<string.h>
#include<stdio.h>

#include<stdexcept>

/* per un bug in mpich2 */
#define MPICH_IGNORE_CXX_SEEK    
#define MPICH_SKIP_MPICXX
#define OMPICH_SKIP_MPICXX
#include"mpi.h"


void settaMPI(int argc, char ** argv) {

  MPI_Init(&argc,&argv);
}


int get_MPI_myid() {
  int myid;

  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  return myid;
}

int get_MPI_numprocs() {
  int  numprocs;

  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

  return numprocs;
}

int array_MPI_bcast(int N, double *data,int  root) {
  return MPI_Bcast ( data, N ,  MPI_DOUBLE, root, MPI_COMM_WORLD );
}


int reduce(int N, double *sendbuf,int N1, double *recvbuf, int root ) {
  return MPI_Reduce (    sendbuf,  recvbuf  , N ,  MPI_DOUBLE, 
			 MPI_SUM, root ,  MPI_COMM_WORLD );  
}



int array_MPI_senddouble(int N, double *data,int  dest,int tag) {
  return MPI_Send( data, N, MPI_DOUBLE,  dest, 
		   tag, MPI_COMM_WORLD );
}

int array_MPI_sendint(int N, int  *data,int  dest,int tag) {
  return MPI_Send( data, N, MPI_INT,  dest, 
		   tag, MPI_COMM_WORLD );
}

int array_MPI_sendfloat(int N, float *data,int  dest,int tag) {
  return MPI_Send( data, N, MPI_FLOAT,  dest, 
		   tag, MPI_COMM_WORLD );
}

int array_MPI_receiveadddouble(int N, double *data,int  source,int tag, int Nbuffer, double *buffer) {
  int res;
  MPI_Status status;
  if( N > Nbuffer) {
    throw std::out_of_range("buffer too short in array_MPI_receiveadddouble ");   
  }
  res=    MPI_Recv(buffer,N , MPI_DOUBLE, source , tag , MPI_COMM_WORLD,&status);
  for(int i=0; i<N; i++) {
    data[i]+=buffer[i];
  }
  return res;
}

int array_MPI_receiveaddfloat(int N, float *data,int  source,int tag, int Nbuffer, float *buffer) {
  int res;
  MPI_Status status;
  if( N > Nbuffer) {
    throw std::out_of_range("buffer too short in array_MPI_receiveaddfloat ");   
  }
  res=    MPI_Recv(buffer,N , MPI_FLOAT, source , tag , MPI_COMM_WORLD,&status);
  for(int i=0; i<N; i++) {
    data[i]+=buffer[i];
  }
  return res;
}

int array_MPI_receivedouble(int N, double *data,int  source,int tag) {
  int res;
  MPI_Status status;
  res=    MPI_Recv(data,N , MPI_DOUBLE, source , tag , MPI_COMM_WORLD,&status);
  return res;
}


int array_MPI_receiveint(int N, int *data,int  source,int tag) {
  int res;
  MPI_Status status;
  res=    MPI_Recv(data,N , MPI_INT, source , tag , MPI_COMM_WORLD,&status);
  return res;
}



int array_MPI_receivefloat(int N, float *data,int  source,int tag, int Nbuffer, float *buffer) {
  int res;
  MPI_Status status;

  res=    MPI_Recv(data,N , MPI_FLOAT, source , tag , MPI_COMM_WORLD,&status);
  return res;
}



int MPI_barrier() {
  return MPI_Barrier (  MPI_COMM_WORLD );
};

int prova(int  N, double * x) {
  x[0]=1.22345;
  return 1;
};


