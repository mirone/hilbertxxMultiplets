## /************************************************************************

##   Copyright
##   Alessandro MIRONE
##   mirone@esrf.fr

##   Copyright 2002  by European Synchrotron Radiation Facility, Grenoble, 
##                   France

##                                ----------
 
##                            All Rights Reserved
 
##                                ----------

## Permission to use, copy, modify, and distribute this software and its
## documentation for any purpose and without fee is hereby granted,
## provided that the above copyright notice appear in all copies and that
## both that copyright notice and this permission notice appear in
## supporting documentation, and that the names of European Synchrotron
## Radiation Facility or ESRF or SCISOFT not be used in advertising or 
## publicity pertaining to distribution of the software without specific, 
## written prior permission.

## EUROPEAN SYNCHROTRON RADIATION FACILITY DISCLAIMS ALL WARRANTIES WITH
## REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
## MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL EUROPEAN SYNCHROTRON
## RADIATION FACILITY OR ESRF BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
## CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
## DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
## TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
## PERFORMANCE OF THIS SOFTWARE.

## **************************************************************************/

import math
import time
import  numpy.fft as  FFT
import Hilbertxx
try:
    import Sparsa
    sparsamodulo=1
except:
    print " il modulo Sparsa non e stato caricato " 
    sparsamodulo=0
    pass
try:
    import Sparsa3AP
    PARALLEL=1
except:
    print " il modulo Sparsa3AP non e stato caricato "
    PARALLEL=0

# import numpy.oldnumeric as Numeric
import numpy as Numeric
import   numpy.linalg #  LinearAlgebra
#import numpy.core._dotblas as dotblas

import random

######################################
## interfaccia per sparse
# __class__
# static    .getClass4Vect()
# object    .gohersch()
# object    .goherschMax()
# object    .goherschMin()

######################################
## interfaccia per vettori
# __class__ === class4vect
# class Vector(dim)
# object.set_value(n,v)
# object.set_all_random(v)

def REAL(a):
    if(    isinstance(a , (complex, numpy.complex128) ) ):
        return a.real
    else:
        return a

class Callable:
    def __init__(self, anycallable):
        self.__call__=anycallable





class NumericMatrix:
    tipo="d"
    def __init__(self, mR):
        self.mR=mR
        self.dim= int(self.mR.shape[1])
        self.shift=0.0

    def Moltiplica(self,res,v):
        # res.vr[:]+=dotblas.dot(self.mR,v.vr)
        res.vr[:]+=numpy.dot(self.mR,v.vr)
        

        if( self.shift !=0.0):
          res.add_from_vect_with_fact(v,self.shift)

    def trasforma(self, fattore, addendo):
        self.mR[:]=self.mR*Numeric.array([fattore],self.tipo) + numpy.eye(self.dim).astype(self.tipo)*Numeric.array([addendo],self.tipo)

                   
    def getClass4Vect(self):
        return NumericVector


class NumericVector:
    tipo="d"
    def __init__(self, *dim):
        if( dim!=(0,) ):
            self.vr=Numeric.zeros(dim,self.tipo)
        else:
            pass
        
    def __getitem__(self, i):
        res=NumericVector(0)
        res.vr=self.vr[i]
        return res

    def mat_mult(self, evect , q):

        self.vr[:evect.shape[0]]=numpy.dot(evect.astype(self.tipo),q.vr[:evect.shape[1]])


    def dividebyarray(self,prec):
        self.vr[:]=self.vr/prec


    def multbyarray(self,prec):
        self.vr[:]=self.vr*prec



    def len(self):
	return len(self.vr)

    def copy(self):
	duma=Numeric.array(self.vr)
        res=Numeric.Vector(0)
        res.vr=duma
        return res 

    def copy_to_a_from_b(self,b):
        self.vr[:]=b.vr

    def set_value(self, n, val):
        self.vr[n]=val

    def set_to_zero(self):
        self.vr[:]=0
        
    def set_to_one(self):
        self.vr[:]=1

        
    def set_all_random(self, v):
        self.vr[:]=[random.random() for i in range(len(self.vr)) ]

        
    
    def scalare(self,b         ):
        resR =Numeric.sum(self.vr*b.vr, axis= -1)
        return resR

    def sqrtscalare(self,b):
        assert( self is b)
        return Numeric.sqrt( self.scalare(b) )

    def normalizzaauto(self):
        norma = self.sqrtscalare(self)
        self.vr[:]=self.vr/norma
        

    def normalizza(self,norma):
        self.vr.normalizza(norma)


    def rescale(self,fact):
      if(fact==0.0):
        self.set_to_zero()
      else:
        norma=1.0/fact
        self.normalizza(norma)

        
    def add_from_vect(self, b):
        self.vr[:]=self.vr+b.vr
        
     
        
    def add_from_vect_with_fact(self, b, fact):
        self.vr[:]=self.vr+Numeric.array([fact],self.tipo)*b.vr



class TrialVectorC:

    def __init__(self, *dim):
        if( dim!=(0,) ):
            print dim
            self.vr=Sparsa.Array(*dim)
            self.vi=Sparsa.Array(*dim)

        else:
            pass
        
    def __getitem__(self, i):
        res=TrialVectorC(0)
        res.vr=self.vr[i]
        res.vi=self.vi[i]
        return res

    def mat_mult(self, evect , q):
        Sparsa.Array.mat_mult(self.vr, evect , q.vr)
        Sparsa.Array.mat_mult(self.vi, evect , q.vi)

    def dividebyarray(self,prec):
        self.vr.dividebyarray(prec)
        self.vi.dividebyarray(prec)

    def multbyarray(self,prec):
        self.vr.multbyarray(prec)
        self.vi.multbyarray(prec)


    def len(self):
	return self.vr.len()

    def copy(self):
	duma=self.vr.copy()
	dumb=self.vi.copy()
        res=TrialVectorC(0)
        res.vr=duma
        res.vi=dumb
        return res 

    def copy_to_a_from_b(self,b):
        Sparsa.Array.copy_to_a_from_b(self.vr, b.vr  )
        Sparsa.Array.copy_to_a_from_b(self.vi, b.vi  )

    def set_value(self, n, val):
        if(    isinstance(val, (complex, numpy.complex128) ) ):
            self.vr.set_value(n, val.real)
            self.vi.set_value(n, val.imag)
        else:
            self.vr.set_value(n, val)
            self.vi.set_value(n, 0.0)

    def set_to_zero(self):
        self.vr.set_to_zero()
        self.vi.set_to_zero()
        
    def set_to_one(self):
        self.vr.set_to_one()
        self.vi.set_to_zero()
        
    def set_all_random(self, v):
        self.vr.set_all_random(v)
        self.vi.set_all_random(v)
        
    
    def scalare(self,b         ):
        resR =  Sparsa.Array.scalare(self.vr, b.vr)+ Sparsa.Array.scalare(self.vi, b.vi)
        resI =  Sparsa.Array.scalare(self.vr, b.vi)- Sparsa.Array.scalare(self.vi, b.vr)
        return resR + resI * 1.0j

    def sqrtscalare(self,b):
        assert( self is b)
        return math.sqrt( self.scalare(b).real )

    def normalizzaauto(self):
        norma = self.sqrtscalare(self)
        self.vr.normalizza(norma)
        self.vi.normalizza(norma)
        

    def normalizza(self,norma):
        self.vr.normalizza(norma)
        self.vi.normalizza(norma)

    def rescale(self,fact):
      if(fact==0.0):
        self.vr.set_to_zero()
        self.vi.set_to_zero()
      else:
        if isinstance(fact , (complex, numpy.complex128) ):
          norma=1.0/fact.real
        else:
          norma=1.0/fact
        self.vr.normalizza(norma)
        self.vi.normalizza(norma)
        
    def add_from_vect(self, b):
            self.vr.add_from_vect(b.vr   )
            self.vi.add_from_vect(b.vi     )
        
     
        
    def add_from_vect_with_fact(self, b, fact):
        if( isinstance(fact, (complex, numpy.complex128)) ):
            self.vr.add_from_vect_with_fact(b.vr  ,    fact.real   )
            self.vi.add_from_vect_with_fact(b.vi  ,    fact.real   )
            self.vr.add_from_vect_with_fact(b.vi  ,    -fact.imag   )
            self.vi.add_from_vect_with_fact(b.vr  ,    fact.imag   )
        else:
            self.vr.add_from_vect_with_fact(b.vr  ,    fact   )
            self.vi.add_from_vect_with_fact(b.vi  ,    fact   )



class TrialVectorP2:

    def __init__(self, *dim):
        if( dim!=(0,) ):
            print dim[:-1]
            self.vr=Sparsa3AP.ArrayP2( *(dim[:-1])  )
            self.vr.setta(  (dim[-1])  )
        else:
            pass
        
    def __getitem__(self, i):
        res=TrialVectorP2(0)
        res.vr=self.vr[i]
        return res

    def mat_mult(self, evect , q):
        Sparsa3AP.ArrayP2.mat_mult(self.vr, evect , q.vr)

    def dividebyarray(self,prec):
        self.vr.dividebyarray(prec)

    def multbyarray(self,prec):
        self.vr.multbyarray(prec)

    def copy_to_a_from_b(self,b):
        Sparsa3AP.ArrayP2.copy_to_a_from_b(self.vr, b.vr  )


    def set_to_zero(self):
        self.vr.set_to_zero()
        
    def set_to_one(self):
        self.vr.set_to_one()
        
    def set_all_random(self, v):
        self.vr.set_all_random(v)
        
    
    def scalare(self,b         ):
        resR =  Sparsa3AP.ArrayP2.scalare(self.vr, b.vr)
        return resR 

    def sqrtscalare(self,b):
        assert( self is b)
        return math.sqrt( self.scalare(b) )

    def normalizzaauto(self):
        norma = self.sqrtscalare(self)
        self.vr.normalizza(norma)
        

    def normalizza(self,norma):
        self.vr.normalizza(norma)

    def rescale(self,fact):
      if(fact==0.0):
        self.vr.set_to_zero()
      else:
        if  isinstance(fact, (complex, numpy.complex128)) :
          norma=1.0/fact.real
        else:
          norma=1.0/fact
        self.vr.normalizza(norma)
        
    def add_from_vect(self, b):
            self.vr.add_from_vect(b.vr   )
        
            
    def add_from_vect_with_fact(self, b, fact):
            self.vr.add_from_vect_with_fact(b.vr  ,    fact   )




class TrialVectorCP2:


    usecomplex=1

    def __init__(self, *dim):
        if( dim!=(0,) ):
            print dim[:-1]
            self.vr=Sparsa3AP.ArrayP2( *(dim[:-1])  )
            if self.usecomplex:
                self.vi=Sparsa3AP.ArrayP2( *(dim[:-1])  )
            self.vr.setta(  (dim[-1])  )
            if self.usecomplex:
                self.vi.setta(  (dim[-1])  )
        else:
            pass
        
    def __getitem__(self, i):
        res=TrialVectorCP2(0)
        res.vr=self.vr[i]
        
        if self.usecomplex:
            res.vi=self.vi[i]
            
        return res

    def mat_mult(self, evect , q):
        Sparsa3AP.ArrayP2.mat_mult(self.vr, evect , q.vr)
        
        if self.usecomplex:
            Sparsa3AP.ArrayP2.mat_mult(self.vi, evect , q.vi)

    def dividebyarray(self,prec):
        self.vr.dividebyarray(prec)
        
        if self.usecomplex:
            self.vi.dividebyarray(prec)


    def multbyarray(self,prec):
        self.vr.multbyarray(prec)

        if self.usecomplex:
            self.vi.multbyarray(prec)

    def copy_to_a_from_b(self,b):
        Sparsa3AP.ArrayP2.copy_to_a_from_b(self.vr, b.vr  )

        if self.usecomplex:
            Sparsa3AP.ArrayP2.copy_to_a_from_b(self.vi, b.vi  )


    def set_to_zero(self):
        self.vr.set_to_zero()

        if self.usecomplex:
            self.vi.set_to_zero()
        
    def set_to_one(self):
        self.vr.set_to_one()
        
        if self.usecomplex:
            self.vi.set_to_zero()
        
    def set_all_random(self, v):
        self.vr.set_all_random(v)
        
        if self.usecomplex:
            self.vi.set_all_random(v)
        
    
    def scalare(self,b         ):

        if self.usecomplex:
            resR =  Sparsa3AP.ArrayP2.scalare(self.vr, b.vr)+ Sparsa3AP.ArrayP2.scalare(self.vi, b.vi)
            resI =  Sparsa3AP.ArrayP2.scalare(self.vr, b.vi)- Sparsa3AP.ArrayP2.scalare(self.vi, b.vr)
            return resR + resI * 1.0j
        else:
            resR =  Sparsa3AP.ArrayP2.scalare(self.vr, b.vr)
            return resR 
            

    def scalaremulti(self,b ,n        ):

        if self.usecomplex==1:

            res_rr=  Sparsa3AP.ArrayP2.scalaremulti(self.vr, b.vr , n )

            res_ri=  Sparsa3AP.ArrayP2.scalaremulti(self.vr, b.vi , n )

            res_ir=  Sparsa3AP.ArrayP2.scalaremulti(self.vi, b.vr , n )

            res_ii=  Sparsa3AP.ArrayP2.scalaremulti(self.vi, b.vi , n )
            
##             res_rr, res_ri =  Sparsa3AP.ArrayP2.scalaremultiC(self.vr, b.vr , b.vi , n )
##             res_ir, res_ii =  Sparsa3AP.ArrayP2.scalaremultiC(self.vi, b.vr , b.vi , n )
            
            resR = res_rr + res_ii
            resI = res_ri- res_ir
            return resR + resI * 1.0j
        else:
            res_rr =  Sparsa3AP.ArrayP2.scalaremulti(self.vr, b.vr , n )
            return res_rr
            

    def sqrtscalare(self,b):
        assert( self is b)
        if self.usecomplex:
            return math.sqrt( self.scalare(b).real )
        else:
            return math.sqrt( self.scalare(b) )

    def normalizzaauto(self):
        norma = self.sqrtscalare(self)
        self.vr.normalizza(norma)
        if self.usecomplex:
            self.vi.normalizza(norma)
        

    def normalizza(self,norma):
        self.vr.normalizza(norma)
        if self.usecomplex:
            self.vi.normalizza(norma)

    def rescale(self,fact):
      if(fact==0.0):
        self.vr.set_to_zero()
        if self.usecomplex:
            self.vi.set_to_zero()
      else:
        if  isinstance( fact , (complex, numpy.complex128)) :
          norma=1.0/fact.real
        else:
          norma=1.0/fact
        self.vr.normalizza(norma)
        if self.usecomplex:
            self.vi.normalizza(norma)
        
    def add_from_vect(self, b):
            self.vr.add_from_vect(b.vr   )
            if self.usecomplex:
                self.vi.add_from_vect(b.vi     )
        
            
    def add_from_vect_with_fact(self, b, fact):
        if(    isinstance( fact , (complex, numpy.complex128))   ):
            self.vr.add_from_vect_with_fact(b.vr  ,    fact.real   )
            self.vi.add_from_vect_with_fact(b.vi  ,    fact.real   )
            self.vr.add_from_vect_with_fact(b.vi  ,    -fact.imag   )
            self.vi.add_from_vect_with_fact(b.vr  ,    fact.imag   )
        else:
            self.vr.add_from_vect_with_fact(b.vr  ,    fact   )
            if self.usecomplex:
                self.vi.add_from_vect_with_fact(b.vi  ,    fact   )

    def add_from_vect_with_fact_multi(self, b, fact):

        if self.usecomplex==1:

            print " add from vect multi " 
            if( fact.typecode()=="D"):
                self.vr.add_from_vect_with_fact_multi(b.vr  ,    fact.real   )
                self.vi.add_from_vect_with_fact_multi(b.vi  ,    fact.real   )
                self.vr.add_from_vect_with_fact_multi(b.vi  ,    -fact.imag   )
                self.vi.add_from_vect_with_fact_multi(b.vr  ,    fact.imag   )
            else:
                self.vr.add_from_vect_with_fact_multi(b.vr  ,    fact   )
                self.vi.add_from_vect_with_fact_multi(b.vi  ,    fact   )

        else:

            self.vr.add_from_vect_with_fact_multi(b.vr  ,    fact   )

            
        
class TrialMatrixC:
    usegrouped=0
    usecomplex=1
    testaLU=0
    def __init__(self, mR=None, mI=None, parallel=0, procfrom=None):
        if mR is not None:
            self.mR=mR
            self.mI=mI
            self.dim= int(self.mR.dim)
        self.shift=0.0
        self.procfrom=procfrom

        self.PARALLEL=parallel



    def Preco(self, res,v, omega):
        if(self.usecomplex==0):
            self.mR.Preco(res.vr,v.vr, omega)
            return
        else:
            raise " Preco with complex not implemented "


    def Moltiplica(self,res,v):

        if( self.testaLU):
            print " TESTOLU" 
            dum=self.dum
            dum.set_to_zero()
            self.mR.MoltiplicaExpL(dum,v.vr)
            self.mR.MoltiplicaExp(res.vr,dum)
            if( self.shift !=0.0):
                res.add_from_vect_with_fact(v,self.shift)
            

            return
        
        if(self.usecomplex==0):
            self.mR.Moltiplica(res.vr,v.vr)
            return

        
        self.mR.Moltiplica(res.vr,v.vr)
        self.mR.Moltiplica(res.vi,v.vi)


        
        if( self.mI is not None):
          self.mI.Moltiplica(res.vi,v.vr)
          self.mI.MoltiplicaMinus(res.vr,v.vi)

        if( self.shift !=0.0):
          res.add_from_vect_with_fact(v,self.shift)

    def MoltiplicaMinus(self,res,v):
        if(self.usecomplex==0):
            self.mR.MoltiplicaMinus(res.vr,v.vr)
            return
        
        self.mR.MoltiplicaMinus(res.vr,v.vr)
        self.mR.MoltiplicaMinus(res.vi,v.vi)
        
        if( self.mI is not None):
          self.mI.MoltiplicaMinus(res.vi,v.vr)
          self.mI.Moltiplica(res.vr,v.vi)

        if( self.shift !=0.0):
          res.add_from_vect_with_fact(v,-self.shift)

    def MoltiplicaDiag(self,res,v):
        if(usecomplex==0):
            self.mR.MoltiplicaDiag(res.vr,v.vr)
            return

        
        self.mR.MoltiplicaDiag(res.vr,v.vr)
        self.mR.MoltiplicaDiag(res.vi,v.vi)
        
        if( self.mI is not None):
          self.mI.MoltiplicaDiag(res.vi,v.vr)
          self.mI.MoltiplicaDiagMinus(res.vr,v.vi)

        if( self.shift !=0.0):
          res.add_from_vect_with_fact(v,self.shift)

    def MoltiplicaDiagMinus(self,res,v):
        if(usecomplex==0):
            self.mR.MoltiplicaDiagMinus(res.vr,v.vr)
            return
        
        self.mR.MoltiplicaDiagMinus(res.vr,v.vr)
        self.mR.MoltiplicaDiagMinus(res.vi,v.vi)
        
        if( self.mI is not None):
          self.mI.MoltiplicaDiagMinus(res.vi,v.vr)
          self.mI.MoltiplicaDiag     (res.vr,v.vi)

        if( self.shift !=0.0):
          res.add_from_vect_with_fact(v,self.shift)
         
        
    def gohersch(self):
        self.mR.gohersch()
        print " dim R est " , int(self.mR.dim)
        print " dim I est " , int(self.mI.dim)
        if( self.mI is not None):
          self.mI.gohersch()
        print "gohersch OK "
        
    def goherschMax(self):
      if( self.mI is not None):
        return self.mR.goherschMax()+self.mI.goherschMax()
      else:
        return self.mR.goherschMax()

    def goherschMin(self):
      if( self.mI is not None):
        return self.mR.goherschMin()+self.mI.goherschMin()
      else:
        return self.mR.goherschMin()

    def trasforma(self, *args):
        self.mR.trasforma(*args)
        if self.testaLU:
            self.shift=args[1]
        

                   
##     def getClass4Vect():
##         return TrialVectorC
##     getClass4Vect=Callable(getClass4Vect)
        
    def getClass4Vect(self):
        if self.PARALLEL==0:
            return TrialVectorC
        else:
            class Vect4P2(TrialVectorCP2):     
                def __init__(Vself, *dim):
                    Vself.Hami_self = self
                    print " in Vect4P2 " , dim
                    TrialVectorCP2.__init__(Vself,*(dim+ (self.procfrom,) ) )
            return Vect4P2

def stampa(z):
     for i in range(5):
         print z.vr.get_value(i)


def biCGsolve(A, At,  b,  tol=1.0e-10, nmax = 1000, prec=None, restart=None):
    dim=int(A.dim)
    print dim
    
    class4sparse = A.__class__
    class4vect   = class4sparse.getClass4Vect()
    
    r=class4vect(dim)
    rbar=class4vect(dim)
    
    w=class4vect(dim)
    wbar=class4vect(dim)

    x = class4vect(dim)



    if restart is None:
      x.set_to_zero()
    else:
      x.copy_to_a_from_b(restart)

    
    p=class4vect(dim)
    pbar=class4vect(dim)
    
    dum=class4vect(dim)
    z    = class4vect(dim)
    zbar = class4vect(dim)
    
    # r =  b - dot(self, x)
    
    r.copy_to_a_from_b(b)
    A.MoltiplicaMinus(r,x)

    rbar.copy_to_a_from_b(r) # manca il coniugato che fovrebbe essere preso da scalar

    


    z   .copy_to_a_from_b(r)
    if not (prec is  None):
        z.dividebyarray(prec)



    
    k = 0
    err=10
    tolcount=0
    while ( abs(err) > tol or tolcount<5 )and k < nmax:




        # zz = rr/kvect
        zbar.copy_to_a_from_b(rbar)
        if not (prec is None):        
            zbar.dividebyarray(prec)


       
        rho = class4vect.scalare(rbar,z).real

        print rho
        if( abs(rho)<1.0e-15):
          return x

        beta=0
        if(k==0):
            p.copy_to_a_from_b(z)

            pbar.copy_to_a_from_b(zbar)

        else:
            beta=rho/rhoold
            
            p.rescale(beta)
            p.add_from_vect(z)
            
            pbar.rescale(beta)
            pbar.add_from_vect(zbar)
            
        rhoold=rho

        z.set_to_zero()
        A.Moltiplica(z,p)
         
        alpha=(rho/(class4vect.scalare(pbar,z).real))



        
        zbar.set_to_zero()
        At.Moltiplica(zbar,pbar)


        x.add_from_vect_with_fact(p,alpha)
        r   .add_from_vect_with_fact(z   ,-alpha)
        rbar.add_from_vect_with_fact(zbar,-alpha)




        class4vect.scalare(pbar,z).real


        z.copy_to_a_from_b(r)


        if prec is not None:
            z.dividebyarray(prec)

        dum.set_to_zero()
        A.Moltiplica(dum,x)
        dum.add_from_vect_with_fact(b,-1)
        err=class4vect.scalare(dum,dum)
        
        print " ERR ", k,"  ", err
        k = k+1

        if abs(err)< tol: tolcount+=1
        
    return x





if PARALLEL:
    class TrialMatrix_P(Sparsa3AP.Sparsa3AP):
        def __init__(self, *args):

            self.shift=0.0
            if  len(args)==3:
                from_,  to, coeffs = args
                Sparsa3AP.Sparsa3AP.__init__(self)

                self.caricaArrays(  from_,  to, coeffs )

            else:
               raise " numero argomenti sbagliato "




    def getClass4Vect():
            return Sparsa3AP.ArrayP
    getClass4Vect=Callable(getClass4Vect)



    def getClass4Vect_P2(self):
     class Vect4P2(Sparsa3AP.ArrayP2):     
        def __init__(Vself, *dim):
            Vself.Hami_self = self
            
            Sparsa3AP.ArrayP2.__init__(Vself,*dim)

            Vself.setta(self.procfrom)


    def getClass4Vect_P2(self):
     class Vect4P2(TrialVectorP2):
         scalare = TrialVectorP2.scalare
         def __init__(Vself, *dim):
            Vself.Hami_self = self
            
            TrialVectorP2.__init__(Vself,*( dim+(self.procfrom, ) ) )

            Vself.setta(self.procfrom)


     return Vect4P2



        
    class TrialMatrix_P2(Sparsa3AP.Sparsa3AP2):
        def __init__(self, *args):


            self.shift=0.0
            if  len(args)==5:
                from_,  to, coeffs, procfrom  , procto = args
                Sparsa3AP.Sparsa3AP2.__init__(self)

                Sparsa3AP.Sparsa3AP2.caricaArrays(  self,   from_,  to, coeffs , procfrom  , procto)


                self.procto=procto
                self.procfrom=procfrom
            elif len(args)==0:
                Sparsa3AP.Sparsa3AP2.__init__(self)
            else:
               raise " numero argomenti sbagliato "

        def caricaArrays(self,from_,  to, coeffs , procfrom  , procto ):
            Sparsa3AP.Sparsa3AP2.caricaArrays(  self,   from_,  to, coeffs , procfrom  , procto   )

            self.procto=procto
            self.procfrom=procfrom
            
 
        def Moltiplica(self,res,v):
            if hasattr(res,"vr"):
                A=res.vr
            else:
                A=res
                
            if hasattr(v,"vr"):
                B=v.vr
            else:
                B=v
            
            Sparsa3AP.Sparsa3AP2.Moltiplica(self,A,B)
               
            if( self.shift !=0.0):
                res.add_from_vect_with_fact(v,self.shift)
                   
##         def MoltiplicaMinus(self,res,v):

            
##             self.mR.MoltiplicaMinus(res.vr,v.vr)
##             if( self.shift !=0.0):
##                 res.add_from_vect_with_fact(v,-self.shift)

        def getClass4Vect(self):
            return getClass4Vect_P2(self)

        def getClass4Vect(self):
            class Vect4P3(TrialVectorP2): 
                def __init__(Vself, *dim):
                    Vself.Hami_self = self
                    print " in Vect4P2 " , dim
                    TrialVectorP2.__init__(Vself,*(dim+ (self.procfrom,) ) )
            return Vect4P3



if sparsamodulo:
    class TrialMatrix(Sparsa.Sparsa3A):
        def __init__(self, *args):

            if  len(args)==3:
                Sparsa.Sparsa3A.__init__(self, *args)
            elif len(args)==1:
                (ifrom, to , coeffs) = args[0].get_3arrays()

                Sparsa.Sparsa3A.__init__(self, *( coeffs, ifrom, to ) )

            elif len(args)==0:
                ifrom, to , coeffs = Numeric.zeros([0],"i"),Numeric.zeros([0],"i") ,  Numeric.zeros([0],"d")
                Sparsa.Sparsa3A.__init__(self, *( coeffs, ifrom, to ) )
            else:
                Sparsa.Sparsa3A_FE(self, *args)

        def get_parallel(self):
            arrais = self.get_3arrays()
            return TrialMatrix_P(*arrais)

        def get_parallel2(self,procfrom, procto):
            arrais = self.get_3arrays()
            return TrialMatrix_P2(*( arrais+[ procfrom, procto]))

    ##     def getClass4Vect():
    ##         return Sparsa.Array
    ##     getClass4Vect=Callable(getClass4Vect)

        def getClass4Vect(self):
            return Sparsa.Array

    class Vector4Distributed(Sparsa.Array):
        def set_all_random(self, v):
            import mpilight
            myid=0
            nprocs=1
            try:
                myid = mpilight.get_MPI_myid()
                nprocs = mpilight.get_MPI_numprocs()
            except:
                pass
            
            if myid==0:
                Sparsa.Array.set_all_random(self, v)

            if nprocs>1:
                dim =self.len()
                vr = self.get_numarrayview( (0, dim) )
                mpilight.array_MPI_bcast( vr, 0) ;

            
    class TrialMatrixDistributed:
        def __init__(self, repe, coeff, names, BINARY_OUTPUT  ):
            import mpilight
            myid=0
            nprocs=1
            try:
                myid = mpilight.get_MPI_myid()
                nprocs = mpilight.get_MPI_numprocs()
            except:
                pass

            self.Hterms=[]
            self.dim=0
            self.dim2=0
            for i in range(len(coeff) ):
                if i%nprocs != myid:
                    continue
                t_base    = Hilbertxx.Hxx_TransitionMatrix()
                Afrom, Ato, Acoeffs =Hilbertxx.Hxx_TransitionMatrix.Read_arrays( repe+names[i] ,
                                                                                    BINARY_OUTPUT  )


                if( coeff[i] !=0.0) :
                    if   not isinstance( coeff[i] , (complex, numpy.complex128))  :
                        pass
                    else:
                        raise "not yet "


                if(len(Ato)):    
                    S_base=Sparsa.Sparsa3A(Acoeffs*coeff[i] , Afrom , Ato  )
                    self.Hterms.append(S_base)
                    maxto=max(Ato)
                    if maxto>self.dim:
                        self.dim=maxto
                    if maxto>self.dim2:
                        self.dim2=maxto

            print "PROCESSO ", myid , "dim, dim2 ", self.dim, self.dim2
            self.dim=max(self.dim, self.dim2)+1
            
            self.shift=0.0
            self.factor=1.0



        def Moltiplica(self,res,v):
            myid=0
            nprocs=1
            try:
                myid = mpilight.get_MPI_myid()
                nprocs = mpilight.get_MPI_numprocs()
            except:
                pass
            
            dim = res.len()
            add = Vector4Distributed(dim)
            add.set_to_zero()
            add2 = Vector4Distributed(dim)
            add2.set_to_zero()

            if nprocs>1:
            
                for h in self.Hterms:
                    h.Moltiplica(add,v)

                addview = add.get_numarrayview((0, dim))
                add2view = add2.get_numarrayview((0, dim))
                resview = res.get_numarrayview((0, dim))
                mpilight.reduce(addview,add2view,   0 )

                if myid==0:
                    if self.factor !=1.0:
                        add2view[:]=add2view*self.factor
                    if self.shift!=0.0:
                        add2view[:]=add2view + self.shift*v.get_numarrayview((0, dim))
                    res.add_from_vect_with_fact(v,1.0)

                mpilight.array_MPI_bcast(resview , 0) 
            else:
                for h in self.Hterms:
                    h.Moltiplica(res,v)
                if self.factor !=1.0:
                    raise " not yet implemented "
                res.add_from_vect_with_fact(v,self.shift)
            
                

        def trasforma(self, fattore, addendo):
            self.factor=self.factor*fattore
            self.shift=self.shift*fattore+addendo
        def getClass4Vect(self):
            return Vector4Distributed



    
class Lanczos:
    dump_count=0;
    countdumpab=0
    DUMP=0
    def __init__(self, sparse, metrica=None, tol=1.0e-15):


##         print sparse
##         print metrica
##         raise " OK " 

        self.matrice=sparse
        self.metrica=metrica

        self.class4sparse = sparse.__class__
        try:
            self.class4vect   = self.class4sparse.getClass4Vect()
        except:
            self.class4vect   = sparse.getClass4Vect()


        self.nsteps = 0
        self.dim=int(self.matrice.dim)

        self.q = None

        self.alpha = None
        self.beta = None
        self.omega=None
                
        self.tol = tol
        self.maxIt = 50

        self.old = False
        



    def diagoCustom(self, minDim=5, shift=None, start=None):
        print "in diagoCustom  shift= ", shift

        if shift is  None:
            self.matrice.gohersch()
            shift = - self.matrice.goherschMax()
 
        if shift =="MAX":
            self.matrice.gohersch()
            shift = - self.matrice.goherschMin()
      

        print " SHIFT", shift
        self.cerca(minDim, shift, start)

        for ne in range(len(self.eval)):
            self.eval[ne] =  self.eval[ne] - shift

    def passeggia(self, k, m, start, gram=0, NT=4):


        print " in passeggia  " 

        gramcount=0

        if k<0 or m>self.nsteps:
            print "k = ",k," m = ",m," nsteps = ",self.nsteps
            print "Something wrong in passe k<0 or m>nsteps"
            sys.exit(1)

        sn   = math.sqrt(float(self.dim))
        eu   = 1.1e-15
        eusn = eu*sn
        eq   = math.sqrt(eu)


        if k==0:


            try:
                self.class4vect.copy_to_a_from_b(self.q[0],start)
##             print self.q[0]
##             print self.q[0].copy_to_a_from_b
##             print self.q.copy_to_a_from_b
##             print self.class4vect.copy_to_a_from_b
##             print start
##             raise " OK "
            except:
                self.q[0].copy_to_a_from_b(start)

             
            if self.metrica is None:
                self.q[0].normalizzaauto()
            else:


                self.tmp4met.set_to_zero()

                print  " moltiplica per metrica "  

                self.metrica.Moltiplica( self.tmp4met , self.q[0] )
                

                dim =self.tmp4met.vr.len()                    
##                 v=self.tmp4met.vr.get_numarrayview( (0, dim)     )
##                 print Numeric.sum(v)

                # tmpfat = math.sqrt(REAL(self.class4vect.scalare(self.q[0] , self.tmp4met)))


                print self.q[0].scalare( self.tmp4met)
                tmpfat = math.sqrt(REAL(self.q[0].scalare( self.tmp4met)))

                self.q[0].normalizza(tmpfat)



            # self.q[0].dumptofile("q_0")



        p= self.class4vect(int(self.dim))

        for i in range(k,m):
            p.set_to_zero()
            # self.q[i].dumptofile("qbef_"+str(self.dump_count) )

            self.matrice.Moltiplica(p,self.q[i])
            gramcount+=1

            if self.metrica is not None:
                
                self.tmp4met.set_to_zero()
                self.metrica.Moltiplica(self.tmp4met , self.q[i])
                self.alpha[i] = REAL(self.class4vect.scalare(p , self.tmp4met))
                
            else:
                self.alpha[i] = REAL(self.class4vect.scalare(p , self.q[i]))


            # p.dumptofile("p"+str(self.dump_count) )
            self.dump_count+=1

            
            if i==k:

                p.add_from_vect_with_fact( self.q[k] ,   -self.alpha[k]     ) 
                for l in range(k):

                    p.add_from_vect_with_fact( self.q[l] ,  -self.beta[l]     ) 

            else:
                # self.q[i].dumptofile("q_i"+str(self.dump_count) )
                # self.q[i-1].dumptofile("q_im1"+str(self.dump_count) )

                p.add_from_vect_with_fact(self.q[i]  ,    -self.alpha[i]    ) 

                p.add_from_vect_with_fact(self.q[i-1]  ,  -self.beta[i-1]   ) 


            # p.dumptofile("pp"+str(self.dump_count) )
            self.dump_count+=1

            
            if self.metrica is not None:
                self.tmp4met.set_to_zero()
                self.metrica.Moltiplica(self.tmp4met , p)
                self.beta[i] = math.sqrt(REAL(self.class4vect.scalare(p , self.tmp4met)))
                
            else:
                self.beta[i]=self.class4vect.sqrtscalare(p,p) 



            self.omega[i,i]=1.

            max0 = 0.0




            if self.beta[i] != 0:  #and  not scipy.isnan(self.beta[i]):
                for j in range(i+1):
                    self.omega[i+1,j] = eusn



                    if j<k:

                        add = 2 * eusn + abs(self.alpha[j]-self.alpha[i]) \
                              * abs(self.omega[i,j])
                        if i!=k:
                            add += self.beta[j]*abs(self.omega[i,k])
                        if i>0 and j!=i-1:
                            add += self.beta[i-1]*abs(self.omega[i-1,j])
                        self.omega[i+1,j] += add / self.beta[i]

                       
                    elif j==k :                        
                        add = 2 * eusn + abs(self.alpha[j]-self.alpha[i]) \
                              * abs(self.omega[i,j])

                        for w in range(k):
                            add += self.beta[w]*abs(self.omega[i,w])

                        if i!=k+1:
                            add += self.beta[k]*abs(self.omega[i,k+1])
                        if i>0 and i!=k+1:
                            add += self.beta[i-1]*abs(self.omega[i-1,k])

                        self.omega[i+1,j] += add / self.beta[i]



                    elif j<i :

                        add = 2 * eusn + abs(self.alpha[j]-self.alpha[i]) \
                              * abs(self.omega[i,j])

                        if i!=j+1:
                            add += self.beta[j]*abs(self.omega[i,j+1])

                        if i>0 and j>0:
                            add += self.beta[j-1]*abs(self.omega[i-1,j-1])

                        if i>0 and i!=j+1:
                            add += self.beta[i-1]*abs(self.omega[i-1,j])

                        self.omega[i+1,j] += add / self.beta[i]


                            
                    else:

                        add = eusn

                        if i>0:
                            add += self.beta[i-1]*abs(self.omega[i,i-1] )

                        self.omega[i+1,j] += add / self.beta[i]


                    self.omega[j,i+1] = self.omega[i+1,j]

                    max0 += self.omega[i+1,j]**2




                    
            if self.beta[i]==0 or (  max0>eu and not gram  ) or ( gram and ((gramcount%gram) == gram-1)) :
                
                gramcount==0
                
##            if self.beta[i]==0 or max0>0. :

                if i>0:
                    print " GRAMO  self.beta[i]==0 or max0>eu and i>0", i,"  ", self.dump_count, max0,eu
                    self.GramSchmidt(self.q[i],i, NT)

                    
                    if self.metrica is None:
                        self.q[i].normalizzaauto()
                    else:
                        self.tmp4met.set_to_zero()
                        self.metrica.Moltiplica( self.tmp4met , self.q[i] )
                        # tmpfat = math.sqrt(REAL(self.class4vect.scalare(self.q[i] , self.tmp4met)))
                        tmpfat = math.sqrt(REAL(self.q[i].scalare(  self.tmp4met)))
                        self.q[i].normalizza(tmpfat)


                    p.set_to_zero()
                    
                    self.matrice.Moltiplica(p, self.q[i])





                    if self.metrica is not None:
                
                        self.tmp4met.set_to_zero()
                        self.metrica.Moltiplica(self.tmp4met , self.q[i])
                        self.alpha[i] = REAL(self.class4vect.scalare(p , self.tmp4met))
                        
                    else:
                        self.alpha[i] = REAL(self.class4vect.scalare(p , self.q[i]))




                    
                if self.metrica is None:

                    self.GramSchmidt(p,i+1,NT)
                    
                    self.beta[i] = self.class4vect.sqrtscalare(p,p)
                    p.normalizzaauto()
                else:
                    
                    # p.add_from_vect_with_fact( self.q[i] ,   -self.alpha[i]     )
                    # p.add_from_vect_with_fact( self.q[i-1] ,   -self.beta[i-1]     )
                    
                    self.GramSchmidt(p,i+1,NT)

                    self.tmp4met.set_to_zero()
                    self.metrica.Moltiplica(self.tmp4met , p)
                    tmpfat = math.sqrt(REAL(self.class4vect.scalare(p , self.tmp4met)))
                    self.beta[i] = tmpfat
                    p.normalizza(tmpfat)




                if i>0:
                    condition = eu * \
                                math.sqrt(self.dim * (self.alpha[i]**2+\
                                                      self.beta[i-1]**2))
                else:
                    condition = eu * math.sqrt(self.dim * (self.alpha[i]**2))


                if self.beta[i]< condition:

                    print "starting with a vector perpendicular"

                    self.beta[i]=0.
                    
                    # p.set_to_one()
                    # p.set_value(i,i)
                    p.set_all_random(1.0)

                    
                    self.GramSchmidt(p,i+1)



                    if self.metrica is None:
                        p.normalizzaauto()
                    else:
                        self.tmp4met.set_to_zero()
                        self.metrica.Moltiplica( self.tmp4met , p )
                        tmpfat = math.sqrt(REAL(self.class4vect.scalare(p , self.tmp4met)))
                        p.normalizza(tmpfat)




                for l in range(i) :
                    self.omega[i,l]=self.omega[l,i]=eusn
                for l in range(i+1):
                    self.omega[i+1,l]=self.omega[l,i+1]=eusn
                    
            else:
                # print " NORMALIZZO PRIMA DI PASSARE ", self.dump_count
                # p.dumptofile("p_bef_norm"+str(self.dump_count) )

                if self.metrica is None:
                    p.normalizzaauto()
                else:
                    self.tmp4met.set_to_zero()
                    self.metrica.Moltiplica( self.tmp4met , p )
                    tmpfat = math.sqrt(REAL(self.class4vect.scalare(p , self.tmp4met)))
                    p.normalizza(tmpfat)

                
                # p.dumptofile("normprelude"+str(self.dump_count))


            try:
                self.class4vect.copy_to_a_from_b(self.q[i+1],p)
            except:
                self.q[i+1].copy_to_a_from_b(p)

          

    def GramSchmidt(self, vect , n, NT=4):
        for h in range(NT):
            
            if self.metrica is None :
                if(n and  hasattr(self.q,"scalaremulti") ):
                    smulti =self.q.scalaremulti(  vect , n)
                    print " chiamo add-from_multi no metrica " 
                    vect.add_from_vect_with_fact_multi(self.q,-smulti )
                else:
                    for i in range(n):
                        try:
                            s=self.class4vect.scalare( self.q[i], vect)
                        except:
                            s=self.q[i].scalare(  vect)
                        vect.add_from_vect_with_fact(self.q[i],-s)
            else:
                if(n and  hasattr(self.tmp4met,"scalaremulti") ):
                    self.tmp4met.set_to_zero()
                    self.metrica.Moltiplica(self.tmp4met, vect)
                    print " chiamo scalar multi " 
                    smulti =self.q.scalaremulti(  self.tmp4met , n)
                    print " chiamo add-from_multi " 
                    vect.add_from_vect_with_fact_multi(self.q,-smulti )
                    
                else:
                    
                    self.tmp4met.set_to_zero()
                    self.metrica.Moltiplica(self.tmp4met, vect)
                    for i in range(n):

                        # s=self.class4vect.scalare( self.q[i], self.tmp4met)
                        s=self.q[i].scalare(  self.tmp4met)
                        vect.add_from_vect_with_fact(self.q[i],-s)

    def allocaMemory(self):
        self.alpha = Numeric.zeros(self.nsteps,Numeric.float64)
        self.beta  = Numeric.zeros(self.nsteps,Numeric.float64)
        self.omega = Numeric.zeros((self.nsteps+1,self.nsteps+1),Numeric.float64)
        self.evect = Numeric.zeros((self.nsteps,self.nsteps),Numeric.float64)
        self.eval  = Numeric.zeros((self.nsteps),Numeric.float64)
        self.oldalpha = Numeric.zeros((self.nsteps),Numeric.float64)

        
        self.q=self.class4vect(self.nsteps+1,int(self.dim))
        if self.metrica is not None:
            self.tmp4met = self.class4vect( int(self.dim) )

      
    def cerca(self, nd, shift, start=None):
        
        self.shift=shift



        self.matrice.trasforma(1.0, shift)

        
        m = min(4*nd, self.dim)

        self.nsteps = m

        self.allocaMemory()


##         self.q=[]
##         for i in range(self.nsteps+1):
##             self.q.append(self.class4vect(self.dim) )



        vect_init = self.class4vect(int(self.dim))
        # vect_init.set_value(0,1.0)


        if start is None:
            vect_init.set_all_random(1.0)
        else:
            vect_init.copy_to_a_from_b(start)
            
        # vect_init.set_to_one()

        

        k=0
        nc=0
        self.passeggia(k,m,vect_init)
        
        while nc<nd :

            # Sparsa.dumptofile(self.alpha,  "a"+str(self.countdumpab))  
            # Sparsa.dumptofile(self.beta[:-1] ,  "b"+str(self.countdumpab))  

            print " DIAGONALIZZAZIONE "
            self.diago(k,m)

            if self.dim==1:
                nc = 1
                break
            
            # Sparsa.dumptofile(Numeric.resize(self.evect,[m*m]) ,  "vect_"+str(self.countdumpab))  

            nc = self.converged(m)

            if k and not Numeric.sometrue(abs(self.beta[:k])>self.tol) :
                break

            if (nc+2*nd) >= m:
                k=m-1
            else:
                k=nc+2*nd

            # self.q.dumptofile("qtotbef"+str(self.countdumpab))
            print "KKKKKKKKK  " ,  k 
            self.ricipolla(k,m)

            # self.q[0].dumptofile("q0afte"+str(self.countdumpab))
            self.countdumpab+=1
            
            print " k,m , dim", k, m, self.dim
            # return m # sentinell
        
            self.passeggia(k,m,self.q[m])

            if self.DUMP:
                for vtd in range(200):
                    # offset=2999253 2997876
                    offset=  2998226
                    self.q[vtd].vr.scriviArray("DUMP/vect_%d_ALL"%vtd, offset + 25*4*56 *0, 58*        25*4 ) ;
##                    self.q[vtd].vr.scriviArray("DUMP/vect_%d_Ti"%vtd, offset + 25*4*56 , 25*4 ) ;
##                    self.q[vtd].vr.scriviArray("DUMP/vect_%d_O"%vtd, offset + 25*4*57 , 25*4 ) ;
##                    self.q[vtd].vr.scriviArray("DUMP/vect_%d_N"%vtd, offset  + 25*4*0 , 25*4 *4) ;

            


        self.matrice.trasforma(1.0, -shift)

        if m==self.dim:
            return m
        else:
            return k


    def converged(self, m):

        print self.eval

        for j in range(1,m):
            for i in range(m-j):
                if abs(self.eval[i]) < abs(self.eval[i+1]):
                    self.eval[i],self.eval[i+1]=self.eval[i+1],self.eval[i]
                    self.evect[i:i+2]=self.evect[i+1],self.evect[i]


        # print "eval", self.eval[0:m]
        res = 0
        if self.old:
            while res<m:
                print self.oldalpha[res]
                if (abs(self.eval[res]-self.oldalpha[res])\
                    /abs(self.oldalpha[res])>self.tol):
                    break
                res+=1
        else:
            self.old=True

        self.oldalpha[0:m]=self.eval[0:m]

        return res

    def ricipolla(self, k,m):






        self.alpha[0:k]= self.eval[0:k]


        self.beta[0:k] = self.beta[m-1]*self.evect[0:k,m-1]


        # E = self.evect[0:k,0:m].copy()

        a=self.class4vect(k,  int(self.dim) )



        self.class4vect.mat_mult(a, self.evect[0:k,0:m] , self.q)
## void  Array::mat_mult(Array & b, double * mat,Array & a) {
##   int m=a.firstdimension-1;
##   int k=b.firstdimension;
##   int dim = a.size/a.firstdimension;

##   // uMatrix<double> E(m, k,  mat);
## //   uMatrix<double> A(dim, m, a.data );
## //   uMatrix<double> B(dim, k, b.data );
  
##   ::dumptofile(mat, m*k, "bloccovectpy");
   
##   char transa='N', transb='N';
##   double alpha = double(1.0);
##   double beta = double(0.0);
  
##   dgemm_( &transa, &transb, &dim, &k, &m, &alpha, a.data, &dim, 
##            mat, &m, &beta, b.data, &dim );
## }



        for i in range(k):
            a[i].normalizzaauto()
            try:
                self.class4vect.copy_to_a_from_b( self.q[i] , a[i]     )
            except:
                self.q[i].copy_to_a_from_b(   a[i]     )
        a=None

        try:
            self.class4vect.copy_to_a_from_b(self.q[k]  ,  self.q[m] )
        except:
            self.q[k].copy_to_a_from_b(  self.q[m] )
 
        o = self.omega[0:m,0:m].copy() 


        o = Numeric.dot(o,Numeric.transpose(self.evect))


        for i in range(k):
            self.omega[i,k]=self.omega[k,i]=o[i,k]

        
        o = Numeric.dot(self.evect,o)

        
        self.omega[0:k,0:k]=o[0:k,0:k]



         
    def diago(self, k, m):

        if  sparsamodulo:

            eval=Numeric.zeros([m,m],Numeric.float64)
            
            
            Sparsa.diagonalizza4py(k,self.alpha,  self.beta, self.evect, eval)
            for i in range(m):
                self.eval[i]=eval[i,i]


        else:
            mat = Numeric.zeros([m,m],"d")
            mat.shape=[m*m]
            mat[0:m*m:m+1] = self.alpha
            mat[k*m+k+1:m*m:m+1] =self.beta[k:m-1]
            mat[(k+1)*m+k:m*m:m+1] =self.beta[k:m-1]
            mat.shape=[m,m]
            mat[   k     ,0:k  ] = self.beta[:k]
            mat[   0:k, k ] = self.beta[:k]



            # self.eval,self.evect = LinearAlgebra.Heigenvectors(mat)

            self.eval,self.evect = numpy.linalg.eigh(mat)
            self.evect=numpy.array(numpy.transpose(self.evect))


            

def solveEigenSystem( S_base , nsearchedeigen, shift=None, metrica=None,  tol=1.0e-15, start=None, DUMP=0):
    
    lnczs=Lanczos( S_base , metrica=metrica, tol=tol)
    lnczs.DUMP=DUMP
    lnczs.diagoCustom(minDim=nsearchedeigen, shift=shift, start=start)
    return lnczs.eval, lnczs.q



def  CalcolaSpettroChebychev( cheb_shift, fact_cheb,  alphas ) :
    Nu=len(alphas)
    Pi=math.pi
    Nbar =1
    while(Nbar<Nu) :
        Nbar=Nbar*2
    Nbar=Nbar*2   

    cfft=Numeric.zeros( 2*Nbar  ,"D")
    Xs=Numeric.zeros( Nbar, "d")
    Res=Numeric.zeros( Nbar, "d")
    

    
    cfft[:]=0

    for i in range(Nbar):
       Xs[i]=math.cos( (Nbar-1 - i +0.5)*Pi/Nbar)
       
       if(i<Nu) :
          fact = 1.0/(Nu+1)*( (Nu-i +1.0)*math.cos(Pi* i /(Nu+1.0))+ math.sin(Pi* i /(Nu+1.0))* math.cos(Pi/(Nu+1))/math.sin(Pi/(Nu+1))    )
          
          cfft[i]= alphas[i]*fact*Numeric.exp(   ((0.0+1.0j) * ( Nbar -0.5)) *i*Pi/Nbar     )
       else:
          cfft[i]=0
      
    res=FFT.fft(cfft) [:Nbar]

    res=res -  alphas[0]*0.5

    res[:] = 2 * res / Pi / (Numeric.sqrt(1-Xs*Xs)*fact_cheb)
    
    return Xs * fact_cheb + cheb_shift , res



def faispettriChebyChev( evals, evects   , S_ecci , Dipos , maxene, NstepsTridiag, evalsMIN, evalsMAX, ilevel=0):
  try:
      class4vect   = S_ecci.__class__.getClass4Vect()
  except:
      class4vect   = S_ecci.getClass4Vect()

  print " dim " , S_ecci.dim
  original = [class4vect(S_ecci.dim) for dipo in Dipos ]
  dumA     = class4vect(S_ecci.dim) 
  
  X_n_plus_1  = [class4vect(S_ecci.dim) for dipo in Dipos ]
  X_n         = [class4vect(S_ecci.dim) for dipo in Dipos ]
  X_n_minus_1 = [class4vect(S_ecci.dim) for dipo in Dipos ]

  data4spect={}
  
  dene = evals[ilevel:] - evals[ilevel]
  
  neigtc=0
  while( neigtc< len( evals)-ilevel and  dene[neigtc] < maxene):
    neigtc+=1

  data4spect["neig"] = neigtc
  data4spect["ndipo"] = len(Dipos)
  data4spect["alphas"] = {}
  data4spect["a_b"] = {}
  data4spect["enes"] = {}


  oldfact=1.0
  oldcenter=0.0

  for ival in range(neigtc):
    data4spect["enes"][ival]=evals[ival+ilevel]
    data4spect["alphas"][ival]={}


    emax=evalsMAX[0]
    emin=evalsMIN   [0]

    center = dene[ival]+ (emax+emin)*0.5
    fact_a = ((emax-emin)*0.5 +dene[neigtc-1 ])*1.00001
    
    data4spect["a_b"][ival] = {"center":center, "fact_a": fact_a }

    S_ecci.trasforma(  oldfact, oldcenter        )
    S_ecci.trasforma(  1.0/fact_a, -center/fact_a        )

    oldfact=fact_a
    oldcenter=center
    
    for idip in range(len(Dipos )):
      print "idip ", idip
      original [idip].set_to_zero()
      Dipos[idip].Moltiplica(original[idip], evects[ival+ilevel]  )
      data4spect["alphas"][ival][idip]={}
      for jdip in range(len(Dipos )):
          data4spect["alphas"][ival][idip][jdip]=[]

      class4vect.copy_to_a_from_b( X_n[idip],original[idip] )
      class4vect.copy_to_a_from_b(X_n_minus_1[idip],original[idip] )

##       X_n[idip].copy_to_a_from_b(original[idip])
##       X_n_minus_1[idip].copy_to_a_from_b(original[idip])
      
    for idip in range(len(Dipos )):
        print "idip ", idip
        for jdip in range(len(Dipos )):
            print "jdip ", jdip
            # data4spect["alphas"][ival][idip][jdip].append( original[idip] .scalare(X_n[jdip])    )
            data4spect["alphas"][ival][idip][jdip].append(   class4vect.scalare(original[idip], X_n[jdip])    )


    for m in range( NstepsTridiag):
         print " FRONT Cheb " , m
         for idip in  range(len(Dipos )):


            dumA.set_to_zero()
            S_ecci.Moltiplica(dumA,X_n[idip]  )       

            if(m>0):
                 # dumA . rescale(2.0)
                 dumA . normalizza(1.0/2.0)
                 dumA .  add_from_vect_with_fact(X_n_minus_1[idip],-1.0 )
                 
##             X_n_minus_1[idip].copy_to_a_from_b(  X_n[idip]   )     
##             X_n        [idip].copy_to_a_from_b(  dumA  )
            class4vect.copy_to_a_from_b(X_n_minus_1[idip],   X_n[idip]   )     
            class4vect.copy_to_a_from_b(X_n        [idip],  dumA  )
            
         for idip in range(len(Dipos )):
             for jdip in range(len(Dipos )):
                 # data4spect["alphas"][ival][idip][jdip].append( original[idip] .scalare(X_n[jdip])    )
                 data4spect["alphas"][ival][idip][jdip].append(  class4vect.scalare( original[idip] , X_n[jdip])    )
  return data4spect





      


def   faispettri( evals, evects   , S_ecci , maxene, Dipos, tospan, getvects=0,
                  gram=0, NT=4, metrica=None, ilevel=0, Slevelsout =None ):

  try:
      class4vect   = S_ecci.__class__.getClass4Vect()
  except:
      class4vect   = S_ecci.getClass4Vect()

  print " dim " , int(S_ecci.dim)
  dumA = class4vect(int(S_ecci.dim))
  data4spect={}
  
  dene = evals[ilevel:] - evals[ilevel]
  
  neigtc=0
  while( neigtc< len( evals)-ilevel and  dene[neigtc] < maxene):
    neigtc+=1

  data4spect["neig"] = neigtc
  data4spect["ndipo"] = len(Dipos)
  data4spect["enes"] = {}
  data4spect["alphas"] = {}
  data4spect["norms"] = {}
  data4spect["storevect"] = {}



  lnczs=Lanczos( S_ecci , metrica)
  lnczs.nsteps = tospan
  lnczs.allocaMemory()

  print " allocato memory " 

  for ival in range(neigtc):
    data4spect["enes"][ival]=evals[ival+ilevel]
    data4spect["alphas"][ival]={}
    data4spect["norms"][ival]={}
    data4spect["storevect"][ival]={}

    for idip in range(len(Dipos )):

      dumA.set_to_zero()
      print evects[ival+ilevel] 


      print " calcolo norma " 
      try:
          norm_to_remember = evects[ival+ilevel] .scalare(  evects[ival+ilevel] )
      except:
          norm_to_remember = class4vect.scalare( evects[ival+ilevel] , evects[ival+ilevel] )


      print "NORMA " , norm_to_remember

      if(Dipos[idip] is not None):
        Dipos[idip].Moltiplica(dumA, evects[ival+ilevel]  )
      else:
        dumA.copy_to_a_from_b(evects[ival+ilevel]  )

      if Slevelsout  is not None:
          lout_fun, facts = Slevelsout
          for f in facts:
              dumA=lout_fun(f, dumA   )

      


      
      norm_to_remember = class4vect.scalare( dumA,  dumA )

      print "NORMA " , norm_to_remember
      data4spect["norms"][ival][idip] = norm_to_remember
      if( abs(norm_to_remember) > 1.0e-20):
        lnczs .passeggia(0, lnczs.nsteps, dumA, gram, NT);

        data4spect["alphas"][ival][idip]=Numeric.array([ (lnczs.alpha[j], lnczs.beta[j]) for j in range(lnczs.nsteps)    ])
        if getvects:
          data4spect["storevect"][ival][idip]=lnczs.q.copy()

      else:
        data4spect["alphas"][ival][idip]=[ (0.0, 0.0) for j in range(lnczs.nsteps)    ]
        

  return data4spect
      



def spectra(alphas, energies):
    AB=Numeric.array(alphas)
    A=AB[:,0]
    B=AB[:,1]
    res=ContFracSpectra( A,B, energies )
    return res
  
def ContFracSpectra( A,B, W ):
    n=len(A)
    res=0*W
    for i in range(n-1, -1, -1):
        if( i!=(n-1) ):
            res= -B[i]*B[i]/res
        res=res +A[i]-W
    return 1./res
     
def getinvfirstcolumns( alphas, ein, gammain):

    print alphas
    print ein
    print gammain

    N=len(alphas)
    detes=Numeric.zeros(len(alphas)+1,"D")
    detes[N]=1
    detes[N-1]= (alphas[N-1][0]-ein- 1.0j *gammain)
    for i in range(N-2,-1,-1):
      detes[i] = detes[i+1] * (alphas[i][0]-ein- 1.0j* gammain)- detes[i+1]*alphas[i][1]*alphas[i][1]
      print detes[i]
   
    fattori=Numeric.zeros(len(alphas),"D")
    prod=1
    
    for j in range(N):
      fattori[j]= prod*detes[j+1] /detes[0]
      prod=-prod*alphas[j][1]*alphas[j][1]


    return fattori

def CGsolve(H,eps2,  b, tol=1.0e-10, nmax = 1000, restart=None):
        """
        Solve self*x = b and return x using the conjugate gradient method
        """
        try:
            class4sparse = H.__class__
            class4vect   = class4sparse.getClass4Vect()
        except:
            class4sparse = H.getClass4Vect()
            class4vect=H.getClass4Vect()

                      
        print " in CG " 
        dim = int(H.dim)
        r= class4vect(dim)
        p = class4vect(dim)
        diff = class4vect(dim)
        dum=class4vect(dim)
        z=class4vect(dim)
        x0=class4vect(dim)

        x0.set_to_zero()
        if restart is not None:
          x0.copy_to_a_from_b(restart )

        # kvec = self.diag() # preconditionner
        x = x0 # initial guess


        #  residup 
        dum.set_to_zero()
        H.Moltiplica(dum,x)
        r.set_to_zero()
        H.Moltiplica(r,dum)
        r.add_from_vect_with_fact(x, eps2)
        r.normalizza(-1.0)
        r.add_from_vect_with_fact(b, 1.0)


        p.set_to_zero()
        beta = 0.0;
                        
        rho = REAL(class4vect.scalare(r, r))


        err= rho
                        

                        
        k = 0
        print "ERR " , err
        while abs(err) > tol and k < nmax:
          
          #  p = r + beta*p;

          dum.set_to_zero()
          dum.add_from_vect_with_fact(r,1.0)
          dum.add_from_vect_with_fact(p,beta)
          p.set_to_zero()
          p.add_from_vect_with_fact(dum,1.0)
          
          
          # z = dot(self, p);
          dum.set_to_zero()
          H.Moltiplica(dum,p)
          z.set_to_zero()
          H.Moltiplica(z,dum)
          z.add_from_vect_with_fact(p, eps2)



          # alpha = rho/vector.dot(p, z);
          alpha = rho/(REAL(class4vect.scalare(p, z)))
                                
          # r = r - alpha*z;
          r.add_from_vect_with_fact(z,-alpha)

                                
                                
          rhoold = rho;
          #rho = vector.dot(r, w);
          
          rho = REAL(class4vect.scalare(r, r))


                                
          # x = x + alpha*p;
          x.add_from_vect_with_fact(p,alpha)
                                
          beta = rho/rhoold;
                                
          # err = vector.norm(dot(self, x) - b);



##           dum.set_to_zero()
##           H.Moltiplica(dum,x)
##           diff.set_to_zero()
##           H.Moltiplica(diff,dum)
##           diff.add_from_vect_with_fact(x, eps2)
##           diff.normalizza(-1.0)
##           diff.add_from_vect_with_fact(b, 1.0)
          

##           err= class4vect.scalare(diff,diff).real
          rabs = REAL(class4vect.scalare(x,b))
          
          err=rho
          print err
          print type(err)
          print " ERR "  , k,"  ", rho, "  ", rabs
          k = k+1
        return x,err





def checkInv( S_ecci, ein, gammain, qsomma, evect):
      class4sparse = S_ecci .__class__
      class4vect   = S_ecci.getClass4Vect()

      dum1 = class4vect( evect.len() )
      dum1.set_to_zero()

      S_ecci.Moltiplica(dum1, qsomma )
      print dum1.__class__
      if dum1.__class__== TrialVectorC:
          dum1.add_from_vect_with_fact( qsomma  , -ein-1.0j*gammain   )
      elif dum1.__class__== Sparsa.Array:
          if( gammain !=0.0):
              raise " gammain !=0.0 in operazione con vettore float "
          dum1.add_from_vect_with_fact( qsomma  , -ein   )
      else:
          raise "classe non ancora prevista "
      dum1.add_from_vect_with_fact( evect  , -1.0   )

      return dum1

def Real(x):
    if( x.__class__ == complex):
        return x.real
    else:
        return x
def EQsolve(H,  b,precov,  tol=1.0e-10, nmax = 1000, restart=None, volta=[]):
        """
        Solve self*x = b and return x using the conjugate gradient method
        """
        class4sparse = H.__class__
        class4vect   = H.getClass4Vect()

        print " in CG " 
        dim = int(H.dim)
        if len(volta)==0:
            r= class4vect(dim)
            p = class4vect(dim)
            diff = class4vect(dim)
            dum=class4vect(dim)
            z=class4vect(dim)
            x0=class4vect(dim)
            volta.extend( [r,p,diff,dum,z,x0]             ) 
        else:
           r,p,diff,dum,z,x0    =volta
           
        x0.set_to_zero()
##         x0.copy_to_a_from_b(b )
##         x0.multbyarray(precov)
##         x0.multbyarray(precov)
       


        # x0.set_to_one()
        
        if restart is not None:
          x0.copy_to_a_from_b(restart )

        # kvec = self.diag() # preconditionner
        x = x0 # initial guess

        #  residup 
        r.set_to_zero()


        dum.copy_to_a_from_b(x )
        
        dum.multbyarray(precov)
        
        H.Moltiplica(r,dum)
        
        r.multbyarray(precov)

        
        r.normalizza(-1.0)
        r.add_from_vect_with_fact(b, 1.0)

        p.set_to_zero()
        beta = 0.0;
                        
        # rho = Real(class4vect.scalare(r, r))
        rho = Real(r.scalare( r))
        
        err= rho
                        
        k = 0
        tolcount=0
        while (  abs(err) > tol and k < nmax)  or tolcount<1:
          
          #  p = r + beta*p;

          dum.set_to_zero()
          dum.add_from_vect_with_fact(r,1.0)
          dum.add_from_vect_with_fact(p,beta)
          p.set_to_zero()
          p.add_from_vect_with_fact(dum,1.0)
          
          
          # z = dot(self, p);
          z.set_to_zero()



          dum.copy_to_a_from_b(p )
          
          dum.multbyarray(precov)
          
          H.Moltiplica(z,dum)
          
          z.multbyarray(precov)






          

          # alpha = rho/vector.dot(p, z);
          # alpha = rho/Real(class4vect.scalare(p, z))
          alpha = rho/Real(p.scalare( z))
                                
          # r = r - alpha*z;
          r.add_from_vect_with_fact(z,-alpha)

                                
                                
          rhoold = rho;
          #rho = vector.dot(r, w);
          
          rho = Real(r.scalare( r))
                                
          # x = x + alpha*p;
          x.add_from_vect_with_fact(p,alpha)
                                
          beta = rho/rhoold;
                                
          # err = vector.norm(dot(self, x) - b);

          diff.set_to_zero()
          # H.Moltiplica(diff,x)



	  if(1):
		  err= Real( r.scalare(r))
		  print k," CG ERR  ", err
		  
	  else:
		  dum.copy_to_a_from_b(x )
		  
		  dum.multbyarray(precov)
		  
		  H.Moltiplica(diff,dum)
		  
		  diff.multbyarray(precov)
		  
		  diff.normalizza(-1.0)
		  diff.add_from_vect_with_fact(b, 1.0)
		  
		  err= Real( diff.scalare(diff))
          
          
		  print k," CG ERR  ", err, Real( r.scalare(r))
	  
          k = k+1
          if abs(err)< tol: tolcount+=1

          
        return x











def EQsolvePreco(H,  b, omega=None,  tol=1.0e-10, nmax = 1000, restart=None, volta=[], LU=None):
        """
        Solve self*x = b and return x using the conjugate gradient method
        """
        class4sparse = H.__class__
        class4vect   = H.getClass4Vect()




        print " in CG " 
        dim = (H.dim)
        if len(volta)==0:
            r= class4vect(dim)
            p = class4vect(dim)
            Ap = class4vect(dim)
            dum=class4vect(dim)
            z=class4vect(dim)
            zz=class4vect(dim)
            x0=class4vect(dim)
            volta.extend( [r,p,Ap,dum,z,zz,x0]             ) 
        else:
           r,p,Ap,dum,z,zz,x0    =volta
           
        x0.set_to_zero()
##         x0.copy_to_a_from_b(b )
##         x0.multbyarray(precov)
##         x0.multbyarray(precov)
       

        # x0.set_to_one()
        
        if restart is not None:
          x0.copy_to_a_from_b(restart )

        # kvec = self.diag() # preconditionner
        x = x0 # initial guess

        #  residup 
        r.set_to_zero()
        dum.copy_to_a_from_b(x )        
        H.Moltiplica(r,dum)



       
        print " r.r " , r.scalare(r)

      
        r.normalizza(-1.0)
        r.add_from_vect(b)


        print " r.r bis " , r.scalare(r)
        

        if LU is not None:
            zz.set_to_zero()
            LU.MoltiplicaExpL(zz.vr,r.vr)
            z.set_to_zero()
            LU.MoltiplicaExp(z.vr,zz.vr)
            # z.vr.add_from_vect_with_fact(r.vr,0.1)
            
        else:
            
            if(       isinstance( omega , (complex, numpy.complex128))   ):
                z.set_to_zero()
                H.Preco(z,r, omega)
            else:
                z.copy_to_a_from_b(r)
                z.multbyarray(omega)
                z.multbyarray(omega)
                
        print " z.z  " , z.scalare(z)

        # z.copy_to_a_from_b(r)
	# z.rescale(0.5)
	
        p.copy_to_a_from_b(z)

        
        beta = 0.0;
                        
        # rho = Real(class4vect.scalare(r, r))
        rho = Real(z.scalare( r))
        
        err= rho
                        
        k = 0
        tolcount=0
        while (  abs(err) > tol and k < nmax)  or tolcount<1:


          Ap.set_to_zero()
          H.Moltiplica(Ap, p)

          alpha = rho/ p.scalare(Ap)

	


          x.add_from_vect_with_fact(p,alpha)


	  
          r.add_from_vect_with_fact(Ap,-alpha)
          

          
          if LU is not None:
              zz.set_to_zero()
              LU.MoltiplicaExpL(zz.vr,r.vr)
              z.set_to_zero()
              LU.MoltiplicaExp(z.vr,zz.vr)
              # z.vr.add_from_vect_with_fact(r.vr,0.1)
          else:
              
              if(    isinstance( omega , (complex, numpy.complex128))  ):
                  z.set_to_zero()
                  H.Preco(z,r, omega)
              else:
                  z.copy_to_a_from_b(r)
                  z.multbyarray(omega)
                  z.multbyarray(omega)
              

           
          rhoold=rho
          rho = Real(z.scalare( r))

          beta= rho/rhoold
          

          p.rescale(beta )

          p.add_from_vect(z )
          

          err= Real( r.scalare(r))
          print k," CG ERR  ", err
		  

	  
          k = k+1
          if abs(err)< tol: tolcount+=1

          
        return x







    
def RoughSolve(H,  b,precov,  tol=1.0e-10, nmax = 1000, restart=None, volta=[]):
        """
        Solve self*x = b and return x using the conjugate gradient method
        """
        class4sparse = H.__class__
        class4vect   = H.getClass4Vect()

        print " in CG " 
        dim = (H.dim)
        if len(volta)==0:
            r= class4vect(dim)
            p = class4vect(dim)
            diff = class4vect(dim)
            dum=class4vect(dim)
            z=class4vect(dim)
            x0=class4vect(dim)
            volta.extend( [r,p,diff,dum,z,x0]             ) 
        else:
           r,p,diff,dum,z,x0    =volta
           
        x0.set_to_zero()

        # x0.set_to_one()
        
        if restart is not None:
          x0.copy_to_a_from_b(restart )

        # kvec = self.diag() # preconditionner
        x = x0 # initial guess



        while(1):
            dum.copy_to_a_from_b(b )
            H.MoltiplicaMinus ( dum, x  )
            print " ERRORE " , dum.scalare(dum)
            z.set_to_zero()
            H.mR.MoltiplicaExp(z.vr,dum.vr)
            dum.set_to_zero()
            H.mR.MoltiplicaExpL(dum.vr,z.vr)
            x.add_from_vect_with_fact(dum, 1.0)
        


