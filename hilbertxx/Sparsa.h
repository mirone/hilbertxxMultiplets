/************************************************************************

  Copyright
  Alessandro MIRONE
  mirone@esrf.fr

  Copyright 2002  by European Synchrotron Radiation Facility, Grenoble, 
                  France

                               ----------
 
                           All Rights Reserved
 
                               ----------

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the names of European Synchrotron
Radiation Facility or ESRF or SCISOFT not be used in advertising or 
publicity pertaining to distribution of the software without specific, 
written prior permission.

EUROPEAN SYNCHROTRON RADIATION FACILITY DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL EUROPEAN SYNCHROTRON
RADIATION FACILITY OR ESRF BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

**************************************************************************/
/***************************************************************************
                          Sparsa.h  -  description
                             -------------------
    begin                : Tue Feb 1 2000
    copyright            : (C) 2000 by Alessandro MIRONE
    email                : mirone@lure.u-psud.fr
 ***************************************************************************/
#ifndef _SPARSAH_
#define _SPARSAH_

#include<map>
#undef NDEBUG
#include<assert.h>

#define fwrite(ptr , itemsize, nitems ,f)  {int res; res =fwrite((ptr) , (itemsize), (nitems) ,(f)); assert(res==(nitems)); }

#define fread(ptr , itemsize, nitems ,f)  {int res; res =fread ((ptr) , (itemsize), (nitems) ,(f)); assert(res==(nitems)); }

#define fscanf(...)     {int res; res =   fscanf (__VA_ARGS__)  ; assert(res>-1); }
#define fprintf(...)     {int res; res =   fprintf(__VA_ARGS__)  ; assert(res>-1); }

class Array ;

class Sparsa3A  {

 public:
  Sparsa3A();
  void inizializza(int N, double *c, int *i, int *j, double *pot_for_from  , double *pot_for_to );
  void add2template(int N, double *c, int * goes2) ;
  void add2template_forhops(int N,  double *coeffs , int *q,
	  int *cc , double * hopfacts);



  void  add_( int Nto , int Nfrom,double *c, int *corrto, int *corrfrom, int symm);

  void addSymm(int m, int n,double *c, int *corrto, int *corrfrom ) {
    add_( m , n, c, corrto, corrfrom, 1);
  }
  void addSymmI(int m, int n,double *c, int *corrto, int *corrfrom){
    add_( m , n, c, corrto, corrfrom, -1);
  };

  void add( int m, int n,double *c, int *corrto, int *corrfrom, int symm=0){
    add_( m , n, c, corrto, corrfrom, 0);
  };
  void adddiag( int n,double *c, int *corrto, int *corrfrom){
    add_( -1 , n, c, corrto, corrfrom, 0);
  };
  void merge( Sparsa3A &a ) {
    add_( -1 , a.n, a.coeff, a.row, a.col, 0);
  } ;


  void extractM( double *&mat, int &ncF , int *&corrF,  int &ncT , int *&corrT,       int sF,int dF ,int sT , int dT ) ;



  Sparsa3A(int N, double *c, int *i, int *j);
  void setpot(  double *pot_for_i  , double *pot_for_j );
   ~Sparsa3A();
   void inizializza(char *file, double fact=1);

  void inizializza( Sparsa3A &a ) ;
  void inizializza( Sparsa3A &a , double fact) ;
  void pulisci();
  void Moltiplica(double *ris, double *vect  );
  void MoltiplicaMinus(double *ris, double *vect  );
  void MoltiplicaDiag     (double *ris, double *vect  );
  void MoltiplicaDiagMinus(double *ris, double *vect  );

  void Moltiplica(Array *ris, Array *vect  );
  void MoltiplicaMinus(Array *ris, Array *vect  );

  void MoltiplicaDiag(Array * ris, Array*vect  );
  void MoltiplicaDiagMinus(Array * ris, Array*vect  );

  void trasforma(double fattore, double addendo) ;	
  void transpose() { int *dum; dum=col; col=row; row=dum; };
  void Moltiplica(double *ris, double *vect , int start, int end );


  void riordina();
  void preparaLLt(double tol);
  void LLtSolve(Array *ris, Array *vect  );



  void aggiungimemoria();

  void   gohersch   ();
  double goherschMin();
  double goherschMax();

  void debug_sufile(char * nome);
  void readfile(char * nome) ;

  int nG;
  double *Gmin, *Gmax;


  int n;
  int nsize;
  int dim;
  int dim2;

  void  get_3arrays(int &Nels , int *&from, int *&to, double *&coeffs);

  double *coeff  ;
  int    *col   ;
  int    *row   ;


  // per llt
  int *fromstart, *diagpos; 
  int *patternbyrow;                       
  int *patternrowstart;    
  int lltsize;       
  int *lltcol, *lltrow;   
  double *lltcoeff;    
  int *lltstart;  
  int *lltdiagpos ;
  int lltn;
  
  
};



#include<stdexcept>


struct Array {
  Array( int size ) { 
    firstdimension=0;
    ownsdata=1;
    init_array( size ); 
  }

  Array( int fd, int sd ) { 
    firstdimension=fd;
    ownsdata=1;
    init_array( fd*sd ); 
  }
  Array( int size , double * data) { 
    firstdimension=0;
    ownsdata=0;
    this->data=data;
    this->size=size;
  }

  ~Array() { 
    if( ownsdata) 
      free_array(); 
    else
      std::cout << " cancellata una finestra su array \n";
  }


  Array * get_array(int index) {
    if( index >=firstdimension || index<0) {
      throw std::out_of_range("index invalido in Array * get_array(int index)");
    }
    return new Array(  size/firstdimension, data+ index*(size/firstdimension)  );
  }
  void get_numarrayview(int start, int  N,  double *&ArrayFLOAT) ;


  double get_value( int index ) const
  {
    printf("get_value(%d)\n",index);
    return data[index];
  }
  
  void set_value( int index, double value )
  {
    printf("set_value(%d,%e)\n",index,value);
    data[index]=value;
  }

  void  set_values( int  N, int *indices, double *values) {

    for(int i=0; i<N; i++ )  {
      data[indices[i]]=values[i];
    }

  };


  
#undef RAND_MAX
#define RAND_MAX        2147483647
  
  void set_all_random(  double value )
  {
    
    int index;
    for(index= 0; index<size; index++) {
      data[index]=value*random()*1.0/RAND_MAX;
    }
    
  }
  void set_to_zero(  )
  {
    int index;
    for(index= 0; index<size; index++) {
      data[index]=0.0;
    }      
  }
  void set_to_one(  )
  {
    int index;
    for(index= 0; index<size; index++) {
      data[index]=1.0;
    }      
  }
  
  static void copy_to_a_from_b(Array &a, Array&b) {
    if( a.size != b.size) {
      throw std::out_of_range("frames are not aligned");   
    }
    memcpy(a.data, b.data, a.size*sizeof(double) );
  }
  
  
  void copy_from_b( Array&b) {
    if( this->size != b.size) {
      throw std::out_of_range("frames are not aligned");   
    }
    memcpy(this->data, b.data, this->size*sizeof(double) );
  }
  
  
  void add_from_vect( Array&vect) {
    if( size != vect.size) {
      throw std::out_of_range("frames are not aligned");   
    }
    for(int i=0;i<size;++i)
      data[i]+=vect.data[i];
  }
  
  void add_from_vect_with_fact( Array&vect, double fact) {
    if( size != vect.size) {
      throw std::out_of_range("frames are not aligned");   
    }
    for(int i=0;i<size;++i)
      data[i]+=vect.data[i]*fact;
  }
  void mult_by_fact(  double fact){
    for(int i=0;i<size;++i)
      data[i]*=fact;
  }
  
  
  void  set_indices_inv(int N, int *indices, double *values) ;
  void  set_indices( int  N, int *indices, double *values) ;
  
  
  Array* copy() const
  {
    Array *b=new Array(size);
    b->firstdimension=firstdimension;
    for(int i=0;i<size;++i)
      b->data[i]=data[i];
    return b;
  }
  
  Array& operator+=( const Array& a ) {
    for(int i=0;i<len() && i<a.len();++i) data[i]+=a.data[i];
    return *this; 
  }

  static void  mat_mult(Array & b, double * mat,Array & a) ;


  int len() const { return size; }
  
  double *dataAddress() {return data;}
  
  void dumptofile(char * nome) {
    FILE * f=fopen(nome,"w");
    fwrite(&size, sizeof(int), 1, f);
    printf(" scrivo %d elementi \n", size);
    fwrite(dataAddress(), sizeof(double), len(), f);
    fclose(f);
    
  }
  void readfile(char * nome) {
    FILE * f=fopen(nome,"r");
    delete data;
    fread(&size, sizeof(int), 1, f);
    printf(" leggo  %d elementi \n", size);
    data = new double[size];
    fread(data, sizeof(double), size, f);
    fclose(f);
    
  }
  void  dividebyarray( Array &b);
  void  multbyarray( Array &b);
  static double scalare(Array &a, Array &b);
  static double sqrtscalare(Array &a, Array &b);



  void normalizza(double norm);
  void normalizzaauto();
  
protected:

  int firstdimension;
  int ownsdata;
  int size;
  double* data;
  
  void init_array( int n )
  {
    ownsdata=1;
    size=n;
    data=(double*)malloc(sizeof(double)*n);
    memset(data, 0, sizeof(double)*n);
    printf("array %p, size=%d, data=%p\n", this, size, data);
  }
  void free_array()
  {
    printf("free_array: %p\n", this );
    free(data);
  }
  
};

typedef Array ListArray;


double sqrtscalare(double * a, double *b, int n);
double scalare(double * , double *, int);
double scalare( int , double * , double *);
void normalizza(double * , double , int);
void normalizzaauto(double * ,  int);
void somma      (double * ,    double * ,    double , int);
void diagonalizza4py(int k, int m,double *alpha,  double *beta, double *pevect, double *peval);



#define NOSPARSAMPI
#ifdef SPARSAMPI
#define MAXNSPARSA3AP 1000
#define MAXNVECTORP   10000
#define MPICH_IGNORE_CXX_SEEK    
#include<mpi.h>
class inizializzaMPI {
 public:
  inizializzaMPI();
  ~inizializzaMPI();
  void finalizza();

};


class VectorP {
 public:
  VectorP();
  ~VectorP();
  void caricaArray(int Nels,  double * coeffs);
  void riceviArray();
  void set_all_random(double value);

  void set_to_zero();
  void set_to_one();
  void add( double factor, VectorP * v   );
  void scale( double factor  );
  
  friend double scalarP(VectorP *a, VectorP *b);


  static void copy_to_a_from_b(VectorP *a, VectorP *b) ;
  void add_from_vect(VectorP * vect) ;

  void add_from_vect_with_fact( VectorP * vect, double fact);
  void mult_by_fact( double  fact);
  void dividebyarray(VectorP * a);
  void scriviArray();
  static void error( const char * msg ) {
    printf("%s\n",msg);
    exit(1);
  }




  int item_id;
  int proc_id;
  int n_procs;
  

  static VectorP** items;

  void crea();
  int dim;
  double *coeffs;
};


class VectorP2 {
 public:
  VectorP2(int dim_from, int * dominiofrom);
  ~VectorP2();
  void crea(int dim_from, int * dominiofrom);
  void caricaArray(int Nels,  double * coeffs);

  void set_all_random(double value);

  void set_to_zero();
  void set_to_one();
  //  void add( double factor, VectorP * v   );
  void add_from_vect_with_fact( VectorP2 * vect, double fact);
  static void copy_to_a_from_b(VectorP2 *a, VectorP2 *b) ;


  // void scale( double factor  );
  
  friend double scalarP2(VectorP2 *a, VectorP2 *b);
  friend void  scalarP2multi(int * ids, VectorP2 *b, int n , double *&risultato);


  void add_from_vect_with_fact_multi( int *ids, int n, double *fact);
  



  void add_from_vect(VectorP2 * vect) ;

  void mult_by_fact( double  fact);

  void dividebyarray(VectorP2 * a);
  void multbyarray  (VectorP2 * a);

  void scriviArray(char * nomefile, int start, int dim);


  static void error( const char * msg ) {
    printf("%s\n", msg);
    exit(1);
  }




  int item_id;
  int proc_id;
  int n_procs;
  

  static VectorP2** items;


  int *dims2;
  int * loc2glob;

  // -------------------------------------------------------
  static int **dims2static;
  static int ** loc2globstatic;
  static int nshapes;
  static int * dimsstatic;



  double *coeffs;


};






double scalarP(VectorP *a, VectorP *b);
double scalarP2(VectorP2 *a, VectorP2 *b);
void  scalarP2multi(int *ids, VectorP2 *b, int n , double *&risultato);


struct ArrayP {
  ArrayP(){};
  ArrayP( int size ) { 
    firstdimension=0;
    this->size=size;
    ownsdata=1;
    data2d=0;
    data=new VectorP; 
    double *dum= new double[size ]; 
    data->caricaArray(size, dum);
    delete dum;
  }

  ArrayP( int fd, int sd ) { 
    firstdimension=fd;
    this->size=sd;
    ownsdata=1;
    data2d = new VectorP*[fd];
    double *dum= new double[sd ]; 
    for(int i=0; i<fd; i++) {
      data2d[i]=new VectorP;
      data2d[i]->caricaArray(sd, dum);
    }
    delete dum; 
  }


  ~ArrayP() { 
    if( ownsdata) {
      if(firstdimension) {
	for(int i=0; i< firstdimension; i++) delete data2d[i];
      } else {
	delete data;
      }
    }   else
      std::cout << " cancellata una finestra su array \n";
  }


  ArrayP * get_array(int index) {
    if( index >=firstdimension || index<0) {
      throw std::out_of_range("index invalido in Array * get_array(int index)");
    } else {
      ArrayP *res;
      res=  new ArrayP;
      res->ownsdata=0;
      res->firstdimension=0;
      res->size=this->size;
      res->data=this->data2d[index];
      return res;
    }
    return 0;
  }
  // void get_numarrayview(int start, int  N,  double *&ArrayFLOAT) ;


  /// double get_value( int index ) const
  
  // void set_value( int index, double value )
  
#undef RAND_MAX
#define RAND_MAX        2147483647
  
  void set_all_random(  double value )
  {
    this->data->set_all_random(value);
  }


  void set_to_zero(  )
  {
    this->data->set_to_zero();
  }

  void set_to_one(  )
  {
    this->data->set_to_one();
  }
  
  static void copy_to_a_from_b(ArrayP &a, ArrayP&b) {
    VectorP::copy_to_a_from_b(a.data , b.data);
  }
  
  
  void copy_from_b( ArrayP&b) {
    VectorP::copy_to_a_from_b(this->data , b.data) ;
  }
  
  
  void add_from_vect( ArrayP &vect) {
    this->data->add_from_vect(vect.data);
  }

  
  void add_from_vect_with_fact( ArrayP & vect, double fact) {
    if( size != vect.size) {
      throw std::out_of_range("frames are not aligned");   
    }
    this->data->add_from_vect_with_fact(vect.data,  fact);

  }


  void mult_by_fact(  double fact){
    this->data->mult_by_fact(   fact);
  }
  
  
  /*   void  set_indices_inv(int N, int *indices, double *values) ; */
  /*   void  set_indices( int  N, int *indices, double *values) ; */
  

  void caricaArray(int Nels,  double * coeffs) {

    data->caricaArray( Nels,   coeffs) ; 

  }


  
  ArrayP* copy() const
  {
    ArrayP *b;
    if(firstdimension) {
      b=new ArrayP(firstdimension, size);
      for(int i=0; i<firstdimension; i++) {
	VectorP::copy_to_a_from_b( b->data2d[i], this->data2d[i] );
      }
    }    else {
      b=new ArrayP( size);
      VectorP::copy_to_a_from_b( b->data, this->data );
    }

    return b;
  }
  
 
/*   si fara in python usande addwithfactor */
/*   static void  mat_mult(Array & b, double * mat,Array & a) ; */
  

  int len() const { return size; }
  
  void  dividebyarray( ArrayP &b){
    this->data->dividebyarray(b.data);
  };
  static double scalare(ArrayP &a, ArrayP &b) {
    return scalarP( a.data, b.data);
  }
  static double sqrtscalare(ArrayP &a, ArrayP &b) {
    return sqrt(scalare(   a, b      ));
  }



  void normalizza(double norm){
    this->data->mult_by_fact(1.0/norm);
  };
  void normalizzaauto(){
    double norm = sqrt(scalarP(   this->data, this->data      ));
    this->normalizza(norm);
  };


  void scriviArray() {
    this->data->scriviArray();
  }

  static void  mat_mult(ArrayP & b, double * mat,ArrayP & a) {
    int m=a.firstdimension-1;
    int k=b.firstdimension;
    // int dim = a.size;
 #define MAT(i,j) mat[   (i)*m    +j]

    for(int i=0; i<k; i++) {
      ArrayP * B = b.get_array(i);
      B->set_to_zero();
      for(int j=0; j<m; j++) {
	ArrayP * A = a.get_array(j);
	B->add_from_vect_with_fact( *A, MAT(i,j));
      }
    }
#undef MAT
  }
  


  VectorP * data, **data2d;
  int firstdimension;
  int size;
  int ownsdata;
};


void settaMPI(int argc, char ** argv) ;

class Sparsa3AP {
 public:

  Sparsa3AP();
 
  ~Sparsa3AP();

  void caricaArrays(int Nels, int * from, int * to, double * coeffs);
  // ,int dim_from, int dim_to);
  void riceviArrays();



  void Moltiplica(VectorP *ris, VectorP *vect  );
  void Moltiplica(ArrayP *ris, ArrayP *vect  ) {
     Moltiplica(ris->data, vect->data  );

  };
  void trasforma(double fattore, double addendo) ;	




  int item_id;
  int proc_id;
  int n_procs;

  static Sparsa3AP** items;

  double goherschMin();
  double goherschMax();
 

  int dim;

 private:
  void crea();


  int dim_from;
  int dim_to;
  int *Nelspp;
  int **topp;
  int **frompp;
  double ** coeffspp;

  int *dims2;
  int ** loc2glob;

  int **others_loc2glob;
  int * others_dims2;

};




struct ArrayP2 {
  ArrayP2() {};
  ArrayP2( int dim_from) { 
    firstdimension=0;
    this->size=dim_from;
    ownsdata=1;
    data2d=0;
    //  data=new VectorP2(dim_from, dominiofrom )   ; 
  }

  void setta( int dim_from, int * dominiofrom ) {
    // printf( " SETTA \n");
    if(firstdimension==0) {
      data=new VectorP2(dim_from, dominiofrom )   ; 
    } else {
      for(int i=0; i<firstdimension; i++) {
	// printf("i = %d \n", i);
	data2d[i]=new VectorP2    ( dim_from, dominiofrom ) ;
      }
    }
  }

  ArrayP2( int fd, int sd   ) { 
    firstdimension=fd;
    this->size=sd;
    ownsdata=1;
    data2d = new VectorP2*[fd];
/*     for(int i=0; i<fd; i++) { */
/*       data2d[i]=new VectorP2    (sd , dominiofrom ) ; */
/*       } */
  }


  ~ArrayP2() { 
    printf(" distruggo Array2P\n");
    if( ownsdata) {
      if(firstdimension) {
	for(int i=0; i< firstdimension; i++) delete data2d[i];
	delete data2d;
      } else {
	delete data;
      }
    }   else
      std::cout << " cancellata una finestra su array \n";
  }
  

  ArrayP2 * get_array(int index) {
    if( index >=firstdimension || index<0) {
      throw std::out_of_range("index invalido in Array * get_array(int index)");
    } else {
      ArrayP2 *res;
      res=  new ArrayP2;
      res->ownsdata=0;
      res->firstdimension=0;
      res->size=this->size;
      res->data=this->data2d[index];
      return res;
    }
    return 0;
  }
  
#undef RAND_MAX
#define RAND_MAX        2147483647
  
  void set_all_random(  double value )
  {
    this->data->set_all_random(value);
  }


  void set_to_zero(  )
  {
    this->data->set_to_zero();
  }

  void set_to_one(  )
  {
    this->data->set_to_one();
  }
  
  static void copy_to_a_from_b(ArrayP2 &a, ArrayP2&b) {
    VectorP2::copy_to_a_from_b(a.data , b.data);
  }
  
  
  void copy_from_b( ArrayP2 &b) {
    VectorP2 ::copy_to_a_from_b(this->data , b.data) ;
  }
  
  
  void add_from_vect( ArrayP2  &vect) {
    this->data->add_from_vect(vect.data);
  }

  
  void add_from_vect_with_fact( ArrayP2 & vect, double fact) {
    if( size != vect.size) {
      throw std::out_of_range("frames are not aligned");   
    }
    this->data->add_from_vect_with_fact(vect.data,  fact);
  }



  void mult_by_fact(  double fact){
    this->data->mult_by_fact(   fact);
  }
  
  
  /*   void  set_indices_inv(int N, int *indices, double *values) ; */
  /*   void  set_indices( int  N, int *indices, double *values) ; */
  

  void caricaArray(int Nels,  double * coeffs) {

    data->caricaArray( Nels,   coeffs) ; 

  }


  
  ArrayP2* copy( int dim_from, int * dominiofrom) const
  {
    ArrayP2 *b;
    if(firstdimension) {
      b=new ArrayP2(firstdimension, size);
      b->setta(size,dominiofrom);


      for(int i=0; i<firstdimension; i++) {
	VectorP2::copy_to_a_from_b( b->data2d[i], this->data2d[i] );
      }
    }    else {
      b=new ArrayP2( size);
      b->setta(size,dominiofrom);
      VectorP2::copy_to_a_from_b( b->data, this->data );
    }

    return b;
  }
  
 
/*   si fara in python usande addwithfactor */
/*   static void  mat_mult(Array & b, double * mat,Array & a) ; */
  

  int len() const { return size; }
  
  void  dividebyarray( ArrayP2 &b){
    this->data->dividebyarray(b.data);
  };
  void  multbyarray( ArrayP2 &b){
    this->data->multbyarray(b.data);
  };
  static double scalare(ArrayP2 &a, ArrayP2 &b) {
    return scalarP2( a.data, b.data);
  }
  static void scalaremulti( ArrayP2 &a , ArrayP2 &b , int n , double *&risultato ) {

    // printf( " sono in  static void scalaremulti  n=%d \n", n);
    int ids[n];
    for(int i=0; i<n; i++) {
      ids[i] = a.data2d[i]->item_id       ;
      // printf("%d \n", ids[i]);
    }
    // printf (" chaimo scalarP2multi\n");
    scalarP2multi( ids, b.data, n , risultato);
    // printf (" chaimo scalarP2multi  OK \n");
    
  }

  void add_from_vect_with_fact_multi( ArrayP2 & vect, int n, double *fact) {
    if( size != vect.size) {
      throw std::out_of_range("frames are not aligned");   
    }
    int ids[n];
    for(int i=0; i<n; i++) {
      ids[i] = vect.data2d[i]->item_id       ;
    }
    this->data->add_from_vect_with_fact_multi(ids, n,  fact);
  }
  




  static double sqrtscalare(ArrayP2 &a, ArrayP2 &b) {
    return sqrt(scalare(   a, b      ));
  }



  void normalizza(double norm){
    this->data->mult_by_fact(1.0/norm);
  };
  void normalizzaauto(){
    double norm = sqrt(scalarP2(   this->data, this->data      ));
    this->normalizza(norm);
  };


  void scriviArray(char * nomefile, int start, int dim) {
    this->data->scriviArray( nomefile,  start, dim);
  }

  static void  mat_mult(ArrayP2 & b, double * mat,ArrayP2 & a) {
    int m=a.firstdimension-1;
    int k=b.firstdimension;
    // int dim = a.size;
 #define MAT(i,j) mat[   (i)*m    +j]

    for(int i=0; i<k; i++) {
      ArrayP2 * B = b.get_array(i);
      B->set_to_zero();
      for(int j=0; j<m; j++) {
	ArrayP2 * A = a.get_array(j);
	B->add_from_vect_with_fact( *A, MAT(i,j));
      }
    }
#undef MAT
  }
  

  VectorP2 * data, **data2d;
  int firstdimension;
  int size;
  int ownsdata;
};





class Trasmissione {
public:
  Trasmissione(int taglia, int proca, int procb);
  Trasmissione& operator= (const Trasmissione & tet);
  int operator== (const Trasmissione& tet) const;
  int operator< (const Trasmissione& tet) const;

  int proca;
  int procb;
  int taglia;
};

class Sparsa3AP2 {
 public:

  Sparsa3AP2();
 
  ~Sparsa3AP2();

  void caricaArrays(int Nels, int * from, int * to, double * coeffs, int Nf,  int * dominiofrom, int Nt, int * dominioto );
  // ,int dim_from, int dim_to); // usare nels neg per accumulare senza dominio, dominio  per concludere (Nf), questa
  // deve anche dispacciare. L' acc si inizia con dominio vuoto, si finisce con dominio
  void riceviArrays(); //  vale discorso qui sopra, deve accumulare, alla fine gli acc si rim a zero

  void riceviArraysLocal(int Nels, int * from, int * to, double * coeffs,int dim_from,
			 int * dominiofrom, int dim_to, int * dominioto ) ;
 


  void Moltiplica(VectorP2 *ris, VectorP2 *vect  );

  void MoltiplicaExp(VectorP2 *ris, VectorP2 *vect  );

  void MoltiplicaExpL(VectorP2 *ris, VectorP2 *vect  );




  void Moltiplica(ArrayP2 *ris, ArrayP2 *vect  ) {
     Moltiplica(ris->data, vect->data  );
  };

  void MoltiplicaExp(ArrayP2 *ris, ArrayP2 *vect  ) {
     MoltiplicaExp(ris->data, vect->data  );
  };

  void MoltiplicaExpL(ArrayP2 *ris, ArrayP2 *vect  ) {
     MoltiplicaExpL(ris->data, vect->data  );
  };


  void MoltiplicaMinus(VectorP2 *ris, VectorP2 *vect  );
  void MoltiplicaMinus(ArrayP2 *ris, ArrayP2 *vect  ) {
     MoltiplicaMinus(ris->data, vect->data  );
  };


  void trasforma(double fattore, double addendo) ;	



  void LLtSolve(VectorP2 *Ares,  VectorP2 *Avect  );
  void LLtSolve(ArrayP2 *ris, ArrayP2 *vect  ) {
     LLtSolve(ris->data, vect->data  );
  };
  void preparaLLt() ;
  void riceviOthersLLt(int &otherproc, int ** &otherlltn,int *** &otherlltrow,
		       double *** &otherlltcoeff )  ;
  void addnewelements(int startf, int endf,int newifrom, int ito,  double coefftarget,
		    int **otherlltn, int***otherlltrow, double ***otherlltcoeff,
		    int **lltn, int ***lltrow, double ***lltcoeff, int **lltsize,
				   int otherproc, int thisproc);

  void liberaOthersLLt(int &otherproc, int ** &otherlltn,int *** &otherlltrow,
		       double *** &otherlltcoeff );

  void trasmettiOthersLLt( ) ;

  void preparaPreco( ) ; 
  void preparaPreco_( ) ; 
  void preparaPrecoExp( ) ; 
  void preparaPrecoExp_( ) ; 

  void  Preco(VectorP2 *ris, VectorP2 *vect , double omega );
  void  Preco(ArrayP2 *ris,ArrayP2  *vect , double omega ){
    Preco(ris->data, vect->data , omega );
  };

  void expandi();
  void expandiMult(int a, int b);
  void expandimpi(int target=-1, int source=-1);
  void fissaexp();
  void expsettozero(int a) ;
  void expsettodiag(int a) ;
  void expsum(int a,int b,int c) ;
  void expsub(int a,int b,int c) ;
  void expmultbyfact(int a,int b,float c) ;
  void expgensum(int a,int b,int c, float B, float C) ;
  float scalareexp(int a, int b) ;
  void expPreco(int a, int b);
  void expcopy(int a, int b) ;



  int item_id;
  int proc_id;
  int n_procs;

  static Sparsa3AP2** items;


  int dim;

 private:
  void crea();


  int dim_from;
  int dim_to;
  int *Nelspp;
  int **topp;
  int **frompp;
  double ** coeffspp;


  int *exp_Nelspp;
  int **exp_topp;
  int **exp_frompp;
#define NEXPVECT 5
  float ** exp_coeffspp;
  int ** exp_loc2glob;
  int ** exp_others_loc2glob;
  int * exp_dims2;
  int * exp_others_dims2;
  int exp_maxdim2;


  int *dims2;
  int * dims;
  int ** loc2glob;

  int **others_loc2glob;
  int * others_dims2;

  double **others_buffer;
  double * bufferinvio;



  int **lltn ;
  int **lltsize ;
  int ***lltrow ;
  double ***lltcoeff;


  int **Tlltn ;
  int ***Tlltcol ;
  double ***Tlltcoeff;
  int **perto;

  int multdone;
  MPI_Request *reqs;
  int Nreqs;

  int * Ntorecv;
  double ** buffertorecv;
  int Nmax2trasm;

  int len2trasm;
  int len2recv;
  
  int * precoN;
  double * precodiag;
  int ** precoFrom;
  double ** precoCoeff;

  int * precoN_t;
  int ** precoFrom_t;
  double ** precoCoeff_t;


  int acc_Nels;
  int * acc_from;
  int * acc_to;
  double * acc_coeffs;


  std::map<Trasmissione,int> trasmissioni;
  std::map<Trasmissione,int> trasmissioniExp;

};

















#endif

#endif

