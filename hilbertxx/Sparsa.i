/************************************************************************

  Copyright
  Alessandro MIRONE
  mirone@esrf.fr

  Copyright 2002  by European Synchrotron Radiation Facility, Grenoble, 
                  France

                               ----------
 
                           All Rights Reserved
 
                               ----------

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the names of European Synchrotron
Radiation Facility or ESRF or BLISS not be used in advertising or 
publicity pertaining to distribution of the software without specific, 
written prior permission.

EUROPEAN SYNCHROTRON RADIATION FACILITY DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL EUROPEAN SYNCHROTRON
RADIATION FACILITY OR ESRF BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

**************************************************************************/
%module Sparsa
%{
#include<string.h>
#include<string.h>
#include<stdio.h>
#include <stdlib.h>
#include<iostream>
#include<math.h>
#include<complex.h>
#include"Sparsa.h"
%}



%{
#include <numpy/oldnumeric.h>
#include<numpy/arrayobject.h>
%}


%init %{	
  import_array();
%}	



%include typemaps.i

// THE FOLLOWING TYPEMAPS HAVE BEEN STOLEN FROM
// THE SWIG *.I FILES OF THE PLPLOT PROJECT

%{
   // global variables for consistency check on argument patterns
  static int Alen = 0;
//  static int Xlen = 0, Ylen = 0;
%}


%typemap(in) (int  N, double * ArrayFLOAT) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 1, 1);
  if(tmp == NULL) return NULL;
  $1 = Alen = tmp->dimensions[0];
  $2 = (double  *)tmp->data;
}
%typemap(freearg) (int  N, double * ArrayFLOAT) {Py_DECREF(tmp$argnum);}



%typemap(in) (int  N, int M,  double * ArrayFLOAT) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 2, 2);

  $1 = Alen = tmp->dimensions[0];
  $2 = Alen = tmp->dimensions[1];
  $3 = (double  *)tmp->data;

}
%typemap(freearg) (int  N, int M, double * ArrayFLOAT) {Py_DECREF(tmp$argnum);}









%typemap(in) (int  N, int * Array) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_INT, 1, 1);
  if(tmp == NULL) return NULL;
  $1 = Alen = tmp->dimensions[0];
  $2 = (int  *)tmp->data;
}
%typemap(freearg) (int  N, int  * Array) {Py_DECREF(tmp$argnum);}




%typemap(in) (double * ArrayFLOATnoN) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 1, 1);
  if(tmp == NULL) return NULL;
  Alen = tmp->dimensions[0];
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * ArrayFLOATnoN) {Py_DECREF(tmp$argnum);}




%typemap(in) (int * ArrayNoN) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_INT, 1, 1);
  if(tmp == NULL) return NULL;
  Alen = tmp->dimensions[0];
  $1 = (int  *)tmp->data;
}
%typemap(freearg) (int * ArrayNoN) {Py_DECREF(tmp$argnum);}









%typemap(in) (double * VectFLOATnoN) (PyArrayObject* tmp=NULL) {
  PyObject* array;
  array = PyObject_GetAttrString($input,"data");
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject(array, PyArray_DOUBLE, 1, 1);
  Py_DECREF(array);
  if(tmp == NULL) return NULL;
  Alen = tmp->dimensions[0];
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * VectFLOATnoN) {Py_DECREF(tmp$argnum);}



%typemap(in) (double * ArrayFLOAT2dNOCHECK) (PyArrayObject* tmp=NULL) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 2, 2);
  if(tmp == NULL) return NULL;
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * ArrayFLOAT2dNOCHECK) {Py_DECREF(tmp$argnum);}





%typemap(in) ( double * ArrayFLOATCHECK) (PyArrayObject* tmp=NULL ) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 1, 1);
  if(tmp == NULL) return NULL;
  if(tmp->dimensions[0] != Alen) {
    PyErr_SetString(PyExc_ValueError, "Vectors must be same length.");
    return NULL;
  }
  $1 = (double  *)tmp->data;
}
%typemap(freearg) (double * ArrayFLOATCHECK) {Py_DECREF(tmp$argnum);}





%typemap(in) ( int * ArrayCHECK) (PyArrayObject* tmp=NULL ) {
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_INT, 1, 1);
  if(tmp == NULL) return NULL;
  if(tmp->dimensions[0] != Alen) {
    PyErr_SetString(PyExc_ValueError, "Vectors must be same length.");
    return NULL;
  }
  $1 = (int  *)tmp->data;
}
%typemap(freearg) (int * ArrayCHECK) {Py_DECREF(tmp$argnum);}







%typemap(in, numinputs=1) ( int start, int  N,  double *&ArrayFLOAT) (double * tmp_vista) {




    {	
	if(!PyInt_Check(PyTuple_GetItem(	obj1,0))) return NULL;
	arg2 = (int) PyInt_AsLong( PyTuple_GetItem(	obj1,0) );
   
    }
    {
  	if(!PyInt_Check(PyTuple_GetItem(	obj1,1))) return NULL;
	arg3 = (int) PyInt_AsLong( PyTuple_GetItem(	obj1,1) );
    }


  $3 = &tmp_vista ;
} 
%typemap(argout, fragment="t_output_helper") ( int start, int  N,  double *&ArrayFLOAT  )  {
  int nd=1;
  int dims[1];
  PyObject *arr_a;
  dims[0]=$2;

  // arr_a = PyArray_FromDims(nd,dims,PyArray_DOUBLE) ;
  arr_a = PyArray_FromDimsAndData(nd,dims,PyArray_DOUBLE, (char * )  *$3 ) ;
	
  // memcpy( ( (PyArrayObject *) arr_a )->data  ,  *$3 , dims[0]*sizeof(double) );



  resultobj = t_output_helper(resultobj,arr_a );
} 





%typemap(in, numinputs=0) (int &Nels , int *&from, int *&to, double *&coeffs) (int *ia, int *ib, double * coeF, int N_tmp) {
  $2 = &ia;
  $3 = &ib;
  $4 = &coeF ;
  $1 = &N_tmp;
} 
%typemap(argout) ( int &Nels , int *&from, int *&to, double *&coeffs)  {
  int nd=1;
  int dims[1];
  PyObject *arr_a, *arr_b, *arr_c;
  dims[0]=N_tmp$argnum;

  arr_a = PyArray_FromDims(nd,dims,PyArray_INT) ;
  memcpy( ( (PyArrayObject *) arr_a )->data  ,  *$2 , dims[0]*sizeof(int) );
  arr_b = PyArray_FromDims(nd,dims,PyArray_INT) ;
  memcpy( ( (PyArrayObject *) arr_b )->data  ,  *$3 , dims[0]*sizeof(int) );
  arr_c = PyArray_FromDims(nd,dims,PyArray_DOUBLE) ;
  memcpy( ( (PyArrayObject *) arr_c )->data  ,  *$4 , dims[0]*sizeof(double) );


//   delete [] *$2;
//   delete [] *$3;
//   delete [] *$4;

  resultobj = t_output_helper(resultobj,arr_a );
  resultobj = t_output_helper(resultobj,arr_b );
  resultobj = t_output_helper(resultobj,arr_c );

//   Py_DECREF(arr_b);
//   Py_DECREF(arr_c);	


} 




%typemap(in, numinputs=0) (    double *&mat, int &ncF , int *&corrF,  int &ncT , int *&corrT) ( int ncT ,  int ncF , double *mat,  int *corrF,   int *corrT   ) {
	$1=&mat;	
	$2=&ncF;	
	$3=&corrF;	
	$4=&ncT;	
	$5=&corrT;	
} 	


%typemap(argout) (  double *&mat, int &ncF , int *&corrF,  int &ncT , int *&corrT)  {
  int nd=2;
  int dims[2];
  PyObject *arr_a, *arr_b, *arr_c;


  dims[0]= ncT$argnum;
  dims[1]= ncF$argnum;

  arr_c = PyArray_FromDims(nd,dims,PyArray_DOUBLE) ;
  memcpy( ( (PyArrayObject *) arr_c )->data  ,  *$1 , dims[0]*dims[1]*sizeof(double) );



  nd=1;
  dims[0]= ncF$argnum;


  arr_a = PyArray_FromDims(nd,dims,PyArray_INT) ;
  memcpy( ( (PyArrayObject *) arr_a )->data  ,  *$3 , dims[0]*sizeof(int) );


  dims[0]= ncT$argnum;

  arr_b = PyArray_FromDims(nd,dims,PyArray_INT) ;
  memcpy( ( (PyArrayObject *) arr_b )->data  ,  *$5 , dims[0]*sizeof(int) );

  delete [] *$1;
  delete [] *$3;
  delete [] *$5;

  resultobj = t_output_helper(resultobj,arr_c );
  resultobj = t_output_helper(resultobj,arr_a );
  resultobj = t_output_helper(resultobj,arr_b );

 //  Py_DECREF(arr_a);
//   Py_DECREF(arr_b);	


} 



class Sparsa3A {
public:
  Sparsa3A(int N,  double *ArrayFLOAT , int *ArrayCHECK /*from*/,    int *ArrayCHECK /*to*/ );
  void setpot( double *ArrayFLOATnoN ,double *ArrayFLOATnoN );
	   
  void add2template(int N,  double *ArrayFLOAT , int *ArrayCHECK ) ;
  // void add2template_forhops(c  *   coeff[i]  ,  q  ,  cc, self.facts_hop );  int * cc, double *hopfacts
 
  void add2template_forhops(int N,  double *ArrayFLOAT , int *ArrayCHECK,
	  int *ArrayNoN , double * ArrayFLOATnoN);


  void  get_3arrays(int &Nels , int *&from, int *&to, double *&coeffs);





  void extractM( double *&mat, int &ncF , int *&corrF,  int &ncT , int *&corrT,       int sF,int dF ,int sT , int dT ) ;


  void addSymm(int N, int M,double *ArrayFLOAT, int *ArrayNoN, int * ArrayNoN) ; 

  void addSymmI(int N, int M,double *ArrayFLOAT, int *ArrayNoN, int *ArrayNoN);

  void add( int N, int M,double *ArrayFLOAT, int *ArrayNoN , int *ArrayNoN);

  void adddiag( int  N, double * ArrayFLOAT , int *ArrayNoN, int *ArrayNoN);
  void merge( Sparsa3A &a )  ;





  void Moltiplica(Array * ris, Array*vect  );
	




  void MoltiplicaMinus(Array * ris, Array*vect  );
  void MoltiplicaDiag(Array * ris, Array*vect  );
  void MoltiplicaDiagMinus(Array * ris, Array*vect  );


  void riordina();	
  void preparaLLt(double tol);
  void LLtSolve(Array *ris, Array *vect  );




	  void debug_sufile(char * nome);
	  void readfile(char * nome) ;


  void   gohersch   ();
  double goherschMin();
  double goherschMax();
  void trasforma(double fattore, double addendo) ;	


  int dim;
  int dim2;



};


void diagonalizza4py(  int k , int N , double *ArrayFLOAT, double *ArrayFLOATCHECK, 
		double * ArrayFLOAT2dNOCHECK,double * ArrayFLOAT2dNOCHECK );


double scalare( int N , double *ArrayFLOAT  , double *ArrayFLOATCHECK);


%exception {
        try {
        $action
        }
        catch ( std::out_of_range &e ) {
           PyErr_SetString(PyExc_IndexError, e.what() );
           return NULL;
        }

}

%rename (__getitem__) get_array(int index);

struct Array {
    Array( int size ) ;
    Array( int fd, int sd ) ;	

    ~Array() ;
    Array * get_array(int index);

	

    double get_value( int index ) const;
    void set_to_zero(  )	;
    void set_to_one(  )	;
    void set_value( int index, double value );
    void set_values( int N, int *Array , double *ArrayFLOATnoN );
    void set_all_random(  double value );	

    Array* copy() const;

    Array& operator+=( const Array& a ) ;
    int len();

     static void copy_to_a_from_b(Array &a, Array&b);
     void copy_from_b( Array&b);


     void add_from_vect( Array&vect);		
     void add_from_vect_with_fact( Array&vect, double fact);
	
     void  set_indices_inv(int N, int *Array , double *ArrayFLOATnoN) ;
     void  set_indices( int  N, int *Array, double *ArrayFLOATnoN) ;
 
     static void mat_mult(Array & b, double * ArrayFLOAT2dNOCHECK ,Array & a);

     // crea una vista 
  void get_numarrayview(int start, int  N,  double *&ArrayFLOAT);	

  void  dividebyarray( Array &b);
  void  multbyarray( Array &b);

     

  double *dataAddress() ;
  void 	dumptofile(char * nome);
		  void readfile(char * nome);

  static double scalare(Array &a, Array &b) ;
  static double sqrtscalare(Array &a, Array &b);
  void normalizza(double norm);
  void normalizzaauto();
  void mult_by_fact(double fact);


};


typedef Array ListArray;


%exception ;



