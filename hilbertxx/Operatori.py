## /************************************************************************

##   Copyright
##   Alessandro MIRONE
##   mirone@esrf.fr

##   Copyright 2002  by European Synchrotron Radiation Facility, Grenoble, 
##                   France

##                                ----------
 
##                            All Rights Reserved
 
##                                ----------

## Permission to use, copy, modify, and distribute this software and its
## documentation for any purpose and without fee is hereby granted,
## provided that the above copyright notice appear in all copies and that
## both that copyright notice and this permission notice appear in
## supporting documentation, and that the names of European Synchrotron
## Radiation Facility or ESRF or SCISOFT not be used in advertising or 
## publicity pertaining to distribution of the software without specific, 
## written prior permission.

## EUROPEAN SYNCHROTRON RADIATION FACILITY DISCLAIMS ALL WARRANTIES WITH
## REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
## MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL EUROPEAN SYNCHROTRON
## RADIATION FACILITY OR ESRF BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
## CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
## DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
## TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
## PERFORMANCE OF THIS SOFTWARE.

## **************************************************************************/


import Hilbertxx
import math
from math import sqrt
import os

# indipendente
def counter(couches):
    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)
    fromsite=[]
    tosite =[]
    orbitals = []
    #  couches s 
    for couche in couches:
        __fromsites = [couche]
        __tosites   = [couche]
        L = coucheL(couche)
        if L>=0 :
            __orbitals   = [(one, ([mza,spina],),([mza,spina],) ) for spina in range(2) for mza in range(-L,L+1)
                       ]
        else:
            L=-L
            __orbitals   = [(one, ([mza,spina],),([mza,spina],) ) for spina in range(2) for mza in range(-L,L+2)    # per l' estra orbitale S
                       ]
            
        fromsite.append(__fromsites )
        tosite.append  (__tosites   )
        orbitals.append(__orbitals  )
        
    create_operator(result, fromsite, tosite, orbitals, memory_map)
    return result


def counterU(couche, stati = None):
    result =  Hilbertxx.Hxx_Normal_Operators_Collection(2,2)
    fromsite=[]
    tosite =[]
    orbitals = []


    __fromsites = [couche,couche ]    
    __tosites   = [couche,couche ]
    L = coucheL(couche)

    if stati is None:
        if L>=0 :
            __orbitals   = [(one, ([mza,spina],[mza,1-spina]),([mza,1-spina],[mza,spina]) ) for spina in range(2) for mza in range(-L,L+1)
                            ]
        else:
            L=-L
            __orbitals   = [(one, ([mza,spina],[mza,1-spina]),([mza,1-spina],[mza,spina]) ) for spina in range(2) for mza in range(-L,L+2)
                            ]
    else:
        __orbitals = []
        for stato in stati:
            
            if L>=0 :
                
                s__orbitals   = [(  (stato[L+mza  ]*stato[L+mzb ]* stato[L+mzc  ]*stato[L+mzd ]  ) , ([mza,spina],[mzb,1-spina]),([mzb,1-spina],[mza,spina]) ) for spina in range(2) for mza in range(-L,L+1)    for mzb in range(-L,L+1)     for mzc in range(-L,L+1)    for mzd in range(-L,L+1)              ]

                
            else:
                
                LL=-L
                s__orbitals   = [(  (stato[LL+mza  ]*stato[LL+mzb ]* stato[LL+mzc  ]*stato[LL+mzd ]) , ([mza,spina],[mzb,1-spina]),([mzc,1-spina],[mzd,spina]) ) for spina in range(2) for mza in range(-LL,LL+2)      for mzb in range(-LL,LL+2) for mzc in range(-LL,LL+2)      for mzd in range(-LL,LL+2) ]

                
            __orbitals.extend(s__orbitals)

            



        
    fromsite.append(__fromsites )
    tosite.append  (__tosites   )
    orbitals.append(__orbitals  )
    
    create_operator(result, fromsite, tosite, orbitals, memory_map)
    return result

    
# indipendente
def Coulomb_NoCI(  coucheA  , coucheB , multiK, direct=1):
    lA=coucheL(coucheA)
    lB=coucheL(coucheB)

    result =  Hilbertxx.Hxx_Normal_Operators_Collection(2,2)
    fromsite=[]
    tosite =[]
    orbitals = []

    __fromsites = [coucheB,coucheA]
    __tosites   = [coucheA,coucheB]
    if(direct):
        lim4A2 = lA
    else:
        __tosites.reverse()
        lim4A2 = lB

    if( coucheA==coucheB):
        factS=0.5
    else:
        factS=1.0
        
    __orbitals  = [ (factor2el,
                     ([ mzB1,spinB ],[ mzA1,spinA ]),
                     ([ mzA2,spinA ],[ mzA1+mzB1-mzA2,spinB ]),
                     multiK,factS
                     )
                    for mzA1 in range(-lA,lA+1)
                    for mzA2 in range(-lim4A2,lim4A2+1)
                    for spinA in range(2)
                    for spinB in range(2)
                    for mzB1 in range(-lB ,lB+1)
                    ]

    fromsite.append( __fromsites )
    tosite.append  ( __tosites   )
    orbitals.append( __orbitals  )
    create_operator(result, fromsite, tosite, orbitals, memory_map)
    return result

# indipendente        
def SpinOrbital(  couche):

    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)
    fromsite=[]
    tosite =[]
    orbitals = []
    L = coucheL(couche)
    
    __fromsite=[couche]
    __tosite =[couche]
    __orbitals=  [ (factorSO,
                    ([ mz,spin ],),
                    ([ mz-step,  spin + step ],),
                    L
                    )
                   for mz in range(-L,L+1)
                   for spin in range(2)
                   for step in [0,1-2*spin]
                   ]
    fromsite.append( __fromsite )
    tosite.append  ( __tosite )
    orbitals.append( __orbitals  )
        
    create_operator(result, fromsite, tosite, orbitals, memory_map)
    return result



# indipendente

def Multipole(  __coucheA  , __coucheB , multiK,Km ):

    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)
    fromsite=[]
    tosite =[]
    orbitals = []

    if(1):
        coucheA = __coucheA
        coucheB = __coucheB

        lA=coucheL(coucheA)
        lB=coucheL(coucheB)
        __fromsites = [coucheA]
        __tosites   = [coucheB]
        
        __orbitals  = [ (factorMultipole,
                         ([ mzA,spin ], ),
                         ([ mzA+Km,spin ],),
                         multiK
                         )
                        for mzA in range(  max(-lA, -lB-Km )  ,min(lA, lB-Km) +1)
                        for spin in range(2)
                        ]


        
        fromsite.append( __fromsites )
        tosite.append  ( __tosites   )
        orbitals.append( __orbitals  )

    create_operator(result, fromsite, tosite, orbitals, memory_map, symmetrize=1)
    return result


def MultipoleProj(  __coucheA  , __coucheB , multiK,Km , v, spinfixed):

    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)
    fromsite=[]
    tosite =[]
    orbitals = []

    if(1):
        coucheA = __coucheA
        coucheB = __coucheB

        lA=coucheL(coucheA)
        lB=coucheL(coucheB)
        __fromsites = [coucheA]
        __tosites   = [coucheB]
        
        __orbitals  = [ ((v[lB+mzA+Km])**2*factorMultipole((coucheA,),(coucheB,),
                                                           ( ( (mzA, spin) , )
                                                             ,((mzA+Km,spin),),multiK) ),
                         ([ mzA,spin ], ),
                         ([ mzA+Km,spin ],),
                         multiK
                         )
                        for mzA in range(  max(-lA, -lB-Km )  ,min(lA, lB-Km) +1)
                        for spin in [spinfixed]
                        ]
        
        fromsite.append( __fromsites )
        tosite.append  ( __tosites   )
        orbitals.append( __orbitals  )

    create_operator(result, fromsite, tosite, orbitals, memory_map, symmetrize=1)
    return result

def GenericTransition(  __coucheA  , __coucheB , Mfrom, Mto ):

    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)
    fromsite=[]
    tosite =[]
    orbitals = []

    if(1):
        coucheA = __coucheA
        coucheB = __coucheB

        lA=coucheL(coucheA)
        lB=coucheL(coucheB)
        __fromsites = [coucheA]
        __tosites   = [coucheB]
        
        __orbitals  = [ (  1.0 ,
                         ([ Mfrom,spin ], ),
                         ([ Mto,spin ],),
                        )
                        for spin in range(2)
                        ]
        
        fromsite.append( __fromsites )
        tosite.append  ( __tosites   )
        orbitals.append( __orbitals  )

    create_operator(result, fromsite, tosite, orbitals, memory_map)
    return result







# indipendente
        
def GenericLigandField(  couche, mat, symmetrize=0, tocouche=None):
    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)
    fromsite=[]
    tosite =[]
    orbitals = []
    L = coucheL(couche)
    __fromsite=[couche]
    if tocouche is None: tocouche=couche
    __tosite =[tocouche]
    Ltosite = coucheL(tocouche)

    if type(Ltosite)==type([]):
        listaLtosite=Ltosite
    elif Ltosite>=0:
        listaLtosite=range(-Ltosite,Ltosite+1)
    else:
        Ltosite=-Ltosite
        listaLtosite = range(-Ltosite,Ltosite+2)
        
    __orbitals=  [ (factorForGenericLigandField,
                        ([ mz1 ,spin ],),
                        ([ mz2 ,  spin  ],),
                        mat
                        )
                       for mz1 in range(-L,L+1)
                       for mz2 in listaLtosite
                       for spin in range(2)
                       ]

       
    fromsite.append( __fromsite )
    tosite.append  ( __tosite )
    orbitals.append( __orbitals  )

    create_operator(result, fromsite, tosite, orbitals, memory_map,symmetrize = symmetrize )
    return result


# indipendente

def Loperator(what, couches):
    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)
    fromsite=[]
    tosite =[]
    orbitals = []
    #  couches s
    foo,step = {"Zero":[Lz_value, 0],"Plus":[LPlus, 1],"Minus":[LMinus, -1] }[what]
    for couche in couches:
        L= coucheL(couche)
        __fromsites = [couche]
        __tosites   = [couche]
        L = coucheL(couche)
        __orbitals   = [(foo, ([mza,spin],),([mza+step,spin],),L ) for spin in range(2) for mza in range(max( -L, -L-step),min(L,L-step) + 1)
                       ]
        fromsite.append(__fromsites )
        tosite.append  (__tosites   )
        orbitals.append(__orbitals  )
        
    create_operator(result, fromsite, tosite, orbitals, memory_map)
    return result


# indipendente

def Soperator(what, couches):
    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)
    fromsite=[]
    tosite =[]
    orbitals = []
    foo,step = {"Zero":[spinvalue, 0],"Plus":[one, 1],"Minus":[one, -1] }[what]
    for couche in couches:
        L= coucheL(couche)
        __fromsites = [couche]
        __tosites   = [couche]
        L = coucheL(couche)
        __orbitals   = [(foo, ([mza,spin],),([mza,spin+step],) ) for spin in range(max(0,-step),min(1,1-step)+1)
                        for mza in range(-L, L+1 )
                       ]
        fromsite.append(__fromsites )
        tosite.append  (__tosites   )
        orbitals.append(__orbitals  )
        
    create_operator(result, fromsite, tosite, orbitals, memory_map)
    return result




#-----------------------------------------------------------------------------
# indipendente

def one(fromi, toi, orbi):
    return 1.0
# indipendente

def  spinvalue( (cA,  ) ,( cB,  ) ,    ( ( (mz1, spin1),  ) ,( (mz2,spin2), )  )  ):
    assert(cA==cB)
    assert(mz1==mz2)
    assert(spin1==spin2)
    return [-0.5,0.5][spin1]
# indipendente

def   LPlus( (cA,  ) ,( cB,  ) ,    ( ( (mz1, spin1),  ) ,( (mz2,spin2), ) ,L )  ):
    assert(cA==cB)
    assert(mz2==mz1+1)
    assert(spin2==spin1)
    return  sqrt( (L- mz1)*(L+ mz2)       )
# indipendente

def   LMinus( (cA,  ) ,( cB,  ) ,    ( ( (mz1, spin1),  ) ,( (mz2,spin2), ) ,L )  ):
    assert(cA==cB)
    assert(mz2==mz1-1)
    assert(spin2==spin1)
    return sqrt( (L+ mz1)*(L- mz2)       )

# indipendente
def  Lz_value( (cA,  ) ,( cB,  ) ,    ( ( (mz1, spin1),  ) ,( (mz2,spin2), ),L  )  ):
    assert(cA==cB)
    assert(mz1==mz2)
    assert(spin1==spin2)
    return mz1
##--------------------------------------------------------------------------------------------------

##def   spinvalue( *args ):
##    return spinvalue_(*args)
##    # return [-1,1][spina]
# indipendente

def    factorSO( (cA,  ) ,( cB,  ) ,    ( ( (mz1, spin1),  ) ,( (mz2,spin2),  ) , L)  ):
    step =  mz2-mz1
    if(step==0):
	 amplitude = - 0.5*mz1*(1-2*spin1)  ;
    elif(step==-1):
        amplitude =   math.sqrt( (L+mz1)*(L-mz2) )*0.5
    elif(step== 1):
        amplitude =   math.sqrt( (L-mz1)*(L+mz2) )*0.5
    return amplitude

# m2 +( (l+m)(l-m+1)+(l-m)(l+m+1)      )/2
# m2     (  2l2 -2m2 +2l )/2 = l2+l
# indipendente

def factorMultipole((cAf,),(cBt,),( ( (mzAf, spinAf) , ) ,((mzBt,spinBt),),multiK) ):
    lAf = coucheL(cAf)
    lBt = coucheL(cBt)
    assert(     abs(mzAf) <=   lAf   )
    assert(     abs(mzBt) <=   lBt   )
    assert(spinAf == spinBt)
    K=multiK
    KM=mzBt-mzAf
    if( abs(mzBt-mzAf)>K):
        amplitude=0
    else:
        if(mzBt%2):
            segno=-1
        else:
            segno=1
        amplitude = segno* (Hilbertxx.Wigner3J( 2*lBt , 2*K , 2*lAf ,
                                                    -2*mzBt             ,
                                                    2*KM                ,
                                                    2*mzAf
                                                    )*
                                Hilbertxx.Wigner3J( 2*lBt , 2*K , 2*lAf ,
                                                    0            ,
                                                    0                ,
                                                    0
                                                    )
                                )*math.sqrt( (2*lBt +1.)*(2*K +1.)*(2*lAf +1.)/(4*math.pi))
    return amplitude

# indipendente

def factor2el( (cAf  ,  cBf ) ,( cBt , cAt  ) ,    ( ( (mzAf, spinAf)  ,  (mzBf,spinBf) ) ,( (mzBt,spinBt) , (mzAt, spinAt) ) , multiK, factS)   ):
    lAf = coucheL(cAf)
    lBf = coucheL(cBf)
    lAt = coucheL(cAt)
    lBt = coucheL(cBt)
    assert(     mzAf+mzBf ==   mzAt+mzBt   )
    assert(spinAf == spinAt)
    assert(spinBf == spinBt)
    K=multiK
    if( abs(mzAt-mzAf)>K):
        amplitude=0
    else:
        amplitude = Couplage( lAt,lAf, K ,mzAt,mzAf )*Couplage(lBf,lBt, K ,mzBf, mzBt);
    return amplitude*factS

def Couplage(lt,lf, K ,mzt,mzf):
    if( mzt%2 ):
        segno=-1
    else:
        segno= 1
    resultW = (  Hilbertxx.Wigner3J(2*lt,2*K,2*lf, -2*mzt  ,2*(mzt-mzf), 2*mzf )* 
                 Hilbertxx.Wigner3J(2*lt,2*K,2*lf,  0     ,0        , 0    )*
                 segno *math.sqrt( (2*lt+1.)*(2*lf+1.)    )
               )    
    return resultW

# indipendente

def factorForGenericLigandField((cAf,),(cBt,),( ( (mzAf, spinAf) , ) ,((mzBt,spinBt),),mat) ):
    lAf = coucheL(cAf)
    lBt = coucheL(cBt)
    # assert(cAf==cBt)
    assert(     abs(mzAf) <=   lAf   )
    if type(mzBt)==type(""):
        toindex=mzBt
    
    elif lBt>=0:
        assert(     abs(mzBt) <=   lBt   )
        assert(len( mat)  == 2*lBt+1 )
        toindex =  lBt + mzBt 
    else:
        lBt=-lBt
        toindex =  lBt + mzBt 

    assert(spinAf == spinBt)
    

    amplitude = mat[  toindex   ][ lAf+mzAf  ]
    return amplitude

# indipendente

def matFactor((cAf,),(cBt,),( ( (mzAf, spinAf) , ) ,((mzBt,spinBt),),mat) ):
    
    lAf = coucheL(cAf)
    lBt = coucheL(cBt)
    assert(     abs(mzAf) <=   lAf   )
    assert(     abs(mzBt) <=   lBt   )
    assert(spinAf == spinBt)
    assert(len( mat)  == 2*lBt+1 )
    assert(len( mat[0])  == 2*lAf+1 )
    amplitude = mat[    lBt + mzBt  ][ lAf+mzAf  ]
    return amplitude


#------------------------------------------------------------------------------
#--------------------------------------------------------------------

# indipendente
def ScriviOperatore(baseFrom, Op , baseTo, nome , binary_output=0, Npezzi=None):
    if Npezzi is None:
        t_mat = Hilbertxx.Hxx_TransitionMatrix()
        t_mat.add_contribution (baseFrom, baseTo, Op , 1.0, 0)
        t_mat.Write_and_Clean(nome, binary_output)
    else:
        for i in range(Npezzi):
            t_mat = Hilbertxx.Hxx_TransitionMatrix()
            t_mat.add_contribution (baseFrom, baseTo, Op , 1.0, 0, i, Npezzi)
            t_mat.Write_and_Clean(nome+ "_"+str(i)+"_over_"+str(Npezzi), binary_output)
        

# indipendente
def create_operator(Op, fromsite, tosite, orbitals, memory_map, symmetrize=0):
    # print orbitals

    if len(fromsite)!=len(tosite) or len(fromsite)!=len(orbitals):
        raise "len(fromsite)!=len(tosite) or len(fromsite)!=len(orbitals) "
    for from_i, to_i , orbitalGroup in zip(fromsite, tosite, orbitals):
        for orb_i in orbitalGroup:
            from_op=[]
            to_op=[]

            if len(from_i)!=len(orb_i[1]) :
                raise " len(from_i)!=len(orb_i)"

            for elem_site_from, elem_orbital_from, \
                    in zip(from_i, orb_i[1]):
                from_op.append(memory_map(elem_site_from, elem_orbital_from))
                
            if len(to_i)!=len(orb_i[2]) :
                raise " len(from_i)!=len(orb_i)"
            
            for elem_site_to, elem_orbital_to, \
                    in zip(to_i, orb_i[2]):
                to_op.append(memory_map(elem_site_to, elem_orbital_to))



             
            if(symmetrize==0):
                if( hasattr(orb_i[0],"__call__")):
                    tamp = orb_i[0](from_i,to_i,orb_i[1:])
                    if tamp!=0:
                        Op.add_product(to_op, from_op, tamp)
                else:
                    tamp = orb_i[0]
                    if tamp!=0:
                        Op.add_product(to_op, from_op, tamp  )
            else:
                if( hasattr(orb_i[0],"__call__")):
                    tamp = orb_i[0](from_i,to_i,orb_i[1:])
                else:
                    tamp=orb_i[0]
                if tamp!=0:
                    Op.add_product(to_op, from_op  , tamp*abs(symmetrize))
                    Op.add_product(from_op , to_op , tamp*symmetrize )
    Op.reorder()
#--------------------------------------------------------------------
