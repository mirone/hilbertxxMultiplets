#!/usr/bin/env python

# try:
#     import PyQt4
#     import PyQt4.QtGui
#     def fdialog():
#         app =  PyQt4.QtGui.QApplication(sys.argv)
#         return PyQt4.QtGui.QFileDialog.getOpenFileName(None, 'Open file',filter="out36* cowan files (out36*)\nall files ( * )"   )
# except:
#     print "PROBLEM:"
#     print "could not launch fdialog because could not load PyQt4 module"


class dictiowithdefault( dict):
    def __getitem__(self, key):
        if self.has_key(key):
            return dict.__getitem__(self, key)
        else:
            return " You must still define this" 

import sys,re
RYD=13.6072
class Atom:
    """ Read the file to extract atomic values.
    the class can be called with the name of the file, by default it is "out36"
    the object Atom, has three dictionnaries:
    self.zetaDict["1s"]
    self.FDict["(1s,1s)0"]
    self.GDict["(2p,2p)2"]

    For FDict and GDict the integer after (1s,1s) is the value of k
    """

    def __init__(self,filename="out36"):
        self.filename=filename
        self.zeta=[]
        self.Fk=[]
        self.zetaDict= dictiowithdefault()
        self.FDict   = dictiowithdefault()
        self.GDict   = dictiowithdefault()

        self.readFile()
        self.parse()
        self.createDict()

    def readFile(self):
        try:
            f=open(self.filename,"r")
        except IOError:
            self.data=""
            return
        
            print "file %s does not exist", filename 
            sys.exit(1)

        self.data=f.readlines()

        f.close()

    def parse(self):
        startZeta = False
        startF = False
        for line in self.data:
            if line.find("-zeta-")!=-1:
                startZeta = True
            if line.find("fk")!=-1 and line.find("gk")!=-1:
                startF = True
            if startZeta:
                if re.search("^\s+\d[spdfgh]\s+",line)!=None:
                    self.zeta.append(line)
                if line.find("slater")!=-1:
                    startZeta=False
            if startF:
                if re.search("^\s+\(\s*\d[spdfgh],\s*\d[spdfgh]\)",line)!=None:
                    self.Fk.append(line)
                if line.find("nconf=")!=-1:
                    startF=False

    def createDict(self):

        for line in self.zeta:
            line=line.strip().split()
            self.zetaDict[line[0]]=RYD * float(line[2])

        for line in self.Fk:
            line=line.strip().split()
            keyF=''.join(line[0:4])
            keyG=''.join(line[0:3]+[line[10]])
            self.FDict[keyF]=RYD* float(line[4])
            self.GDict[keyG]=RYD* float(line[11])
                
if __name__ == '__main__':

    a = Atom()
#    a.readFile()
#    a.parse()

##    print "zeta",a.zeta
##    print "Fk",a.Fk

#    a.createDict()

    print "zeta", a.zetaDict
    print "FDict",a.FDict
    print "GDict",a.GDict


