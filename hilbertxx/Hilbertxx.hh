#define DONTUSESHARED


#ifdef USESHARED

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

#endif

#include<vector>
#include<string>



/************************************************************************

  Copyright
  Alessandro MIRONE
  mirone@esrf.fr

  Copyright 2002  by European Synchrotron Radiation Facility, Grenoble, 
                  France

                               ----------
 
                           All Rights Reserved
 
                               ----------

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the names of European Synchrotron
Radiation Facility or ESRF or SCISOFT not be used in advertising or 
publicity pertaining to distribution of the software without specific, 
written prior permission.

EUROPEAN SYNCHROTRON RADIATION FACILITY DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL EUROPEAN SYNCHROTRON
RADIATION FACILITY OR ESRF BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

**************************************************************************/

#ifndef __HILBERTXX__
#define __HILBERTXX__





// #define first_bit_mask 00000000000010000000000

#define LONG
static  unsigned LONG    first_bit_mask_ =  010000000000 ;
static  unsigned LONG   first_bit_mask  =  2* first_bit_mask_ ;


#define last_bit_mask  000000000001

class  Hxx_TypeForBits {
// Class to represent second quantization states.
// 32 bits ( on intel ) was  enough at the beginning.
// The second quatization states were represented  with longs :
//     #define Hxx_TypeForBits long
// Now  we use a custom class containing a
// list of longs. Their number is N_Hxx_TypeForBits :
// #define N_Hxx_TypeForBits 16
#define N_Hxx_TypeForBits 4


public:
  inline friend  Hxx_TypeForBits   operator >>(Hxx_TypeForBits  &a, int i   ); 
  inline friend  Hxx_TypeForBits   operator <<(Hxx_TypeForBits  &a, int i   ); 
  inline friend  Hxx_TypeForBits   operator & (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b); 
  inline friend  Hxx_TypeForBits   operator | (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b); 
  inline friend  Hxx_TypeForBits   operator ^ (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b); 
  inline friend  Hxx_TypeForBits   operator ~ (Hxx_TypeForBits  &a) ;

//   inline friend  int   operator == (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b); 
//   inline friend  int   operator != (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b); 
//   inline friend  int   operator < (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b); 

  int countOne();

  void return_string(char*) const;

  inline void setTo0() {
    for (int i=0; i< N_Hxx_TypeForBits ; i++) {
      x[i]=0;
    }
  };

  void print_values() ;

  inline void setToOne(){
    for (int i=0; i< N_Hxx_TypeForBits ; i++) {
      x[i]=0;
    }
    x[N_Hxx_TypeForBits-1]=last_bit_mask  ;
  } 
  
  inline void setToValue(int v) { x[N_Hxx_TypeForBits-1]=v; } ;

  inline int isZero()  {
    for (int i=0; i< N_Hxx_TypeForBits ; i++) {
      if(x[i]) return 0;
    }
    return 1;
  };

  int getfilledpos(int *postoconsider) ;


  inline unsigned LONG   lastBit()  {  return x[N_Hxx_TypeForBits-1]&last_bit_mask ;  } ;
  inline unsigned LONG   firstBit() { return x[0]&first_bit_mask ; } ;
  unsigned LONG    x[N_Hxx_TypeForBits];
  static unsigned *filtri;
  static unsigned  filter4Byte;
  static int * pos2consider; 

}  ;



// inline   int   operator == (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b) {
//   return Hxx_FermionicState::are_equal(a,b);
// }
// inline   int   operator >= (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b) {
//   return 1-Hxx_FermionicState::less(a,b);
// }
// inline   int   operator <= (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b) {
//   return 1-Hxx_FermionicState::less(b,a);
// }
// inline   int   operator >  (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b) {
//   return Hxx_FermionicState::less(b,a);
// }
// inline   int   operator <  (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b) {
//   return Hxx_FermionicState::less(a,b);
// }


inline Hxx_TypeForBits   operator >>(Hxx_TypeForBits  &a, int ishift   ) {
  Hxx_TypeForBits res = a;
  unsigned LONG    residual;
  unsigned LONG    new_residual;

  for(int ish =0; ish<ishift; ish++) {
    residual=0;

    for(int i=0; i< N_Hxx_TypeForBits ; i++) {

      new_residual = res.x[i] & last_bit_mask ;

      res.x[i] = res.x[i] >> 1;
      if(residual) {

	res.x[i] = res.x[i] |  first_bit_mask;
      }
      residual = new_residual;
    }
  }
  return res;
}


inline Hxx_TypeForBits   operator << (Hxx_TypeForBits  &a, int ishift   ) {

  Hxx_TypeForBits res = a;
  unsigned LONG    residual;
  unsigned LONG    new_residual;
  for(int ish =0; ish<ishift; ish++) {
    residual=0;

    for(int i= N_Hxx_TypeForBits-1; i>=0 ; i--) {
       
      new_residual = res.x[i] & first_bit_mask ;
      res.x[i] = res.x[i] << 1;
      if(residual) {
	res.x[i] = res.x[i] | last_bit_mask ;
      }
      residual = new_residual;
    }
  }
  return res;
}


inline   Hxx_TypeForBits   operator ~ (Hxx_TypeForBits  &a) {
  Hxx_TypeForBits res ;
  for(int i=0; i< N_Hxx_TypeForBits ; i++) {
    res.x[i]= ~  a.x[i];
  }
  return res;
} 



inline   Hxx_TypeForBits   operator & (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b) {
  Hxx_TypeForBits res ;
  for(int i=0; i< N_Hxx_TypeForBits ; i++) {
    res.x[i]= a.x[i] & b.x[i];
  }
  return res;
} 

inline   Hxx_TypeForBits   operator | (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b) {
  Hxx_TypeForBits res ;
  for(int i=0; i< N_Hxx_TypeForBits ; i++) {
    res.x[i]= a.x[i] |  b.x[i];
  }
  return res;
} 

inline   Hxx_TypeForBits   operator ^ (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b) {
  Hxx_TypeForBits res ;
  for(int i=0; i< N_Hxx_TypeForBits ; i++) {
    res.x[i]= a.x[i] ^  b.x[i];
  }
  return res;
} 


inline  int   operator == (const Hxx_TypeForBits  &a, const  Hxx_TypeForBits  &b) {
  for(int i=0; i< N_Hxx_TypeForBits ; i++) {
    if(a.x[i] !=  b.x[i]) return 0;
  }
  return 1;
} 


inline  int   operator != (const Hxx_TypeForBits  &a,const   Hxx_TypeForBits  &b) {
  for(int i=0; i< N_Hxx_TypeForBits ; i++) {
    if(a.x[i] !=  b.x[i]) return 1;
  }
  return 0;
} 



inline  int  Hxx_TypeForBits_compare(const Hxx_TypeForBits  &a, const  Hxx_TypeForBits  &b) {
  for(int i=0; i< N_Hxx_TypeForBits ; i++) {
    
    if(a.x[i] <  b.x[i])  return -1;
    else if(a.x[i] >  b.x[i]) return 1;
  }
  return 0;
} 

inline  int   operator < (const Hxx_TypeForBits  &a, const  Hxx_TypeForBits  &b) {
  for(int i=0; i< N_Hxx_TypeForBits ; i++) {
    
    if(a.x[i] <  b.x[i])  return 1;
    else if(a.x[i] >  b.x[i]) return 0;
  }
  return 0;
} 
inline  int   operator >= (const Hxx_TypeForBits  &a, const  Hxx_TypeForBits  &b) {
  for(int i=0; i< N_Hxx_TypeForBits ; i++) {
    
    if(a.x[i] >  b.x[i])  return 1;
    else if(a.x[i] <  b.x[i]) return 0;
  }
  return 1;
}
struct opFilter_struct {
  Hxx_TypeForBits mask;
  char operation;
  double factor;
  double offset;
}  ;

class Hxx_Filter {

public:
  void addCondition (Hxx_TypeForBits &mask, char operation, double factor, double offset=0);
  Hxx_Filter (int);
  ~Hxx_Filter ();
  int filtre(Hxx_TypeForBits &state);
  opFilter_struct opFilter; 
  double min;
  double max;


private:

  struct opFilter_struct * filtreOperation;
  int validate(double res);
  int nop;
};

class Hxx_Utilities {
public:
  static int * SCRAMBLE_FOR_BITS ;
  static int * create_scrambler(int nstates);

};
// that because gnu-cc does not not like type casting from void *
#define Hxx_Object char


#define Hxx_FLOAT double

class Hxx_Chunker;

/**
 * provides a chained list for memory buffers
 */
class Hxx_Chunck {
public:
  char * ptr;
  Hxx_Chunck  * next;
};



/**
 * @short[ Hxx_Chunker provides  memory ]
 * Allocation is faster if done by bunches. Hxx_Chunck have
 * the get_space() that return a pointer to a bloc of memory whose
 * size is given by the stride parameter.
 * Allocation is not done on a one by one basis, but allocating
 * when necessary a buffer having size stride*chunck_size bytes
 */

class Hxx_Chunker {
public:

  /** 
   * Create a new object and set the stride and chunk_size parameters 
   */
  Hxx_Chunker(int stride, int chunck_size ) ;

  /** 
   * Give a pointer to N=stride free bytes 
   */
  inline Hxx_Object  * get_space();  

  /** 
   * When the memory given by the object is no more necessary, it can be freed by this function 
   */ 
  void free_space();

  inline ~Hxx_Chunker() {
    this->free_space();
    delete this->actual_chunk; // newmod
  };


public:
  long int stride;
  long int chunck_size;
  long int chunck_pos ; 
  Hxx_Chunck * top_chunk;
  Hxx_Chunck * actual_chunk ; 
};


class Hxx_1p_FermionicOperator_;

/**
 * @short[ Objects of this class represent a second quantisation state ]
 * Objects of this class represent a second quantisation state
 * as a sequence of 1 and 0, like for example
 *     100101011
 * Which are the occupancies. Together with the occupancies
 * the signes to extract a given fermion are given as the integral modulus 2 
 * of the occupancies
 *     011100110
 */
class Hxx_FermionicState {
public:
  
  Hxx_FermionicState(){ coeff=1; fisso=0; diag_ene=0.0;};
  Hxx_FermionicState(Hxx_TypeForBits occupancies);
  void initialise(Hxx_TypeForBits occupancies);

  static  inline int  are_equal(Hxx_FermionicState *a, Hxx_FermionicState *b ) ;
  static  inline int  less(Hxx_FermionicState *a, Hxx_FermionicState *b  ) ;

  // inline friend  int   operator == (Hxx_FermionicState  &a,  Hxx_FermionicState  &b); 
  inline friend  int   operator >= (Hxx_FermionicState  &a,  Hxx_FermionicState  &b); 
  inline friend  int   operator <= (Hxx_FermionicState  &a,  Hxx_FermionicState  &b); 
  inline friend  int   operator >  (Hxx_FermionicState  &a,  Hxx_FermionicState  &b); 
  inline friend  int   operator <  (Hxx_FermionicState  &a,  Hxx_FermionicState  &b); 


  inline friend  Hxx_TypeForBits   operator | (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b); 
  inline friend  Hxx_TypeForBits   operator ^ (Hxx_TypeForBits  &a,  Hxx_TypeForBits  &b); 
  


  friend class Hxx_1p_FermionicOperator_;


  // just for testing, never use this function ......
  inline Hxx_TypeForBits& get_value() { return occupancies ; }
  void print_values(int mask=0, int offset=0, int stride=1);
  int n_particles();
  int n_particles(int no, int *ops);
  int tag;
  Hxx_TypeForBits occupancies;

  double coeff;

 

  // private:

  Hxx_TypeForBits signes;

  double diag_ene;
  int    fisso;
};

class Hxx_1p_FermionicOperator_  {
public:
    
  
  inline Hxx_1p_FermionicOperator_(int pos=0) { 
    this->initialize(pos); 
  };

  void initialize( int pos);
  
  inline void Creation( Hxx_FermionicState * original , Hxx_FermionicState * result,
			int & sign ) ;
  
  inline void Destruction( Hxx_FermionicState * original , Hxx_FermionicState * result,
			   int & sign ) ;

  inline Hxx_TypeForBits & get_1p_state() { return this->c_d_mask; }
  
private:
  
  inline void operation( Hxx_FermionicState * original , Hxx_FermionicState * result,
			 int & sign) ;
  Hxx_TypeForBits c_d_mask ;
  Hxx_TypeForBits sign_mask;
  
};


class class4_Hxx_1p_FermionicOperator_Initialiser {
public:
  Hxx_1p_FermionicOperator_ *operators;

   class4_Hxx_1p_FermionicOperator_Initialiser() {
    int nop = sizeof(Hxx_TypeForBits)*8;   
    operators  = new Hxx_1p_FermionicOperator_[nop];
    
    for(int i=0; i<nop; i++) {
      operators[i].initialize(i);
    }
  }
  ~class4_Hxx_1p_FermionicOperator_Initialiser () {
    delete [] operators;
  }
};

static class4_Hxx_1p_FermionicOperator_Initialiser   Hxx_1p_FermionicOperator_Initialiser;



class Hxx_1p_FermionicOperators  {  // usata da interfaccia python
public:  
  static Hxx_1p_FermionicOperator_ * operators ;
  static Hxx_1p_FermionicOperator_ * Initialise(){ 
    return Hxx_1p_FermionicOperator_Initialiser.operators; 
  };
};



/**
 * @short[ Hxx_N_counter counts occupancy ]
 * Initialisation is done with an array of ints that refers to  Hxx_1p_FermionicOperators::operators
 * and a lenght
 */

class Hxx_N_counter  {

public:

  inline Hxx_N_counter( int n, int *ops, int reference_n=0) {
    this->n = n ;
    this->reference_n=reference_n;
    this->ops = new int [n];
    memcpy(this->ops, ops, n *sizeof(int) );
  };
  inline ~Hxx_N_counter() {
    delete[] this->ops;
  }


  int N(Hxx_FermionicState* orig);


  int n;
  int *ops;
  int reference_n;
};


typedef int  (Hxx_Generic_Comparator) (Hxx_Object   *a, Hxx_Object *b, Hxx_Object * user_data );


class Hxx_Tree ;


void serial( void *ptr, size_t itemsize, int nitems, FILE *f, int direction   ) ;

class Hxx_Normal_Operators_Collection {
public:

  void serialize(FILE *f,
		 int direction,
		 void (*mem2ret )(int mempos, int &iy, int &ix,int &m,int &s)=NULL ,
		 int (* tellwhich)(int npos, int *yy, int *xx, int *norbs)=NULL,
		 int myid=0
		 );

  Hxx_Normal_Operators_Collection( FILE *f,
				   void (*mem2ret )(int mempos, int &iy, int &ix,int &m,int &s )=NULL ,
				   int (* tellwhich)(int npos, int *yy, int *xx, int *norbs)=NULL,
				   int myid=0,
				   int chunck_size = 10000
				   ) ;

  Hxx_Normal_Operators_Collection( int Ncreation, int Ndestruction,
				   int chunck_size = 10000);

  ~Hxx_Normal_Operators_Collection(   );

  void add_product(int * creators, int *destructors, Hxx_FLOAT coeff );



  void reorder();

#ifdef USESHARED  
  void reordered2shared(  int local_id ) ;
#endif
  
  inline int get_n_items() { return this->n_items; }
  
  Hxx_FLOAT operate( int i, Hxx_FermionicState* orig, Hxx_FermionicState* result, int & next);
  Hxx_FLOAT operate_CC( int i, Hxx_FermionicState* orig, Hxx_FermionicState* result, int & next);
  void print();
  int Ncreation    ;
  int Ndestruction ; 
  
  double overallcoefficient ;

  friend void commuta(    Hxx_Normal_Operators_Collection * A, Hxx_Normal_Operators_Collection * B          ,
			  int nopsres  , 	Hxx_Normal_Operators_Collection ** commres ) ;
  friend void  commutaRestrained(    Hxx_Normal_Operators_Collection * A, Hxx_Normal_Operators_Collection * B          ,
				     int nopsres  , 	Hxx_Normal_Operators_Collection ** commres,int  mpi_id, int mpi_numprocs ) ;

  friend void  commutaRestrainedArray(    Hxx_Normal_Operators_Collection * A, Hxx_Normal_Operators_Collection * B          ,
					  int nopsres  ,  Hxx_Normal_Operators_Collection ** commres,int  mpi_id, int mpi_numprocs,
					  int *procperorb);

  friend void Hxx_cc_scatters( Hxx_Normal_Operators_Collection * op_ptr,int dimstati,double *Md2c, double *Greens , int  *Cel  );

  friend void Hxx_cc_Hscatters( Hxx_Normal_Operators_Collection * opA, int dimstati, double *Mc2d, double *Greens, int  *Cel, 
				double fact , int twobodyalso=1 );


  friend void  interferenceFactors( int  Ndestruction ,int *Idest ,int Ncreation , int *Icrea  , Hxx_Normal_Operators_Collection * B   ,
			      int &NInterf, double &FacInterf ) ;
  
  long int estimated_size() {
    return  2*n_items*op_order*sizeof(int);
  }


  std::vector<int> diagposs;
private:

  char * get_item_space();
  void clean_space();
  static  int  less(int  *a, int  *b , int *user_data ) ;
  void add_memorized(int *newTuple, int count);
  void set_next_indexes();

  int user_data[2] ;  //  Ncreation, Ndestruction
  
  int n_items;

  Hxx_Tree  * orderer ;
  
  Hxx_Chunker * chunker;

  int *indexes;
  Hxx_FLOAT * coeffs;
  int *next_indexes;

  int size_of_teeth;
  int op_order;

  double average_depth;
  double average_depth2;
  int size_of_creators;
  int size_of_destructors; 

#ifdef USESHARED  
  int usa_shared;
#endif
};





#define BLACK 0
#define RED   1
class Hxx_Tree ;
// ---------------------------------------------------------------------
class Hxx_Tree_Node {
public:
  int color;
  Hxx_Tree_Node *left;
  Hxx_Tree_Node *right;
  Hxx_Tree_Node *previous;
  
  Hxx_Object * Leaf;
};

class Hxx_Tree : public Hxx_Tree_Node  {
public:

  Hxx_Tree(Hxx_Generic_Comparator * Less , Hxx_Object * user_data=NULL , int chunck_size=10000) ;
  
  Hxx_Tree_Node * TopNode;
  Hxx_Tree_Node * ActualNode;
  
  Hxx_Object *  Add_Leaf(Hxx_Object * leaf, int &depth);
  
  Hxx_Object * getLowest(int &depth);
  Hxx_Object * getLowest_and_remove(int &depth);
  Hxx_Object * Again_getLowest_and_remove(int &depth);

  void Free_Chunkes();

  Hxx_Tree_Node *grandparent(Hxx_Tree_Node *n)
  {
    if ((n != NULL) && (n->previous != NULL))
      return n->previous->previous;
    else
      return NULL;
  }
  
  Hxx_Tree_Node *uncle(Hxx_Tree_Node *n)
  {
    Hxx_Tree_Node *g = grandparent(n);
    if (g == NULL)
      return NULL; // No grandparent means no uncle
    if (n->previous == g->left)
      return g->right;
    else
      return g->left;
  }
  
  inline int get_n_items() { return n_items; }

  inline ~Hxx_Tree() {
    this->Free_Chunkes();
    delete chunker; // newmod
  };
  
  void insert_case1(Hxx_Tree_Node *n)
  {
    if (n->previous == NULL)
      n->color = BLACK;
    else
      this->insert_case2(n);
  }

  void insert_case2(Hxx_Tree_Node *n)
  {
    if (n->previous->color == BLACK)
      return; /* Tree is still valid */
    else
      this->insert_case3(n);
  }

  void insert_case3(Hxx_Tree_Node *n)
  {
 
    Hxx_Tree_Node *u = this->uncle(n), *g;
    
    if ((u != NULL) && (u->color == RED)) {
      n->previous->color = BLACK;
      u->color = BLACK;
      g = this->grandparent(n);
      g->color = RED;
      this->insert_case1(g);
    } else {
      // this->insert_case4(n);
      g = this->grandparent(n);
      if(g==NULL) {
      	// n->previous->color=BLACK;
      } else {
      	this->insert_case4(n);
      }
    }
  }
   /*
       * rotate_left can be the below because of already having *g =  grandparent(n) 
       *
       * struct node *saved_p=g->left, *saved_left_n=n->left;
       * g->left=n; 
       * n->left=saved_p;
       * saved_p->right=saved_left_n;
       * 
       * and modify the parent's nodes properly
       */
      

  void insert_case4(Hxx_Tree_Node *n)
  {

    Hxx_Tree_Node *g = this->grandparent(n);
    

    if ((n == n->previous->right) && (n->previous == g->left)) {
      // this->rotate_left(n->previous);
      
      Hxx_Tree_Node *saved_p=g->left, *saved_left_n=n->left;
      g->left=n; 
      n-> previous =   g; 

      n->left=saved_p;
      saved_p->previous = n;

      saved_p->right=saved_left_n;
      if(saved_left_n) 
	saved_left_n->previous = saved_p; 

      n = saved_p;
      
    } else if ((n == n->previous->left) && (n->previous == g->right)) {
      
      Hxx_Tree_Node *saved_p=g->right, *saved_right_n=n->right;
      g->right=n; 
      n-> previous =   g;  

      n->right=saved_p;
      saved_p-> previous = n;

      saved_p->left=saved_right_n;
      if(saved_right_n ) 
	saved_right_n->previous = saved_p ;   

      n = saved_p; 
    }

    this->insert_case5(n);
    
  }

    
  void insert_case5(Hxx_Tree_Node *n)
  {
    // printf(" in insert_case5 %p\n",n);
    // printf(" in insert_case5 %p\n",n->previous);
    // printf(" in insert_case5 %p\n",n->previous->previous);

    Hxx_Tree_Node *g = grandparent(n);
    n->previous->color = BLACK;
    g->color = RED;

    Hxx_Tree_Node *saved_p=g->previous ;
    if (n == n->previous->left) {

      Hxx_Tree_Node *gleft  = g->left ;
      g->left = gleft->right;
      if( g->left) 
	g->left->previous =  g ; 

      gleft->right = g;
      g->previous  = gleft;

      gleft-> previous = saved_p; 
      if(saved_p) {
	if (saved_p->right== g) {
	  saved_p->right = gleft;
	} else  {
	   saved_p->left = gleft;
	}
      }
    } else {
      Hxx_Tree_Node *gright  = g->right ;
      g->right = gright->left;
      if(g->right) 
	g->right->previous = g;

      gright->left = g;
      g->previous  = gright;

      gright-> previous = saved_p; 
      if(saved_p) {
	if (saved_p->left== g) {
	  saved_p->left = gright;
	} else  {
	  saved_p->right = gright;
	}
      }
    }
  }
  
private:

  Hxx_Generic_Comparator * Less   ;

  Hxx_Chunker * chunker;

  Hxx_Object * user_data;

  int n_items;
};


// ---------------------------------------------------------------------

int  riquadra( int *yy, int npos  , int py,  double &y ,int &miny, int &maxy )  ;

// -------------------------------------------------------------------------------------------
typedef int (CounterForHxxBasisFilter) (Hxx_FermionicState *);    
typedef int (*Callback) (Hxx_FermionicState *, void *);

class Hxx_basis {
public:
  Hxx_basis(int initialdimension=0, int chunck_size=10000);
  Hxx_basis(FILE *file, int chunck_size=10000);
  void serialize(FILE *file, int direction ) ;
    
  ~Hxx_basis();
  void add_state(Hxx_FermionicState * state);

  int operate_andputin_Tree( Hxx_Normal_Operators_Collection * collection , int Ncolls=1, Hxx_Filter* filtre=0, int my_id=0, int oneeveryn=1 ); 
  int operate_andputin_Tree_CC( Hxx_Normal_Operators_Collection * collection , int Ncolls=1, int CC=1, Hxx_Filter* filtre=0,int my_id=0, int oneeveryn=1 );

  void operate_andaddin_diagonal( Hxx_Normal_Operators_Collection * collection ,  double coeff, double *diagonal, int fissa);

  int put_in_Tree_with_fact(  Hxx_basis * source   , double fact );
  int put_in_Tree_with_fixedfact(  Hxx_basis * source   , double fact );
  int put_in_Tree_with_fact_d(  Hxx_basis * source   , double fact );
  int put_in_Tree_with_fact(  Hxx_basis * source, double fact   , Hxx_Filter* Hxfiltre     );
	
 
  int  put_in_Tree_with_fact_rangefiltered(  Hxx_basis * source   , double fact ,
					      void (*mem2ret )(int mempos, int &iy, int &ix, int &m, int &s ) ,
					     int (* accetta)(double y, double x, int dy, int dx) ,
					     int py, int px,
					     Hxx_TypeForBits & occu_ref) ;



  static int  put_in_fracTree_with_fact_rangefiltered(
					       Hxx_basis *target,
					       Hxx_basis * source   , double fact ,
					       void (*mem2ret )(int mempos, int &iy, int &ix , int &m, int &s) ,
					       int (* accetta)(int npos, int *yy, int *xx, int *norbs, int &which) ,
					       int py, int px,
					       Hxx_TypeForBits & occu_ref,
					       int filteronmyid=-1);
  

  static int   put_in_fracTree_with_fact_rangefiltered_from_basis(
								  Hxx_basis *target,
								  Hxx_basis * source   , double fact ,
								  void (*mem2ret )(int mempos, int &iy, int &ix,int &m,int &s) ,
								  int (* accetta)(int npos, int *yy, int *xx, int *norbs, int &which) ,
								  int py, int px,
								  Hxx_TypeForBits & occu_ref,
					       int filteronmyid=-1) ;
  



  int put_in_Tree_with_fact_from_basis(  Hxx_basis * source   , double fact , double tolerance=0);
  int put_in_Tree_with_fact_from_basis(  Hxx_basis * source   , double fact ,
					 void (*mem2ret )(int mempos, int &iy, int &ix,int &m,int &s ) ,
					 int (* accetta)(double y, double x, int dx, int dy) ,
					 double tolerance=0);
  int  put_in_Tree_with_fact_rangefiltered_from_basis(  Hxx_basis * source   , double fact ,
							void (*mem2ret )(int mempos, int &iy, int &ix,int &m,int &s) ,
							int (* accetta)(double y, double x, int dy, int dx),
							int py, int px,
							Hxx_TypeForBits & occu_ref);
  int put_in_Tree_with_fact_from_basis(  Hxx_FermionicState *source_basis  , int source_nstates,  double fact, double tolerance=0        );
  
  void reconstruct_basis();
  
#ifdef USESHARED
  void reconstructedbasis2shared(  int local_id ) ;
#endif

  void set_filter(Callback filtre, void *clientdata);

  void reconstruct_basis(CounterForHxxBasisFilter *);
  void reconstruct_basis(Hxx_Filter* Hxfiltre);	
  void construct_basis( Hxx_TypeForBits *strings, int n);
  


  inline double scalar_with_basis(Hxx_basis *b) {return scalar_between_basis(this,b);  };

  static double scalar_between_basis(Hxx_basis *a,Hxx_basis *b);

  inline int get_dimension() { return this->nstates; }
  inline int get_Ncreated() { return this->Ncreated; }

  int find_state(Hxx_FermionicState * state  );

  // inline Hxx_FermionicState *get_basis() { return this->basis;}
  inline Hxx_FermionicState *get_basis(int n=0) { return this->basis+n;}
  inline Hxx_FermionicState *get_state_permanent(int n=0) { 
    Hxx_FermionicState * res = new Hxx_FermionicState;
    memcpy(res,this->basis+n, sizeof(Hxx_FermionicState)); 
    return res;
  }

  void reconstruct_basis_on_target(Hxx_basis * target);
  void trim_basis(double tol);

public:
  int nstates;
  int Ncreated;
  Hxx_FermionicState *basis ;
  Callback  filtre;
  void *clientdata;
  Hxx_Tree  * orderer ;
  Hxx_Chunker * states_chunker;
  double average_depth;
  double average_depth2;

  int ninitial ;
  
#ifdef USESHARED  
  int usa_shared;
#endif
  
//   double tmp_sum;
//   double tmp_sum_s;
};

// -----------------------------------------------------------------------------------------

class Hxx_transition {
public:
  
  inline Hxx_transition(int from , int to, Hxx_FLOAT coeff) {
    this->initialise(from,to,coeff);
  }
  
  inline void initialise(int from , int to, Hxx_FLOAT coeff) {   
    this->from=from;
    this->to=to;
    this->coeff=coeff;
  }
  
  inline void add_coeff(Hxx_FLOAT coeff) {
    this->coeff +=coeff;
  }
  
  static  int less(Hxx_transition *a, Hxx_transition *b) ;

  inline int get_from() { return this->from;}
  inline int get_to() { return this->to;}
  inline Hxx_FLOAT  get_coeff() { return this->coeff;}
  

public:
  int from;
  int to;
  Hxx_FLOAT coeff;
};

inline int Hxx_transition::less(Hxx_transition *a, Hxx_transition *b) {
  if(a->from < b->from) {
    return 1;
  } 
  if(a->from > b->from) {
    return 0;
  } 
  
  if(a->to < b->to) {
    return 1;
  } 
  if(a->to > b->to) {
    return 0;
  } 
  return 0;
}

class Hxx_TransitionMatrix {
public:

  Hxx_TransitionMatrix();
  ~Hxx_TransitionMatrix();

  void add_contribution(Hxx_basis * a_basis, Hxx_basis *b_basis ,
			Hxx_Normal_Operators_Collection  * Norm_Op, Hxx_FLOAT coeff=1.0, int complete_basis=1,
			int ipezzo=0, int Npezzi=1);

  void add_contribution_debug(Hxx_basis * a_basis, Hxx_basis *b_basis ,
			Hxx_Normal_Operators_Collection  * Norm_Op, Hxx_FLOAT coeff=1.0, int complete_basis=1);

  void add_contribution(Hxx_basis * a_basis, Hxx_basis *b_basis ,
			int n_counters, Hxx_N_counter  ** diag_pro, Hxx_FLOAT  coeff);

  double    trace(Hxx_basis * a_basis, 
                  Hxx_Normal_Operators_Collection  * Norm_Op);

  void Write_and_Clean(char *nome_file,            int binary=1);
 
  void Write_and_Clean_with4C(char *nome_file, int * from_t, int * to_t, Hxx_FLOAT * coeff_t,int ntrans_t, int nadd,  int binary=1);
  void Write_and_Clean_with4C(char *nome_file, int ntrans_t,  Hxx_FLOAT * coeff_t,int * from_t, int * to_t, int nadd,  int binary=1);


  static void Read_arrays( char * nome_file, int &Nels , int *&from, int *&to, double *&coeffs, int binary) ;

     
  static void  Read_4arrays( char * nome_file, int &Nels , int *&from, 
						   int *&to, double *&coeffs,int * & qcol, int binary) ;



  void Read_add(char *nome_file, Hxx_FLOAT, int binary=1, int fixedcoeff=0);



  void  Read_add_forhops( char * nome_file, Hxx_FLOAT coeff, int binary,
			  int * cc, double *hopfacts) ;


  void BuildAux();
  void AddFrom(Hxx_TransitionMatrix * term, Hxx_FLOAT fact, int dosquare=0);


  void create_3Array_and_Clean(int *& from, int *& to, Hxx_FLOAT *& coeff, int &ntrans);

  void free_space();

  void treeCoeffsToZero();


private:

  Hxx_Tree *Transitions_Orderer;
  Hxx_Chunker * chunker;
  int n_basis_from;
  int n_basis_to;

  int   aux_total_transitions;
  int * aux_i_from;
  int * aux_i_to;
  Hxx_FLOAT * aux_coeff;
};


void  Write_with4C_F(char *nome_file, int ntrans_t, Hxx_FLOAT * coeff_t,int * from_t, int * to_t,    int nadd, char *nome_source ,  int binary);





// ############################################################################################
//                                      END OF INTERFACE
// ############################################################################################


inline Hxx_FermionicState::Hxx_FermionicState(Hxx_TypeForBits occupancies) {
  coeff=1;fisso=0; diag_ene=0.0;
  this->initialise(occupancies);
}

inline void Hxx_FermionicState::initialise(Hxx_TypeForBits occupancies) {
  this->occupancies=occupancies;
  this->signes.setTo0();
  int n = 8*sizeof(Hxx_TypeForBits);
  Hxx_TypeForBits x;

  x = this->occupancies;

  x = x>>1;

  for(int k=0; k<n;k++) {
    this->signes = this->signes ^ x;
    x = x>>1;
  }
  tag=0;

}



inline int  Hxx_FermionicState::are_equal(Hxx_FermionicState *a, Hxx_FermionicState *b ) {
  return a->occupancies == b->occupancies;
}

inline int  Hxx_FermionicState::less(Hxx_FermionicState *a, Hxx_FermionicState *b ) {
  return a->occupancies  < b->occupancies;
}


inline void Hxx_1p_FermionicOperator_::Creation( Hxx_FermionicState * original , Hxx_FermionicState * result,
						 int & sign ) {    
  sign = (  (original->occupancies        &  this->c_d_mask).isZero()) ? sign : 0  ;
  if(sign==0) return;
  this->operation(original,result,sign);
}

inline void Hxx_1p_FermionicOperator_::Destruction( Hxx_FermionicState * original , Hxx_FermionicState * result,
						    int & sign ) {    
  sign = (  (original->occupancies        &  this->c_d_mask).isZero()) ? 0 : sign  ;
  if(sign==0) return;
  this->operation(original,result,sign);
}


inline void Hxx_1p_FermionicOperator_::operation( Hxx_FermionicState * original , Hxx_FermionicState * result,
						  int & sign) {
  sign  = (  (original->signes        &  this->c_d_mask).isZero()) ? sign : -sign ;
  result->occupancies = original->occupancies  ^  (this->c_d_mask) ;
  result->signes      = original->signes       ^  (this->sign_mask) ;
}



void  interferenceFactors( int  Ndestruction ,int *Idest ,int Ncreation , int *Icrea  , Hxx_Normal_Operators_Collection * B   ,
			   int &NInterf, double &FacInterf ) ;

inline char *  Hxx_Chunker::get_space() {
  if(this->chunck_pos < this->chunck_size-1) {
    this->chunck_pos++;
    return   this->actual_chunk->ptr + (this->chunck_pos  -  1) * this->stride    ; 
  } else {
    this->actual_chunk->next  = new Hxx_Chunck;
    this->actual_chunk  =this->actual_chunk->next  ;
    this->chunck_pos=0;
    this->actual_chunk->next  = NULL ; 
    this->actual_chunk->ptr = new char [(this->chunck_size)*(this->stride)];
    return this->get_space();
  }
};  

// void Inizializza_Hilbertxx();
void   build_from_template( char ** nomi_adds, int nadds, double *coeffs_adds, int * &from_t, int * &to_t, double * &coeff_t, int &ntrans_t, int binary) ;
#undef LONG 
#endif



#ifdef USESHARED
class SharedMemory {
public:
  static char * pointer;
  static char * pointer_end;

  boost::interprocess::mapped_region * mappa ; 
  static size_t size;
 
  SharedMemory(int local_id, size_t size) ;
  ~SharedMemory();
};
#endif
