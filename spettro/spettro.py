import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as la

def set_diag(Hc, Diag):
    assert(Hc.shape[0]==Hc.shape[1])
    for i in range(Hc.shape[0]):
        trovato=0
        for j in  range(Hc.indptr[i],  Hc.indptr[i+1] ):
            if(Hc.indices[j]==i):
                # print i,j,Hc.indices[j]
                trovato+=1
                Hc.data[j] = Diag[i]
        if(trovato!=1):
            print i,j
        assert(trovato==1)
    assert(Hc.has_sorted_indices)


vs=np.fromstring(open("datas.dat","r").read(), dtype=np.complex128 )
Js=np.fromstring(open("froms.dat","r").read(), dtype=np.int32 )
Is=np.fromstring(open("tos.dat"  ,"r").read(), dtype=np.int32 )
diag=np.fromstring(open("diag.dat","r").read(), dtype=np.complex128 )
coeffs =np.fromstring(open("spettro_coeffs.dat","r").read(), dtype=np.complex128 )
enes = np.loadtxt("minene.txt")

import scipy.sparse as sp
H  = sp.coo.coo_matrix( (vs,  (  Is , Js  )) ) 
Hc = sp.csc.csc_matrix(H)
Hc.sort_indices()


Diag = Hc.diagonal()
c = np.zeros(Hc.shape[0])
c[0]=1
x=c*0

minene = enes[-1]

for ene in np.arange(-5.0,10.0,0.1):
    set_diag(Hc, Diag -(minene+ ene+0.1j)*diag )
    # x = la.spsolve(Hc,c)
    info=0
    x, info = la.bicgstab(Hc,c,x)
    # x, info = la.bicg(Hc,c)
    # x, info = la.lgmres(Hc,c)
    # x, info = la.qmr(Hc,c)
    d = Hc.dot(x)
    res= ((d-c)*np.conjugate(d-c)).sum()
    print ene, x[0].imag , (x.imag*coeffs.real).sum(),   res.real, info # , info

