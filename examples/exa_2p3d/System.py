import Hilbertxx
import Sparsa
# import numpy.oldnumeric as Numeric
import numpy

from numpy import array

import math
import time
import string
import math
import copy

import os
place=os.path.dirname(__file__)
print place
import sys

sys.path.append(place+"/../../python")

from matrixop import *
 
usecomplex = 1
 
def ReadS( name )  :
  t_base    = Hilbertxx.Hxx_TransitionMatrix()
  t_base.Read_add(name,1.0,0)
  ars_base=t_base   .create_3Array_and_Clean()
  S_base=Sparsa.Sparsa3A( ars_base[2], ars_base[0], ars_base[1] )
  return S_base

def SfromT( t_base )  :
  if t_base is None:
    return None
  ars_base=t_base   .create_3Array_and_Clean()
  S_base=Sparsa.Sparsa3A( ars_base[2], ars_base[0], ars_base[1] )
  return S_base


def   SalvaSparsa3A( name ,   S  ) :
  if S is not None:
    j,i,c = S.get_3arrays()
  else:
    i,j,c = numpy.array([],"i"), numpy.array([],"i"),numpy.array([],"d")
  res = numpy.zeros([j.shape[0]],dtype=[("i","i"),("j","i"),("c","d")])
  res["i"]=i
  res["j"]=j
  res["c"]=c
  numpy.save(name, res)



class System:
  def __init__(self , basenames, basepars , excinames, excipars,  calcnames, calcpars,   quadrunames, angularnames, counternames):
    self.basenames=basenames
    self.excinames=excinames
    self.calcnames=calcnames
    self.quadrunames = quadrunames
    self.angularnames  = angularnames
    self.counternames=counternames

    for nomelist, vallist in zip( [basenames, excinames, calcnames ], [ basepars, excipars, calcpars ] ):
      for name,par in zip(  nomelist, vallist ):
        setattr(self,name,par)


  def GetESLcounters(self, polarisation=[0.0,1.0,0.0], shift=None):

    polarisation = array(polarisation, "D")
    # polarisation=polarisation/math.sqrt(Numeric.sum(Numeric.conjugate(polarisation)*polarisation).real)
    
    H=self.GetH()

    if usecomplex:
      H = TrialMatrixC(*H )
    else:
      H=H[0]
    vals , vects = solveEigenSystem( H , self.nsearchedeigen, shift=shift)

    try:
      class4sparse = H.__class__
      class4vect   = class4sparse.getClass4Vect()
    except:
      class4vect   = H.getClass4Vect()
      
    dim = H.dim
    L2s=[]
    S2s=[]
    SLs=[]
    opslist=[]
    SzLzList = []


    ########
    ## here 
    ##  self.angularnames[:3] is  ['base_Sop_Minus', 'base_Sop_Zero', 'base_Sop_Plus']
    ##   self.angularnames[3:]    ['base_Lop_Minus', 'base_Lop_Zero', 'base_Lop_Plus']

    for  opsnames in [self.angularnames[:3], self.angularnames[3:]]      :
      ops = [ ReadS( self.case+"/" + name )      for name in opsnames       ]
      opslist.append(ops)

      ## The Hxx_TransitionMatrix
      ## is the wrapping of a C++ object
      ## which uses a binary tree to store
      ## each entry, so that entries
      ## having the same incexes couple (i,j)
      ## are cumulated on the same one
      tR=Hilbertxx.Hxx_TransitionMatrix()
      tI=None

      ### So we here create an aligned S (or Z ) operator (default points along Z when polarisation = [0,1,0] )
      for c,n in zip( polarisation, opsnames  ):
          cR=0
          cI=0
          if  not isinstance(c, (complex, numpy.complex128)) :
            cR=c
          else:
            cR=c.real
            cI=c.imag
          if( cR):
            print " aggiungo ", n , cR
            tR.Read_add( self.case+"/" + n, cR,0)
          if(cI):
            if(tI is None):
              tI=Hilbertxx.Hxx_TransitionMatrix()
            print " III aggiungo ", n , cI
            tI.Read_add( self.case+"/" + n, cI,0)

      ## SfromT
      ## transforms a Hxx_TransitionMatrix into a Sparse matrix
      sR=SfromT(tR)
      sI=SfromT(tI)

      ## cnt is a Composite sparse matrix
      ## it is made of a real part sR and imag sI
      cnt=TrialMatrixC(sR,sI)
      cresList=[]
      dum1 = class4vect(dim)
      for v in vects:
        dum1.set_to_zero()
        cnt.Moltiplica(dum1, v)
        ## res is the expectation value of cnt
        res=class4vect.scalare(v, dum1)
        cresList.append(res)

      ## this is what we are going to expose as a result
      ## a list of all the Sz (and thes Lz) expectancies for all the ground states
      SzLzList.append(cresList)
          
    
    ## the operators of S and L components have been stored in the opslist list
    ## We can combine them together to calculate S2 L2  and SL scalar products
    ## 
    ##  for S2 we combine opslist[0] with himself 'opslist[0] is made of 'base_Sop_Minus', 'base_Sop_Zero', 'base_Sop_Plus' )
    ##      L2                             ['base_Lop_Minus', 'base_Lop_Zero', 'base_Lop_Plus']
    ## and SL   the one with the others

    for l,(As,Bs) in zip(    [S2s, L2s, SLs ] ,           [ [opslist[0]]*2, [opslist[1]]*2,   [opslist[0], opslist[1]] ]):
      for v in vects:
        res=0.0
        dum2 = class4vect(dim)
        for (A,B), fact in zip( zip(As,Bs    )  , [0.5,1.0,0.5]):

          if(usecomplex):
            A=TrialMatrixC(A,None)
            B=TrialMatrixC(B,None)
          dum1.set_to_zero()
          dum2.set_to_zero()

          A.Moltiplica(dum1, v)
          B.Moltiplica(dum2, v)
          add=class4vect.scalare(dum1, dum2)
          res=res+add*fact
        l.append(res)
    cntlist=[]
    cntlist = [ ReadS( self.case+"/" + name )      for name in self.counternames      ]
    cresListS=[]
    for cnt in cntlist:
      if(usecomplex):
        cnt=TrialMatrixC(cnt,None)
      cresList=[]
      for v in vects:
        dum1.set_to_zero()
        cnt.Moltiplica(dum1, v)
        res=class4vect.scalare(v, dum1)
        cresList.append(res)
      cresListS.append(cresList)
 
    return {
      "Evals":vals,
      "S2s": S2s,
      "L2s": L2s,
      "SLs": SLs,
      "counters":cresListS,
      "SzLzList": SzLzList
    }

  def GetSpectrum(self, polarisation=None, saveon=None):
    return self.Get( polarisation=polarisation, composeonly=0,saveon=saveon )
  
  def GetH(self,):
    return self.Get( composeonly=1)
    
  def Get(self, composeonly=0, polarisation=None, saveon=None):
    calcpars=[  getattr(self,name) for name in  self.calcnames ]    
    basenames = self.basenames
    basecoeffs = [  getattr(self,name) for name in  basenames ]
    Riduci( basecoeffs, basenames , self.calcnames, calcpars )
    
    excinames = self.excinames
    excicoeffs = [  getattr(self,name) for name in  excinames ]
    Riduci(excicoeffs ,  excinames, self.calcnames, calcpars )

    if(composeonly):
      
      t_base    = Hilbertxx.Hxx_TransitionMatrix()
      t_baseI = Hilbertxx.Hxx_TransitionMatrix()
      self.compose( t_base, t_baseI,        "%s/" % self.case+"/", basecoeffs, basenames, "base"  , isexci=0 )
      ars_base=t_base   .create_3Array_and_Clean()

      if( usecomplex  ) :
        S_base=Sparsa.Sparsa3A( ars_base[2], ars_base[0], ars_base[1] )
      else:
        S_base=TrialMatrix( ars_base[2], ars_base[0], ars_base[1] )
      
      ars_base=t_baseI   .create_3Array_and_Clean()
      if( usecomplex  ) :
        S_baseI=Sparsa.Sparsa3A( ars_base[2], ars_base[0], ars_base[1] )
      else:
        S_baseI=TrialMatrix( ars_base[2], ars_base[0], ars_base[1] )
      return S_base, S_baseI
    
    result=self.spectrum("%s/"%self.case+"/", basecoeffs, basenames,
                         excicoeffs, excinames, polarisation=polarisation, saveon = saveon )
    return result
  
  
  def compose(self,t_base  , t_baseI,  repe, coeff, names, prefix, isexci=0, saveon=None )  :
    self.get_nomi_coeffs()
    for i in range(len(coeff) ):
      if( coeff[i] !=0.0) :
        if   not isinstance(coeff[i]  , (complex, numpy.complex128))  :
          t_base.Read_add( repe+names[i], coeff[i], 0)
          if saveon is not None:
            
            t_saveon = Hilbertxx.Hxx_TransitionMatrix()
            t_saveon.Read_add( repe+names[i], coeff[i], 0)

            ars_saveon=t_saveon   .create_3Array_and_Clean()
            S_saveon=Sparsa.Sparsa3A( ars_saveon[2], ars_saveon[0], ars_saveon[1] )
            SalvaSparsa3A(saveon+"_"+names[i]+".npy", S_saveon)

          
        else:
          if coeff[i].real:
            t_base.Read_add( repe+names[i], coeff[i].real, 0)
            if saveon is not None:
              t_saveon = Hilbertxx.Hxx_TransitionMatrix()
              t_saveon.Read_add( repe+names[i], coeff[i], 0)

              ars_saveon=t_saveon   .create_3Array_and_Clean()
              S_saveon=Sparsa.Sparsa3A( ars_saveon[2], ars_saveon[0], ars_saveon[1] )
              SalvaSparsa3A(saveon+"_"+names[i]+"_R.npy", S_saveon)
              
          if coeff[i].imag:
            t_baseI.Read_add( repe+names[i], coeff[i].imag, 0)
            if saveon is not None:
              t_saveon = Hilbertxx.Hxx_TransitionMatrix()            
              t_saveon.Read_add( repe+names[i], coeff[i].imag, 0)

              ars_saveon=t_saveon   .create_3Array_and_Clean()
              S_saveon=Sparsa.Sparsa3A( ars_saveon[2], ars_saveon[0], ars_saveon[1] )
              SalvaSparsa3A(saveon+"_"+names[i]+"_I.npy", S_saveon)

          

    countername = names[0]
    pos = string.find(countername,"_")
    countername=countername[:pos+1]+"counterDL"
    t_counter = Hilbertxx.Hxx_TransitionMatrix()
    t_counter.Read_add(   repe+ countername , 1.0, 0      )
    fc, tc, cc = t_counter.create_3Array_and_Clean()
    cc=(10.0-cc).astype("i")

    if  self.facts_hop is not None and  int( max(cc)) !=len(self.facts_hop)-1:
      print max(cc)
      
      raise " self.facts_hop non corrisponde in lunghezza al numero di stati di valenza "


    for n,c in zip(self.grapnomi+self.crystalnomi, self.grapamps+self.crystalamps):
      if n in self.grapnomi  and isexci :
        c=c*self.factorhopexci
      if( c.real !=0.0):
        print " AGGOINGO A PARTE REALE ",  repe+prefix+n, c.real
        if  self.facts_hop is not None :
          t_base.Read_add_forhops( repe+prefix+n, c.real, 0,cc, self.facts_hop  )
        else:
          t_base.Read_add( repe+prefix+n, c.real, 0 )
      if(c.imag!=0.0):
        print " AGGOINGO A PARTE IMMAGINARIA ",  repe+"anti"+prefix+n, c.imag
        if  self.facts_hop is not None :
          t_baseI.Read_add_forhops( repe+"anti"+prefix+n, c.imag, 0,cc, self.facts_hop )
        else:
          t_baseI.Read_add( repe+"anti"+prefix+n, c.imag, 0)

##     for n,c in zip(self.grapnomi+self.crystalnomi, self.grapamps+self.crystalamps):
##       if n in self.grapnomi  and isexci :
##         c=c*self.factorhopexci
      
##       if( c.real !=0.0):
##         print " AGGOINGO A PARTE REALE ",  repe+prefix+n, c.real
##         t_base.Read_add( repe+prefix+n, c.real, 0)
##       if(c.imag!=0.0):
##         print " AGGOINGO A PARTE IMMAGINARIA ",  repe+"anti"+prefix+n, c.imag
##         t_baseI.Read_add( repe+"anti"+prefix+n, c.imag, 0)

  
  def get_nomi_coeffs(self):
    from miscele import getCoefficients

    grapamps,self.grapdipolamps = getCoefficients(self.BONDS, self.Vs, self.Vp, self.ALPHAVSP, 1.0,1.0,  1.0, self.DREF)
    

    print grapamps
    print self.grapdipolamps
    
    self.grapnomi=[]
    self.grapamps=[]

    for i in range(5):
      for j in range(5):
        self.grapnomi.append(  "_mixer_"+str(-2+i)+"_"+str(-2+j)  )
        self.grapamps.append(grapamps[i][j])

    print self.grapnomi
    print self.grapamps

    from miscele import getCoefficientsCrystal
    crystalamps  = getCoefficientsCrystal(self.BONDS, self.VC0, self.VC1, self.ALPHAVC,  self.DREF)

    self.crystalnomi=[]
    self.crystalamps=[]
    for i in range(5):
      for j in range(i,5):
        self.crystalnomi.append(  "_CRYSTAL_1_%d_%d_"%(j,i) )
        self.crystalamps.append(crystalamps[j][i])
     
  def spectrum(self, repe, coeff,  names,coeff_exc, names_exc, polarisation=None, saveon = None):

    t_base = Hilbertxx.Hxx_TransitionMatrix()
    t_baseI = Hilbertxx.Hxx_TransitionMatrix()
    
    self.compose( t_base, t_baseI,  repe,  coeff, names , "base" , isexci=0)
        
    t_excited = Hilbertxx.Hxx_TransitionMatrix()
    t_excitedI = Hilbertxx.Hxx_TransitionMatrix()
    
    self.compose( t_excited, t_excitedI,  repe, coeff_exc, names_exc ,"exci", isexci=1, saveon=saveon) 


        
    ars_base=t_base.create_3Array_and_Clean()
    ars_ecci=t_excited.create_3Array_and_Clean()

    if( usecomplex ):
      S_base=Sparsa.Sparsa3A( ars_base[2], ars_base[0], ars_base[1] )
    else:
      S_base=TrialMatrix( ars_base[2], ars_base[0], ars_base[1] )

    if( usecomplex  ):
      S_ecci=Sparsa.Sparsa3A( ars_ecci[2], ars_ecci[0], ars_ecci[1] )
    else:
      S_ecci=TrialMatrix( ars_ecci[2], ars_ecci[0], ars_ecci[1] )


    ars_base=t_baseI.create_3Array_and_Clean()
    ars_ecci=t_excitedI.create_3Array_and_Clean()

    S_baseI=Sparsa.Sparsa3A( ars_base[2], ars_base[0], ars_base[1] )

    S_ecciI=Sparsa.Sparsa3A( ars_ecci[2], ars_ecci[0], ars_ecci[1] )


    operatorS=[]
      
    if polarisation is not None:
      
      t_dipR=Hilbertxx.Hxx_TransitionMatrix()
      t_dipI=None

      for opname, fact  in zip(self.quadrunames, polarisation):

        cR=0
        cI=0
        if  not isinstance(fact  , (complex, numpy.complex128)):
          cR=fact
        else:
          cR=fact.real
          cI=fact.imag
        if( cR):
          t_dipR.Read_add(repe+opname , cR,0)
        if( cI):
          if(t_dipI is None):
            t_dipI=Hilbertxx.Hxx_TransitionMatrix()
          t_dipI.Read_add(  repe+opname   , cI,0)

      sR=SfromT(t_dipR)
      sI=SfromT(t_dipI)
 
      operatorS.append(TrialMatrixC(sR,sI)  )
      
    else:
      print " LOOP su  ", self.quadrunames
      for opname  in self.quadrunames:
        t_dip = Hilbertxx.Hxx_TransitionMatrix() 
        t_dip.Read_add( repe+opname, 1.0, 0)
        ars = t_dip.create_3Array_and_Clean()
        if( usecomplex):
          operatorS.append(TrialMatrixC(Sparsa.Sparsa3A( ars[2], ars[0], ars[1] ), None)  )
        else:
          operatorS.append(Sparsa.Sparsa3A( ars[2], ars[0], ars[1] ) )
        
    allX=array([-100000.0, self.El2l3-0.2-self.shift*0 , self.El2l3+0.2-self.shift*0, 100000.0])
    allY=array([(self.all1),(self.all1),(self.all2),(self.all2) ])

    #####

    if usecomplex:
      S_base = TrialMatrixC( S_base, S_baseI)
      S_ecci = TrialMatrixC( S_ecci, S_ecciI)

    evals, evects = solveEigenSystem( S_base , self.nsearchedeigen)

    if saveon is not None:
      
      evals_name = saveon+"_evals.npy"
      numpy.save(evals_name, evals)

      vtow=[]
      for i in range(evals.shape[0]):
        VV = evects[i]
        n = VV.vr.len()
        VV.vr.get_numarrayview((0, n))
        print VV.vi.get_numarrayview((0, n))
        tok = [ VV.vr.get_numarrayview((0, n)),VV.vi.get_numarrayview((0, n))  ]
        vtow.append(tok)
      vtow = numpy.array(vtow)
      evects_name = saveon+"_evects.npy"
      numpy.save(evects_name, vtow)

      name = saveon+"_HbaseR.npy"
      SalvaSparsa3A( name ,   S_base.mR  ) 
      name = saveon+"_HbaseI.npy"
      SalvaSparsa3A( name ,   S_base.mI  ) 
    
      name = saveon+"_HecciR.npy"
      SalvaSparsa3A( name ,   S_ecci.mR  ) 
      name = saveon+"_HecciI.npy"
      SalvaSparsa3A( name ,   S_ecci.mI  ) 

      for i in range(len(  operatorS   )):
        name = saveon+"_operatorS_%d_R.npy"%i
        SalvaSparsa3A( name ,   operatorS[i].mR  ) 
        name = saveon+"_operatorS_%d_I.npy"%i
        SalvaSparsa3A( name ,   operatorS[i].mI  ) 
        

    data4spect=faispettri( evals, evects   , S_ecci , self.erange , operatorS, min(S_ecci.dim, self.NstepsTridiag))

    factors = getFactors( data4spect["enes"] , self.temp , self.tolefact ) 

    result=[0]*(len(operatorS)+1)
    energies=numpy.arange( self.dxleft ,  self.dxright, (-self.dxleft + self.dxright+0.0)/ self.npunti  )
    result[-1]=energies
    energies = energies + self.all1*1.0j * numpy.less(self.El2l3 , energies ) + self.all2*1.0j * numpy.less(energies ,  self.El2l3 )

    for pol in range(len(operatorS) ):
      result[pol]=0.0
      for i in range(len(factors)):
        result[pol]=result[pol] +  factors[i]*spectra(data4spect["alphas"][i][pol],
                                                      energies+evals[i])*data4spect["norms"][i][pol]

    print " spec calcolato "
    result[len(operatorS)][:]=result[len(operatorS)]+self.shift
    result = [result[len(operatorS)],]+result[:len(operatorS) ]

    return result
     

def    Riduci(coeffs ,  names, calcnames, calcpars ):
  for i in range(len(names)):
    name=names[i]
    reduction = searchReduction(name, calcnames, calcpars   )
    if( reduction!=1.0):
      coeffs[i]=coeffs[i]*reduction
    
def searchReduction(name, calcnames, calcpars ):
  if(name[-2:]=="F0"):
    return 1
  import re
  tags = { "couche\d_\d_[FG]":[6,9], "couche\d_[FG]":[6,7],"MixCouche\d_\d_K":[9,12]}
  namepar = None
  for tag in tags.keys():
    searchobject = re.search(tag,name)
    if searchobject is not None:
      range1,range2= tags[tag]
      postfix= searchobject.group(0)[range1:range2]
      namepar = "reduc_"+postfix
      break
  print " per ", name ," la riduzione est data dal parametro ", namepar
  if namepar is not None:
    try:
      pos = calcnames.index(namepar)
    except ValueError:
      pos=-1
    if pos != -1:
      print "       che e nella lista  = ", calcpars[pos]
      return calcpars[pos]
    else:
      print "       che non e nella lista  = "
      return 1
  return 1

  
def getFactors(enes, temperature, tole):
    ne = len(enes.keys())
    E = numpy.zeros(ne,"d")
    for i in range(ne):   E[i] = enes[i]
    factors = numpy.exp(-(E-E[0])/temperature)
    factors = factors/numpy.sum(factors)
    Sum=0
    imax=ne
    for i in range(ne):
        Sum=Sum+factors[i]
        if( factors[i] < tole ):
            imax=i
            break
    factors=factors[:imax]/Sum
    return factors
          

