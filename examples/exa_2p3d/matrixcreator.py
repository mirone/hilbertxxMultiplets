import Hilbertxx
import math
from math import sqrt
import os
import sys

import os
place=os.path.dirname(__file__)
sys.path.append(place+"/../../python")


from Operatori import *
import Operatori
# ----------------------------------------------------------------------
# termini intracouche


def WriteIntra(basis, prefix):
    for couche in [0,1,2]:
        L=coucheL(couche)
        # termini diretti
        for multiK in range(2*L,-2,-2):
            operatore = Coulomb_NoCI(  couche  , couche , multiK, direct=1)
            for base,nome in zip(basis, NOMIBASI):
                ScriviOperatore(base, operatore,base, prefix+nome+"_couche%d_F%d"%(couche,multiK) )
   
# termini intercouche
def WriteInter(basis, prefix):
    for coucheA in range(2):
        La=coucheL(coucheA)
        for coucheB in range(coucheA+1,2):
            Lb=coucheL(coucheB)
            # termini diretti
            for multiK in range( 0,2*min(La,Lb)+1,2 ):
                operatore = Coulomb_NoCI(  coucheA  , coucheB , multiK, direct=1)
                for base,nome in zip(basis, NOMIBASI):
                    ScriviOperatore(base, operatore,base,  prefix+nome+"_couche%d_%d_F%d"%(coucheA, coucheB, multiK) )
            #termini di scambio
            for multiK in range(La+Lb,0,-2 ):
                operatore = Coulomb_NoCI(  coucheA  , coucheB , multiK, direct=0)
                for base,nome in zip(basis, NOMIBASI):                
                    ScriviOperatore(base, operatore,base, prefix+nome+"_couche%d_%d_G%d"%(coucheA, coucheB, multiK) )
            


#termini So
def WriteSo(basis, prefix):
    for couche in [0,1]:
        operatore = SpinOrbital(  couche)
        for base,nome in zip(basis, NOMIBASI):                
            ScriviOperatore(base, operatore,base, prefix+nome+"_SO_%d"% couche )


# quanti els nel guscio p
def WriteCounterP(basis, prefix):
    operatore = counter([2])
    for base,nome in zip(basis, NOMIBASI):                
        ScriviOperatore(base, operatore,base, prefix+nome+"_counterDL" )

# operatore mixer
def WriteMixerParametrized(basis, prefix):
##     for postfix, funzione in zip( ["_mixerEG","_mixerTG"],[createMixerEG, createMixerTG ]  ) :
##         operatore = funzione()
    for Dmz in range(-2,2+1):
        for Pmz in range(-2,2+1):
            for symm in [-1,1]:
                operatore = createMixerParametrized(Dmz, Pmz, symm)
                for base,nome in zip(basis, NOMIBASI):
                    
                    postfix= "_mixer_"+str(Pmz)+"_"+str(Dmz)
                    symmprefix={-1:"anti", 1:""}[symm]
                    ScriviOperatore(base, operatore,base, prefix+symmprefix+nome+postfix)




# termini di operatore multipolare

#OK I
def WriteQuadrupoles(base, exci, prefix):
    coucheA= 0
    for multiK in [1]:
        nome, coucheB={2:("quadrupole", 1  ),1:("dipole", 1  ) }[multiK]
        
        for Km  in range(-multiK, multiK+1):
            operatore = Multipole(  coucheA  , coucheB , multiK,Km )
            ScriviOperatore(base, operatore,exci, prefix+nome+"_couches_%d_%d_Km_%d"%(coucheA, coucheB, Km) )
    if(0):
     for mz in [-2,-1,0,1,2]:
        nome="mixedDipole"
        operatore = createMixedDipole(mz) 
        ScriviOperatore(base, operatore,exci, prefix+nome+"_couches_%d_%d_mz_%d"%(0, 2, mz) )

            
# termini di ligand field
## def WriteLigands(basis, prefix):

##     Z2_mat   =   [ [0]*5,[0]*5,[0,0,1.0,0,0],[0]*5,[0]*5,]

##     X2Y2_mat = 	 [ [0.5 ,0, 0. ,0, 0.5],  [0]*5,  [0]*5,   [0]*5,   [ 0.5 ,0, 0. ,0, 0.5]    ]

##     XY_mat   = 	 [ [0.5 ,0, 0. ,0,-0.5],  [0]*5,  [0]*5,   [0]*5,   [-0.5 ,0, 0. ,0,0.5]    ]
    
##     XYZ_mat  =   [ [0]*5,[0,1, 0,0,0],  [0]*5, [0,0,0, 1.0,0],  [0]*5,]    

##     toconsider = [XY_mat,Z2_mat, X2Y2_mat, XYZ_mat ]
##     names      = ["XY_mat","Z2_mat", "X2Y2_mat",  "XYZ_mat" ]
##     coucheA=1
##     for base,nome in zip(basis, NOMIBASI):                
##         for mat, matname in zip(toconsider, names ):
##             operatore = GenericLigandField(  coucheA  ,  mat )
##             ScriviOperatore(base, operatore,base, prefix+nome+"_ligand_%d_"%(coucheA,)+matname )


# termini di ligand field
def WriteLigands(basis, prefix):
    for symm,symmprefix in zip( [0.5,-0.5], ["","anti"]):
        for j in range(5):
            for  i in range(j,5):
                mat   =   [ [0]*5,[0]*5,[0,0,0.0,0,0],[0]*5,[0]*5,]
                mat[i][j]=1.0
                coucheA=1
                for base,nome in zip(basis, NOMIBASI):
                    if ( i==j):
                        fatt=1.0
                    else:
                        fatt=2.0
                    operatore = GenericLigandField(  coucheA  ,  mat, symmetrize =  symm*fatt )
                    # nota bene : si va da j a i  ( vedi Operatori.py per conferma )
                    ScriviOperatore(base, operatore,base, prefix+symmprefix+nome+"_CRYSTAL"+"_%d_%d_%d_"%(coucheA,i,j) )
  


# operatori per il momento angolare
def WriteAngulars(basis, prefix):
    for nome, func in zip(["_Lop_", "_Sop_"],[Loperator, Soperator] ):
        for what in ["Zero","Plus","Minus"]:
            operatore = func(what,[1])
            for base,nomebase in zip(basis, NOMIBASI):                
                ScriviOperatore(base, operatore,base, prefix+nomebase+nome+what )

# DIPENDENTISSMI

# OK I
def coucheL(couche):
    return [1,2,2][couche]

# OK I
def memory_map(couche, ( mz, spin) ):
    # s ,  d, p
    l= coucheL(couche)
    #    mz va da -l a l, spin puo essere 0 (giu) 1 (su)
    return [0,6,16][couche]+ 2*(l+mz) +spin 


Operatori.coucheL=coucheL
Operatori.memory_map=memory_map



# OK I
def createWanderer():
    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)

    fromsite=[]
    tosite =[]
    orbitals = []
    # vagabondiamo nelle couches   s, d , dl
    for couche in [0,1,2]:
        __fromsites = [couche]
        __tosites   = [couche]
        L = coucheL(couche)
        __orbitals   = [(one, ([mza,spina],),([mzb,spinb],) ) for spina in range(2) for mza in range(-L,L+1)
                        for spinb in range(2) for mzb in range(-L,L+1) ]
        fromsite.append(__fromsites )
        tosite.append  (__tosites   )
        orbitals.append(__orbitals  )
    create_operator(result, fromsite, tosite, orbitals, memory_map)        
    return result

# OK I



def createMixerParametrized(Dfrom, Pto, symm):
    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)

    fromsite=[]
    tosite =[]
    orbitals = []
    
    # togliamo un elettrone da d per metterlo in dl
    __fromsites = [1]
    __tosites   = [2]
    Lf = 2
    Lt = 2
    __orbitals   = [(1.0, ([mf,sf],),([mt,sf],),  ) for mf in [Dfrom] for mt in [Pto]
                    for sf in range(2) ]

    fromsite.append(__fromsites )
    tosite.append  (__tosites   )
    orbitals.append(__orbitals  )
    
    create_operator(result, fromsite, tosite, orbitals, memory_map, symmetrize=symm)        
    return result




def createMixerEG():
    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)

    fromsite=[]
    tosite =[]
    orbitals = []
    
    # togliamo un elettrone da dl per metterlo in d
    __fromsites = [2]
    __tosites   = [1]
    Lf = 2
    Lt = 2
    mat=[
        [  1./2 ,         0          ,   0    ,   0    ,  1./2 ],
        [    0  ,         0          ,   0    ,   0    ,   0   ],
        [    0  ,         0          ,   1.0  ,   0    ,   0   ],
        [    0  ,         0          ,   0    ,   0    ,   0   ],
        [  1./2 ,         0          ,   0    ,   0    ,  1./2 ],
        ]
    __orbitals   = [(matFactor, ([mf,sf],),([mt,sf],), mat ) for mf in range(-Lf, Lf+1) for mt in range(-Lt, Lt+1)
                    for sf in range(2) ]

    
    fromsite.append(__fromsites )
    tosite.append  (__tosites   )
    orbitals.append(__orbitals  )
    
    create_operator(result, fromsite, tosite, orbitals, memory_map, symmetrize=1)        
    return result



def createMixedDipole(mz):
    print " questo non lo voglio "
    raise " OK "
    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)

    fromsite=[]
    tosite =[]
    orbitals = []
    
    # togliamo un elettrone da dl per metterlo in d
    __fromsites = [0]
    __tosites   = [2]
    Lf = 0
    Lt = 2
    mat=[
        [ 0 ],
        [    0   ],
        [    0    ],
        [    0   ],
        [  0],
        ]
    mat[2+mz][0]=1
    
    __orbitals   = [(matFactor, ([mf,sf],),([mt,sf],), mat ) for mf in range(-Lf, Lf+1) for mt in range(-Lt, Lt+1)
                    for sf in range(2) ]

    
    fromsite.append(__fromsites )
    tosite.append  (__tosites   )
    orbitals.append(__orbitals  )
    
    create_operator(result, fromsite, tosite, orbitals, memory_map)        
    return result

# OK I
def createMixerTG():
    result =  Hilbertxx.Hxx_Normal_Operators_Collection(1,1)

    fromsite=[]
    tosite =[]
    orbitals = []
    
    # togliamo un elettrone da dl per metterlo in d
    __fromsites = [2]
    __tosites   = [1]
    Lf = 2
    Lt = 2
    mat=[
        [  1./2 ,         0          ,   0    ,   0    ,  -1./2 ],
        [    0  ,         1.0        ,   0    ,   0    ,   0   ],
        [    0  ,         0          ,    .0  ,   0    ,   0   ],
        [    0  ,         0          ,   0    ,   1.0  ,   0   ],
        [  -1./2 ,         0          ,   0    ,   0    ,  1./2 ],
        ]
    __orbitals   = [(matFactor, ([mf,sf],),([mt,sf],), mat ) for mf in range(-Lf, Lf+1) for mt in range(-Lt, Lt+1)
                    for sf in range(2) ]

    fromsite.append(__fromsites )
    tosite.append  (__tosites   )
    orbitals.append(__orbitals  )
    
    create_operator(result, fromsite, tosite, orbitals, memory_map, symmetrize=1)        
    return result


#------------------------------------------------------------------------


    
NOMIBASI=["base","exci"]



def scriviFiles(nel3d, prefix="", nhopped=0, maxnelonL=None, BAVARDAGE=0):
## Create initial state

    if( not os.path.isdir(prefix) ):
        print " PROVIDE A DIRECTORY NAME WHERE YOU WRITE MATRICES!! "
        print " THIS DIRECTORY MUST EXISTS!! "
        return
    try:
        f=open(prefix+"/deldjfhwkeqos.del","w")
    except:
        print " you cannot write to directory  ", prefix
        return
        
    base_seed = Hilbertxx.Hxx_TypeForBits()
    base_seed.setTo0()
    
    operators=Hilbertxx.Hxx_1p_FermionicOperators.Initialise()

    # def memory_map(couche, ( mz, spin) ):

    ### verificazione dell ordinamento bits in stampa
    if BAVARDAGE:
        bavardage = open("tell.txt","w")
        bavardage.write("###   ONE PARTICLE STATES\n" )
        couche = 0
        for mz in range(-1,1+1):
            for spin in range(0,1+1):
                i = memory_map(couche, (mz,spin))
                test_seed = Hilbertxx.Hxx_TypeForBits()
                test_seed.setTo0()
                test_seed = test_seed | operators[i].get_1p_state()
                s = "L = 1      mz = %d    spin = %s \n"%(mz, {0:"-1/2",1:"+1/2"}[spin] )
                s = s+str(test_seed)+"\n"
                bavardage.write(s)

        couche = 1
        for mz in range(-2,3):
            for spin in range(0,2):
                i = memory_map(couche, (mz,spin))
                test_seed = Hilbertxx.Hxx_TypeForBits()
                test_seed.setTo0()
                test_seed = test_seed | operators[i].get_1p_state()
                s = "L = 2      mz = %d    spin = %s \n"%(mz, {0:"-1/2",1:"+1/2"}[spin] )
                s = s+str(test_seed) +"\n"
                bavardage.write(s)

    for  occ_state in range(  memory_map(1, ( -coucheL(1), 0) )+   nel3d) :
        base_seed = base_seed | operators[occ_state].get_1p_state()

    Lcouche2=coucheL(2)
    gcouche2 = (2*Lcouche2+1)*2

    if maxnelonL is not None:
        gcouche2=1
    
    for  occ_state in range( memory_map(2, ( -coucheL(2), 0) ) ,  memory_map(2, ( -coucheL(2), 0) ) +gcouche2 ) :
        base_seed = base_seed | operators[occ_state].get_1p_state()

## Calcul des vecteurs de base

    base = Hilbertxx.Hxx_basis(1)
    exci = Hilbertxx.Hxx_basis(1)

    # filtro sul numero di elettroni couche 4p
    mask_2 = Hilbertxx.Hxx_TypeForBits()
    mask_2.setTo0()
    for  occ_state in range(   memory_map(2, ( -coucheL(2), 0) )  , memory_map(2, ( -coucheL(2), 0) )   +gcouche2) :
         mask_2 = mask_2 | operators[occ_state].get_1p_state()

    filtre2 =  Hilbertxx.Hxx_Filter(1)
    filtre2.addCondition(mask_2,'&',1.0)
    filtre2.min = gcouche2-nhopped
    filtre2.max = gcouche2

    exci_seed=base_seed ^ operators[  memory_map(1, ( -coucheL(1), 0) )+ nel3d].get_1p_state()
    exci_seed=exci_seed ^ operators[0].get_1p_state()

    wanderer = createWanderer()
    mixerEG    = createMixerEG   ()
    mixerTG    = createMixerTG   ()

    # creazione della base
    
    for BB, SS , FF, NN in zip([base,exci], [base_seed,exci_seed] , [filtre2, filtre2] ,["base","exci"] ):
        # BB.set_pyfilter(FF)
        starting_state = Hilbertxx.Hxx_FermionicState(SS)
        BB.add_state(starting_state)

        old_total = -1
        total = 0
        while total != old_total:
            old_total=total
            total=BB.operate_andputin_Tree( wanderer, 2 , FF)
            print "SPANNED %d states after wanderer"%( total)
            BB.reconstruct_basis()
            total=BB.operate_andputin_Tree( mixerEG, 2 , FF)
            print "SPANNED %d states after mixer"%( total)
            BB.reconstruct_basis()
            print "Base %s contains %d states"% (NN, BB.get_dimension())

        if BAVARDAGE:
            bavardage.write("\n\n\n\n########## FOR BASE %s \n"% NN)
            bavardage.write("####   we have %d  states \n\n"%total) 
            for i in range(total):
                bavardage.write( "%s\n"% str(BB.get_basis(i).get_value()))

### verificare la progressione sui seeds    --OK--
#get_basis(int n=0)
#get_value()
#return_string
## e per le matrici operare in compose con SalvaSparsa3A che gia si usa 



            
    if( prefix!=""  and prefix[-1]!="/" ):
        prefix=prefix+"/"

## Create transition Matrices
    WriteIntra  ([base, exci], prefix )
    WriteInter  ([base, exci], prefix )
    WriteSo     ([base, exci], prefix )
    WriteCounterP([base, exci], prefix )
    WriteLigands([base,exci],prefix)
    WriteMixerParametrized([base,exci],prefix)
    WriteAngulars([base,exci],prefix)
    
## Creates quadrupoles
    WriteQuadrupoles(base, exci, prefix )

if __name__=="__main__":
    scriviFiles(5,prefix="data", nhopped=0)

 
