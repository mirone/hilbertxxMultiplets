#include<stdlib.h>
#include<stdio.h>
#include"mpi.h"
#include<assert.h>
#include<complex>
extern "C" {
  void sl_init_( int *ICTXT, int *NPROW, int *NPCOL );
  void blacs_gridinfo_(int * ICTXT,int * NPROW,
		      int * NPCOL,int * MYROW,int * MYCOL );
  void descinit_( int * DESCA, int * M, int * N, int * Mb, int * Nb, int * RSRC, int * CSRC, int * ICTXT,  int *MXLLDA,  int * info );

  void pzgetrf_(  int *N, int * ,std::complex<double>* A,int * one, int *, int *DESCA, int *ipiv,  int *info    );
  void pzgetrs_( char * , int *N, int *NRHS, std::complex<double>*A, int *one, int *,
	    int * DESCA, int *ipiv, std::complex<double>*B, int *, int *, int *DESCB, int *info );

  void pzgesv_( int *N, int *NRHS, std::complex<double>*A, int *one, int *,
	    int * DESCA, int *ipiv, std::complex<double>*B, int *, int *, int *DESCB, int *info );


  void blacs_gridexit_( int * );
  void blacs_exit_( int * );
  int numroc_( int *, int *, int *, int *, int * );


}


int  build_corr_old( int * DESCA, int *& gr_A , int rowcol  ) {
  int ictxt = DESCA[1];
  int M,Mb;
  M = DESCA[2+rowcol];
  Mb= DESCA[4+rowcol];
  
  // int NPROW, NPCOL, MYROW, MYCOL;
  // blacs_gridinfo_( &ictxt,  &NPROW, &NPCOL, &MYROW, &MYCOL  );

  int NPROW, MYROW;
  int NPS[2], MYS[2];
  blacs_gridinfo_( &ictxt,  NPS, NPS+1,  MYS, MYS+1  );
  
  NPROW = NPS[rowcol];
  MYROW=MYS[rowcol];

  int ncards  = M/Mb;
  int wlastcard = M%Mb;
  ncards++;

  int ntours = (ncards-1)/NPROW ; 
  int lasttour =  ncards%NPROW +1; 

  int ncorr = ntours*Mb;
  if (lasttour >MYROW) {
    ncorr += Mb;
  } else if(lasttour == MYROW) {
    ncorr += wlastcard ; 
  }
  
  gr_A = new int[ ncorr ];
  
  int icount=0;
  int itour=0;
  int nels;
  for( itour=0; itour <= ntours ; itour++) {
    
    if(itour==ntours)  {
      nels=0;
      if( lasttour == MYROW) nels = wlastcard ;
      if( lasttour > MYROW) nels =   Mb;
    }    else               nels =     Mb   ;

    for(int ir=0; ir<nels ; ir++) {
      gr_A[icount] = (NPROW*itour + MYROW)*Mb+ir; 
      icount++;
    } 
  }
  return ncorr;
}

int  build_corr( int M, int Mb,   int NPROW, int MYROW  , int *& gr_A  ) {

  int ncards  = M/Mb; 
  int wlastcard = M%Mb;  

  ncards++;   
  int ntours =    (ncards-1)/NPROW ; 
  int lasttour =  (ncards-1)%NPROW +1; 

  if(ncards) {
  } else {
    ntours =    0  ; 
    lasttour =  1;    
  }
 
  int ncorr = ntours*Mb;
  if ((lasttour-1) >MYROW) {
    ncorr += Mb;
  } else if((lasttour-1) == MYROW) {
    ncorr += wlastcard ; 
  }
  
  gr_A = new int[ ncorr ];
  
  int icount=0;
  int itour=0;
  int nels;
  for( itour=0; itour <= ntours ; itour++) {
    
    if(itour==ntours)  {
      nels=0;
      if( (lasttour-1) == MYROW) nels = wlastcard ;
      if( (lasttour-1) > MYROW) nels =   Mb;
    }    else               nels =     Mb   ;

    for(int ir=0; ir<nels ; ir++) {
      gr_A[icount] = (NPROW*itour + MYROW)*Mb+ir; 
      icount++;
    } 
  }
  return ncorr;
}


#define DIDIM 11

int  main(int argc, char ** argv) {

  int zero=0;
  
  int myid;
  int  numprocs;
  MPI_Init(&argc,&argv);  
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  
  int  ICTXT, NPROW,  NPCOL, MYROW, MYCOL;
  NPROW=2;
  NPCOL=2;
  int Mb=11;
  int Nb=11;
  assert(numprocs==(NPROW*NPCOL) );
  
  MYCOL = myid%NPCOL;
  MYROW = myid/NPCOL;

  int  M,N ;
  std::complex<double> *A,*newA;
  A=NULL;
  newA=NULL;
  M=0;
  N=0;

  int  newM , newN;
  int locdimM, locdimN, new_locdimM ,  new_locdimN;
    
  locdimM=0;
  locdimN=0 ;

  int *corrM=NULL;
  int *corrN=NULL;
  srandom(0);

  std::complex<double> G[DIDIM][DIDIM];

  for(int increments=0; increments<DIDIM; increments++) {
    newM=M+1;
    newN=N+1;
    std::complex<double> riga[newN], colonna[newM];

    for(int i=0; i<newM; i++) {
      colonna[i]=std::complex<double> (random()*1.0/(RAND_MAX+1.0),random()*1.0/(RAND_MAX+1.0));
      G[  i  ][ newN-1  ] = colonna[i]  ; 
    }
    for(int i=0; i<newN; i++) {
      riga[i]=std::complex<double> (random()*1.0/(RAND_MAX+1.0),random()*1.0/(RAND_MAX+1.0));
      G[  newM-1 ][  i  ] = riga[i]  ; 
    }
    colonna[newM-1]=riga[newN-1];

    if(corrM) delete []corrM;
    new_locdimM = build_corr(  newM,  Mb,    NPROW,  MYROW  , corrM ) ;

    if(corrN) delete []corrN;
    new_locdimN = build_corr(  newN,  Nb,    NPCOL,  MYCOL  , corrN ) ;

    if( new_locdimM> locdimM ||   new_locdimN > locdimN ) {
      newA = new std::complex<double>[ new_locdimM*new_locdimN ];
      for(int n=0; n< locdimN; n++) {
	for(int m=0; m< locdimM; m++) {
	  newA[ n * new_locdimM + m ]=A[  n*locdimM + m  ];
	}
      }
      if( new_locdimM > locdimM) {
	for(int n=0; n< new_locdimN; n++) {
	  newA[n*new_locdimM  + (new_locdimM-1) ]   =  riga[  corrN[n] ]  ;
	}
      }
      if( new_locdimN > locdimN) {
	for(int m=0; m< new_locdimM; m++) {
	  newA[ (new_locdimN-1) * new_locdimM+ m  ]  =  colonna[  corrM[m] ]  ;
	}
      }
      delete [] A;
      A = newA;
    }
    M=newM;
    N=newN;

    locdimM = new_locdimM ;
    locdimN = new_locdimN ;    
  }

  std::complex<double> data[DIDIM];
  for(int i=0; i<DIDIM;i++) data[i] =  std::complex<double> (random()*1.0/(RAND_MAX+1.0),random()*1.0/(RAND_MAX+1.0))  ; 
      
  sl_init_( &ICTXT, &NPROW, &NPCOL );
  // *  SL_INIT initializes an NPROW x NPCOL process grid using a row-major
  // *  ordering  of  the  processes. This routine retrieves a default system
  // *  context  which  will  include all available processes. In addition it
  // *  spawns the processes if needed.
  
  blacs_gridinfo_( &ICTXT, &NPROW, &NPCOL, &MYROW, &MYCOL );
  MPI_Barrier(MPI_COMM_WORLD);

  int DESCA[9],  RSRC, CRSRC, MXLLDA, info;
  RSRC=0;
  CRSRC=0;
  MXLLDA= locdimM;
  if(MXLLDA==0 ) MXLLDA=1;

  // attenzione per il fortran l' indice di linea e' il secondo mxllda

  descinit_( DESCA, &M, &N   , &Mb, &Nb, &RSRC, &CRSRC, &ICTXT, &MXLLDA,  &info );

  printf("   per processo %d %d \n", myid,locdimM   );

  int DESCB[9], NRHS, MXLLDB,NBRHS;
  NRHS=1;
  MXLLDB = locdimM;
  if(MXLLDB==0 ) MXLLDB=1;
  NBRHS=1;

  printf(" illegal ?  N  %d, NRHS %d, Nb %d, NBRHS %d, RSRC %d, CRSRC %d, ICTXT %d ,MXLLDB %d   \n",  N, NRHS, Nb, NBRHS, RSRC, CRSRC, ICTXT,MXLLDB   );
	 
  descinit_( DESCB, &M, &NRHS, &Mb, &Nb, &RSRC, &CRSRC, &ICTXT,&MXLLDB, &info );

  std::complex<double>  B[  MXLLDB  ], newB[  MXLLDB  ]; // attenzione per il fortran l' indice di linea e' il secondo mxllda

  int nx;
  if(MYCOL==0) {
    for(int ir=0; ir< locdimM ; ir++) {
      B[ ir   ]  = data[  corrM[ir]       ] ;
    }  
    nx=locdimM;
  } else {
    nx = 0;
  }

  int one=1;
  int ipiv[locdimM+Mb];

  newA = new std::complex<double>[ locdimM*locdimN ];
  memcpy(newA,A, locdimM*locdimN * sizeof(std::complex<double> )  ); // if corrM[m]==corrN[n]  mandare avanti il piu piccolo dei due. Quando si trova uguale cambiare diagonale 
  memcpy(newB,B, locdimM * sizeof(std::complex<double> )  );


  for(int ir=0; ir< locdimM ; ir++) {
    for(int ic=0; ic< locdimN ; ic++) {
      if( corrM[ir]== corrN[ic]) {
	newA[ic* locdimM +ir   ]+=0.1;
      }
    }
  }


  pzgesv_( &N, &NRHS, newA, &one, &one, DESCA, ipiv, newB, &one, &one, DESCB, &info );

 
  


  // pzgetrf_(  &N, &N, newA, &one, &one, DESCA, ipiv,  &info    );
  // pzgetrs_( "No transpose", &N, &NRHS, newA, &one, &one, DESCA, ipiv, newB, &one, &one, DESCB, &info );

  int pncorr[numprocs];

  MPI_Allgather( &nx, 1, MPI_INT , pncorr, 1 ,  MPI_INT , MPI_COMM_WORLD ) ;
  
  int globcorr[N];
  std::complex<double> globB[N];
  std::complex<double> solution[N];

  int scounts [numprocs];
  int sdisps  [numprocs];

  int rcounts [numprocs];
  int rdisps  [numprocs];

  for(int i=0; i<numprocs; i++) {
    scounts[i] = nx ;
    sdisps [i] = 0 ; 
  }
  int last=0;
  for(int i=0; i<numprocs; i++) {
    rcounts[i] = pncorr[i];
    rdisps [i] = last ;
    last = last +  pncorr[i]; 
  }

  MPI_Allgatherv( corrM  , nx, MPI_INT, 
		 globcorr, rcounts, rdisps, MPI_INT, 
		 MPI_COMM_WORLD);

  MPI_Allgatherv(newB, nx, MPI_DOUBLE_COMPLEX, 
		globB, rcounts, rdisps, MPI_DOUBLE_COMPLEX, 
		MPI_COMM_WORLD);


  int globipiv[N];
  MPI_Allgatherv( ipiv  , nx, MPI_INT, 
		 globipiv, rcounts, rdisps, MPI_INT, 
		 MPI_COMM_WORLD);



  for(int ir=0; ir< N; ir++) {
    solution[globcorr[ir]]  = globB[ ir   ]   ;
  }  
  if(myid==1) {
    for(int ir=0; ir< N; ir++) {
      printf(" solution[%d]=%e %e   %d\n",ir,solution[ir].real(),solution[ir].imag(), globipiv[ir]);
    }
    std::complex<double> ret[N];
    for(int i=0; i<N; i++) {
      ret[i]=0;
      for(int j=0; j<N; j++) {
	ret[i]+=  (G[i][j]+.1*(i==j))  *solution[j] ; 

      }
      // printf( "   %e %e             %e %e \n",  ret[i].real()  , ret[i].imag(),   data[i].real() ,   data[i].imag()      );
      printf( "   %e %e             %e %e \n",  ret[i].real()  , ret[i].imag(),   data[i].real() ,   data[i].imag()      );
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);
  blacs_gridexit_( &ICTXT );
  blacs_exit_( &zero );
}
