#include<vector>
#include<map>
#include <algorithm>
#include<string>
#include<string.h>
#include<iostream>
#include<iomanip>
#include<fstream>
#include<assert.h>
#include<math.h>
#include<complex>
#include<stdlib.h>

class LU {
public:
  std::complex<double> * A;
  int dim;
  LU() {
    A=NULL;
    dim=0;
  };
  void moltiplica(std::complex<double> *res, std::complex<double>* x ) {
    // guarda file zsymm.txt per implementazione moltiplicazione LU
    for(int i=0; i<dim; i++) {
      res[i]=0;
      for(int j=i; j<dim; j++) {
	res[i]=res[i]+ A[ i * dim + j ]* x[j] ; 
      }
    }

    for(int i=dim-1; i>=0 ; i-- ) {
      res[i]=res[i]; // per questo rimpiazzare la diagonale 
      for(int j=0; j<i; j++) {
	res[i]=res[i]+ A[ i * dim + j ]* res[j] ; 
      }
    }
    // e poi rimetterla 
  }
 void upgrade( std::complex<double> *RIGA , std::complex<double> *COLONNA ,
		int N ) {

    assert(dim == (N-1) );

    std::complex<double> *newA= new std::complex<double>[N*N];
    
    for(int i=0; i<dim; i++) {
      for(int j=0; j<dim; j++) {
	newA[i*N+j] =  A[i*dim+j]  ; 
      }
    }
    for(int j=0; j<N; j++) {
      newA[(N-1)*N+j] =  RIGA [j] ; 
    }
    for(int j=0; j<N; j++) {
      newA[ j*N+ (N-1) ] = COLONNA  [ j ]   ; 
    }
    if(A) delete A;
    A=newA ;     

    std::complex<double> sum;
    for(int i1=0; i1<N-1; i1++) {
      sum=0;
      for(int i2=0; i2<i1; i2++) {
	sum+=A[  i1*N+i2 ] *  A[ i2*N+ (N-1) ]  ; 
      }
      A[ i1*N+ (N-1) ] = A[ i1*N+ (N-1) ] -sum ; 
    }

    for(int i1=0; i1<N-1; i1++) {
      sum=0;
      for(int i2=0; i2<i1; i2++) {
	sum+=A[ i2 *N+ i1 ] *  A[ (N-1)*N+  i2 ]  ; 
      }
      A[ (N-1)*N+  i1  ] = (A[(N-1)*N+  i1 ] -sum)/A[ i1*N+ i1 ] ; 
    }
    int i1=N-1;
    sum=0;
    for(int i2=0; i2<i1; i2++) {
      sum+=A[ i2*N+ i1 ] *  A[ (N-1)*N+  i2 ]  ; 
    }
    A[ i1*N+ (N-1) ] = A[ i1*N+ (N-1) ] -sum ; 

    dim=N;
  };


  void risolvi(std::complex<double> *res, std::complex<double>* x ) {
    std::complex<double> sum;
    for(int i=0; i<dim; i++) {
      sum=0;
      for(int j=0; j<i; j++) {
	sum=sum+ res[j]*A[i*dim+j];
      }
      res[i] = x[i]-sum;
    }

    for(int i=dim-1; i>=0 ; i--) {
      sum=0;
      for(int j=i+1; j<dim; j++) {
	sum=sum+ res[j]*A[i*dim+j];
      }

      res[i] = (res[i]-sum)/A[i*dim+i] ;
    }
  }


};

main() {
  int N=10;
  LU lu;
  std::complex<double> A[N*N];
  std::complex<double> V1[N];
  std::complex<double> V2[N];
  std::complex<double> V3[N];


  for(int i=0; i<N*N; i++) {
    A[i]=   std::complex<double> (random()*1.0/(RAND_MAX+1.0),random()*1.0/(RAND_MAX+1.0));
    std::cout << "A["<<i<<"]="<<A[i]<<std::endl;
  }
  for(int i=0; i<N; i++) {
    V1[i]=   std::complex<double> (random()*1.0/(RAND_MAX+1.0),random()*1.0/(RAND_MAX+1.0));
    std::cout << "V1["<<i<<"]="<<V1[i]<<std::endl;
  }

  for(int i=0; i<N; i++) {
    std::vector<std::complex<double> >riga, colonna;
    for(int k=0; k<=i; k++) {
      colonna.push_back( A[k*N+i] );
      riga   .push_back( A[i*N+k] );
    }
    lu.upgrade(&(riga[0]),&(colonna[0]),i+1);
  }

  for(int i=0; i<N; i++) {
    std::cout << "V1["<<i<<"]="<<V1[i]<<std::endl;
  }
  
  
  for(int i=0;i<N;i++) {
    V2[i]=0;
    for(int j=0;j<N;j++) {
      // printf(" moltiplica %e con V1[%d] =%e \n",  A[i*N+j],j,V1[j]);
      V2[i]= V2[i] + A[i*N+j]*V1[j] ;
    }
  }
  for(int i=0; i<N*N; i++) {
    std::cout << "A["<<i<<"]="<<A[i]<<std::endl;
    // printf("A[%d]=%e\n", i,A[i]);
  }
  for(int i=0; i<N; i++) {
    std::cout << "V1["<<i<<"]="<<V1[i]<<std::endl;
  }


  lu.moltiplica( &(V3[0]), &(V1[0]));
  for(int k=0; k<N; k++) {
    std::cout << "V3["<<k<<"]="<<V3[k]<<"              "   << "V2["<<k<<"]="<<V2[k]                          <<std::endl;
  }


  lu.risolvi( &(V2[0]), &(V3[0]));
  for(int k=0; k<N; k++) {
    std::cout << "V2["<<k<<"]="<<V2[k]<<"              "   << "V1["<<k<<"]="<<V1[k]                          <<std::endl;
  }


}
