#include<vector>
#include<map>
#include <algorithm>
#include<string>
#include<string.h>
#include<iostream>
#include<iomanip>
#include<fstream>
#include<assert.h>
#include<math.h>
#include<complex>
#include<stdlib.h>
#include"mpi.h"

#define min(a,b) ((a)<(b))? (a):(b)
#define max(a,b) ((a)>(b))? (a):(b)

#define COLSPERPROC 2
#define CONTROLLAPRECISIONE 1

class LU {
public:
  std::complex<double> * A;
  std::complex<double> * Original;
  std::complex<double> * T;
  std::complex<double> * At;
  int  * column_map;
  int  * column_inverse_map;
  int dim;
  int myid;
  int  numprocs;
  int  nmycolumns;
  LU() {
    column_map = NULL; 
    column_inverse_map = NULL ;
    A=NULL;
    T=NULL;
    At=NULL;
    Original = NULL;
    dim=0;
    nmycolumns=0;
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  };
  
  void upgrade( std::complex<double> *RIGA , std::complex<double> *COLONNA ,  int N ) {
    int root;
    
    assert(dim == (N-1) );
    
    int nmycolumns_new=0;
    for(int i=0; i<N ; i++) {
      int iproc = i/COLSPERPROC ; 
      if(iproc%numprocs==myid) nmycolumns_new++;
    }
    if(column_map) {
      delete [] column_map;
      delete [] column_inverse_map;
    }
    int ncolperproc[numprocs];
    int sumncolperproc[numprocs];
    
    for(int i=0; i< numprocs; i++) {
      ncolperproc[i]    = 0;
      sumncolperproc[i] = 0;
    }
    
    column_map = new int [N];
    column_inverse_map = new int [N];
    nmycolumns_new=0;
    for(int i=0; i<N ; i++) {
      int iproc = i/COLSPERPROC ; 
      int iiproc = iproc%numprocs;
      if(i==N-1) root=iiproc ; 
      ncolperproc[iiproc]++;
      if(iiproc==myid) {
	column_map[nmycolumns_new]=i;
	column_inverse_map[i] =  nmycolumns_new;
	nmycolumns_new++;
      }else {
	column_inverse_map[i]=-1;
      }
    }
    for(int i=1; i<numprocs; i++) {
      sumncolperproc[i]=sumncolperproc[i-1]+ncolperproc[i-1];
    }
    {
      int ratio = N/COLSPERPROC ; 
      int formul;
      formul = (ratio/numprocs   +(ratio%numprocs > myid) )*COLSPERPROC  +(ratio%numprocs == myid)*(N%COLSPERPROC  )  ; 
      assert(nmycolumns_new == formul  );
    }


    std::complex<double> *newOriginal;
    
    std::complex<double> *newA;
    std::complex<double> *newAt;
    std::complex<double> *newT;

    if(nmycolumns_new) {
      if(CONTROLLAPRECISIONE) newOriginal  = new std::complex<double>[N*nmycolumns_new ];
      newA= new std::complex<double>[(N)*nmycolumns_new ];
      newT= new std::complex<double>[nmycolumns_new*(N) ];
      newAt= new std::complex<double>[(N)*nmycolumns_new ];
    } else {
      newAt=NULL;
      newA=NULL;
      newT=NULL;
      newOriginal=NULL;
    }
    // for(int i=0; i<dim; i++) {
    //   for(int j=0; j<nmycolumns; j++) {
    // 	newA[i*nmycolumns_new+j] =  A[i*nmycolumns+j]  ; 
    //   }
    // }
    for(int j=0; j<nmycolumns; j++) {
      for(int i=0; i<dim; i++) {
	newT[j*N+i] =  T[j*(N-1)+i]  ; 
	newA[j*N+i] =  A[j*(N-1)+i]  ; 
	newAt[j*N+i] =  At[j*(N-1)+i]  ; 	
      }
    }
    
    if(CONTROLLAPRECISIONE) {
      for(int j=0; j<nmycolumns; j++) {
	for(int i=0; i<dim; i++) {
	  newOriginal[j*N+i] =  Original[j*(N-1)+i]  ; 
	}
      }
    }

    // if(myid==0) {
    //   for(int i=0; i<nmycolumns_new; i++) {
    // 	for(int j=0; j<N; j++) {
    // 	  printf(" %e %e ", newA[i*N+j   ].real(), newA[i*N+j ].imag());
    // 	}
    // 	printf("\n");
    //   }
    //   printf("-------AAAAAAAAAAAAAAAA------\n");
    // }

    if(CONTROLLAPRECISIONE) {
      for(int j=0; j<nmycolumns_new; j++) {
	newOriginal[ j*N +  N-1 ] = COLONNA  [ column_map[j] ]   ; 
      }
      if(nmycolumns_new>nmycolumns) {
	assert(root==myid);
	for(int j=0; j<N; j++) {
	  newOriginal[ (nmycolumns_new-1)*N + j ] = RIGA  [ j ]   ; 
	}
      }
    }
    

    if(CONTROLLAPRECISIONE) {
      if(Original) delete []Original;
      Original=newOriginal ;  
    }
    if(A) delete []A;
    if(At) delete []At;
    if(T) delete []T;
    A=newA ;     
    At=newAt ;     
    T=newT ;     
    nmycolumns = nmycolumns_new ;
    dim=N;
    
    int dutylen=0;
    std::complex<double> duty[COLSPERPROC];
    std::complex<double> avanzamento[nmycolumns ];
    int status;
    

    for(int irc=0; irc<2; irc++) {
      for(int i=0; i<nmycolumns ; i++) {
	avanzamento[i]=0;
      }
      int palier=0;
      for(int i=0; i<N-1 ; i++) {
	// printf(" myid %d i %d COLSPERPROC %d \n");
	int iproc = (i/COLSPERPROC)%numprocs  ;
	if( i%COLSPERPROC==0) {
	  palier=i;
	  dutylen=min(COLSPERPROC,(N-1)-i);
	}
	if(myid==iproc) {
	  if( i%COLSPERPROC==0) {
	    if(irc==0) {
	      for(int j=0; j<dutylen; j++) {
		duty[j] = COLONNA[i+j];
	      }
	    } else {
	      for(int j=0; j<dutylen; j++) {
		duty[j] = RIGA[ i+j ];
	      }
	    }
	  }
	  std::complex<double> accu = avanzamento[column_inverse_map[i]];
	  if(irc==0) {
	    for(int k=palier; k<i; k++) {
	      accu+=  duty[k-palier]    * T[ column_inverse_map[i] *N   + k   ];
	    }
	  } else {
	    for(int k=palier; k<i; k++) {
	      accu+=  duty[k-palier]    * A[ column_inverse_map[i] *N   + k   ];
	    }	    
	  }
	  duty[i-palier] = duty[i-palier] - accu  ;
	  if(irc==0) {
	    // non c' e' bisogno di dividere
	  } else {
	    duty[i-palier] = duty[i-palier]/A[ column_inverse_map[i] * N   + i ] ;
	  }
	}
	if( (i)%COLSPERPROC==dutylen-1) {
	  MPI_Bcast ( duty ,  2*dutylen, MPI_DOUBLE , iproc,   MPI_COMM_WORLD );

	  if(myid==root) {
	    if(irc==0) {
	      if(myid==1) {
	        //printf("setto %d %d a %e %e\n", (nmycolumns-1),i,duty[i-palier].real(),duty[i-palier].imag());
	      }
	      for(int k=0; k<dutylen ; k++) A[ (nmycolumns-1)* N   + palier+k ]=duty[k];
	    } else {
	      for(int k=0; k<dutylen ; k++) T[  (nmycolumns-1) * N   + palier+k ]=duty[k];
	    }
	  }
	  // printf(" DOPO A\n");
	  MPI_Barrier( MPI_COMM_WORLD);
	  for(int j=0; j<nmycolumns ; j++) {
	    if( column_map[j]>i) {
	      std::complex<double> accu = avanzamento[j];	    
	      if(irc==0) {
		for(int k=0; k<dutylen; k++) {
		  accu = accu + duty[k] *  T[ j*N + (palier+k) ] ; 
		}
	      } else {
		for(int k=0; k<dutylen; k++) {
		  accu = accu + duty[k] *  A[ j*N + (palier+k) ] ; 
		}
	      }
	      avanzamento[j] = accu ;
	    }
	  }
	}
      }
    }
    // if(myid==0) {
    //   for(int i=0; i<nmycolumns_new; i++) {
    // 	for(int j=0; j<N; j++) {
    // 	  printf(" %e %e ", newA[i*N+j   ].real(), newA[i*N+j ].imag());
    // 	}
    // 	printf("\n");
    //   }
    //   printf("-------BBBBBBBBBBBBBB------\n");
    // }
    {
      int i1=N-1;
      int iproc = (i1/COLSPERPROC)%numprocs  ;
      if(myid==iproc) {
	// printf(" sono il processo %d nmycolumns %d COLONNA[%d-1] %e  %e -- %e %e\n",iproc, nmycolumns ,N, 
	//        COLONNA[N-1].real(),  COLONNA[N-1].imag() , A[ (nmycolumns-1)*N+ (N-1) ].real(), A[ (nmycolumns-1)*N+ (N-1) ].imag()  );
	std::complex<double> sum=0;
	for(int i2=0; i2<i1; i2++) {
	  sum+=T[  (nmycolumns-1)*N+ i2 ] *  A[  (nmycolumns-1)*N+  i2 ]  ; 
	}
	A[ (nmycolumns-1)*N+ (N-1) ] = COLONNA[N-1] -sum ; 
      }
    }
    
    std::complex<double> *transfert = new std::complex<double>[  N]; 
    int global_map[N];
    {
      int senddsplcs[numprocs];
      int sendcounts[numprocs];
      for(int k=0; k<numprocs; k++) senddsplcs[k]=0;
      for(int k=0; k<numprocs; k++) sendcounts[k]=nmycolumns;
      
      MPI_Alltoallv(  column_map  ,  sendcounts  , senddsplcs , MPI_INT,
		      global_map  ,  ncolperproc  , sumncolperproc,  MPI_INT,
		      MPI_COMM_WORLD);
    }


    {
      std::complex<double> receive[  N]; 
      int ninproc[numprocs];
      for(int k=0; k<numprocs; k++) ninproc[k]=0;
      int K, coloffset;
      int zeros[numprocs];
      int masked_ncolperproc[numprocs];
      int masked_sumncolperproc[numprocs];
      
      for(int myline=0; myline<N ; myline++) {
	int jproc =  ((myline/COLSPERPROC)%numprocs)  ;
	if(myid==root) {
	  // std::cout << " da mandare " <<  A [ ( nmycolumns-1  )*N+myline]<< std::endl;
	  transfert[sumncolperproc[ jproc ] +ninproc[jproc] ]   = A [ ( nmycolumns-1  )*N+myline] ; 
	}
	ninproc[jproc]++;
      }
      for(int k=0; k<numprocs; k++) {
	zeros[k]=0; // queste per mandare
	            // quete per prendere
	masked_ncolperproc[k]     = 0;
	masked_sumncolperproc[k]  = 0;
	if(k==root) {
	  masked_ncolperproc[k]     =nmycolumns;
	  masked_sumncolperproc[k]  = 0;
	}
      }

      if(myid==root) {
	MPI_Alltoallv(  transfert  ,  ncolperproc  , sumncolperproc, MPI_DOUBLE_COMPLEX,
			receive      , masked_ncolperproc   ,masked_sumncolperproc , MPI_DOUBLE_COMPLEX,
			MPI_COMM_WORLD);
      } else {
	MPI_Alltoallv(  transfert  ,  zeros  , zeros, MPI_DOUBLE_COMPLEX,
			receive      ,  masked_ncolperproc  ,masked_sumncolperproc , MPI_DOUBLE_COMPLEX,
			MPI_COMM_WORLD);
      }
      for(int myline=0; myline<nmycolumns ; myline++) {
	// std::cout << " arrivato " << receive[myline] << std::endl;
	At[myline*dim + (dim-1)]=receive[myline];
      }
    }
    MPI_Barrier( MPI_COMM_WORLD);

    {
      std::complex<double> receive[  N]; 
      int zeros[numprocs];
      int masked_ncolperproc[numprocs];
      int masked_sumncolperproc[numprocs];  
      {
	for(int mycol =0 ; mycol <nmycolumns ; mycol++ ) {
	  transfert[  mycol ]   = A [ mycol*dim+(dim-1)] ; 
	}
      }
      for(int k=0; k<numprocs; k++) {
	zeros[k]=0; 
	
	masked_ncolperproc[k]     = 0;
	masked_sumncolperproc[k]  = 0;
	if(k==root) {
	  masked_ncolperproc[k]     = nmycolumns;
	  masked_sumncolperproc[k]  = 0;
	}
      }
      if(myid==root) {
	MPI_Alltoallv(  transfert  ,  masked_ncolperproc  ,masked_sumncolperproc , MPI_DOUBLE_COMPLEX,
			receive      ,  ncolperproc  ,sumncolperproc , MPI_DOUBLE_COMPLEX,
			MPI_COMM_WORLD);

	std::complex<double> *buffer = receive;
	for(int kproc=0; kproc<numprocs; kproc++) {
	  int localdim = ncolperproc   [kproc];
	  int offset = sumncolperproc   [kproc]; 
	  int myline=nmycolumns-1;
	  for(int mycol = 0; mycol <localdim  ; mycol ++ ) {
	    // std::cout << " arrivato buffer" << buffer[   mycol] << std::endl;
	    At[  myline*dim + global_map[offset+mycol] ]= buffer[   mycol];
	  }
	  buffer += localdim   ; 
	}
      } else {
	MPI_Alltoallv(  transfert  ,  masked_ncolperproc   ,masked_sumncolperproc , MPI_DOUBLE_COMPLEX,
			receive      , zeros   , zeros, MPI_DOUBLE_COMPLEX,
			MPI_COMM_WORLD);
      }
    }

    delete [] transfert;
  };
  
  
  void risolvi(std::complex<double> *res, std::complex<double>* x ) {
    

    int dutylen=0;
    std::complex<double> duty[COLSPERPROC];
    std::complex<double> avanzamento[nmycolumns ];
    int status;

    for(int i=0; i<nmycolumns ; i++) {
      avanzamento[i]=0;
    }
    int palier=0;
    for(int i=0; i<dim ; i++) {
      int iproc = (i/COLSPERPROC)%numprocs  ;
      if( i%COLSPERPROC==0) {
	palier=i;
	dutylen=min(COLSPERPROC,dim-palier);
	for(int j=0; j<dutylen; j++) {
	  duty[j] = x[palier+j];
	}	    
      }
      if(myid==iproc) {
	std::complex<double> accu = avanzamento[column_inverse_map[i]];
	for(int k=palier; k<i; k++) {
	  accu+=  duty[k-palier]    * T[ column_inverse_map[i] *dim   + k   ];
	}
	duty[i-palier] =  duty[i-palier] - accu  ; 
      } 
      if( (i)%COLSPERPROC==dutylen-1) {
	MPI_Bcast ( duty ,  2*dutylen, MPI_DOUBLE , iproc,   MPI_COMM_WORLD );
	// printf(" DOPO B\n");
	MPI_Barrier( MPI_COMM_WORLD);

	for(int j=0; j<dutylen; j++) {
	  res[palier+j] = duty[j] ;
	}	    
	for(int j=0; j<nmycolumns ; j++) {
	  if( column_map[j]>i) {
	    std::complex<double> accu = avanzamento[j];	    
	    for(int k=0; k<dutylen; k++) {
	      accu = accu + duty[k] *  T[ j*dim + (palier+k) ] ; 
	    }
	    avanzamento[j] = accu ;
	  }
	}
      }
    }
    for(int i=0; i<nmycolumns ; i++) {
      avanzamento[i]=0;
    }
    palier=0;
    int palierup=0;
    for(int i=dim-1; i>=0 ; i--) {
      int iproc = (i/COLSPERPROC)%numprocs  ;

      if( (i+1)%COLSPERPROC==0 || i==dim-1  ) {
	palier=(i/COLSPERPROC)*COLSPERPROC;
	palierup=i;
	dutylen=min(COLSPERPROC, dim-palier);
	for(int j=0; j<dutylen; j++) {
	  duty[j] = res[palier+j];
	}	    
      }
      if(myid==iproc) {
	std::complex<double> accu = avanzamento[column_inverse_map[i]];
	for(int k = i+1 ; k<=palierup; k++) {
	  accu+=  duty[k-palier]    *     At[  column_inverse_map[i] *dim   + k    ]   ;
	}
	duty[i-palier] =  (duty[i-palier] - accu)/ At[  column_inverse_map[i] *dim   + i    ]   ; 
      } 

      if( (i)%COLSPERPROC == 0 ) {
	MPI_Bcast ( duty ,  2*dutylen, MPI_DOUBLE , iproc,   MPI_COMM_WORLD );
	// printf(" DOPO C\n");
	MPI_Barrier( MPI_COMM_WORLD);

	for(int j=0; j<dutylen; j++) {
	  res[palier+j] = duty[j] ;
	}	    
	for(int j=0; j<nmycolumns ; j++) {
	  if( column_map[j]<i) {
	    std::complex<double> accu = avanzamento[j];	    
	    for(int k=0; k<dutylen; k++) {
	      accu = accu + duty[k] *  At[ j*dim + (palier+k) ] ; 
	    }
	    avanzamento[j] = accu ;
	  }
	}
      }
    }
    if(CONTROLLAPRECISIONE) {
      std::complex<double> sum;
      double maxdiff=0.0, redmax;
      for(int j=0; j<nmycolumns ; j++) {
	sum=0.0;
	for(int k=0; k< dim; k++)  sum+= res[k]*Original[ j*dim + k ]  ;
	maxdiff=max( maxdiff , abs(sum- x[column_map[j]]   )   );
      }
      MPI_Reduce(&maxdiff ,&redmax , 1, MPI_DOUBLE , 
		 MPI_MAX, 0, MPI_COMM_WORLD);
      if(myid==0) {
	printf( " ERRORE MASSIMO %e\n", maxdiff ) ;
      }
    }
    
  };

  void moltiplica(std::complex<double> *res, std::complex<double>* x ) {
    int dutylen=0;
    std::complex<double> duty[COLSPERPROC];
    std::complex<double> avanzamento[nmycolumns ];
    int status;

    for(int oproc=0; oproc<-1; oproc++) {
      MPI_Barrier(MPI_COMM_WORLD);
      if(myid==oproc) {
	for(int i=0; i<nmycolumns; i++) {
	  for(int j=0; j<dim; j++) {
	    printf(" %e %e ", A[i*dim+j   ].real(), A[i*dim+j ].imag());
	  }
	  printf("\n");
	}
	printf("-------------\n");
	for(int i=0; i<nmycolumns; i++) {
	  for(int j=0; j<dim; j++) {
	    printf(" %e %e ", At[i*dim+j   ].real(), At[i*dim+j ].imag());
	  }
	  printf("\n");
	}
	printf("-------------\n");
	for(int i=0; i<nmycolumns; i++) {
	  for(int j=0; j<dim; j++) {
	    printf(" %e %e ", T[i*dim+j   ].real(), T[i*dim+j ].imag());
	  }
	  printf("\n");
	}
	printf("-------------\n");
      }
      MPI_Barrier(MPI_COMM_WORLD);

    }
    int palier=0;
    int palierup=0;

    if(1){
    for(int i=0; i<nmycolumns ; i++) {
      avanzamento[i]=0;
    }
    for(int i=dim-1; i>=0 ; i--) {
      int iproc = (i/COLSPERPROC)%numprocs  ;

      if( (i+1)%COLSPERPROC==0 || i==dim-1  ) {
	palier=(i/COLSPERPROC)*COLSPERPROC;
	palierup=i;
	dutylen=min(COLSPERPROC, dim-palier);
	for(int j=0; j<dutylen; j++) {
	  duty[j] = x[palier+j];
	}	    
      }
      if(myid==iproc) {
	std::complex<double> accu = avanzamento[column_inverse_map[i]];
	for(int k = i ; k<=palierup; k++) {
	  accu+=  duty[k-palier]*At[column_inverse_map[i] *dim + k];
	}
	res[i]=accu  ; 
      } 

      if( (i)%COLSPERPROC == 0 ) {
	for(int j=0; j<nmycolumns ; j++) {
	  if( column_map[j]<i) {
	    std::complex<double> accu = avanzamento[j];	    
	    for(int k=0; k<dutylen; k++) {
	      accu = accu + duty[k] *  At[ j*dim + (palier+k) ] ; 
	    }
	    avanzamento[j] = accu ;
	  }
	}

	MPI_Bcast ( &(res[palier]) ,  2*dutylen, MPI_DOUBLE , iproc,   MPI_COMM_WORLD );
	// printf(" DOPO D\n");
	MPI_Barrier( MPI_COMM_WORLD);

      }
    }
    }
    if(1) {

    for(int i=0; i<nmycolumns ; i++) {
      avanzamento[i]=0;
    }
    palier=0;
    for(int i=0; i<dim ; i++) {
      int iproc = (i/COLSPERPROC)%numprocs  ;
      if( i%COLSPERPROC==0) {
	palier=i;
	dutylen=min(COLSPERPROC,dim-palier);
	for(int j=0; j<dutylen; j++) {
	  duty[j] = res[palier+j];
	}	    
      }
      if(myid==iproc) {
	std::complex<double> accu = avanzamento[column_inverse_map[i]];
	for(int k=palier; k<i; k++) { // l ultimo scartato est 1
	  accu+=  duty[k-palier]    * T[ column_inverse_map[i] *dim   + k   ];
	}
	res[i]=accu + duty[i-palier]  ; 
      } 
      if( (i)%COLSPERPROC==dutylen-1) {
	for(int j=0; j<nmycolumns ; j++) {
	  if( column_map[j]>i) {
	    std::complex<double> accu = avanzamento[j];	    
	    for(int k=0; k<dutylen; k++) {
	      accu = accu + duty[k] *  T[ j*dim + (palier+k) ] ; 
	    }
	    avanzamento[j] = accu ;
	  }
	}
	
	MPI_Bcast ( &(res[palier+0]) ,  2*dutylen, MPI_DOUBLE , iproc,   MPI_COMM_WORLD );

	// if(iproc==myid) {
	//   for(int j=0; j<dutylen; j++) {
	//     duty[j]  = res[palier+j];
	//     printf("proc %d est %e %e \n", myid, res[palier+j].real(),res[palier+j].imag()); 
	//   }	    
	// } else {
	//   for(int j=0; j<dutylen; j++) {
	//     duty[j]  = 0;
	//   }	    	  
	// }

	// MPI_Bcast ( &(duty[0]) ,  2*dutylen, MPI_DOUBLE , iproc,   MPI_COMM_WORLD );
	// for(int j=0; j<dutylen; j++) {
	//    res[palier+j] = duty[j] ;
	//    // printf("proc %d  res[%d] est %e %e  iproc = %d dutylen  %d, palier %d\n", myid,palier+j,  duty[j].real(),duty[j].imag(), iproc,dutylen,palier ); 
	// }	    
      }
    }
    };
  }
};

main(int argc, char ** argv) {
  MPI_Init(&argc,&argv);  

  int myid;
  int  numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

  int N=2000;
  LU lu;
  std::complex<double> A[N*N];
  std::complex<double> V1[N];
  std::complex<double> V2[N];
  std::complex<double> V3[N];

  srandom(0);

  for(int i=0; i<N*N; i++) {
    A[i]=   std::complex<double> (random()*1.0/(RAND_MAX+1.0),random()*1.0/(RAND_MAX+1.0));
    // if(myid==0) std::cout << "A["<<i<<"]="<<A[i]<<std::endl;
  };


  for(int i=0; i<N; i++) {
    V1[i]=   std::complex<double> (random()*1.0/(RAND_MAX+1.0),random()*1.0/(RAND_MAX+1.0));
    // std::cout << "V1["<<i<<"]="<<V1[i]<<std::endl;
  }

  for(int i=0; i<N; i++) {
    std::vector<std::complex<double> >riga, colonna;
    for(int k=0; k<=i; k++) {
      colonna.push_back( A[k*N+i] );
      riga   .push_back( A[i*N+k] );
    }
    lu.upgrade(&(riga[0]),&(colonna[0]),i+1);
  }

  if(0)
  for(int i=0; i<N; i++) {
    std::cout << "V1["<<i<<"]="<<V1[i]<<std::endl;
  };
    
  for(int i=0;i<N;i++) {
    V2[i]=0;
    for(int j=0;j<N;j++) {
      // printf(" moltiplica %e %e con V1[%d] =%e %e\n",  A[i*N+j].real(), A[i*N+j].imag(),j,V1[j].real(),V1[j].imag()  );
      V2[i]= V2[i] + A[i*N+j]*V1[j] ;
    }
  }

  if(0)
  for(int i=0; i<N*N; i++) {
    std::cout << "A["<<i<<"]="<<A[i]<<std::endl;
    // printf("A[%d]=%e\n", i,A[i]);
  } ; 

  for(int i=0; i<N; i++) {
    // std::cout << "V1["<<i<<"]="<<V1[i]<<std::endl;
  }


  lu.moltiplica( &(V3[0]), &(V1[0]));

  if(myid==0)
  for(int k=0; k<N; k++) {
    std::cout << "V3["<<k<<"]="<<V3[k]<<"              "   << "V2["<<k<<"]="<<V2[k]                          <<std::endl;
  };


  lu.risolvi( &(V2[0]), &(V3[0]));


  if(myid==0)
  for(int k=0; k<N; k++) {
    std::cout << "V2["<<k<<"]="<<V2[k]<<"              "   << "V1["<<k<<"]="<<V1[k]                          <<std::endl;
  };


  MPI_Finalize();
  
  printf(" processo %d esce \n", myid);
}
